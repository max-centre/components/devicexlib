
/*
!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
*/

#if defined __DXL_CUDAF  || defined __DXL_OPENACC  || defined __DXL_OPENMP_GPU
#  define __DXL_HAVE_DEVICE
#endif

/*
! directive sentinels
*/

#if defined __DXL_OPENACC
#  define DEV_ACC $acc
#else
#  define DEV_ACC !!!!
#endif

#if defined __DXL_CUDAF
#  define DEV_CUF $cuf
#else
#  define DEV_CUF !!!!
#endif

#if defined __DXL_OPENMP_GPU
#  define DEV_OMPGPU $omp
#else
#  define DEV_OMPGPU !!!!
#endif

#if defined __DXL_OPENMP && !defined (__DXL_HAVE_DEVICE)
#  define DEV_OMP $omp
#else
#  define DEV_OMP !!!!
#endif

