/*
!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
*/

#define  __VERSION_NAME    "devxlib"
#define  __VERSION_MAJOR   "0"
#define  __VERSION_MINOR   "8"
#define  __VERSION_PATCH   "5"
#define  __VERSION_LABEL   ""




