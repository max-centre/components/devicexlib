#! /bin/awk -f

BEGIN{
  #
  # definitions about colors & styles
  #
  bold_on="[1m"
  bold_off="[0m"
  green_on="[1;32;48m"
  green_off="[0m"
  orange_on="[1;33;48m"
  orange_off="[0m"
  red_on="[1;31;48m"
  red_off="[0m"
  #
  stat_pass= green_on"passed"green_off ;
  stat_warn= orange_on"warning"orange_off ;
  stat_fail= red_on"failed"red_off ;
  #
  npass=0
  nfail=0
  completed=0
}

{
  if ( match($0,"SUMMARY") ) {completed=1; next}
  if ( match($0,"# passed") ) {npass+=$(NF)}
  if ( match($0,"# failed") ) {nfail+=$(NF)}
  if ( match($1,"#") ) {print}
}

END{
  ntot=npass+nfail
  #
  str=stat_fail
  if (ntot > 0) {
    if (nfail+0.0 < 0.15*(ntot+0.0)) {str=stat_warn}
    if (nfail==0) {str=stat_pass}
  }
  if (completed==0) {
    str=stat_fail
    printf "   # %s (crashed)\n", str
  } else {
    printf "   # %s (fails %d/%d)\n", str, nfail, ntot
  }
}
