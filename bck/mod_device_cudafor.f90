!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Wrappers and interfaces for cudaFortran and Cuda lib
!
#include<devxlib_defs.h>
#include<devxlib_macros.h>
!
!#if !defined __DXL_CUDAF
!#include<mod_cudafor.f90>
!#endif
!
module dev_cudafor_m
  !
#if defined __DXL_CUDAF
  use cudafor
#else
  use cudafor_m
#endif
  implicit none

contains
  subroutine cudafor_foo ()
  end subroutine

end module dev_cudafor_m
