!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! F-wrappers for selected acc functions
!
module openacc_m
  use iso_c_binding

#if defined __DXL_OPENACC

  interface
    !
    function acc_get_cuda_stream(async) &
        result(stream) bind(c, name="acc_get_cuda_stream")
      import c_int, c_intptr_t
      integer(c_intptr_t) :: stream
      integer(c_int) :: async
    end function acc_get_cuda_stream
    !
  end interface

#else
  integer :: acc_dummy  
#endif

end module

