!
! F-wrappers for selected cuda functions
! freely copied and modified from:
!
! FCUDA (C18096) is open source software; it is distributed under the terms of
! the 3-clause BSD license.
!
! Copyright (c) 2019-2020. Triad National Security, LLC. All rights reserved.
!
! This program was produced under U.S. Government contract 89233218CNA000001 for
! Los Alamos National Laboratory (LANL), which is operated by Triad National
! Security, LLC for the U.S. Department of Energy/National Nuclear Security
! Administration. All rights in the program are reserved by Triad National
! Security, LLC, and the U.S. Department of Energy/National Nuclear Security
! Administration. The Government is granted for itself and others acting on its
! behalf a nonexclusive, paid-up, irrevocable worldwide license in this material
! to reproduce, prepare derivative works, distribute copies to the public, perform
! publicly and display publicly, and to permit others to do so.
!
! https://github.com/zjibben/fcuda
!
module cudafor_m
  use iso_c_binding

! XXX to be fixed
!#define __DXL_CUDA 
#if defined __DXL_CUDA

  interface
    function cudaDeviceSynchronize() &
        result(ierr) bind(c, name="cudaDeviceSynchronize")
      import c_int
      integer(c_int) :: ierr
    end function cudaDeviceSynchronize
    function cudaStreamCreate(pstream) &
        result(ierr) bind(c, name="cudaStreamCreate")
      import c_ptr, c_int
      type(c_ptr) :: pstream
      integer(c_int) :: ierr
    end function cudaStreamCreate
    function cudaStreamDestroy(pstream) &
        result(ierr) bind(c, name="cudaStreamDestroy")
      import c_ptr, c_int
      type(c_ptr), value :: pstream
      integer(c_int) :: ierr
    end function cudaStreamDestroy
    function cudaStreamSynchronize(pstream) &
        result(ierr) bind(c, name="cudaStreamSynchronize")
      import c_ptr, c_int
      type(c_ptr), value :: pstream
      integer(c_int) :: ierr
    end function cudaStreamSynchronize
  end interface

#else
  integer :: cudafor_dummy
#endif

end module
