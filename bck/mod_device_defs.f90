!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! general definitions
!
module device_defs_m
  !
  use dev_cudafor_m
  use dev_openacc_m
!  use dev_openmpgpu_m
  use iso_fortran_env
  implicit none
  !
  !integer, parameter :: DP = selected_real_kind(14,200)
  !integer, parameter :: SP = selected_real_kind(6, 37) 
  integer, parameter :: DP = real64
  integer, parameter :: SP = real32
  !
end module

