!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Wrappers for openacc symbols
!
#if defined __DXL_OPENACC && defined __GFORTRAN__
#include<mod_openacc.f90>
#endif
!
!
module dev_openacc_m
  !
#if defined __DXL_OPENACC
  use openacc
#if defined __GFORTRAN__
  use openacc_m
#endif
#endif
  implicit none
 
contains
  subroutine openacc_foo ()
  end subroutine

end module dev_openacc_m
