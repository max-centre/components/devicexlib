#
# Copyright (C) 2021, MaX CoE
# Distributed under the MIT License
# (license terms are at http://opensource.org/licenses/MIT).
#
#===============================
# deviceXlib package
#===============================
#
-include make.inc
MAKE=make

#
# manual
#
default:
	@echo
	@echo  "  *** deviceXlib *** "
	@echo
	@echo  "  to configure, type:  ./configure [<options>]"
	@echo  "  to compile,   type:  make <target>"
	@echo
	@echo  "  Possible <target>'s are: "
	@echo
	@echo  "     core               builds the library"
	@echo  "     all                core"
	@echo  "     test               all the executables testing the library"
	@echo  "     deps               update fortran90 dependencies"
	@echo  "     gen                generate Fortran sources"
	@echo  "     clean              remove executables and objects"
	@echo  "     distclean          revert distribution to the original status"
	@echo

#
# MAIN target
#
core: libextlibs libsrc
all: core

deps:
	if test -x ./config/makedeps.sh ; then ./config/makedeps.sh ; fi
gen:
	if test -d src_generator ; then ( cd src_generator ; $(MAKE) ) ; fi

#
# INSTALL target
#
install: all
	mkdir -p $(PREFIX)/lib ; \
		for x in `find src/* -name *.a -type f` ; do \
		cp -v $$x $(PREFIX)/lib/ ; done
	mkdir -p $(PREFIX)/include ; \
		for x in `find include/* -name *.h -type f` ; do \
		cp -v $$x $(PREFIX)/include/ ; done
	mkdir -p $(PREFIX)/include ; \
		for x in `find src/* -name "*.mod*" -type f` ; do \
		cp -v $$x $(PREFIX)/include/ ; done
	mkdir -p $(PREFIX)/include ; \
		for x in `find src/* -name "*.smod*" -type f` ; do \
		cp -v $$x $(PREFIX)/include/ ; done
	mkdir -p $(PREFIX)/bin ; \
		for x in `find src/* -name *.x -type f` ; do \
		cp -v $$x $(PREFIX)/bin/ ; done
	@echo -e '\ndeviceXlib installed in $(PREFIX)\n'

#
# LIBS and MODULES
#
libextlibs:
	if test -d extlibs ; then \
	( cd extlibs ; $(MAKE) ) ; fi

libsrc: libextlibs
	if test -d src/ ; then \
	( cd src/ ; $(MAKE) ) ; fi

test:
	if test -d tests/ ; then \
	( cd tests/ ; ./run.sh -a ) ; fi

clean:
	if test -d extlibs ;   then ( cd extlibs;   $(MAKE) clean ) ; fi
	if test -d src ;       then ( cd src;       $(MAKE) clean ) ; fi
	- /bin/rm -rf *.dat *.mod
	- /bin/rm -rf ./bin/*x

distclean: clean
	if test -d extlibs ;   then ( cd extlibs;   $(MAKE) distclean ) ; fi
	if test -d tests ;     then ( cd tests;   ./run.sh -c ) ; fi
	- /bin/rm -rf make.inc config.log ./config/configure.msg \
		./config/config.log ./config/config.status \
		./config/*.lineno \
		./include/configure.h ./include/fft_defs.h \
	        ./include/c_defs.h    ./include/f_defs.h\
		*/dum1 */dum2

