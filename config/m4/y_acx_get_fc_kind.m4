#
#        Copyright (C) 2000-2021 the YAMBO team
#              http://www.yambo-code.org
#
# Authors (see AUTHORS file for details): AM
#
# This file is distributed under the terms of the GNU
# General Public License. You can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation;
# either version 2, or (at your option) any later version.
#
# This program is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A
# PARTICULAR PURPOSE.  See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place - Suite 330,Boston,
# MA 02111-1307, USA or visit http://www.gnu.org/copyleft/gpl.txt.
#
AC_DEFUN([ACX_GET_FC_KIND],
[
INTELVERSION="unknown"
FCKIND="unknown"
#
case "${F90}" in
    *ftn*)
      FCVERSION=`$F90 --version`
      if test x"`echo "$FCVERSION" | grep -i 'Cray Fortran'`" != "x" ; then FCKIND=cray ; fi
      if test x"`echo "$FCVERSION" | grep -i 'gfortran'`" != "x" ; then FCKIND=gfortran ; fi
      ;;
    *abf90*)
      FCVERSION=`$F90 --version`
      ;;
    *pgf*)
      FCKIND="pgi"
      FCVERSION=`$F90 --version`
      ;;
    *pgi*)
      FCKIND="pgi"
      FCVERSION=`$F90 --version`
      ;;
    *nvfortran*)
      FCKIND="nvfortran"
      FCVERSION=`$F90 --version`
      ;;
    *gfortran*)
      FCKIND="gfortran" 
      FCVERSION=`$F90 --version`
      ;;
    *g95*)
      FCKIND="g95"
      ;;
    *ifc*)
      FCKIND="intel"
      FCVERSION=`$F90 -v 2>&1`
      ;;
    *ifort*)
      FCKIND="intel"
      FCVERSION=`$F90 --version 2>&1`
      $F90 -v >& ver_
      VER_15=`grep 15. ver_ | wc -l`
      VER_16=`grep 16. ver_ | wc -l`
      VER_17=`grep 17. ver_ | wc -l`
      VER_18=`grep 18. ver_ | wc -l`
      VER_19=`grep 19. ver_ | wc -l`
      VER_2021=`grep 2021. ver_ | wc -l`
      if ! test "$VER_15" = "0"; then INTELVERSION="15" ; fi
      if ! test "$VER_16" = "0"; then INTELVERSION="16" ; fi
      if ! test "$VER_17" = "0"; then INTELVERSION="17" ; fi
      if ! test "$VER_18" = "0"; then INTELVERSION="18" ; fi
      if ! test "$VER_19" = "0"; then INTELVERSION="19" ; fi
      if ! test "$VER_2021" = "0"; then INTELVERSION="2021" ; fi
      rm -f ver_
      ;;
    *ifx*)
      FCKIND="intel"
      FCVERSION=`$F90 --version 2>&1`
      $F90 -v >& ver_
      VER_2021=`grep 2021. ver_ | wc -l`
      VER_dev=`grep 2022 ver_ | wc -l`
      if ! test "$VER_2021" = "0"; then INTELVERSION="2021" ; fi
      if ! test "$VER_2022" = "0"; then INTELVERSION="2022" ; fi
      rm -f ver_
      ;;
    *)
      FCKIND=""
      FCVERSION=`$F90 --version`
esac
#
FCVERSION=`echo "$FCVERSION" | sed "/^\s*$/d" | head -n 1`
#
AC_MSG_CHECKING([for $F90 kind and version])
AC_MSG_RESULT([$FCKIND $FCVERSION $INTELVERSION])

AC_SUBST(FCKIND)
AC_SUBST(FCVERSION)
AC_SUBST(INTELVERSION)

])
