
# deviceXlib

### deviceXlib, fortran library wrapping device-oriented routines and utilities

- deviceXlib is a Fortran library wrapping device-oriented routines and utilities;
- deviceXlib is Fortran 2008 standard compliant;

#### Table of Contents

+ [What is deviceXlib?](#what-is-deviceXlib?)
	+ [What are OpenMP Device Memory Routines?](#what-are-omp-routines?)
+ [Main features](#main-features)
+ [Status](#status)
+ [Copyrights](#copyrights)

## What is deviceXlib?

deviceXlib is a library that wraps device-oriented routines and utilities, such as device data allocation, host-device data transfers.
deviceXlib supports CUDA language, together with OpenACC and OpenMP programming paradigms.
deviceXlib wraps a subset of functions from Nvidia cuBLAS, Intel oneMKL BLAS and AMD rocBLAS libraries.

## Main features

deviceXlib is aimed to be a Fortran library providing basic and advanced functionalities needed for GPU offload in MaX CoE codes.

+ [x] KISS and user-friendly:
  + [x] simple API;
  + [x] easy building and porting on heterogeneous architectures;
+ [x] comprehensive:
  + [x] GPU programming models:
     + [x] CUDA support;
     + [x] OpenACC support;
     + [x] OpenMP Offload support;
  + [x] GPU BLAS support (via cuBLAS, Intel oneMKL BLAS and AMD rocBLAS):
     + [x] ?DOT;
     + [x] ?AXPY;
     + [x] ?GEMV;
     + [x] ?GEMM;
     + [ ] ?SCAL;
     + [ ] ?SWAP;
     + [ ] ?COPY;
     + [ ] ?DOTC;
     + [ ] ?GER;
     + [ ] ?TRSM;

Any feature request is welcome.

Go to [Top](#top)

## Status

deviceXlib provides the following Fortran routines:

+ [x] GPU buffer allocation via linked list --> device_fbuff
+ [x] data allocation on GPU --> device_malloc
+ [x] data mapping on GPU --> device_mapping
+ [x] data synchronization between CPU and GPU --> device_memcpy and device_memcpy_async
+ [x] auxiliary functions for GPU data manipulation --> device_auxfunc
+ [x] subset of linear algebra routines for GPU --> device_linalg

Go to [Top](#top)

## Copyrights

&copy; 2024, MaX - Materials at the eXascale - CoE

Anyone is interest to use, to develop or to contribute to deviceXlib is welcome!

Go to [Top](#top)
