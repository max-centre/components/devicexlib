!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to perform device memory mapping from host memory
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_mapping.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
module devxlib_mapping

   use iso_fortran_env,     only : int32, int64, real32, real64
   use devxlib_environment, only : devxlib_error
#if defined __DXL_CUDAF
   use cudafor
#elif defined __DXL_OPENACC
   use openacc
#elif defined __DXL_OPENMP_GPU
   use iso_c_binding, only : c_loc
   use omp_lib,       only : omp_get_default_device, omp_target_is_present
#endif

   implicit none

   interface devxlib_map
      module procedure &
         sp_devxlib_mapping_map_r1d, sp_devxlib_mapping_map_r2d, sp_devxlib_mapping_map_r3d, &
         sp_devxlib_mapping_map_r4d, sp_devxlib_mapping_map_r5d, sp_devxlib_mapping_map_r6d, &
         dp_devxlib_mapping_map_r1d, dp_devxlib_mapping_map_r2d, dp_devxlib_mapping_map_r3d, &
         dp_devxlib_mapping_map_r4d, dp_devxlib_mapping_map_r5d, dp_devxlib_mapping_map_r6d, &
         sp_devxlib_mapping_map_c1d, sp_devxlib_mapping_map_c2d, sp_devxlib_mapping_map_c3d, &
         sp_devxlib_mapping_map_c4d, sp_devxlib_mapping_map_c5d, sp_devxlib_mapping_map_c6d, &
         dp_devxlib_mapping_map_c1d, dp_devxlib_mapping_map_c2d, dp_devxlib_mapping_map_c3d, &
         dp_devxlib_mapping_map_c4d, dp_devxlib_mapping_map_c5d, dp_devxlib_mapping_map_c6d, &
         i4_devxlib_mapping_map_i1d, i4_devxlib_mapping_map_i2d, i4_devxlib_mapping_map_i3d, &
         i4_devxlib_mapping_map_i4d, i4_devxlib_mapping_map_i5d, i4_devxlib_mapping_map_i6d, &
         i8_devxlib_mapping_map_i1d, i8_devxlib_mapping_map_i2d, i8_devxlib_mapping_map_i3d, &
         i8_devxlib_mapping_map_i4d, i8_devxlib_mapping_map_i5d, i8_devxlib_mapping_map_i6d, &
         l4_devxlib_mapping_map_l1d, l4_devxlib_mapping_map_l2d, l4_devxlib_mapping_map_l3d, &
         l4_devxlib_mapping_map_l4d, l4_devxlib_mapping_map_l5d, l4_devxlib_mapping_map_l6d
   endinterface devxlib_map

   interface devxlib_unmap
      module procedure &
         sp_devxlib_mapping_unmap_r1d, sp_devxlib_mapping_unmap_r2d, sp_devxlib_mapping_unmap_r3d, &
         sp_devxlib_mapping_unmap_r4d, sp_devxlib_mapping_unmap_r5d, sp_devxlib_mapping_unmap_r6d, &
         dp_devxlib_mapping_unmap_r1d, dp_devxlib_mapping_unmap_r2d, dp_devxlib_mapping_unmap_r3d, &
         dp_devxlib_mapping_unmap_r4d, dp_devxlib_mapping_unmap_r5d, dp_devxlib_mapping_unmap_r6d, &
         sp_devxlib_mapping_unmap_c1d, sp_devxlib_mapping_unmap_c2d, sp_devxlib_mapping_unmap_c3d, &
         sp_devxlib_mapping_unmap_c4d, sp_devxlib_mapping_unmap_c5d, sp_devxlib_mapping_unmap_c6d, &
         dp_devxlib_mapping_unmap_c1d, dp_devxlib_mapping_unmap_c2d, dp_devxlib_mapping_unmap_c3d, &
         dp_devxlib_mapping_unmap_c4d, dp_devxlib_mapping_unmap_c5d, dp_devxlib_mapping_unmap_c6d, &
         i4_devxlib_mapping_unmap_i1d, i4_devxlib_mapping_unmap_i2d, i4_devxlib_mapping_unmap_i3d, &
         i4_devxlib_mapping_unmap_i4d, i4_devxlib_mapping_unmap_i5d, i4_devxlib_mapping_unmap_i6d, &
         i8_devxlib_mapping_unmap_i1d, i8_devxlib_mapping_unmap_i2d, i8_devxlib_mapping_unmap_i3d, &
         i8_devxlib_mapping_unmap_i4d, i8_devxlib_mapping_unmap_i5d, i8_devxlib_mapping_unmap_i6d, &
         l4_devxlib_mapping_unmap_l1d, l4_devxlib_mapping_unmap_l2d, l4_devxlib_mapping_unmap_l3d, &
         l4_devxlib_mapping_unmap_l4d, l4_devxlib_mapping_unmap_l5d, l4_devxlib_mapping_unmap_l6d
   endinterface devxlib_unmap

   interface devxlib_mapped
      module procedure &
         sp_devxlib_mapping_mapped_r1d, sp_devxlib_mapping_mapped_r2d, sp_devxlib_mapping_mapped_r3d, &
         sp_devxlib_mapping_mapped_r4d, sp_devxlib_mapping_mapped_r5d, sp_devxlib_mapping_mapped_r6d, &
         dp_devxlib_mapping_mapped_r1d, dp_devxlib_mapping_mapped_r2d, dp_devxlib_mapping_mapped_r3d, &
         dp_devxlib_mapping_mapped_r4d, dp_devxlib_mapping_mapped_r5d, dp_devxlib_mapping_mapped_r6d, &
         sp_devxlib_mapping_mapped_c1d, sp_devxlib_mapping_mapped_c2d, sp_devxlib_mapping_mapped_c3d, &
         sp_devxlib_mapping_mapped_c4d, sp_devxlib_mapping_mapped_c5d, sp_devxlib_mapping_mapped_c6d, &
         dp_devxlib_mapping_mapped_c1d, dp_devxlib_mapping_mapped_c2d, dp_devxlib_mapping_mapped_c3d, &
         dp_devxlib_mapping_mapped_c4d, dp_devxlib_mapping_mapped_c5d, dp_devxlib_mapping_mapped_c6d, &
         i4_devxlib_mapping_mapped_i1d, i4_devxlib_mapping_mapped_i2d, i4_devxlib_mapping_mapped_i3d, &
         i4_devxlib_mapping_mapped_i4d, i4_devxlib_mapping_mapped_i5d, i4_devxlib_mapping_mapped_i6d, &
         i8_devxlib_mapping_mapped_i1d, i8_devxlib_mapping_mapped_i2d, i8_devxlib_mapping_mapped_i3d, &
         i8_devxlib_mapping_mapped_i4d, i8_devxlib_mapping_mapped_i5d, i8_devxlib_mapping_mapped_i6d, &
         l4_devxlib_mapping_mapped_l1d, l4_devxlib_mapping_mapped_l2d, l4_devxlib_mapping_mapped_l3d, &
         l4_devxlib_mapping_mapped_l4d, l4_devxlib_mapping_mapped_l5d, l4_devxlib_mapping_mapped_l6d
   endinterface devxlib_mapped



   interface
      module subroutine sp_devxlib_mapping_map_r1d(array, range1 )
         implicit none
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
      end subroutine sp_devxlib_mapping_map_r1d
      module subroutine sp_devxlib_mapping_map_r2d(array, range1, &
                                            range2 )
         implicit none
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
      end subroutine sp_devxlib_mapping_map_r2d
      module subroutine sp_devxlib_mapping_map_r3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
      end subroutine sp_devxlib_mapping_map_r3d
      module subroutine sp_devxlib_mapping_map_r4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
      end subroutine sp_devxlib_mapping_map_r4d
      module subroutine sp_devxlib_mapping_map_r5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
      end subroutine sp_devxlib_mapping_map_r5d
      module subroutine sp_devxlib_mapping_map_r6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
      end subroutine sp_devxlib_mapping_map_r6d
      module subroutine dp_devxlib_mapping_map_r1d(array, range1 )
         implicit none
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
      end subroutine dp_devxlib_mapping_map_r1d
      module subroutine dp_devxlib_mapping_map_r2d(array, range1, &
                                            range2 )
         implicit none
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
      end subroutine dp_devxlib_mapping_map_r2d
      module subroutine dp_devxlib_mapping_map_r3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
      end subroutine dp_devxlib_mapping_map_r3d
      module subroutine dp_devxlib_mapping_map_r4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
      end subroutine dp_devxlib_mapping_map_r4d
      module subroutine dp_devxlib_mapping_map_r5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
      end subroutine dp_devxlib_mapping_map_r5d
      module subroutine dp_devxlib_mapping_map_r6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
      end subroutine dp_devxlib_mapping_map_r6d
      module subroutine sp_devxlib_mapping_map_c1d(array, range1 )
         implicit none
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
      end subroutine sp_devxlib_mapping_map_c1d
      module subroutine sp_devxlib_mapping_map_c2d(array, range1, &
                                            range2 )
         implicit none
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
      end subroutine sp_devxlib_mapping_map_c2d
      module subroutine sp_devxlib_mapping_map_c3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
      end subroutine sp_devxlib_mapping_map_c3d
      module subroutine sp_devxlib_mapping_map_c4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
      end subroutine sp_devxlib_mapping_map_c4d
      module subroutine sp_devxlib_mapping_map_c5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
      end subroutine sp_devxlib_mapping_map_c5d
      module subroutine sp_devxlib_mapping_map_c6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
      end subroutine sp_devxlib_mapping_map_c6d
      module subroutine dp_devxlib_mapping_map_c1d(array, range1 )
         implicit none
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
      end subroutine dp_devxlib_mapping_map_c1d
      module subroutine dp_devxlib_mapping_map_c2d(array, range1, &
                                            range2 )
         implicit none
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
      end subroutine dp_devxlib_mapping_map_c2d
      module subroutine dp_devxlib_mapping_map_c3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
      end subroutine dp_devxlib_mapping_map_c3d
      module subroutine dp_devxlib_mapping_map_c4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
      end subroutine dp_devxlib_mapping_map_c4d
      module subroutine dp_devxlib_mapping_map_c5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
      end subroutine dp_devxlib_mapping_map_c5d
      module subroutine dp_devxlib_mapping_map_c6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
      end subroutine dp_devxlib_mapping_map_c6d
      module subroutine i4_devxlib_mapping_map_i1d(array, range1 )
         implicit none
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
      end subroutine i4_devxlib_mapping_map_i1d
      module subroutine i4_devxlib_mapping_map_i2d(array, range1, &
                                            range2 )
         implicit none
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
      end subroutine i4_devxlib_mapping_map_i2d
      module subroutine i4_devxlib_mapping_map_i3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
      end subroutine i4_devxlib_mapping_map_i3d
      module subroutine i4_devxlib_mapping_map_i4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
      end subroutine i4_devxlib_mapping_map_i4d
      module subroutine i4_devxlib_mapping_map_i5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
      end subroutine i4_devxlib_mapping_map_i5d
      module subroutine i4_devxlib_mapping_map_i6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
      end subroutine i4_devxlib_mapping_map_i6d
      module subroutine i8_devxlib_mapping_map_i1d(array, range1 )
         implicit none
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
      end subroutine i8_devxlib_mapping_map_i1d
      module subroutine i8_devxlib_mapping_map_i2d(array, range1, &
                                            range2 )
         implicit none
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
      end subroutine i8_devxlib_mapping_map_i2d
      module subroutine i8_devxlib_mapping_map_i3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
      end subroutine i8_devxlib_mapping_map_i3d
      module subroutine i8_devxlib_mapping_map_i4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
      end subroutine i8_devxlib_mapping_map_i4d
      module subroutine i8_devxlib_mapping_map_i5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
      end subroutine i8_devxlib_mapping_map_i5d
      module subroutine i8_devxlib_mapping_map_i6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
      end subroutine i8_devxlib_mapping_map_i6d
      module subroutine l4_devxlib_mapping_map_l1d(array, range1 )
         implicit none
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
      end subroutine l4_devxlib_mapping_map_l1d
      module subroutine l4_devxlib_mapping_map_l2d(array, range1, &
                                            range2 )
         implicit none
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
      end subroutine l4_devxlib_mapping_map_l2d
      module subroutine l4_devxlib_mapping_map_l3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
      end subroutine l4_devxlib_mapping_map_l3d
      module subroutine l4_devxlib_mapping_map_l4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
      end subroutine l4_devxlib_mapping_map_l4d
      module subroutine l4_devxlib_mapping_map_l5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
      end subroutine l4_devxlib_mapping_map_l5d
      module subroutine l4_devxlib_mapping_map_l6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
      end subroutine l4_devxlib_mapping_map_l6d
   endinterface

   interface
      module subroutine sp_devxlib_mapping_unmap_r1d(array, ierr)
         implicit none
         real(real32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_r1d
      module subroutine sp_devxlib_mapping_unmap_r2d(array, ierr)
         implicit none
         real(real32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_r2d
      module subroutine sp_devxlib_mapping_unmap_r3d(array, ierr)
         implicit none
         real(real32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_r3d
      module subroutine sp_devxlib_mapping_unmap_r4d(array, ierr)
         implicit none
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_r4d
      module subroutine sp_devxlib_mapping_unmap_r5d(array, ierr)
         implicit none
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_r5d
      module subroutine sp_devxlib_mapping_unmap_r6d(array, ierr)
         implicit none
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_r6d
      module subroutine dp_devxlib_mapping_unmap_r1d(array, ierr)
         implicit none
         real(real64), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_r1d
      module subroutine dp_devxlib_mapping_unmap_r2d(array, ierr)
         implicit none
         real(real64), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_r2d
      module subroutine dp_devxlib_mapping_unmap_r3d(array, ierr)
         implicit none
         real(real64), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_r3d
      module subroutine dp_devxlib_mapping_unmap_r4d(array, ierr)
         implicit none
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_r4d
      module subroutine dp_devxlib_mapping_unmap_r5d(array, ierr)
         implicit none
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_r5d
      module subroutine dp_devxlib_mapping_unmap_r6d(array, ierr)
         implicit none
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_r6d
      module subroutine sp_devxlib_mapping_unmap_c1d(array, ierr)
         implicit none
         complex(real32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_c1d
      module subroutine sp_devxlib_mapping_unmap_c2d(array, ierr)
         implicit none
         complex(real32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_c2d
      module subroutine sp_devxlib_mapping_unmap_c3d(array, ierr)
         implicit none
         complex(real32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_c3d
      module subroutine sp_devxlib_mapping_unmap_c4d(array, ierr)
         implicit none
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_c4d
      module subroutine sp_devxlib_mapping_unmap_c5d(array, ierr)
         implicit none
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_c5d
      module subroutine sp_devxlib_mapping_unmap_c6d(array, ierr)
         implicit none
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine sp_devxlib_mapping_unmap_c6d
      module subroutine dp_devxlib_mapping_unmap_c1d(array, ierr)
         implicit none
         complex(real64), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_c1d
      module subroutine dp_devxlib_mapping_unmap_c2d(array, ierr)
         implicit none
         complex(real64), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_c2d
      module subroutine dp_devxlib_mapping_unmap_c3d(array, ierr)
         implicit none
         complex(real64), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_c3d
      module subroutine dp_devxlib_mapping_unmap_c4d(array, ierr)
         implicit none
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_c4d
      module subroutine dp_devxlib_mapping_unmap_c5d(array, ierr)
         implicit none
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_c5d
      module subroutine dp_devxlib_mapping_unmap_c6d(array, ierr)
         implicit none
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine dp_devxlib_mapping_unmap_c6d
      module subroutine i4_devxlib_mapping_unmap_i1d(array, ierr)
         implicit none
         integer(int32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
      end subroutine i4_devxlib_mapping_unmap_i1d
      module subroutine i4_devxlib_mapping_unmap_i2d(array, ierr)
         implicit none
         integer(int32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i4_devxlib_mapping_unmap_i2d
      module subroutine i4_devxlib_mapping_unmap_i3d(array, ierr)
         implicit none
         integer(int32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i4_devxlib_mapping_unmap_i3d
      module subroutine i4_devxlib_mapping_unmap_i4d(array, ierr)
         implicit none
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i4_devxlib_mapping_unmap_i4d
      module subroutine i4_devxlib_mapping_unmap_i5d(array, ierr)
         implicit none
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i4_devxlib_mapping_unmap_i5d
      module subroutine i4_devxlib_mapping_unmap_i6d(array, ierr)
         implicit none
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i4_devxlib_mapping_unmap_i6d
      module subroutine i8_devxlib_mapping_unmap_i1d(array, ierr)
         implicit none
         integer(int64), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
      end subroutine i8_devxlib_mapping_unmap_i1d
      module subroutine i8_devxlib_mapping_unmap_i2d(array, ierr)
         implicit none
         integer(int64), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i8_devxlib_mapping_unmap_i2d
      module subroutine i8_devxlib_mapping_unmap_i3d(array, ierr)
         implicit none
         integer(int64), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i8_devxlib_mapping_unmap_i3d
      module subroutine i8_devxlib_mapping_unmap_i4d(array, ierr)
         implicit none
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i8_devxlib_mapping_unmap_i4d
      module subroutine i8_devxlib_mapping_unmap_i5d(array, ierr)
         implicit none
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i8_devxlib_mapping_unmap_i5d
      module subroutine i8_devxlib_mapping_unmap_i6d(array, ierr)
         implicit none
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine i8_devxlib_mapping_unmap_i6d
      module subroutine l4_devxlib_mapping_unmap_l1d(array, ierr)
         implicit none
         logical(int32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
      end subroutine l4_devxlib_mapping_unmap_l1d
      module subroutine l4_devxlib_mapping_unmap_l2d(array, ierr)
         implicit none
         logical(int32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
      end subroutine l4_devxlib_mapping_unmap_l2d
      module subroutine l4_devxlib_mapping_unmap_l3d(array, ierr)
         implicit none
         logical(int32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine l4_devxlib_mapping_unmap_l3d
      module subroutine l4_devxlib_mapping_unmap_l4d(array, ierr)
         implicit none
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine l4_devxlib_mapping_unmap_l4d
      module subroutine l4_devxlib_mapping_unmap_l5d(array, ierr)
         implicit none
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine l4_devxlib_mapping_unmap_l5d
      module subroutine l4_devxlib_mapping_unmap_l6d(array, ierr)
         implicit none
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
      end subroutine l4_devxlib_mapping_unmap_l6d
   endinterface

   interface
      module function sp_devxlib_mapping_mapped_r1d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:)
#else
         real(real32), allocatable DEV_ATTR :: array(:)
#endif
      end function sp_devxlib_mapping_mapped_r1d
      module function sp_devxlib_mapping_mapped_r2d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:)
#endif
      end function sp_devxlib_mapping_mapped_r2d
      module function sp_devxlib_mapping_mapped_r3d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_r3d
      module function sp_devxlib_mapping_mapped_r4d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_r4d
      module function sp_devxlib_mapping_mapped_r5d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_r5d
      module function sp_devxlib_mapping_mapped_r6d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:,:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_r6d
      module function dp_devxlib_mapping_mapped_r1d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:)
#else
         real(real64), allocatable DEV_ATTR :: array(:)
#endif
      end function dp_devxlib_mapping_mapped_r1d
      module function dp_devxlib_mapping_mapped_r2d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:)
#endif
      end function dp_devxlib_mapping_mapped_r2d
      module function dp_devxlib_mapping_mapped_r3d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_r3d
      module function dp_devxlib_mapping_mapped_r4d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_r4d
      module function dp_devxlib_mapping_mapped_r5d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_r5d
      module function dp_devxlib_mapping_mapped_r6d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:,:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_r6d
      module function sp_devxlib_mapping_mapped_c1d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:)
#endif
      end function sp_devxlib_mapping_mapped_c1d
      module function sp_devxlib_mapping_mapped_c2d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:)
#endif
      end function sp_devxlib_mapping_mapped_c2d
      module function sp_devxlib_mapping_mapped_c3d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_c3d
      module function sp_devxlib_mapping_mapped_c4d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_c4d
      module function sp_devxlib_mapping_mapped_c5d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_c5d
      module function sp_devxlib_mapping_mapped_c6d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:,:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
      end function sp_devxlib_mapping_mapped_c6d
      module function dp_devxlib_mapping_mapped_c1d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:)
#endif
      end function dp_devxlib_mapping_mapped_c1d
      module function dp_devxlib_mapping_mapped_c2d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:)
#endif
      end function dp_devxlib_mapping_mapped_c2d
      module function dp_devxlib_mapping_mapped_c3d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_c3d
      module function dp_devxlib_mapping_mapped_c4d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_c4d
      module function dp_devxlib_mapping_mapped_c5d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_c5d
      module function dp_devxlib_mapping_mapped_c6d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:,:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
      end function dp_devxlib_mapping_mapped_c6d
      module function i4_devxlib_mapping_mapped_i1d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:)
#endif
      end function i4_devxlib_mapping_mapped_i1d
      module function i4_devxlib_mapping_mapped_i2d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:)
#endif
      end function i4_devxlib_mapping_mapped_i2d
      module function i4_devxlib_mapping_mapped_i3d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:)
#endif
      end function i4_devxlib_mapping_mapped_i3d
      module function i4_devxlib_mapping_mapped_i4d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
      end function i4_devxlib_mapping_mapped_i4d
      module function i4_devxlib_mapping_mapped_i5d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
      end function i4_devxlib_mapping_mapped_i5d
      module function i4_devxlib_mapping_mapped_i6d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:,:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
      end function i4_devxlib_mapping_mapped_i6d
      module function i8_devxlib_mapping_mapped_i1d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:)
#endif
      end function i8_devxlib_mapping_mapped_i1d
      module function i8_devxlib_mapping_mapped_i2d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:)
#endif
      end function i8_devxlib_mapping_mapped_i2d
      module function i8_devxlib_mapping_mapped_i3d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:)
#endif
      end function i8_devxlib_mapping_mapped_i3d
      module function i8_devxlib_mapping_mapped_i4d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
      end function i8_devxlib_mapping_mapped_i4d
      module function i8_devxlib_mapping_mapped_i5d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
      end function i8_devxlib_mapping_mapped_i5d
      module function i8_devxlib_mapping_mapped_i6d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:,:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
      end function i8_devxlib_mapping_mapped_i6d
      module function l4_devxlib_mapping_mapped_l1d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:)
#endif
      end function l4_devxlib_mapping_mapped_l1d
      module function l4_devxlib_mapping_mapped_l2d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:)
#endif
      end function l4_devxlib_mapping_mapped_l2d
      module function l4_devxlib_mapping_mapped_l3d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:)
#endif
      end function l4_devxlib_mapping_mapped_l3d
      module function l4_devxlib_mapping_mapped_l4d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
      end function l4_devxlib_mapping_mapped_l4d
      module function l4_devxlib_mapping_mapped_l5d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
      end function l4_devxlib_mapping_mapped_l5d
      module function l4_devxlib_mapping_mapped_l6d(array) result(res)
         implicit none
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:,:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
      end function l4_devxlib_mapping_mapped_l6d
   endinterface
endmodule devxlib_mapping