!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to perform device memory unmapping of host memory
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_mapping_unmap.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_mapping) devxlib_mapping_unmap

   implicit none

   contains
!
      module subroutine sp_devxlib_mapping_unmap_r1d(array, ierr)
         implicit none
         !
         real(real32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_r1d
!
!
      module subroutine sp_devxlib_mapping_unmap_r2d(array, ierr)
         implicit none
         !
         real(real32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_r2d
!
!
      module subroutine sp_devxlib_mapping_unmap_r3d(array, ierr)
         implicit none
         !
         real(real32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_r3d
!
!
      module subroutine sp_devxlib_mapping_unmap_r4d(array, ierr)
         implicit none
         !
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_r4d
!
!
      module subroutine sp_devxlib_mapping_unmap_r5d(array, ierr)
         implicit none
         !
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_r5d
!
!
      module subroutine sp_devxlib_mapping_unmap_r6d(array, ierr)
         implicit none
         !
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_r6d
!
!
      module subroutine dp_devxlib_mapping_unmap_r1d(array, ierr)
         implicit none
         !
         real(real64), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_r1d
!
!
      module subroutine dp_devxlib_mapping_unmap_r2d(array, ierr)
         implicit none
         !
         real(real64), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_r2d
!
!
      module subroutine dp_devxlib_mapping_unmap_r3d(array, ierr)
         implicit none
         !
         real(real64), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_r3d
!
!
      module subroutine dp_devxlib_mapping_unmap_r4d(array, ierr)
         implicit none
         !
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_r4d
!
!
      module subroutine dp_devxlib_mapping_unmap_r5d(array, ierr)
         implicit none
         !
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_r5d
!
!
      module subroutine dp_devxlib_mapping_unmap_r6d(array, ierr)
         implicit none
         !
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_r6d
!
!
      module subroutine sp_devxlib_mapping_unmap_c1d(array, ierr)
         implicit none
         !
         complex(real32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_c1d
!
!
      module subroutine sp_devxlib_mapping_unmap_c2d(array, ierr)
         implicit none
         !
         complex(real32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_c2d
!
!
      module subroutine sp_devxlib_mapping_unmap_c3d(array, ierr)
         implicit none
         !
         complex(real32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_c3d
!
!
      module subroutine sp_devxlib_mapping_unmap_c4d(array, ierr)
         implicit none
         !
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_c4d
!
!
      module subroutine sp_devxlib_mapping_unmap_c5d(array, ierr)
         implicit none
         !
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_c5d
!
!
      module subroutine sp_devxlib_mapping_unmap_c6d(array, ierr)
         implicit none
         !
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine sp_devxlib_mapping_unmap_c6d
!
!
      module subroutine dp_devxlib_mapping_unmap_c1d(array, ierr)
         implicit none
         !
         complex(real64), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_c1d
!
!
      module subroutine dp_devxlib_mapping_unmap_c2d(array, ierr)
         implicit none
         !
         complex(real64), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_c2d
!
!
      module subroutine dp_devxlib_mapping_unmap_c3d(array, ierr)
         implicit none
         !
         complex(real64), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_c3d
!
!
      module subroutine dp_devxlib_mapping_unmap_c4d(array, ierr)
         implicit none
         !
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_c4d
!
!
      module subroutine dp_devxlib_mapping_unmap_c5d(array, ierr)
         implicit none
         !
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_c5d
!
!
      module subroutine dp_devxlib_mapping_unmap_c6d(array, ierr)
         implicit none
         !
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine dp_devxlib_mapping_unmap_c6d
!
!
      module subroutine i4_devxlib_mapping_unmap_i1d(array, ierr)
         implicit none
         !
         integer(int32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i4_devxlib_mapping_unmap_i1d
!
!
      module subroutine i4_devxlib_mapping_unmap_i2d(array, ierr)
         implicit none
         !
         integer(int32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i4_devxlib_mapping_unmap_i2d
!
!
      module subroutine i4_devxlib_mapping_unmap_i3d(array, ierr)
         implicit none
         !
         integer(int32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i4_devxlib_mapping_unmap_i3d
!
!
      module subroutine i4_devxlib_mapping_unmap_i4d(array, ierr)
         implicit none
         !
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i4_devxlib_mapping_unmap_i4d
!
!
      module subroutine i4_devxlib_mapping_unmap_i5d(array, ierr)
         implicit none
         !
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i4_devxlib_mapping_unmap_i5d
!
!
      module subroutine i4_devxlib_mapping_unmap_i6d(array, ierr)
         implicit none
         !
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i4_devxlib_mapping_unmap_i6d
!
!
      module subroutine i8_devxlib_mapping_unmap_i1d(array, ierr)
         implicit none
         !
         integer(int64), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i8_devxlib_mapping_unmap_i1d
!
!
      module subroutine i8_devxlib_mapping_unmap_i2d(array, ierr)
         implicit none
         !
         integer(int64), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i8_devxlib_mapping_unmap_i2d
!
!
      module subroutine i8_devxlib_mapping_unmap_i3d(array, ierr)
         implicit none
         !
         integer(int64), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i8_devxlib_mapping_unmap_i3d
!
!
      module subroutine i8_devxlib_mapping_unmap_i4d(array, ierr)
         implicit none
         !
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i8_devxlib_mapping_unmap_i4d
!
!
      module subroutine i8_devxlib_mapping_unmap_i5d(array, ierr)
         implicit none
         !
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i8_devxlib_mapping_unmap_i5d
!
!
      module subroutine i8_devxlib_mapping_unmap_i6d(array, ierr)
         implicit none
         !
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine i8_devxlib_mapping_unmap_i6d
!
!
      module subroutine l4_devxlib_mapping_unmap_l1d(array, ierr)
         implicit none
         !
         logical(int32), allocatable DEV_ATTR :: array(:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine l4_devxlib_mapping_unmap_l1d
!
!
      module subroutine l4_devxlib_mapping_unmap_l2d(array, ierr)
         implicit none
         !
         logical(int32), allocatable DEV_ATTR :: array(:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine l4_devxlib_mapping_unmap_l2d
!
!
      module subroutine l4_devxlib_mapping_unmap_l3d(array, ierr)
         implicit none
         !
         logical(int32), allocatable DEV_ATTR :: array(:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine l4_devxlib_mapping_unmap_l3d
!
!
      module subroutine l4_devxlib_mapping_unmap_l4d(array, ierr)
         implicit none
         !
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine l4_devxlib_mapping_unmap_l4d
!
!
      module subroutine l4_devxlib_mapping_unmap_l5d(array, ierr)
         implicit none
         !
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine l4_devxlib_mapping_unmap_l5d
!
!
      module subroutine l4_devxlib_mapping_unmap_l6d(array, ierr)
         implicit none
         !
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
         integer, optional, intent(out) :: ierr
         integer :: ierr_
         !
         ierr_=0
         !
#if defined __DXL_CUDAF
         if (allocated(array)) deallocate( array, stat=ierr_)
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC exit data delete(array)
         !DEV_OMPGPU target exit data map(delete:array)
#endif
         if (present(ierr)) ierr=ierr_
         !
      end subroutine l4_devxlib_mapping_unmap_l6d
!

endsubmodule devxlib_mapping_unmap