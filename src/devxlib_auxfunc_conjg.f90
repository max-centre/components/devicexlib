!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Auxiliary functions
!
! AF: OpenACC support is incomplete !!
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_auxfunc_conj.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_auxfunc) devxlib_auxfunc_conjg

   implicit none

   contains

      module subroutine dp_devxlib_conjg_c1d(array_inout, async_id, &
                                         
                                        range1, lbound1 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(1) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               array_inout(i1 ) = &
                   conjg (  array_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(1)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               array_inout(i1 ) = &
                   conjg (  array_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c1d
      !
      module subroutine dp_devxlib_conjg_c2d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(2) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2 ) = &
                   conjg (  array_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(2)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(2)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2 ) = &
                   conjg (  array_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c2d
      !
      module subroutine dp_devxlib_conjg_c3d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(3) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3 ) = &
                   conjg (  array_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(3)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(3)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3 ) = &
                   conjg (  array_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c3d
      !
      module subroutine dp_devxlib_conjg_c4d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(4) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4 ) = &
                   conjg (  array_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(4)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(4)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4 ) = &
                   conjg (  array_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c4d
      !
      module subroutine dp_devxlib_conjg_c5d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(5) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(5)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(5)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c5d
      !
      module subroutine dp_devxlib_conjg_c6d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5, & 
                                        range6, lbound6 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_inout, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(6) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(6)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(6)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c6d
      !
      module subroutine sp_devxlib_conjg_c1d(array_inout, async_id, &
                                         
                                        range1, lbound1 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(1) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               array_inout(i1 ) = &
                   conjg (  array_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(1)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               array_inout(i1 ) = &
                   conjg (  array_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c1d
      !
      module subroutine sp_devxlib_conjg_c2d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(2) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2 ) = &
                   conjg (  array_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(2)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(2)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2 ) = &
                   conjg (  array_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c2d
      !
      module subroutine sp_devxlib_conjg_c3d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(3) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3 ) = &
                   conjg (  array_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(3)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(3)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3 ) = &
                   conjg (  array_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c3d
      !
      module subroutine sp_devxlib_conjg_c4d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(4) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4 ) = &
                   conjg (  array_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(4)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(4)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4 ) = &
                   conjg (  array_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c4d
      !
      module subroutine sp_devxlib_conjg_c5d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(5) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(5)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(5)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c5d
      !
      module subroutine sp_devxlib_conjg_c6d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5, & 
                                        range6, lbound6 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_inout, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(6) async(async_id)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(6)
            !DEV_ACC data present(array_inout)
            !DEV_ACC parallel loop collapse(6)
            !DEV_OMPGPU target map(present,alloc:array_inout)
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               array_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  array_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c6d
      !


      module subroutine dp_devxlib_conjg_c1d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1 )
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(1) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               ptr_inout(i1 ) = &
                   conjg (  ptr_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(1)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(1)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               ptr_inout(i1 ) = &
                   conjg (  ptr_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c1d_p
      !
      module subroutine dp_devxlib_conjg_c2d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2 )
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(2) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2 ) = &
                   conjg (  ptr_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(2)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(2)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2 ) = &
                   conjg (  ptr_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c2d_p
      !
      module subroutine dp_devxlib_conjg_c3d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3 )
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(3) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3 ) = &
                   conjg (  ptr_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(3)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(3)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3 ) = &
                   conjg (  ptr_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c3d_p
      !
      module subroutine dp_devxlib_conjg_c4d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4 )
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(ptr_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(4) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(4)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(4)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c4d_p
      !
      module subroutine dp_devxlib_conjg_c5d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5 )
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(ptr_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(ptr_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(5) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(5)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(5)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c5d_p
      !
      module subroutine dp_devxlib_conjg_c6d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5, & 
                                        range6, lbound6 )
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(ptr_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(ptr_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(ptr_inout, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(6) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(6)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(6)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine dp_devxlib_conjg_c6d_p
      !
      module subroutine sp_devxlib_conjg_c1d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1 )
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(1) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               ptr_inout(i1 ) = &
                   conjg (  ptr_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(1)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(1)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(1)
            !DEV_OMP  parallel do
            do i1 = d1s, d1e
               ptr_inout(i1 ) = &
                   conjg (  ptr_inout (i1 ) )
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c1d_p
      !
      module subroutine sp_devxlib_conjg_c2d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2 )
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(2) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2 ) = &
                   conjg (  ptr_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(2)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(2)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(2)
            !DEV_OMP  parallel do
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2 ) = &
                   conjg (  ptr_inout (i1,i2 ) )
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c2d_p
      !
      module subroutine sp_devxlib_conjg_c3d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3 )
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(3) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3 ) = &
                   conjg (  ptr_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(3)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(3)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(3)
            !DEV_OMP  parallel do
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3 ) = &
                   conjg (  ptr_inout (i1,i2,i3 ) )
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c3d_p
      !
      module subroutine sp_devxlib_conjg_c4d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4 )
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(ptr_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(4) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(4)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(4)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(4)
            !DEV_OMP  parallel do
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4 ) )
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c4d_p
      !
      module subroutine sp_devxlib_conjg_c5d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5 )
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(ptr_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(ptr_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(5) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(5)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(5)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(5)
            !DEV_OMP  parallel do
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c5d_p
      !
      module subroutine sp_devxlib_conjg_c6d_p(ptr_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5, & 
                                        range6, lbound6 )
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(ptr_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(ptr_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(ptr_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(ptr_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(ptr_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(ptr_inout, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !
         if (present(async_id)) then
            !
            !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(6) async(async_id)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         else
            !
            !DEV_CUF kernel do(6)
#if defined __GFORTRAN__
            !DEV_ACC data present(ptr_inout)
#else
            !DEV_ACC data deviceptr(ptr_inout)
#endif
            !DEV_ACC parallel loop collapse(6)
            !DEV_OMPGPU target 
            !DEV_OMPGPU teams loop collapse(6)
            !DEV_OMP  parallel do
            do i6 = d6s, d6e
            do i5 = d5s, d5e
            do i4 = d4s, d4e
            do i3 = d3s, d3e
            do i2 = d2s, d2e
            do i1 = d1s, d1e
               ptr_inout(i1,i2,i3,i4,i5,i6 ) = &
                   conjg (  ptr_inout (i1,i2,i3,i4,i5,i6 ) )
            enddo
            enddo
            enddo
            enddo
            enddo
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
            !
         endif
          !
      end subroutine sp_devxlib_conjg_c6d_p
      !


      module subroutine dp_devxlib_conjg_h_c1d(array_inout, async_id, &
                                         
                                        range1, lbound1 )
         implicit none
         !
         complex(real64), intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !
         do i1 = d1s, d1e
            array_inout(i1 ) = &
                conjg (  array_inout (i1 ) )
         enddo
         !
      end subroutine dp_devxlib_conjg_h_c1d
      !
      module subroutine dp_devxlib_conjg_h_c2d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2 )
         implicit none
         !
         complex(real64), intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2 ) = &
                conjg (  array_inout (i1,i2 ) )
         enddo
         enddo
         !
      end subroutine dp_devxlib_conjg_h_c2d
      !
      module subroutine dp_devxlib_conjg_h_c3d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3 )
         implicit none
         !
         complex(real64), intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3 ) = &
                conjg (  array_inout (i1,i2,i3 ) )
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_conjg_h_c3d
      !
      module subroutine dp_devxlib_conjg_h_c4d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4 )
         implicit none
         !
         complex(real64), intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3,i4 ) = &
                conjg (  array_inout (i1,i2,i3,i4 ) )
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_conjg_h_c4d
      !
      module subroutine dp_devxlib_conjg_h_c5d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5 )
         implicit none
         !
         complex(real64), intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3,i4,i5 ) = &
                conjg (  array_inout (i1,i2,i3,i4,i5 ) )
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_conjg_h_c5d
      !
      module subroutine dp_devxlib_conjg_h_c6d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5, & 
                                        range6, lbound6 )
         implicit none
         !
         complex(real64), intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_inout, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3,i4,i5,i6 ) = &
                conjg (  array_inout (i1,i2,i3,i4,i5,i6 ) )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_conjg_h_c6d
      !
      module subroutine sp_devxlib_conjg_h_c1d(array_inout, async_id, &
                                         
                                        range1, lbound1 )
         implicit none
         !
         complex(real32), intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !
         do i1 = d1s, d1e
            array_inout(i1 ) = &
                conjg (  array_inout (i1 ) )
         enddo
         !
      end subroutine sp_devxlib_conjg_h_c1d
      !
      module subroutine sp_devxlib_conjg_h_c2d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2 )
         implicit none
         !
         complex(real32), intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2 ) = &
                conjg (  array_inout (i1,i2 ) )
         enddo
         enddo
         !
      end subroutine sp_devxlib_conjg_h_c2d
      !
      module subroutine sp_devxlib_conjg_h_c3d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3 )
         implicit none
         !
         complex(real32), intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3 ) = &
                conjg (  array_inout (i1,i2,i3 ) )
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_conjg_h_c3d
      !
      module subroutine sp_devxlib_conjg_h_c4d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4 )
         implicit none
         !
         complex(real32), intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3,i4 ) = &
                conjg (  array_inout (i1,i2,i3,i4 ) )
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_conjg_h_c4d
      !
      module subroutine sp_devxlib_conjg_h_c5d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5 )
         implicit none
         !
         complex(real32), intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3,i4,i5 ) = &
                conjg (  array_inout (i1,i2,i3,i4,i5 ) )
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_conjg_h_c5d
      !
      module subroutine sp_devxlib_conjg_h_c6d(array_inout, async_id, &
                                         
                                        range1, lbound1, & 
                                        range2, lbound2, & 
                                        range3, lbound3, & 
                                        range4, lbound4, & 
                                        range5, lbound5, & 
                                        range6, lbound6 )
         implicit none
         !
         complex(real32), intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_inout, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_inout, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_inout, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_inout, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_inout, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_inout, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_inout(i1,i2,i3,i4,i5,i6 ) = &
                conjg (  array_inout (i1,i2,i3,i4,i5,i6 ) )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_conjg_h_c6d
      !

end submodule devxlib_auxfunc_conjg