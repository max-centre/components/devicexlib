!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
!
! Utility functions to perform data initializations (on host and
! on device), using CUDA-Fortran, OpenACC and OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_memset.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
! Note about dimensions:
! The lower bound of the assumed shape array passed to the subroutine is 1
! lbound and range instead refer to the indexing in the parent caller.
!
module devxlib_memset

   use iso_fortran_env,     only : int32, int64, real32, real64
#if defined __DXL_CUDAF
   use cudafor
#elif defined __DXL_OPENACC
   use openacc
   use devxlib_environment, only : cudaStreamSynchronize, cudaDeviceSynchronize, &
                                   acc_memcpy_device_f, acc_memcpy_from_device_f, acc_memcpy_to_device_f
#endif

   implicit none

   interface devxlib_memset_d
      module procedure &
         sp_devxlib_memset_d_r1d, sp_devxlib_memset_d_r2d, sp_devxlib_memset_d_r3d, &
         sp_devxlib_memset_d_r4d, sp_devxlib_memset_d_r5d, sp_devxlib_memset_d_r6d, &
         dp_devxlib_memset_d_r1d, dp_devxlib_memset_d_r2d, dp_devxlib_memset_d_r3d, &
         dp_devxlib_memset_d_r4d, dp_devxlib_memset_d_r5d, dp_devxlib_memset_d_r6d, &
         sp_devxlib_memset_d_c1d, sp_devxlib_memset_d_c2d, sp_devxlib_memset_d_c3d, &
         sp_devxlib_memset_d_c4d, sp_devxlib_memset_d_c5d, sp_devxlib_memset_d_c6d, &
         dp_devxlib_memset_d_c1d, dp_devxlib_memset_d_c2d, dp_devxlib_memset_d_c3d, &
         dp_devxlib_memset_d_c4d, dp_devxlib_memset_d_c5d, dp_devxlib_memset_d_c6d, &
         i4_devxlib_memset_d_i1d, i4_devxlib_memset_d_i2d, i4_devxlib_memset_d_i3d, &
         i4_devxlib_memset_d_i4d, i4_devxlib_memset_d_i5d, i4_devxlib_memset_d_i6d, &
         i8_devxlib_memset_d_i1d, i8_devxlib_memset_d_i2d, i8_devxlib_memset_d_i3d, &
         i8_devxlib_memset_d_i4d, i8_devxlib_memset_d_i5d, i8_devxlib_memset_d_i6d, &
         l4_devxlib_memset_d_l1d, l4_devxlib_memset_d_l2d, l4_devxlib_memset_d_l3d, &
         l4_devxlib_memset_d_l4d, l4_devxlib_memset_d_l5d, l4_devxlib_memset_d_l6d
   end interface devxlib_memset_d

   interface devxlib_memset_h
      module procedure &
         sp_devxlib_memset_h_r1d, sp_devxlib_memset_h_r2d, sp_devxlib_memset_h_r3d, &
         sp_devxlib_memset_h_r4d, sp_devxlib_memset_h_r5d, sp_devxlib_memset_h_r6d, &
         dp_devxlib_memset_h_r1d, dp_devxlib_memset_h_r2d, dp_devxlib_memset_h_r3d, &
         dp_devxlib_memset_h_r4d, dp_devxlib_memset_h_r5d, dp_devxlib_memset_h_r6d, &
         sp_devxlib_memset_h_c1d, sp_devxlib_memset_h_c2d, sp_devxlib_memset_h_c3d, &
         sp_devxlib_memset_h_c4d, sp_devxlib_memset_h_c5d, sp_devxlib_memset_h_c6d, &
         dp_devxlib_memset_h_c1d, dp_devxlib_memset_h_c2d, dp_devxlib_memset_h_c3d, &
         dp_devxlib_memset_h_c4d, dp_devxlib_memset_h_c5d, dp_devxlib_memset_h_c6d, &
         i4_devxlib_memset_h_i1d, i4_devxlib_memset_h_i2d, i4_devxlib_memset_h_i3d, &
         i4_devxlib_memset_h_i4d, i4_devxlib_memset_h_i5d, i4_devxlib_memset_h_i6d, &
         i8_devxlib_memset_h_i1d, i8_devxlib_memset_h_i2d, i8_devxlib_memset_h_i3d, &
         i8_devxlib_memset_h_i4d, i8_devxlib_memset_h_i5d, i8_devxlib_memset_h_i6d, &
         l4_devxlib_memset_h_l1d, l4_devxlib_memset_h_l2d, l4_devxlib_memset_h_l3d, &
         l4_devxlib_memset_h_l4d, l4_devxlib_memset_h_l5d, l4_devxlib_memset_h_l6d
   end interface devxlib_memset_h

   interface
      module subroutine sp_devxlib_memset_h_r1d(array_out, val, &
                                                  range1, lbound1 )
          implicit none
          !
          real(real32), intent(inout) :: array_out(:)
          real(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memset_h_r1d
      !
      module subroutine sp_devxlib_memset_h_r2d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          !
          real(real32), intent(inout) :: array_out(:,:)
          real(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memset_h_r2d
      !
      module subroutine sp_devxlib_memset_h_r3d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          !
          real(real32), intent(inout) :: array_out(:,:,:)
          real(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memset_h_r3d
      !
      module subroutine sp_devxlib_memset_h_r4d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          !
          real(real32), intent(inout) :: array_out(:,:,:,:)
          real(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memset_h_r4d
      !
      module subroutine sp_devxlib_memset_h_r5d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          !
          real(real32), intent(inout) :: array_out(:,:,:,:,:)
          real(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memset_h_r5d
      !
      module subroutine sp_devxlib_memset_h_r6d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          !
          real(real32), intent(inout) :: array_out(:,:,:,:,:,:)
          real(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memset_h_r6d
      !
      module subroutine dp_devxlib_memset_h_r1d(array_out, val, &
                                                  range1, lbound1 )
          implicit none
          !
          real(real64), intent(inout) :: array_out(:)
          real(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memset_h_r1d
      !
      module subroutine dp_devxlib_memset_h_r2d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          !
          real(real64), intent(inout) :: array_out(:,:)
          real(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memset_h_r2d
      !
      module subroutine dp_devxlib_memset_h_r3d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          !
          real(real64), intent(inout) :: array_out(:,:,:)
          real(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memset_h_r3d
      !
      module subroutine dp_devxlib_memset_h_r4d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          !
          real(real64), intent(inout) :: array_out(:,:,:,:)
          real(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memset_h_r4d
      !
      module subroutine dp_devxlib_memset_h_r5d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          !
          real(real64), intent(inout) :: array_out(:,:,:,:,:)
          real(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memset_h_r5d
      !
      module subroutine dp_devxlib_memset_h_r6d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          !
          real(real64), intent(inout) :: array_out(:,:,:,:,:,:)
          real(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memset_h_r6d
      !
      module subroutine sp_devxlib_memset_h_c1d(array_out, val, &
                                                  range1, lbound1 )
          implicit none
          !
          complex(real32), intent(inout) :: array_out(:)
          complex(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memset_h_c1d
      !
      module subroutine sp_devxlib_memset_h_c2d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          !
          complex(real32), intent(inout) :: array_out(:,:)
          complex(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memset_h_c2d
      !
      module subroutine sp_devxlib_memset_h_c3d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          !
          complex(real32), intent(inout) :: array_out(:,:,:)
          complex(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memset_h_c3d
      !
      module subroutine sp_devxlib_memset_h_c4d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          !
          complex(real32), intent(inout) :: array_out(:,:,:,:)
          complex(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memset_h_c4d
      !
      module subroutine sp_devxlib_memset_h_c5d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          !
          complex(real32), intent(inout) :: array_out(:,:,:,:,:)
          complex(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memset_h_c5d
      !
      module subroutine sp_devxlib_memset_h_c6d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          !
          complex(real32), intent(inout) :: array_out(:,:,:,:,:,:)
          complex(real32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memset_h_c6d
      !
      module subroutine dp_devxlib_memset_h_c1d(array_out, val, &
                                                  range1, lbound1 )
          implicit none
          !
          complex(real64), intent(inout) :: array_out(:)
          complex(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memset_h_c1d
      !
      module subroutine dp_devxlib_memset_h_c2d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          !
          complex(real64), intent(inout) :: array_out(:,:)
          complex(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memset_h_c2d
      !
      module subroutine dp_devxlib_memset_h_c3d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          !
          complex(real64), intent(inout) :: array_out(:,:,:)
          complex(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memset_h_c3d
      !
      module subroutine dp_devxlib_memset_h_c4d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          !
          complex(real64), intent(inout) :: array_out(:,:,:,:)
          complex(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memset_h_c4d
      !
      module subroutine dp_devxlib_memset_h_c5d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          !
          complex(real64), intent(inout) :: array_out(:,:,:,:,:)
          complex(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memset_h_c5d
      !
      module subroutine dp_devxlib_memset_h_c6d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          !
          complex(real64), intent(inout) :: array_out(:,:,:,:,:,:)
          complex(real64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memset_h_c6d
      !
      module subroutine i4_devxlib_memset_h_i1d(array_out, val, &
                                                  range1, lbound1 )
          implicit none
          !
          integer(int32), intent(inout) :: array_out(:)
          integer(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memset_h_i1d
      !
      module subroutine i4_devxlib_memset_h_i2d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          !
          integer(int32), intent(inout) :: array_out(:,:)
          integer(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memset_h_i2d
      !
      module subroutine i4_devxlib_memset_h_i3d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          !
          integer(int32), intent(inout) :: array_out(:,:,:)
          integer(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memset_h_i3d
      !
      module subroutine i4_devxlib_memset_h_i4d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          !
          integer(int32), intent(inout) :: array_out(:,:,:,:)
          integer(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memset_h_i4d
      !
      module subroutine i4_devxlib_memset_h_i5d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          !
          integer(int32), intent(inout) :: array_out(:,:,:,:,:)
          integer(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memset_h_i5d
      !
      module subroutine i4_devxlib_memset_h_i6d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          !
          integer(int32), intent(inout) :: array_out(:,:,:,:,:,:)
          integer(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memset_h_i6d
      !
      module subroutine i8_devxlib_memset_h_i1d(array_out, val, &
                                                  range1, lbound1 )
          implicit none
          !
          integer(int64), intent(inout) :: array_out(:)
          integer(int64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memset_h_i1d
      !
      module subroutine i8_devxlib_memset_h_i2d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          !
          integer(int64), intent(inout) :: array_out(:,:)
          integer(int64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memset_h_i2d
      !
      module subroutine i8_devxlib_memset_h_i3d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          !
          integer(int64), intent(inout) :: array_out(:,:,:)
          integer(int64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memset_h_i3d
      !
      module subroutine i8_devxlib_memset_h_i4d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          !
          integer(int64), intent(inout) :: array_out(:,:,:,:)
          integer(int64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memset_h_i4d
      !
      module subroutine i8_devxlib_memset_h_i5d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          !
          integer(int64), intent(inout) :: array_out(:,:,:,:,:)
          integer(int64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memset_h_i5d
      !
      module subroutine i8_devxlib_memset_h_i6d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          !
          integer(int64), intent(inout) :: array_out(:,:,:,:,:,:)
          integer(int64), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memset_h_i6d
      !
      module subroutine l4_devxlib_memset_h_l1d(array_out, val, &
                                                  range1, lbound1 )
          implicit none
          !
          logical(int32), intent(inout) :: array_out(:)
          logical(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memset_h_l1d
      !
      module subroutine l4_devxlib_memset_h_l2d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          !
          logical(int32), intent(inout) :: array_out(:,:)
          logical(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memset_h_l2d
      !
      module subroutine l4_devxlib_memset_h_l3d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          !
          logical(int32), intent(inout) :: array_out(:,:,:)
          logical(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memset_h_l3d
      !
      module subroutine l4_devxlib_memset_h_l4d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          !
          logical(int32), intent(inout) :: array_out(:,:,:,:)
          logical(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memset_h_l4d
      !
      module subroutine l4_devxlib_memset_h_l5d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          !
          logical(int32), intent(inout) :: array_out(:,:,:,:,:)
          logical(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memset_h_l5d
      !
      module subroutine l4_devxlib_memset_h_l6d(array_out, val, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          !
          logical(int32), intent(inout) :: array_out(:,:,:,:,:,:)
          logical(int32), intent(in)    :: val
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memset_h_l6d
      !
      module subroutine sp_devxlib_memset_d_r1d(array_out, val, &
                                            range1, lbound1 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:)
         real(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memset_d_r1d
      !
      module subroutine sp_devxlib_memset_d_r2d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memset_d_r2d
      !
      module subroutine sp_devxlib_memset_d_r3d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memset_d_r3d
      !
      module subroutine sp_devxlib_memset_d_r4d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memset_d_r4d
      !
      module subroutine sp_devxlib_memset_d_r5d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memset_d_r5d
      !
      module subroutine sp_devxlib_memset_d_r6d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memset_d_r6d
      !
      module subroutine dp_devxlib_memset_d_r1d(array_out, val, &
                                            range1, lbound1 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:)
         real(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memset_d_r1d
      !
      module subroutine dp_devxlib_memset_d_r2d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memset_d_r2d
      !
      module subroutine dp_devxlib_memset_d_r3d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memset_d_r3d
      !
      module subroutine dp_devxlib_memset_d_r4d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memset_d_r4d
      !
      module subroutine dp_devxlib_memset_d_r5d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memset_d_r5d
      !
      module subroutine dp_devxlib_memset_d_r6d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memset_d_r6d
      !
      module subroutine sp_devxlib_memset_d_c1d(array_out, val, &
                                            range1, lbound1 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memset_d_c1d
      !
      module subroutine sp_devxlib_memset_d_c2d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memset_d_c2d
      !
      module subroutine sp_devxlib_memset_d_c3d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memset_d_c3d
      !
      module subroutine sp_devxlib_memset_d_c4d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memset_d_c4d
      !
      module subroutine sp_devxlib_memset_d_c5d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memset_d_c5d
      !
      module subroutine sp_devxlib_memset_d_c6d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memset_d_c6d
      !
      module subroutine dp_devxlib_memset_d_c1d(array_out, val, &
                                            range1, lbound1 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memset_d_c1d
      !
      module subroutine dp_devxlib_memset_d_c2d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memset_d_c2d
      !
      module subroutine dp_devxlib_memset_d_c3d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memset_d_c3d
      !
      module subroutine dp_devxlib_memset_d_c4d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memset_d_c4d
      !
      module subroutine dp_devxlib_memset_d_c5d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memset_d_c5d
      !
      module subroutine dp_devxlib_memset_d_c6d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memset_d_c6d
      !
      module subroutine i4_devxlib_memset_d_i1d(array_out, val, &
                                            range1, lbound1 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memset_d_i1d
      !
      module subroutine i4_devxlib_memset_d_i2d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memset_d_i2d
      !
      module subroutine i4_devxlib_memset_d_i3d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memset_d_i3d
      !
      module subroutine i4_devxlib_memset_d_i4d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memset_d_i4d
      !
      module subroutine i4_devxlib_memset_d_i5d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memset_d_i5d
      !
      module subroutine i4_devxlib_memset_d_i6d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memset_d_i6d
      !
      module subroutine i8_devxlib_memset_d_i1d(array_out, val, &
                                            range1, lbound1 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memset_d_i1d
      !
      module subroutine i8_devxlib_memset_d_i2d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memset_d_i2d
      !
      module subroutine i8_devxlib_memset_d_i3d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memset_d_i3d
      !
      module subroutine i8_devxlib_memset_d_i4d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memset_d_i4d
      !
      module subroutine i8_devxlib_memset_d_i5d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memset_d_i5d
      !
      module subroutine i8_devxlib_memset_d_i6d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memset_d_i6d
      !
      module subroutine l4_devxlib_memset_d_l1d(array_out, val, &
                                            range1, lbound1 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:)
         logical(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memset_d_l1d
      !
      module subroutine l4_devxlib_memset_d_l2d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         logical(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memset_d_l2d
      !
      module subroutine l4_devxlib_memset_d_l3d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         logical(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memset_d_l3d
      !
      module subroutine l4_devxlib_memset_d_l4d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         logical(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memset_d_l4d
      !
      module subroutine l4_devxlib_memset_d_l5d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         logical(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memset_d_l5d
      !
      module subroutine l4_devxlib_memset_d_l6d(array_out, val, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32), intent(in)    :: val
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memset_d_l6d
      !
   endinterface

endmodule devxlib_memset