!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Module for direct memory access on the device (allocation, deallocation
! and allocation check) using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_malloc.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
module devxlib_malloc

   use iso_fortran_env,             only : int32, int64, real32, real64
   use, intrinsic :: iso_c_binding, only : c_loc, c_ptr, c_size_t, c_f_pointer, c_associated
#if defined __DXL_CUDAF || defined __DXL_OPENACC
   use devxlib_environment
#elif defined __DXL_OPENMP_GPU
   use omp_lib, only : omp_get_default_device, omp_target_is_present, omp_target_alloc, omp_target_free
#endif

   implicit none

   interface devxlib_alloc
      module procedure &
         sp_devxlib_malloc_alloc_r1d, sp_devxlib_malloc_alloc_r2d, sp_devxlib_malloc_alloc_r3d, &
         sp_devxlib_malloc_alloc_r4d, sp_devxlib_malloc_alloc_r5d, sp_devxlib_malloc_alloc_r6d, &
         dp_devxlib_malloc_alloc_r1d, dp_devxlib_malloc_alloc_r2d, dp_devxlib_malloc_alloc_r3d, &
         dp_devxlib_malloc_alloc_r4d, dp_devxlib_malloc_alloc_r5d, dp_devxlib_malloc_alloc_r6d, &
         sp_devxlib_malloc_alloc_c1d, sp_devxlib_malloc_alloc_c2d, sp_devxlib_malloc_alloc_c3d, &
         sp_devxlib_malloc_alloc_c4d, sp_devxlib_malloc_alloc_c5d, sp_devxlib_malloc_alloc_c6d, &
         dp_devxlib_malloc_alloc_c1d, dp_devxlib_malloc_alloc_c2d, dp_devxlib_malloc_alloc_c3d, &
         dp_devxlib_malloc_alloc_c4d, dp_devxlib_malloc_alloc_c5d, dp_devxlib_malloc_alloc_c6d, &
         i4_devxlib_malloc_alloc_i1d, i4_devxlib_malloc_alloc_i2d, i4_devxlib_malloc_alloc_i3d, &
         i4_devxlib_malloc_alloc_i4d, i4_devxlib_malloc_alloc_i5d, i4_devxlib_malloc_alloc_i6d, &
         i8_devxlib_malloc_alloc_i1d, i8_devxlib_malloc_alloc_i2d, i8_devxlib_malloc_alloc_i3d, &
         i8_devxlib_malloc_alloc_i4d, i8_devxlib_malloc_alloc_i5d, i8_devxlib_malloc_alloc_i6d, &
         l4_devxlib_malloc_alloc_l1d, l4_devxlib_malloc_alloc_l2d, l4_devxlib_malloc_alloc_l3d, &
         l4_devxlib_malloc_alloc_l4d, l4_devxlib_malloc_alloc_l5d, l4_devxlib_malloc_alloc_l6d
   endinterface devxlib_alloc

   interface devxlib_free
      module procedure &
         sp_devxlib_malloc_free_r1d, sp_devxlib_malloc_free_r2d, sp_devxlib_malloc_free_r3d, &
         sp_devxlib_malloc_free_r4d, sp_devxlib_malloc_free_r5d, sp_devxlib_malloc_free_r6d, &
         dp_devxlib_malloc_free_r1d, dp_devxlib_malloc_free_r2d, dp_devxlib_malloc_free_r3d, &
         dp_devxlib_malloc_free_r4d, dp_devxlib_malloc_free_r5d, dp_devxlib_malloc_free_r6d, &
         sp_devxlib_malloc_free_c1d, sp_devxlib_malloc_free_c2d, sp_devxlib_malloc_free_c3d, &
         sp_devxlib_malloc_free_c4d, sp_devxlib_malloc_free_c5d, sp_devxlib_malloc_free_c6d, &
         dp_devxlib_malloc_free_c1d, dp_devxlib_malloc_free_c2d, dp_devxlib_malloc_free_c3d, &
         dp_devxlib_malloc_free_c4d, dp_devxlib_malloc_free_c5d, dp_devxlib_malloc_free_c6d, &
         i4_devxlib_malloc_free_i1d, i4_devxlib_malloc_free_i2d, i4_devxlib_malloc_free_i3d, &
         i4_devxlib_malloc_free_i4d, i4_devxlib_malloc_free_i5d, i4_devxlib_malloc_free_i6d, &
         i8_devxlib_malloc_free_i1d, i8_devxlib_malloc_free_i2d, i8_devxlib_malloc_free_i3d, &
         i8_devxlib_malloc_free_i4d, i8_devxlib_malloc_free_i5d, i8_devxlib_malloc_free_i6d, &
         l4_devxlib_malloc_free_l1d, l4_devxlib_malloc_free_l2d, l4_devxlib_malloc_free_l3d, &
         l4_devxlib_malloc_free_l4d, l4_devxlib_malloc_free_l5d, l4_devxlib_malloc_free_l6d
   endinterface devxlib_free

   interface devxlib_allocated
      module procedure &
         sp_devxlib_malloc_allocated_r1d, sp_devxlib_malloc_allocated_r2d, sp_devxlib_malloc_allocated_r3d, &
         sp_devxlib_malloc_allocated_r4d, sp_devxlib_malloc_allocated_r5d, sp_devxlib_malloc_allocated_r6d, &
         dp_devxlib_malloc_allocated_r1d, dp_devxlib_malloc_allocated_r2d, dp_devxlib_malloc_allocated_r3d, &
         dp_devxlib_malloc_allocated_r4d, dp_devxlib_malloc_allocated_r5d, dp_devxlib_malloc_allocated_r6d, &
         sp_devxlib_malloc_allocated_c1d, sp_devxlib_malloc_allocated_c2d, sp_devxlib_malloc_allocated_c3d, &
         sp_devxlib_malloc_allocated_c4d, sp_devxlib_malloc_allocated_c5d, sp_devxlib_malloc_allocated_c6d, &
         dp_devxlib_malloc_allocated_c1d, dp_devxlib_malloc_allocated_c2d, dp_devxlib_malloc_allocated_c3d, &
         dp_devxlib_malloc_allocated_c4d, dp_devxlib_malloc_allocated_c5d, dp_devxlib_malloc_allocated_c6d, &
         i4_devxlib_malloc_allocated_i1d, i4_devxlib_malloc_allocated_i2d, i4_devxlib_malloc_allocated_i3d, &
         i4_devxlib_malloc_allocated_i4d, i4_devxlib_malloc_allocated_i5d, i4_devxlib_malloc_allocated_i6d, &
         i8_devxlib_malloc_allocated_i1d, i8_devxlib_malloc_allocated_i2d, i8_devxlib_malloc_allocated_i3d, &
         i8_devxlib_malloc_allocated_i4d, i8_devxlib_malloc_allocated_i5d, i8_devxlib_malloc_allocated_i6d, &
         l4_devxlib_malloc_allocated_l1d, l4_devxlib_malloc_allocated_l2d, l4_devxlib_malloc_allocated_l3d, &
         l4_devxlib_malloc_allocated_l4d, l4_devxlib_malloc_allocated_l5d, l4_devxlib_malloc_allocated_l6d
   endinterface devxlib_allocated

   interface devxlib_allocated_p
      module procedure &
         sp_devxlib_malloc_allocated_r1d_p, sp_devxlib_malloc_allocated_r2d_p, sp_devxlib_malloc_allocated_r3d_p, &
         sp_devxlib_malloc_allocated_r4d_p, sp_devxlib_malloc_allocated_r5d_p, sp_devxlib_malloc_allocated_r6d_p, &
         dp_devxlib_malloc_allocated_r1d_p, dp_devxlib_malloc_allocated_r2d_p, dp_devxlib_malloc_allocated_r3d_p, &
         dp_devxlib_malloc_allocated_r4d_p, dp_devxlib_malloc_allocated_r5d_p, dp_devxlib_malloc_allocated_r6d_p, &
         sp_devxlib_malloc_allocated_c1d_p, sp_devxlib_malloc_allocated_c2d_p, sp_devxlib_malloc_allocated_c3d_p, &
         sp_devxlib_malloc_allocated_c4d_p, sp_devxlib_malloc_allocated_c5d_p, sp_devxlib_malloc_allocated_c6d_p, &
         dp_devxlib_malloc_allocated_c1d_p, dp_devxlib_malloc_allocated_c2d_p, dp_devxlib_malloc_allocated_c3d_p, &
         dp_devxlib_malloc_allocated_c4d_p, dp_devxlib_malloc_allocated_c5d_p, dp_devxlib_malloc_allocated_c6d_p, &
         i4_devxlib_malloc_allocated_i1d_p, i4_devxlib_malloc_allocated_i2d_p, i4_devxlib_malloc_allocated_i3d_p, &
         i4_devxlib_malloc_allocated_i4d_p, i4_devxlib_malloc_allocated_i5d_p, i4_devxlib_malloc_allocated_i6d_p, &
         i8_devxlib_malloc_allocated_i1d_p, i8_devxlib_malloc_allocated_i2d_p, i8_devxlib_malloc_allocated_i3d_p, &
         i8_devxlib_malloc_allocated_i4d_p, i8_devxlib_malloc_allocated_i5d_p, i8_devxlib_malloc_allocated_i6d_p, &
         l4_devxlib_malloc_allocated_l1d_p, l4_devxlib_malloc_allocated_l2d_p, l4_devxlib_malloc_allocated_l3d_p, &
         l4_devxlib_malloc_allocated_l4d_p, l4_devxlib_malloc_allocated_l5d_p, l4_devxlib_malloc_allocated_l6d_p
   endinterface devxlib_allocated_p


   interface
      module subroutine sp_devxlib_malloc_alloc_r1d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:)
         integer(int64), intent(in)           :: dimensions
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds
      end subroutine sp_devxlib_malloc_alloc_r1d
      module subroutine sp_devxlib_malloc_alloc_r2d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:)
         integer(int64), intent(in)           :: dimensions(2)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(2)
      end subroutine sp_devxlib_malloc_alloc_r2d
      module subroutine sp_devxlib_malloc_alloc_r3d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:)
         integer(int64), intent(in)           :: dimensions(3)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(3)
      end subroutine sp_devxlib_malloc_alloc_r3d
      module subroutine sp_devxlib_malloc_alloc_r4d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer(int64), intent(in)           :: dimensions(4)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(4)
      end subroutine sp_devxlib_malloc_alloc_r4d
      module subroutine sp_devxlib_malloc_alloc_r5d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(5)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(5)
      end subroutine sp_devxlib_malloc_alloc_r5d
      module subroutine sp_devxlib_malloc_alloc_r6d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(6)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(6)
      end subroutine sp_devxlib_malloc_alloc_r6d
      module subroutine dp_devxlib_malloc_alloc_r1d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:)
         integer(int64), intent(in)           :: dimensions
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds
      end subroutine dp_devxlib_malloc_alloc_r1d
      module subroutine dp_devxlib_malloc_alloc_r2d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:)
         integer(int64), intent(in)           :: dimensions(2)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(2)
      end subroutine dp_devxlib_malloc_alloc_r2d
      module subroutine dp_devxlib_malloc_alloc_r3d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:)
         integer(int64), intent(in)           :: dimensions(3)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(3)
      end subroutine dp_devxlib_malloc_alloc_r3d
      module subroutine dp_devxlib_malloc_alloc_r4d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer(int64), intent(in)           :: dimensions(4)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(4)
      end subroutine dp_devxlib_malloc_alloc_r4d
      module subroutine dp_devxlib_malloc_alloc_r5d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(5)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(5)
      end subroutine dp_devxlib_malloc_alloc_r5d
      module subroutine dp_devxlib_malloc_alloc_r6d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         real(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(6)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(6)
      end subroutine dp_devxlib_malloc_alloc_r6d
      module subroutine sp_devxlib_malloc_alloc_c1d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:)
         integer(int64), intent(in)           :: dimensions
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds
      end subroutine sp_devxlib_malloc_alloc_c1d
      module subroutine sp_devxlib_malloc_alloc_c2d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:)
         integer(int64), intent(in)           :: dimensions(2)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(2)
      end subroutine sp_devxlib_malloc_alloc_c2d
      module subroutine sp_devxlib_malloc_alloc_c3d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:)
         integer(int64), intent(in)           :: dimensions(3)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(3)
      end subroutine sp_devxlib_malloc_alloc_c3d
      module subroutine sp_devxlib_malloc_alloc_c4d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer(int64), intent(in)           :: dimensions(4)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(4)
      end subroutine sp_devxlib_malloc_alloc_c4d
      module subroutine sp_devxlib_malloc_alloc_c5d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(5)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(5)
      end subroutine sp_devxlib_malloc_alloc_c5d
      module subroutine sp_devxlib_malloc_alloc_c6d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(6)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(6)
      end subroutine sp_devxlib_malloc_alloc_c6d
      module subroutine dp_devxlib_malloc_alloc_c1d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:)
         integer(int64), intent(in)           :: dimensions
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds
      end subroutine dp_devxlib_malloc_alloc_c1d
      module subroutine dp_devxlib_malloc_alloc_c2d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:)
         integer(int64), intent(in)           :: dimensions(2)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(2)
      end subroutine dp_devxlib_malloc_alloc_c2d
      module subroutine dp_devxlib_malloc_alloc_c3d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:)
         integer(int64), intent(in)           :: dimensions(3)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(3)
      end subroutine dp_devxlib_malloc_alloc_c3d
      module subroutine dp_devxlib_malloc_alloc_c4d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer(int64), intent(in)           :: dimensions(4)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(4)
      end subroutine dp_devxlib_malloc_alloc_c4d
      module subroutine dp_devxlib_malloc_alloc_c5d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(5)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(5)
      end subroutine dp_devxlib_malloc_alloc_c5d
      module subroutine dp_devxlib_malloc_alloc_c6d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         complex(real64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(6)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(6)
      end subroutine dp_devxlib_malloc_alloc_c6d
      module subroutine i4_devxlib_malloc_alloc_i1d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:)
         integer(int64), intent(in)           :: dimensions
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds
      end subroutine i4_devxlib_malloc_alloc_i1d
      module subroutine i4_devxlib_malloc_alloc_i2d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:)
         integer(int64), intent(in)           :: dimensions(2)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(2)
      end subroutine i4_devxlib_malloc_alloc_i2d
      module subroutine i4_devxlib_malloc_alloc_i3d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:)
         integer(int64), intent(in)           :: dimensions(3)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(3)
      end subroutine i4_devxlib_malloc_alloc_i3d
      module subroutine i4_devxlib_malloc_alloc_i4d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer(int64), intent(in)           :: dimensions(4)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(4)
      end subroutine i4_devxlib_malloc_alloc_i4d
      module subroutine i4_devxlib_malloc_alloc_i5d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(5)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(5)
      end subroutine i4_devxlib_malloc_alloc_i5d
      module subroutine i4_devxlib_malloc_alloc_i6d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(6)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(6)
      end subroutine i4_devxlib_malloc_alloc_i6d
      module subroutine i8_devxlib_malloc_alloc_i1d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:)
         integer(int64), intent(in)           :: dimensions
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds
      end subroutine i8_devxlib_malloc_alloc_i1d
      module subroutine i8_devxlib_malloc_alloc_i2d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:)
         integer(int64), intent(in)           :: dimensions(2)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(2)
      end subroutine i8_devxlib_malloc_alloc_i2d
      module subroutine i8_devxlib_malloc_alloc_i3d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:)
         integer(int64), intent(in)           :: dimensions(3)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(3)
      end subroutine i8_devxlib_malloc_alloc_i3d
      module subroutine i8_devxlib_malloc_alloc_i4d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer(int64), intent(in)           :: dimensions(4)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(4)
      end subroutine i8_devxlib_malloc_alloc_i4d
      module subroutine i8_devxlib_malloc_alloc_i5d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(5)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(5)
      end subroutine i8_devxlib_malloc_alloc_i5d
      module subroutine i8_devxlib_malloc_alloc_i6d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         integer(int64), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(6)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(6)
      end subroutine i8_devxlib_malloc_alloc_i6d
      module subroutine l4_devxlib_malloc_alloc_l1d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         logical(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:)
         integer(int64), intent(in)           :: dimensions
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds
      end subroutine l4_devxlib_malloc_alloc_l1d
      module subroutine l4_devxlib_malloc_alloc_l2d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         logical(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:)
         integer(int64), intent(in)           :: dimensions(2)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(2)
      end subroutine l4_devxlib_malloc_alloc_l2d
      module subroutine l4_devxlib_malloc_alloc_l3d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         logical(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:)
         integer(int64), intent(in)           :: dimensions(3)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(3)
      end subroutine l4_devxlib_malloc_alloc_l3d
      module subroutine l4_devxlib_malloc_alloc_l4d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         logical(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer(int64), intent(in)           :: dimensions(4)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(4)
      end subroutine l4_devxlib_malloc_alloc_l4d
      module subroutine l4_devxlib_malloc_alloc_l5d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         logical(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(5)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(5)
      end subroutine l4_devxlib_malloc_alloc_l5d
      module subroutine l4_devxlib_malloc_alloc_l6d(dev_ptr, dimensions, ierr, device_id, lbounds)
         implicit none
         logical(int32), pointer, contiguous, intent(out) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer(int64), intent(in)           :: dimensions(6)
         integer(int32), intent(out)          :: ierr
         integer(int32), intent(in), optional :: device_id
         integer(int64), intent(in), optional :: lbounds(6)
      end subroutine l4_devxlib_malloc_alloc_l6d
   endinterface

   interface
      module subroutine sp_devxlib_malloc_free_r1d(dev_ptr, device_id)
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_r1d
      module subroutine sp_devxlib_malloc_free_r2d(dev_ptr, device_id)
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_r2d
      module subroutine sp_devxlib_malloc_free_r3d(dev_ptr, device_id)
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_r3d
      module subroutine sp_devxlib_malloc_free_r4d(dev_ptr, device_id)
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_r4d
      module subroutine sp_devxlib_malloc_free_r5d(dev_ptr, device_id)
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_r5d
      module subroutine sp_devxlib_malloc_free_r6d(dev_ptr, device_id)
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_r6d
      module subroutine dp_devxlib_malloc_free_r1d(dev_ptr, device_id)
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_r1d
      module subroutine dp_devxlib_malloc_free_r2d(dev_ptr, device_id)
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_r2d
      module subroutine dp_devxlib_malloc_free_r3d(dev_ptr, device_id)
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_r3d
      module subroutine dp_devxlib_malloc_free_r4d(dev_ptr, device_id)
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_r4d
      module subroutine dp_devxlib_malloc_free_r5d(dev_ptr, device_id)
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_r5d
      module subroutine dp_devxlib_malloc_free_r6d(dev_ptr, device_id)
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_r6d
      module subroutine sp_devxlib_malloc_free_c1d(dev_ptr, device_id)
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_c1d
      module subroutine sp_devxlib_malloc_free_c2d(dev_ptr, device_id)
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_c2d
      module subroutine sp_devxlib_malloc_free_c3d(dev_ptr, device_id)
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_c3d
      module subroutine sp_devxlib_malloc_free_c4d(dev_ptr, device_id)
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_c4d
      module subroutine sp_devxlib_malloc_free_c5d(dev_ptr, device_id)
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_c5d
      module subroutine sp_devxlib_malloc_free_c6d(dev_ptr, device_id)
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine sp_devxlib_malloc_free_c6d
      module subroutine dp_devxlib_malloc_free_c1d(dev_ptr, device_id)
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_c1d
      module subroutine dp_devxlib_malloc_free_c2d(dev_ptr, device_id)
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_c2d
      module subroutine dp_devxlib_malloc_free_c3d(dev_ptr, device_id)
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_c3d
      module subroutine dp_devxlib_malloc_free_c4d(dev_ptr, device_id)
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_c4d
      module subroutine dp_devxlib_malloc_free_c5d(dev_ptr, device_id)
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_c5d
      module subroutine dp_devxlib_malloc_free_c6d(dev_ptr, device_id)
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine dp_devxlib_malloc_free_c6d
      module subroutine i4_devxlib_malloc_free_i1d(dev_ptr, device_id)
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
         integer, optional, intent(in)  :: device_id
      end subroutine i4_devxlib_malloc_free_i1d
      module subroutine i4_devxlib_malloc_free_i2d(dev_ptr, device_id)
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i4_devxlib_malloc_free_i2d
      module subroutine i4_devxlib_malloc_free_i3d(dev_ptr, device_id)
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i4_devxlib_malloc_free_i3d
      module subroutine i4_devxlib_malloc_free_i4d(dev_ptr, device_id)
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i4_devxlib_malloc_free_i4d
      module subroutine i4_devxlib_malloc_free_i5d(dev_ptr, device_id)
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i4_devxlib_malloc_free_i5d
      module subroutine i4_devxlib_malloc_free_i6d(dev_ptr, device_id)
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i4_devxlib_malloc_free_i6d
      module subroutine i8_devxlib_malloc_free_i1d(dev_ptr, device_id)
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
         integer, optional, intent(in)  :: device_id
      end subroutine i8_devxlib_malloc_free_i1d
      module subroutine i8_devxlib_malloc_free_i2d(dev_ptr, device_id)
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i8_devxlib_malloc_free_i2d
      module subroutine i8_devxlib_malloc_free_i3d(dev_ptr, device_id)
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i8_devxlib_malloc_free_i3d
      module subroutine i8_devxlib_malloc_free_i4d(dev_ptr, device_id)
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i8_devxlib_malloc_free_i4d
      module subroutine i8_devxlib_malloc_free_i5d(dev_ptr, device_id)
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i8_devxlib_malloc_free_i5d
      module subroutine i8_devxlib_malloc_free_i6d(dev_ptr, device_id)
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine i8_devxlib_malloc_free_i6d
      module subroutine l4_devxlib_malloc_free_l1d(dev_ptr, device_id)
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
         integer, optional, intent(in)  :: device_id
      end subroutine l4_devxlib_malloc_free_l1d
      module subroutine l4_devxlib_malloc_free_l2d(dev_ptr, device_id)
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine l4_devxlib_malloc_free_l2d
      module subroutine l4_devxlib_malloc_free_l3d(dev_ptr, device_id)
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine l4_devxlib_malloc_free_l3d
      module subroutine l4_devxlib_malloc_free_l4d(dev_ptr, device_id)
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine l4_devxlib_malloc_free_l4d
      module subroutine l4_devxlib_malloc_free_l5d(dev_ptr, device_id)
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine l4_devxlib_malloc_free_l5d
      module subroutine l4_devxlib_malloc_free_l6d(dev_ptr, device_id)
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
         integer, optional, intent(in)  :: device_id
      end subroutine l4_devxlib_malloc_free_l6d
   endinterface

   interface
      module function sp_devxlib_malloc_allocated_r1d(array) result(res)
         implicit none
         logical :: res
         real(real32), allocatable, intent(in) DEV_ATTR :: array(:)
      end function sp_devxlib_malloc_allocated_r1d
      module function sp_devxlib_malloc_allocated_r2d(array) result(res)
         implicit none
         logical :: res
         real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:)
      end function sp_devxlib_malloc_allocated_r2d
      module function sp_devxlib_malloc_allocated_r3d(array) result(res)
         implicit none
         logical :: res
         real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
      end function sp_devxlib_malloc_allocated_r3d
      module function sp_devxlib_malloc_allocated_r4d(array) result(res)
         implicit none
         logical :: res
         real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
      end function sp_devxlib_malloc_allocated_r4d
      module function sp_devxlib_malloc_allocated_r5d(array) result(res)
         implicit none
         logical :: res
         real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_r5d
      module function sp_devxlib_malloc_allocated_r6d(array) result(res)
         implicit none
         logical :: res
         real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_r6d
      module function dp_devxlib_malloc_allocated_r1d(array) result(res)
         implicit none
         logical :: res
         real(real64), allocatable, intent(in) DEV_ATTR :: array(:)
      end function dp_devxlib_malloc_allocated_r1d
      module function dp_devxlib_malloc_allocated_r2d(array) result(res)
         implicit none
         logical :: res
         real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:)
      end function dp_devxlib_malloc_allocated_r2d
      module function dp_devxlib_malloc_allocated_r3d(array) result(res)
         implicit none
         logical :: res
         real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
      end function dp_devxlib_malloc_allocated_r3d
      module function dp_devxlib_malloc_allocated_r4d(array) result(res)
         implicit none
         logical :: res
         real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
      end function dp_devxlib_malloc_allocated_r4d
      module function dp_devxlib_malloc_allocated_r5d(array) result(res)
         implicit none
         logical :: res
         real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_r5d
      module function dp_devxlib_malloc_allocated_r6d(array) result(res)
         implicit none
         logical :: res
         real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_r6d
      module function sp_devxlib_malloc_allocated_c1d(array) result(res)
         implicit none
         logical :: res
         complex(real32), allocatable, intent(in) DEV_ATTR :: array(:)
      end function sp_devxlib_malloc_allocated_c1d
      module function sp_devxlib_malloc_allocated_c2d(array) result(res)
         implicit none
         logical :: res
         complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:)
      end function sp_devxlib_malloc_allocated_c2d
      module function sp_devxlib_malloc_allocated_c3d(array) result(res)
         implicit none
         logical :: res
         complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
      end function sp_devxlib_malloc_allocated_c3d
      module function sp_devxlib_malloc_allocated_c4d(array) result(res)
         implicit none
         logical :: res
         complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
      end function sp_devxlib_malloc_allocated_c4d
      module function sp_devxlib_malloc_allocated_c5d(array) result(res)
         implicit none
         logical :: res
         complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_c5d
      module function sp_devxlib_malloc_allocated_c6d(array) result(res)
         implicit none
         logical :: res
         complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_c6d
      module function dp_devxlib_malloc_allocated_c1d(array) result(res)
         implicit none
         logical :: res
         complex(real64), allocatable, intent(in) DEV_ATTR :: array(:)
      end function dp_devxlib_malloc_allocated_c1d
      module function dp_devxlib_malloc_allocated_c2d(array) result(res)
         implicit none
         logical :: res
         complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:)
      end function dp_devxlib_malloc_allocated_c2d
      module function dp_devxlib_malloc_allocated_c3d(array) result(res)
         implicit none
         logical :: res
         complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
      end function dp_devxlib_malloc_allocated_c3d
      module function dp_devxlib_malloc_allocated_c4d(array) result(res)
         implicit none
         logical :: res
         complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
      end function dp_devxlib_malloc_allocated_c4d
      module function dp_devxlib_malloc_allocated_c5d(array) result(res)
         implicit none
         logical :: res
         complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_c5d
      module function dp_devxlib_malloc_allocated_c6d(array) result(res)
         implicit none
         logical :: res
         complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_c6d
      module function i4_devxlib_malloc_allocated_i1d(array) result(res)
         implicit none
         logical :: res
         integer(int32), allocatable, intent(in) DEV_ATTR :: array(:)
      end function i4_devxlib_malloc_allocated_i1d
      module function i4_devxlib_malloc_allocated_i2d(array) result(res)
         implicit none
         logical :: res
         integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:)
      end function i4_devxlib_malloc_allocated_i2d
      module function i4_devxlib_malloc_allocated_i3d(array) result(res)
         implicit none
         logical :: res
         integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
      end function i4_devxlib_malloc_allocated_i3d
      module function i4_devxlib_malloc_allocated_i4d(array) result(res)
         implicit none
         logical :: res
         integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
      end function i4_devxlib_malloc_allocated_i4d
      module function i4_devxlib_malloc_allocated_i5d(array) result(res)
         implicit none
         logical :: res
         integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
      end function i4_devxlib_malloc_allocated_i5d
      module function i4_devxlib_malloc_allocated_i6d(array) result(res)
         implicit none
         logical :: res
         integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
      end function i4_devxlib_malloc_allocated_i6d
      module function i8_devxlib_malloc_allocated_i1d(array) result(res)
         implicit none
         logical :: res
         integer(int64), allocatable, intent(in) DEV_ATTR :: array(:)
      end function i8_devxlib_malloc_allocated_i1d
      module function i8_devxlib_malloc_allocated_i2d(array) result(res)
         implicit none
         logical :: res
         integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:)
      end function i8_devxlib_malloc_allocated_i2d
      module function i8_devxlib_malloc_allocated_i3d(array) result(res)
         implicit none
         logical :: res
         integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
      end function i8_devxlib_malloc_allocated_i3d
      module function i8_devxlib_malloc_allocated_i4d(array) result(res)
         implicit none
         logical :: res
         integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
      end function i8_devxlib_malloc_allocated_i4d
      module function i8_devxlib_malloc_allocated_i5d(array) result(res)
         implicit none
         logical :: res
         integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
      end function i8_devxlib_malloc_allocated_i5d
      module function i8_devxlib_malloc_allocated_i6d(array) result(res)
         implicit none
         logical :: res
         integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
      end function i8_devxlib_malloc_allocated_i6d
      module function l4_devxlib_malloc_allocated_l1d(array) result(res)
         implicit none
         logical :: res
         logical(int32), allocatable, intent(in) DEV_ATTR :: array(:)
      end function l4_devxlib_malloc_allocated_l1d
      module function l4_devxlib_malloc_allocated_l2d(array) result(res)
         implicit none
         logical :: res
         logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:)
      end function l4_devxlib_malloc_allocated_l2d
      module function l4_devxlib_malloc_allocated_l3d(array) result(res)
         implicit none
         logical :: res
         logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
      end function l4_devxlib_malloc_allocated_l3d
      module function l4_devxlib_malloc_allocated_l4d(array) result(res)
         implicit none
         logical :: res
         logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
      end function l4_devxlib_malloc_allocated_l4d
      module function l4_devxlib_malloc_allocated_l5d(array) result(res)
         implicit none
         logical :: res
         logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
      end function l4_devxlib_malloc_allocated_l5d
      module function l4_devxlib_malloc_allocated_l6d(array) result(res)
         implicit none
         logical :: res
         logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
      end function l4_devxlib_malloc_allocated_l6d
   endinterface

   interface
      module function sp_devxlib_malloc_allocated_r1d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
      end function sp_devxlib_malloc_allocated_r1d_p
      module function sp_devxlib_malloc_allocated_r2d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
      end function sp_devxlib_malloc_allocated_r2d_p
      module function sp_devxlib_malloc_allocated_r3d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
      end function sp_devxlib_malloc_allocated_r3d_p
      module function sp_devxlib_malloc_allocated_r4d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
      end function sp_devxlib_malloc_allocated_r4d_p
      module function sp_devxlib_malloc_allocated_r5d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_r5d_p
      module function sp_devxlib_malloc_allocated_r6d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_r6d_p
      module function dp_devxlib_malloc_allocated_r1d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
      end function dp_devxlib_malloc_allocated_r1d_p
      module function dp_devxlib_malloc_allocated_r2d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
      end function dp_devxlib_malloc_allocated_r2d_p
      module function dp_devxlib_malloc_allocated_r3d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
      end function dp_devxlib_malloc_allocated_r3d_p
      module function dp_devxlib_malloc_allocated_r4d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
      end function dp_devxlib_malloc_allocated_r4d_p
      module function dp_devxlib_malloc_allocated_r5d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_r5d_p
      module function dp_devxlib_malloc_allocated_r6d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_r6d_p
      module function sp_devxlib_malloc_allocated_c1d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
      end function sp_devxlib_malloc_allocated_c1d_p
      module function sp_devxlib_malloc_allocated_c2d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
      end function sp_devxlib_malloc_allocated_c2d_p
      module function sp_devxlib_malloc_allocated_c3d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
      end function sp_devxlib_malloc_allocated_c3d_p
      module function sp_devxlib_malloc_allocated_c4d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
      end function sp_devxlib_malloc_allocated_c4d_p
      module function sp_devxlib_malloc_allocated_c5d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_c5d_p
      module function sp_devxlib_malloc_allocated_c6d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
      end function sp_devxlib_malloc_allocated_c6d_p
      module function dp_devxlib_malloc_allocated_c1d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
      end function dp_devxlib_malloc_allocated_c1d_p
      module function dp_devxlib_malloc_allocated_c2d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
      end function dp_devxlib_malloc_allocated_c2d_p
      module function dp_devxlib_malloc_allocated_c3d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
      end function dp_devxlib_malloc_allocated_c3d_p
      module function dp_devxlib_malloc_allocated_c4d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
      end function dp_devxlib_malloc_allocated_c4d_p
      module function dp_devxlib_malloc_allocated_c5d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_c5d_p
      module function dp_devxlib_malloc_allocated_c6d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
      end function dp_devxlib_malloc_allocated_c6d_p
      module function i4_devxlib_malloc_allocated_i1d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
      end function i4_devxlib_malloc_allocated_i1d_p
      module function i4_devxlib_malloc_allocated_i2d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
      end function i4_devxlib_malloc_allocated_i2d_p
      module function i4_devxlib_malloc_allocated_i3d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
      end function i4_devxlib_malloc_allocated_i3d_p
      module function i4_devxlib_malloc_allocated_i4d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
      end function i4_devxlib_malloc_allocated_i4d_p
      module function i4_devxlib_malloc_allocated_i5d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
      end function i4_devxlib_malloc_allocated_i5d_p
      module function i4_devxlib_malloc_allocated_i6d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
      end function i4_devxlib_malloc_allocated_i6d_p
      module function i8_devxlib_malloc_allocated_i1d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
      end function i8_devxlib_malloc_allocated_i1d_p
      module function i8_devxlib_malloc_allocated_i2d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
      end function i8_devxlib_malloc_allocated_i2d_p
      module function i8_devxlib_malloc_allocated_i3d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
      end function i8_devxlib_malloc_allocated_i3d_p
      module function i8_devxlib_malloc_allocated_i4d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
      end function i8_devxlib_malloc_allocated_i4d_p
      module function i8_devxlib_malloc_allocated_i5d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
      end function i8_devxlib_malloc_allocated_i5d_p
      module function i8_devxlib_malloc_allocated_i6d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
      end function i8_devxlib_malloc_allocated_i6d_p
      module function l4_devxlib_malloc_allocated_l1d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
      end function l4_devxlib_malloc_allocated_l1d_p
      module function l4_devxlib_malloc_allocated_l2d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
      end function l4_devxlib_malloc_allocated_l2d_p
      module function l4_devxlib_malloc_allocated_l3d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
      end function l4_devxlib_malloc_allocated_l3d_p
      module function l4_devxlib_malloc_allocated_l4d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
      end function l4_devxlib_malloc_allocated_l4d_p
      module function l4_devxlib_malloc_allocated_l5d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
      end function l4_devxlib_malloc_allocated_l5d_p
      module function l4_devxlib_malloc_allocated_l6d_p(dev_ptr) result(res)
         implicit none
         logical :: res
         logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
      end function l4_devxlib_malloc_allocated_l6d_p
   endinterface
endmodule devxlib_malloc