!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! cublas defs and interfaces
!
#include<devxlib_defs.h>
#include<devxlib_macros.h>
!
module devxlib_cublas
  !
#if defined __DXL_CUDAF
  use cublas
#endif
#if defined __DXL_CUBLAS && !defined __DXL_CUDAF
  use cublas_core_m
  use cublas_m
#endif
  !
  use iso_c_binding
  implicit none
  !
#if defined __DXL_CUBLAS
  type(cublasHandle) :: cublas_handle
#else
  type(c_ptr)        :: cublas_handle    ! just to have the symbol defined
#endif
  !
end module devxlib_cublas

