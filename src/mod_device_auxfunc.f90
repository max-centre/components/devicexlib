!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Auxiliary functions with device support via CUDAF, OpenACC, OpenMP-GPU
!
module device_auxfunc_m
  implicit none

#include<device_auxfunc_interf.f90>

end module device_auxfunc_m

