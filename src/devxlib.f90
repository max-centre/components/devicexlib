!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to perform memcpy and memset on the device with CUDA Fortran
! cuf_memXXX contains a CUF KERNEL to perform the selected operation
! cu_memsync are wrappers for cuda_memcpy functions
!
module devXlib

  use devxlib_malloc
  use devxlib_mapping
  use devxlib_memcpy
  use devxlib_memset
  use devxlib_auxfunc
  use devxlib_buffers
  use devxlib_linalg
  use devxlib_async

  implicit none

end module devXlib
