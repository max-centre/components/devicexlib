!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to perform memcpy, memset, malloc, free on accelerator
! devices using CUDAF, OpenACC, OpenMP-GPU
!
module device_memcpy_m
  implicit none

#include<device_memcpy_interf.f90>
#include<device_malloc_interf.f90>
#include<device_mapping_interf.f90>

end module device_memcpy_m

