!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to check host memory mapping state
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_mapping_mapped.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_mapping) devxlib_mapping_mapped

   implicit none
     !
     ! _devxlib_mapping_mapped_  functions will return allocated(array)
     !                           when no GPU support is present
     !
   contains
      module function sp_devxlib_mapping_mapped_r1d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:)
#else
         real(real32), allocatable DEV_ATTR :: array(:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_r1d
      !
      module function sp_devxlib_mapping_mapped_r2d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_r2d
      !
      module function sp_devxlib_mapping_mapped_r3d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_r3d
      !
      module function sp_devxlib_mapping_mapped_r4d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_r4d
      !
      module function sp_devxlib_mapping_mapped_r5d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_r5d
      !
      module function sp_devxlib_mapping_mapped_r6d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real32), allocatable, target :: array(:,:,:,:,:,:)
#else
         real(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_r6d
      !
      module function dp_devxlib_mapping_mapped_r1d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:)
#else
         real(real64), allocatable DEV_ATTR :: array(:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_r1d
      !
      module function dp_devxlib_mapping_mapped_r2d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_r2d
      !
      module function dp_devxlib_mapping_mapped_r3d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_r3d
      !
      module function dp_devxlib_mapping_mapped_r4d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_r4d
      !
      module function dp_devxlib_mapping_mapped_r5d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_r5d
      !
      module function dp_devxlib_mapping_mapped_r6d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         real(real64), allocatable, target :: array(:,:,:,:,:,:)
#else
         real(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_r6d
      !
      module function sp_devxlib_mapping_mapped_c1d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_c1d
      !
      module function sp_devxlib_mapping_mapped_c2d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_c2d
      !
      module function sp_devxlib_mapping_mapped_c3d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_c3d
      !
      module function sp_devxlib_mapping_mapped_c4d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_c4d
      !
      module function sp_devxlib_mapping_mapped_c5d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_c5d
      !
      module function sp_devxlib_mapping_mapped_c6d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real32), allocatable, target :: array(:,:,:,:,:,:)
#else
         complex(real32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function sp_devxlib_mapping_mapped_c6d
      !
      module function dp_devxlib_mapping_mapped_c1d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_c1d
      !
      module function dp_devxlib_mapping_mapped_c2d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_c2d
      !
      module function dp_devxlib_mapping_mapped_c3d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_c3d
      !
      module function dp_devxlib_mapping_mapped_c4d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_c4d
      !
      module function dp_devxlib_mapping_mapped_c5d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_c5d
      !
      module function dp_devxlib_mapping_mapped_c6d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         complex(real64), allocatable, target :: array(:,:,:,:,:,:)
#else
         complex(real64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function dp_devxlib_mapping_mapped_c6d
      !
      module function i4_devxlib_mapping_mapped_i1d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i4_devxlib_mapping_mapped_i1d
      !
      module function i4_devxlib_mapping_mapped_i2d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i4_devxlib_mapping_mapped_i2d
      !
      module function i4_devxlib_mapping_mapped_i3d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i4_devxlib_mapping_mapped_i3d
      !
      module function i4_devxlib_mapping_mapped_i4d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i4_devxlib_mapping_mapped_i4d
      !
      module function i4_devxlib_mapping_mapped_i5d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i4_devxlib_mapping_mapped_i5d
      !
      module function i4_devxlib_mapping_mapped_i6d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int32), allocatable, target :: array(:,:,:,:,:,:)
#else
         integer(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i4_devxlib_mapping_mapped_i6d
      !
      module function i8_devxlib_mapping_mapped_i1d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i8_devxlib_mapping_mapped_i1d
      !
      module function i8_devxlib_mapping_mapped_i2d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i8_devxlib_mapping_mapped_i2d
      !
      module function i8_devxlib_mapping_mapped_i3d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i8_devxlib_mapping_mapped_i3d
      !
      module function i8_devxlib_mapping_mapped_i4d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i8_devxlib_mapping_mapped_i4d
      !
      module function i8_devxlib_mapping_mapped_i5d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i8_devxlib_mapping_mapped_i5d
      !
      module function i8_devxlib_mapping_mapped_i6d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         integer(int64), allocatable, target :: array(:,:,:,:,:,:)
#else
         integer(int64), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function i8_devxlib_mapping_mapped_i6d
      !
      module function l4_devxlib_mapping_mapped_l1d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function l4_devxlib_mapping_mapped_l1d
      !
      module function l4_devxlib_mapping_mapped_l2d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function l4_devxlib_mapping_mapped_l2d
      !
      module function l4_devxlib_mapping_mapped_l3d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function l4_devxlib_mapping_mapped_l3d
      !
      module function l4_devxlib_mapping_mapped_l4d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function l4_devxlib_mapping_mapped_l4d
      !
      module function l4_devxlib_mapping_mapped_l5d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function l4_devxlib_mapping_mapped_l5d
      !
      module function l4_devxlib_mapping_mapped_l6d(array) result(res)
         implicit none
         !
         logical :: res
#if defined __DXL_OPENMP_GPU
         logical(int32), allocatable, target :: array(:,:,:,:,:,:)
#else
         logical(int32), allocatable DEV_ATTR :: array(:,:,:,:,:,:)
#endif
         !
         res = .false.
         !
#if defined __DXL_CUDAF
         res=allocated(array)
#elif defined __DXL_OPENACC
         res=allocated(array)
         if (res) res=acc_is_present(array)
#elif defined __DXL_OPENMP_GPU
         res=allocated(array)
         if (res) res=(omp_target_is_present(c_loc(array), omp_get_default_device())/=0)
#else
         res=allocated(array)
#endif
         return
      end function l4_devxlib_mapping_mapped_l6d
      !

endsubmodule devxlib_mapping_mapped