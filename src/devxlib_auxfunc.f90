!                                                                                                                                                         c
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Auxiliary functions
!
! AF: OpenACC support is incomplete !!
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_auxfunc.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
module devxlib_auxfunc
   !
   use iso_fortran_env,     only : real32, real64
   use devxlib_async,       only : dxl_async_kind
   use devxlib_environment, only : devxlib_error
   !
#if defined __DXL_CUDAF
   use cudafor
#endif

   implicit none

   private :: dxl_async_kind

   interface devxlib_conjg_d
      module procedure &

         dp_devxlib_conjg_c1d, dp_devxlib_conjg_c2d, dp_devxlib_conjg_c3d, &
         dp_devxlib_conjg_c4d, dp_devxlib_conjg_c5d, dp_devxlib_conjg_c6d, &
         sp_devxlib_conjg_c1d, sp_devxlib_conjg_c2d, sp_devxlib_conjg_c3d, &
         sp_devxlib_conjg_c4d, sp_devxlib_conjg_c5d, sp_devxlib_conjg_c6d
   endinterface devxlib_conjg_d


   interface devxlib_conjg_h
      module procedure &

         dp_devxlib_conjg_h_c1d, dp_devxlib_conjg_h_c2d, dp_devxlib_conjg_h_c3d, &
         dp_devxlib_conjg_h_c4d, dp_devxlib_conjg_h_c5d, dp_devxlib_conjg_h_c6d, &
         sp_devxlib_conjg_h_c1d, sp_devxlib_conjg_h_c2d, sp_devxlib_conjg_h_c3d, &
         sp_devxlib_conjg_h_c4d, sp_devxlib_conjg_h_c5d, sp_devxlib_conjg_h_c6d
   endinterface devxlib_conjg_h


   interface devxlib_conjg_d_p
      module procedure &

         dp_devxlib_conjg_c1d_p, dp_devxlib_conjg_c2d_p, dp_devxlib_conjg_c3d_p, &
         dp_devxlib_conjg_c4d_p, dp_devxlib_conjg_c5d_p, dp_devxlib_conjg_c6d_p, &
         sp_devxlib_conjg_c1d_p, sp_devxlib_conjg_c2d_p, sp_devxlib_conjg_c3d_p, &
         sp_devxlib_conjg_c4d_p, sp_devxlib_conjg_c5d_p, sp_devxlib_conjg_c6d_p
   endinterface devxlib_conjg_d_p

   interface devxlib_vec_upd_remap
      module procedure &

         dp_devxlib_vec_upd_remap_r1d, &
         dp_devxlib_vec_upd_remap_c1d, &
         sp_devxlib_vec_upd_remap_r1d, &
         sp_devxlib_vec_upd_remap_c1d
   endinterface devxlib_vec_upd_remap

   interface devxlib_vec_upd_v_remap_v
      module procedure &

         dp_devxlib_vec_upd_v_remap_v_r1d, &
         dp_devxlib_vec_upd_v_remap_v_c1d, &
         sp_devxlib_vec_upd_v_remap_v_r1d, &
         sp_devxlib_vec_upd_v_remap_v_c1d, &


         dp_devxlib_vec_upd_v_remap_v_x_c1d, &
         sp_devxlib_vec_upd_v_remap_v_x_c1d
   endinterface devxlib_vec_upd_v_remap_v

   interface devxlib_mat_upd_dMd
      module procedure &

         dp_devxlib_mat_upd_dMd_r2d, &
         dp_devxlib_mat_upd_dMd_c2d, &
         sp_devxlib_mat_upd_dMd_r2d, &
         sp_devxlib_mat_upd_dMd_c2d
   endinterface devxlib_mat_upd_dMd

   interface devxlib_mem_addscal
      module procedure &
         dp_devxlib_mem_addscal_r1d, dp_devxlib_mem_addscal_r2d, dp_devxlib_mem_addscal_r3d, &
         dp_devxlib_mem_addscal_r4d, dp_devxlib_mem_addscal_r5d, dp_devxlib_mem_addscal_r6d, &
         sp_devxlib_mem_addscal_r1d, sp_devxlib_mem_addscal_r2d, sp_devxlib_mem_addscal_r3d, &
         sp_devxlib_mem_addscal_r4d, sp_devxlib_mem_addscal_r5d, sp_devxlib_mem_addscal_r6d, &
         dp_devxlib_mem_addscal_c1d, dp_devxlib_mem_addscal_c2d, dp_devxlib_mem_addscal_c3d, &
         dp_devxlib_mem_addscal_c4d, dp_devxlib_mem_addscal_c5d, dp_devxlib_mem_addscal_c6d, &
         sp_devxlib_mem_addscal_c1d, sp_devxlib_mem_addscal_c2d, sp_devxlib_mem_addscal_c3d, &
         sp_devxlib_mem_addscal_c4d, sp_devxlib_mem_addscal_c5d, sp_devxlib_mem_addscal_c6d
   endinterface devxlib_mem_addscal

   interface

      module subroutine dp_devxlib_conjg_c1d(array_inout, async_id, &
                                        
                                        range1, lbound1 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_conjg_c1d
      !
      module subroutine dp_devxlib_conjg_c2d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_conjg_c2d
      !
      module subroutine dp_devxlib_conjg_c3d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_conjg_c3d
      !
      module subroutine dp_devxlib_conjg_c4d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_conjg_c4d
      !
      module subroutine dp_devxlib_conjg_c5d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_conjg_c5d
      !
      module subroutine dp_devxlib_conjg_c6d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5, &
                                        range6, lbound6 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_conjg_c6d
      !
      module subroutine sp_devxlib_conjg_c1d(array_inout, async_id, &
                                        
                                        range1, lbound1 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_conjg_c1d
      !
      module subroutine sp_devxlib_conjg_c2d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_conjg_c2d
      !
      module subroutine sp_devxlib_conjg_c3d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_conjg_c3d
      !
      module subroutine sp_devxlib_conjg_c4d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_conjg_c4d
      !
      module subroutine sp_devxlib_conjg_c5d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_conjg_c5d
      !
      module subroutine sp_devxlib_conjg_c6d(array_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5, &
                                        range6, lbound6 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_conjg_c6d
      !
   endinterface

   interface

      module subroutine dp_devxlib_conjg_h_c1d(array_inout, async_id, &
                                     
                                     range1, lbound1 )
         implicit none
         complex(real64), intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_conjg_h_c1d
      !
      module subroutine dp_devxlib_conjg_h_c2d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2 )
         implicit none
         complex(real64), intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_conjg_h_c2d
      !
      module subroutine dp_devxlib_conjg_h_c3d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3 )
         implicit none
         complex(real64), intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_conjg_h_c3d
      !
      module subroutine dp_devxlib_conjg_h_c4d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3, &
                                     range4, lbound4 )
         implicit none
         complex(real64), intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_conjg_h_c4d
      !
      module subroutine dp_devxlib_conjg_h_c5d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3, &
                                     range4, lbound4, &
                                     range5, lbound5 )
         implicit none
         complex(real64), intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_conjg_h_c5d
      !
      module subroutine dp_devxlib_conjg_h_c6d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3, &
                                     range4, lbound4, &
                                     range5, lbound5, &
                                     range6, lbound6 )
         implicit none
         complex(real64), intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_conjg_h_c6d
      !
      module subroutine sp_devxlib_conjg_h_c1d(array_inout, async_id, &
                                     
                                     range1, lbound1 )
         implicit none
         complex(real32), intent(inout) :: array_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_conjg_h_c1d
      !
      module subroutine sp_devxlib_conjg_h_c2d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2 )
         implicit none
         complex(real32), intent(inout) :: array_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_conjg_h_c2d
      !
      module subroutine sp_devxlib_conjg_h_c3d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3 )
         implicit none
         complex(real32), intent(inout) :: array_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_conjg_h_c3d
      !
      module subroutine sp_devxlib_conjg_h_c4d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3, &
                                     range4, lbound4 )
         implicit none
         complex(real32), intent(inout) :: array_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_conjg_h_c4d
      !
      module subroutine sp_devxlib_conjg_h_c5d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3, &
                                     range4, lbound4, &
                                     range5, lbound5 )
         implicit none
         complex(real32), intent(inout) :: array_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_conjg_h_c5d
      !
      module subroutine sp_devxlib_conjg_h_c6d(array_inout, async_id, &
                                     
                                     range1, lbound1, &
                                     range2, lbound2, &
                                     range3, lbound3, &
                                     range4, lbound4, &
                                     range5, lbound5, &
                                     range6, lbound6 )
         implicit none
         complex(real32), intent(inout) :: array_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_conjg_h_c6d
      !
   endinterface

   interface

      module subroutine dp_devxlib_conjg_c1d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1 )
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_conjg_c1d_p
      !
      module subroutine dp_devxlib_conjg_c2d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2 )
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_conjg_c2d_p
      !
      module subroutine dp_devxlib_conjg_c3d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3 )
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_conjg_c3d_p
      !
      module subroutine dp_devxlib_conjg_c4d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4 )
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_conjg_c4d_p
      !
      module subroutine dp_devxlib_conjg_c5d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5 )
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_conjg_c5d_p
      !
      module subroutine dp_devxlib_conjg_c6d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5, &
                                        range6, lbound6 )
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_conjg_c6d_p
      !
      module subroutine sp_devxlib_conjg_c1d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1 )
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_conjg_c1d_p
      !
      module subroutine sp_devxlib_conjg_c2d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2 )
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_conjg_c2d_p
      !
      module subroutine sp_devxlib_conjg_c3d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3 )
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_conjg_c3d_p
      !
      module subroutine sp_devxlib_conjg_c4d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4 )
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_conjg_c4d_p
      !
      module subroutine sp_devxlib_conjg_c5d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5 )
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_conjg_c5d_p
      !
      module subroutine sp_devxlib_conjg_c6d_p(ptr_inout, async_id, &
                                        
                                        range1, lbound1, &
                                        range2, lbound2, &
                                        range3, lbound3, &
                                        range4, lbound4, &
                                        range5, lbound5, &
                                        range6, lbound6 )
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: ptr_inout(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), optional, intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_conjg_c6d_p
      !
   endinterface

   interface

       module subroutine dp_devxlib_vec_upd_remap_r1d(ndim, vout, v1, map1, scal)
          implicit none
          integer,      intent(in)    :: ndim
          real(real64) DEV_ATTR, intent(inout) :: vout(:) 
          real(real64) DEV_ATTR, intent(in)    :: v1(:) 
          integer DEV_ATTR,      intent(in)    :: map1(:) 
          real(real64), optional, intent(in)   :: scal
       end subroutine dp_devxlib_vec_upd_remap_r1d
       !
       module subroutine dp_devxlib_vec_upd_remap_c1d(ndim, vout, v1, map1, scal)
          implicit none
          integer,      intent(in)    :: ndim
          complex(real64) DEV_ATTR, intent(inout) :: vout(:) 
          complex(real64) DEV_ATTR, intent(in)    :: v1(:) 
          integer DEV_ATTR,      intent(in)    :: map1(:) 
          complex(real64), optional, intent(in)   :: scal
       end subroutine dp_devxlib_vec_upd_remap_c1d
       !
       module subroutine sp_devxlib_vec_upd_remap_r1d(ndim, vout, v1, map1, scal)
          implicit none
          integer,      intent(in)    :: ndim
          real(real32) DEV_ATTR, intent(inout) :: vout(:) 
          real(real32) DEV_ATTR, intent(in)    :: v1(:) 
          integer DEV_ATTR,      intent(in)    :: map1(:) 
          real(real32), optional, intent(in)   :: scal
       end subroutine sp_devxlib_vec_upd_remap_r1d
       !
       module subroutine sp_devxlib_vec_upd_remap_c1d(ndim, vout, v1, map1, scal)
          implicit none
          integer,      intent(in)    :: ndim
          complex(real32) DEV_ATTR, intent(inout) :: vout(:) 
          complex(real32) DEV_ATTR, intent(in)    :: v1(:) 
          integer DEV_ATTR,      intent(in)    :: map1(:) 
          complex(real32), optional, intent(in)   :: scal
       end subroutine sp_devxlib_vec_upd_remap_c1d
       !
   end interface

   interface

      module subroutine dp_devxlib_vec_upd_v_remap_v_r1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         integer,      intent(in)    :: ndim
         real(real64) DEV_ATTR, intent(inout) :: vout(:)
         real(real64) DEV_ATTR, intent(in)    :: v1(:)
         integer DEV_ATTR,      intent(in)    :: map1(:)
         real(real64) DEV_ATTR, intent(in)    :: v2(:)
         real(real64), optional, intent(in)   :: scal
      end subroutine dp_devxlib_vec_upd_v_remap_v_r1d
      !
      module subroutine dp_devxlib_vec_upd_v_remap_v_c1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         integer,      intent(in)    :: ndim
         complex(real64) DEV_ATTR, intent(inout) :: vout(:)
         complex(real64) DEV_ATTR, intent(in)    :: v1(:)
         integer DEV_ATTR,      intent(in)    :: map1(:)
         complex(real64) DEV_ATTR, intent(in)    :: v2(:)
         complex(real64), optional, intent(in)   :: scal
      end subroutine dp_devxlib_vec_upd_v_remap_v_c1d
      !
      module subroutine sp_devxlib_vec_upd_v_remap_v_r1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         integer,      intent(in)    :: ndim
         real(real32) DEV_ATTR, intent(inout) :: vout(:)
         real(real32) DEV_ATTR, intent(in)    :: v1(:)
         integer DEV_ATTR,      intent(in)    :: map1(:)
         real(real32) DEV_ATTR, intent(in)    :: v2(:)
         real(real32), optional, intent(in)   :: scal
      end subroutine sp_devxlib_vec_upd_v_remap_v_r1d
      !
      module subroutine sp_devxlib_vec_upd_v_remap_v_c1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         integer,      intent(in)    :: ndim
         complex(real32) DEV_ATTR, intent(inout) :: vout(:)
         complex(real32) DEV_ATTR, intent(in)    :: v1(:)
         integer DEV_ATTR,      intent(in)    :: map1(:)
         complex(real32) DEV_ATTR, intent(in)    :: v2(:)
         complex(real32), optional, intent(in)   :: scal
      end subroutine sp_devxlib_vec_upd_v_remap_v_c1d
      !



      module subroutine dp_devxlib_vec_upd_v_remap_v_x_c1d(ndim, vout, v1,op1, map1, v2,op2, scal)
         implicit none
         integer,      intent(in)    :: ndim
         complex(real64), intent(inout) :: vout(:)
         complex(real64), intent(in)    :: v1(:)
         integer,      intent(in)    :: map1(:)
         complex(real64), intent(in)    :: v2(:)
         character(1), intent(in)    :: op1, op2
         complex(real64), optional, intent(in)    :: scal
#if defined(__DXL_CUDAF)
         attributes(device) :: vout, v1, v2, map1
#endif
      end subroutine dp_devxlib_vec_upd_v_remap_v_x_c1d
      !
      module subroutine sp_devxlib_vec_upd_v_remap_v_x_c1d(ndim, vout, v1,op1, map1, v2,op2, scal)
         implicit none
         integer,      intent(in)    :: ndim
         complex(real32), intent(inout) :: vout(:)
         complex(real32), intent(in)    :: v1(:)
         integer,      intent(in)    :: map1(:)
         complex(real32), intent(in)    :: v2(:)
         character(1), intent(in)    :: op1, op2
         complex(real32), optional, intent(in)    :: scal
#if defined(__DXL_CUDAF)
         attributes(device) :: vout, v1, v2, map1
#endif
      end subroutine sp_devxlib_vec_upd_v_remap_v_x_c1d
      !
   end interface

   interface

      module subroutine dp_devxlib_mat_upd_dMd_r2d(ndim1, ndim2, mat, v1,op1, v2,op2, scal)
         !   
         ! performs: mat(i,j) = scal * op1(v1(i)) * mat(i,j) * op2(v2(j))
         ! op = 'N', 'R', 'C',       'RC'
         !       x   1/x  conjg(x)   conjg(1/x)
         implicit none
         integer,      intent(in)    :: ndim1,ndim2
         real(real64), intent(inout) DEV_ATTR :: mat(:,:)
         real(real64), intent(in)    DEV_ATTR :: v1(:)
         real(real64), intent(in)    DEV_ATTR :: v2(:)
         character(1), intent(in)    :: op1, op2 
         real(real64), optional, intent(in)  :: scal
      end subroutine dp_devxlib_mat_upd_dMd_r2d
      !
      module subroutine dp_devxlib_mat_upd_dMd_c2d(ndim1, ndim2, mat, v1,op1, v2,op2, scal)
         !   
         ! performs: mat(i,j) = scal * op1(v1(i)) * mat(i,j) * op2(v2(j))
         ! op = 'N', 'R', 'C',       'RC'
         !       x   1/x  conjg(x)   conjg(1/x)
         implicit none
         integer,      intent(in)    :: ndim1,ndim2
         complex(real64), intent(inout) DEV_ATTR :: mat(:,:)
         complex(real64), intent(in)    DEV_ATTR :: v1(:)
         complex(real64), intent(in)    DEV_ATTR :: v2(:)
         character(1), intent(in)    :: op1, op2 
         complex(real64), optional, intent(in)  :: scal
      end subroutine dp_devxlib_mat_upd_dMd_c2d
      !
      module subroutine sp_devxlib_mat_upd_dMd_r2d(ndim1, ndim2, mat, v1,op1, v2,op2, scal)
         !   
         ! performs: mat(i,j) = scal * op1(v1(i)) * mat(i,j) * op2(v2(j))
         ! op = 'N', 'R', 'C',       'RC'
         !       x   1/x  conjg(x)   conjg(1/x)
         implicit none
         integer,      intent(in)    :: ndim1,ndim2
         real(real32), intent(inout) DEV_ATTR :: mat(:,:)
         real(real32), intent(in)    DEV_ATTR :: v1(:)
         real(real32), intent(in)    DEV_ATTR :: v2(:)
         character(1), intent(in)    :: op1, op2 
         real(real32), optional, intent(in)  :: scal
      end subroutine sp_devxlib_mat_upd_dMd_r2d
      !
      module subroutine sp_devxlib_mat_upd_dMd_c2d(ndim1, ndim2, mat, v1,op1, v2,op2, scal)
         !   
         ! performs: mat(i,j) = scal * op1(v1(i)) * mat(i,j) * op2(v2(j))
         ! op = 'N', 'R', 'C',       'RC'
         !       x   1/x  conjg(x)   conjg(1/x)
         implicit none
         integer,      intent(in)    :: ndim1,ndim2
         complex(real32), intent(inout) DEV_ATTR :: mat(:,:)
         complex(real32), intent(in)    DEV_ATTR :: v1(:)
         complex(real32), intent(in)    DEV_ATTR :: v2(:)
         character(1), intent(in)    :: op1, op2 
         complex(real32), optional, intent(in)  :: scal
      end subroutine sp_devxlib_mat_upd_dMd_c2d
      !
   end interface

   interface
      module subroutine dp_devxlib_mem_addscal_r1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
          implicit none
          real(real64), intent(inout) :: array_out(:)
          real(real64), intent(in)    :: array_in(:)
          real(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_r1d
      !
      module subroutine dp_devxlib_mem_addscal_r2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          real(real64), intent(inout) :: array_out(:,:)
          real(real64), intent(in)    :: array_in(:,:)
          real(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_r2d
      !
      module subroutine dp_devxlib_mem_addscal_r3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          real(real64), intent(inout) :: array_out(:,:,:)
          real(real64), intent(in)    :: array_in(:,:,:)
          real(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_r3d
      !
      module subroutine dp_devxlib_mem_addscal_r4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          real(real64), intent(inout) :: array_out(:,:,:,:)
          real(real64), intent(in)    :: array_in(:,:,:,:)
          real(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_r4d
      !
      module subroutine dp_devxlib_mem_addscal_r5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          real(real64), intent(inout) :: array_out(:,:,:,:,:)
          real(real64), intent(in)    :: array_in(:,:,:,:,:)
          real(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_r5d
      !
      module subroutine dp_devxlib_mem_addscal_r6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          real(real64), intent(inout) :: array_out(:,:,:,:,:,:)
          real(real64), intent(in)    :: array_in(:,:,:,:,:,:)
          real(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_r6d
      !
      module subroutine sp_devxlib_mem_addscal_r1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
          implicit none
          real(real32), intent(inout) :: array_out(:)
          real(real32), intent(in)    :: array_in(:)
          real(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_r1d
      !
      module subroutine sp_devxlib_mem_addscal_r2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          real(real32), intent(inout) :: array_out(:,:)
          real(real32), intent(in)    :: array_in(:,:)
          real(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_r2d
      !
      module subroutine sp_devxlib_mem_addscal_r3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          real(real32), intent(inout) :: array_out(:,:,:)
          real(real32), intent(in)    :: array_in(:,:,:)
          real(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_r3d
      !
      module subroutine sp_devxlib_mem_addscal_r4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          real(real32), intent(inout) :: array_out(:,:,:,:)
          real(real32), intent(in)    :: array_in(:,:,:,:)
          real(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_r4d
      !
      module subroutine sp_devxlib_mem_addscal_r5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          real(real32), intent(inout) :: array_out(:,:,:,:,:)
          real(real32), intent(in)    :: array_in(:,:,:,:,:)
          real(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_r5d
      !
      module subroutine sp_devxlib_mem_addscal_r6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          real(real32), intent(inout) :: array_out(:,:,:,:,:,:)
          real(real32), intent(in)    :: array_in(:,:,:,:,:,:)
          real(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_r6d
      !
      module subroutine dp_devxlib_mem_addscal_c1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
          implicit none
          complex(real64), intent(inout) :: array_out(:)
          complex(real64), intent(in)    :: array_in(:)
          complex(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_c1d
      !
      module subroutine dp_devxlib_mem_addscal_c2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          complex(real64), intent(inout) :: array_out(:,:)
          complex(real64), intent(in)    :: array_in(:,:)
          complex(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_c2d
      !
      module subroutine dp_devxlib_mem_addscal_c3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          complex(real64), intent(inout) :: array_out(:,:,:)
          complex(real64), intent(in)    :: array_in(:,:,:)
          complex(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_c3d
      !
      module subroutine dp_devxlib_mem_addscal_c4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          complex(real64), intent(inout) :: array_out(:,:,:,:)
          complex(real64), intent(in)    :: array_in(:,:,:,:)
          complex(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_c4d
      !
      module subroutine dp_devxlib_mem_addscal_c5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          complex(real64), intent(inout) :: array_out(:,:,:,:,:)
          complex(real64), intent(in)    :: array_in(:,:,:,:,:)
          complex(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_c5d
      !
      module subroutine dp_devxlib_mem_addscal_c6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          complex(real64), intent(inout) :: array_out(:,:,:,:,:,:)
          complex(real64), intent(in)    :: array_in(:,:,:,:,:,:)
          complex(real64), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine dp_devxlib_mem_addscal_c6d
      !
      module subroutine sp_devxlib_mem_addscal_c1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
          implicit none
          complex(real32), intent(inout) :: array_out(:)
          complex(real32), intent(in)    :: array_in(:)
          complex(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2)
          integer, optional, intent(in) ::  lbound1
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_c1d
      !
      module subroutine sp_devxlib_mem_addscal_c2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
          implicit none
          complex(real32), intent(inout) :: array_out(:,:)
          complex(real32), intent(in)    :: array_in(:,:)
          complex(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2)
          integer, optional, intent(in) ::  lbound1, lbound2
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_c2d
      !
      module subroutine sp_devxlib_mem_addscal_c3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
          implicit none
          complex(real32), intent(inout) :: array_out(:,:,:)
          complex(real32), intent(in)    :: array_in(:,:,:)
          complex(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_c3d
      !
      module subroutine sp_devxlib_mem_addscal_c4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
          implicit none
          complex(real32), intent(inout) :: array_out(:,:,:,:)
          complex(real32), intent(in)    :: array_in(:,:,:,:)
          complex(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_c4d
      !
      module subroutine sp_devxlib_mem_addscal_c5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
          implicit none
          complex(real32), intent(inout) :: array_out(:,:,:,:,:)
          complex(real32), intent(in)    :: array_in(:,:,:,:,:)
          complex(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_c5d
      !
      module subroutine sp_devxlib_mem_addscal_c6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
          implicit none
          complex(real32), intent(inout) :: array_out(:,:,:,:,:,:)
          complex(real32), intent(in)    :: array_in(:,:,:,:,:,:)
          complex(real32), optional, intent(in) :: scal
          integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
          integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
#if defined(__DXL_CUDAF)
          attributes(device) :: array_out, array_in
#endif
      end subroutine sp_devxlib_mem_addscal_c6d
      !
   end interface

endmodule devxlib_auxfunc