!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! cublas defs and interfaces
!
#include<devxlib_defs.h>
#include<devxlib_macros.h>
!
module devxlib_rocblas
  !
#if defined __DXL_ROCBLAS
  use rocblas_m
#endif
  !
  use iso_c_binding
  implicit none
  !
  type(c_ptr) :: handle
  !
end module devxlib_rocblas

