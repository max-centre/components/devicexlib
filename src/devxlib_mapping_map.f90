
!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to perform device memory mapping from host memory
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_mapping.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_mapping) devxlib_mapping_map

   implicit none

   contains
      module subroutine sp_devxlib_mapping_map_r1d(array, range1 )
         implicit none
         !
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
         !
         integer :: i1, d1s, d1e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r1d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_r1d
      !
      module subroutine sp_devxlib_mapping_map_r2d(array, range1, &
                                            range2 )
         implicit none
         !
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r2d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r2d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_r2d
      !
      module subroutine sp_devxlib_mapping_map_r3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         !
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r3d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r3d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r3d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_r3d
      !
      module subroutine sp_devxlib_mapping_map_r4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         !
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r4d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r4d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r4d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r4d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_r4d
      !
      module subroutine sp_devxlib_mapping_map_r5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         !
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r5d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r5d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r5d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r5d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r5d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_r5d
      !
      module subroutine sp_devxlib_mapping_map_r6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         !
         real(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         integer :: i6, d6s, d6e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r6d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r6d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r6d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r6d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r6d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         if (present(range6)) then
            d6s = range6(1)
            d6e = range6(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_r6d","range not present",6)
!#endif
            d6s = lbound(array,6)
            d6e = ubound(array,6)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_r6d
      !
      module subroutine dp_devxlib_mapping_map_r1d(array, range1 )
         implicit none
         !
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
         !
         integer :: i1, d1s, d1e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r1d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_r1d
      !
      module subroutine dp_devxlib_mapping_map_r2d(array, range1, &
                                            range2 )
         implicit none
         !
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r2d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r2d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_r2d
      !
      module subroutine dp_devxlib_mapping_map_r3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         !
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r3d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r3d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r3d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_r3d
      !
      module subroutine dp_devxlib_mapping_map_r4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         !
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r4d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r4d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r4d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r4d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_r4d
      !
      module subroutine dp_devxlib_mapping_map_r5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         !
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r5d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r5d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r5d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r5d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r5d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_r5d
      !
      module subroutine dp_devxlib_mapping_map_r6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         !
         real(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         integer :: i6, d6s, d6e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r6d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r6d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r6d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r6d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r6d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         if (present(range6)) then
            d6s = range6(1)
            d6e = range6(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_r6d","range not present",6)
!#endif
            d6s = lbound(array,6)
            d6e = ubound(array,6)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_r6d
      !
      module subroutine sp_devxlib_mapping_map_c1d(array, range1 )
         implicit none
         !
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
         !
         integer :: i1, d1s, d1e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c1d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_c1d
      !
      module subroutine sp_devxlib_mapping_map_c2d(array, range1, &
                                            range2 )
         implicit none
         !
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c2d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c2d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_c2d
      !
      module subroutine sp_devxlib_mapping_map_c3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         !
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c3d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c3d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c3d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_c3d
      !
      module subroutine sp_devxlib_mapping_map_c4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         !
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c4d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c4d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c4d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c4d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_c4d
      !
      module subroutine sp_devxlib_mapping_map_c5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         !
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c5d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c5d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c5d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c5d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c5d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_c5d
      !
      module subroutine sp_devxlib_mapping_map_c6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         !
         complex(real32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         integer :: i6, d6s, d6e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c6d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c6d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c6d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c6d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c6d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         if (present(range6)) then
            d6s = range6(1)
            d6e = range6(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("sp_dev_mapping_c6d","range not present",6)
!#endif
            d6s = lbound(array,6)
            d6e = ubound(array,6)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine sp_devxlib_mapping_map_c6d
      !
      module subroutine dp_devxlib_mapping_map_c1d(array, range1 )
         implicit none
         !
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
         !
         integer :: i1, d1s, d1e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c1d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_c1d
      !
      module subroutine dp_devxlib_mapping_map_c2d(array, range1, &
                                            range2 )
         implicit none
         !
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c2d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c2d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_c2d
      !
      module subroutine dp_devxlib_mapping_map_c3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         !
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c3d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c3d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c3d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_c3d
      !
      module subroutine dp_devxlib_mapping_map_c4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         !
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c4d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c4d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c4d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c4d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_c4d
      !
      module subroutine dp_devxlib_mapping_map_c5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         !
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c5d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c5d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c5d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c5d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c5d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_c5d
      !
      module subroutine dp_devxlib_mapping_map_c6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         !
         complex(real64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         integer :: i6, d6s, d6e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c6d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c6d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c6d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c6d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c6d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         if (present(range6)) then
            d6s = range6(1)
            d6e = range6(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("dp_dev_mapping_c6d","range not present",6)
!#endif
            d6s = lbound(array,6)
            d6e = ubound(array,6)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine dp_devxlib_mapping_map_c6d
      !
      module subroutine i4_devxlib_mapping_map_i1d(array, range1 )
         implicit none
         !
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
         !
         integer :: i1, d1s, d1e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i1d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i4_devxlib_mapping_map_i1d
      !
      module subroutine i4_devxlib_mapping_map_i2d(array, range1, &
                                            range2 )
         implicit none
         !
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i2d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i2d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i4_devxlib_mapping_map_i2d
      !
      module subroutine i4_devxlib_mapping_map_i3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         !
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i3d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i3d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i3d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i4_devxlib_mapping_map_i3d
      !
      module subroutine i4_devxlib_mapping_map_i4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         !
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i4d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i4d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i4d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i4d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i4_devxlib_mapping_map_i4d
      !
      module subroutine i4_devxlib_mapping_map_i5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         !
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i5d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i5d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i5d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i5d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i5d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i4_devxlib_mapping_map_i5d
      !
      module subroutine i4_devxlib_mapping_map_i6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         !
         integer(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         integer :: i6, d6s, d6e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i6d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i6d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i6d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i6d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i6d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         if (present(range6)) then
            d6s = range6(1)
            d6e = range6(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i4_dev_mapping_i6d","range not present",6)
!#endif
            d6s = lbound(array,6)
            d6e = ubound(array,6)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i4_devxlib_mapping_map_i6d
      !
      module subroutine i8_devxlib_mapping_map_i1d(array, range1 )
         implicit none
         !
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
         !
         integer :: i1, d1s, d1e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i1d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i8_devxlib_mapping_map_i1d
      !
      module subroutine i8_devxlib_mapping_map_i2d(array, range1, &
                                            range2 )
         implicit none
         !
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i2d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i2d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i8_devxlib_mapping_map_i2d
      !
      module subroutine i8_devxlib_mapping_map_i3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         !
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i3d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i3d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i3d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i8_devxlib_mapping_map_i3d
      !
      module subroutine i8_devxlib_mapping_map_i4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         !
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i4d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i4d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i4d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i4d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i8_devxlib_mapping_map_i4d
      !
      module subroutine i8_devxlib_mapping_map_i5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         !
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i5d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i5d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i5d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i5d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i5d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i8_devxlib_mapping_map_i5d
      !
      module subroutine i8_devxlib_mapping_map_i6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         !
         integer(int64), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         integer :: i6, d6s, d6e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i6d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i6d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i6d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i6d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i6d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         if (present(range6)) then
            d6s = range6(1)
            d6e = range6(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("i8_dev_mapping_i6d","range not present",6)
!#endif
            d6s = lbound(array,6)
            d6e = ubound(array,6)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine i8_devxlib_mapping_map_i6d
      !
      module subroutine l4_devxlib_mapping_map_l1d(array, range1 )
         implicit none
         !
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:)
         integer, optional, intent(in) ::  range1(2)
         !
         integer :: i1, d1s, d1e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l1d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine l4_devxlib_mapping_map_l1d
      !
      module subroutine l4_devxlib_mapping_map_l2d(array, range1, &
                                            range2 )
         implicit none
         !
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l2d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l2d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine l4_devxlib_mapping_map_l2d
      !
      module subroutine l4_devxlib_mapping_map_l3d(array, range1, &
                                            range2, &
                                            range3 )
         implicit none
         !
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l3d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l3d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l3d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine l4_devxlib_mapping_map_l3d
      !
      module subroutine l4_devxlib_mapping_map_l4d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4 )
         implicit none
         !
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l4d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l4d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l4d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l4d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine l4_devxlib_mapping_map_l4d
      !
      module subroutine l4_devxlib_mapping_map_l5d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5 )
         implicit none
         !
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l5d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l5d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l5d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l5d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l5d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine l4_devxlib_mapping_map_l5d
      !
      module subroutine l4_devxlib_mapping_map_l6d(array, range1, &
                                            range2, &
                                            range3, &
                                            range4, &
                                            range5, &
                                            range6 )
         implicit none
         !
         logical(int32), allocatable DEV_ATTR, intent(inout) :: array(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         !
         integer :: i1, d1s, d1e
         integer :: i2, d2s, d2e
         integer :: i3, d3s, d3e
         integer :: i4, d4s, d4e
         integer :: i5, d5s, d5e
         integer :: i6, d6s, d6e
         !
         if (present(range1)) then
            d1s = range1(1)
            d1e = range1(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l6d","range not present",1)
!#endif
            d1s = lbound(array,1)
            d1e = ubound(array,1)
         endif
         !
         if (present(range2)) then
            d2s = range2(1)
            d2e = range2(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l6d","range not present",2)
!#endif
            d2s = lbound(array,2)
            d2e = ubound(array,2)
         endif
         !
         if (present(range3)) then
            d3s = range3(1)
            d3e = range3(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l6d","range not present",3)
!#endif
            d3s = lbound(array,3)
            d3e = ubound(array,3)
         endif
         !
         if (present(range4)) then
            d4s = range4(1)
            d4e = range4(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l6d","range not present",4)
!#endif
            d4s = lbound(array,4)
            d4e = ubound(array,4)
         endif
         !
         if (present(range5)) then
            d5s = range5(1)
            d5e = range5(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l6d","range not present",5)
!#endif
            d5s = lbound(array,5)
            d5e = ubound(array,5)
         endif
         !
         if (present(range6)) then
            d6s = range6(1)
            d6e = range6(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("l4_dev_mapping_l6d","range not present",6)
!#endif
            d6s = lbound(array,6)
            d6e = ubound(array,6)
         endif
         !
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array(d1s:d1e,d2s:d2e,d3s:d3e,d4s:d4e,d5s:d5e,d6s:d6e) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine l4_devxlib_mapping_map_l6d
      !

endsubmodule devxlib_mapping_map