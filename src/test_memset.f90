!
! Copyright (C) 2023, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Testing: devxlib_memset, devxlib_memcpy
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from test_memcpy.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
program test_memset
  !
  ! This program tests the routines and interfaces related to
  ! devxlib_memset and devxlib_memcpy
  !
  use iso_fortran_env, only : int32, int64, real32, real64
  use devxlib_memset
  use devxlib_memcpy
  use devxlib_mapping

  implicit none

  integer(int64) :: cnt, count_max
  real(real64), parameter :: thr=1.0d-6
  integer,      parameter :: nranks=4
  integer :: ndim(nranks)
  integer :: vrange(2,nranks)
  integer :: vlbound(nranks)
  real(real64):: t0, t1, count_rate
  logical :: is_alloc, lfail_malloc
  integer :: npass,nfail
  integer :: nfail_malloc
  !
  integer(int32)  :: i_const_int32
  integer(int64)  :: i_const_int64
  real(real32)    :: r_const_real32
  real(real64)    :: r_const_real64
  complex(real32) :: c_const_real32
  complex(real64) :: c_const_real64
  logical         :: l_const
  real(real32), allocatable :: A_hst0__sp_r1d(:)
  real(real32), allocatable :: A_hst1__sp_r1d(:)
  real(real32), allocatable :: A_hst2__sp_r1d(:)
  real(real32), allocatable DEV_ATTR :: A_dev__sp_r1d(:)

  real(real32), allocatable :: A_hst0__sp_r2d(:,:)
  real(real32), allocatable :: A_hst1__sp_r2d(:,:)
  real(real32), allocatable :: A_hst2__sp_r2d(:,:)
  real(real32), allocatable DEV_ATTR :: A_dev__sp_r2d(:,:)

  real(real32), allocatable :: A_hst0__sp_r3d(:,:,:)
  real(real32), allocatable :: A_hst1__sp_r3d(:,:,:)
  real(real32), allocatable :: A_hst2__sp_r3d(:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev__sp_r3d(:,:,:)

  real(real32), allocatable :: A_hst0__sp_r4d(:,:,:,:)
  real(real32), allocatable :: A_hst1__sp_r4d(:,:,:,:)
  real(real32), allocatable :: A_hst2__sp_r4d(:,:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev__sp_r4d(:,:,:,:)


  real(real64), allocatable :: A_hst0__dp_r1d(:)
  real(real64), allocatable :: A_hst1__dp_r1d(:)
  real(real64), allocatable :: A_hst2__dp_r1d(:)
  real(real64), allocatable DEV_ATTR :: A_dev__dp_r1d(:)

  real(real64), allocatable :: A_hst0__dp_r2d(:,:)
  real(real64), allocatable :: A_hst1__dp_r2d(:,:)
  real(real64), allocatable :: A_hst2__dp_r2d(:,:)
  real(real64), allocatable DEV_ATTR :: A_dev__dp_r2d(:,:)

  real(real64), allocatable :: A_hst0__dp_r3d(:,:,:)
  real(real64), allocatable :: A_hst1__dp_r3d(:,:,:)
  real(real64), allocatable :: A_hst2__dp_r3d(:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev__dp_r3d(:,:,:)

  real(real64), allocatable :: A_hst0__dp_r4d(:,:,:,:)
  real(real64), allocatable :: A_hst1__dp_r4d(:,:,:,:)
  real(real64), allocatable :: A_hst2__dp_r4d(:,:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev__dp_r4d(:,:,:,:)



  complex(real32), allocatable :: A_hst0__sp_c1d(:)
  complex(real32), allocatable :: A_hst1__sp_c1d(:)
  complex(real32), allocatable :: A_hst2__sp_c1d(:)
  complex(real32), allocatable DEV_ATTR :: A_dev__sp_c1d(:)

  complex(real32), allocatable :: A_hst0__sp_c2d(:,:)
  complex(real32), allocatable :: A_hst1__sp_c2d(:,:)
  complex(real32), allocatable :: A_hst2__sp_c2d(:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev__sp_c2d(:,:)

  complex(real32), allocatable :: A_hst0__sp_c3d(:,:,:)
  complex(real32), allocatable :: A_hst1__sp_c3d(:,:,:)
  complex(real32), allocatable :: A_hst2__sp_c3d(:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev__sp_c3d(:,:,:)

  complex(real32), allocatable :: A_hst0__sp_c4d(:,:,:,:)
  complex(real32), allocatable :: A_hst1__sp_c4d(:,:,:,:)
  complex(real32), allocatable :: A_hst2__sp_c4d(:,:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev__sp_c4d(:,:,:,:)


  complex(real64), allocatable :: A_hst0__dp_c1d(:)
  complex(real64), allocatable :: A_hst1__dp_c1d(:)
  complex(real64), allocatable :: A_hst2__dp_c1d(:)
  complex(real64), allocatable DEV_ATTR :: A_dev__dp_c1d(:)

  complex(real64), allocatable :: A_hst0__dp_c2d(:,:)
  complex(real64), allocatable :: A_hst1__dp_c2d(:,:)
  complex(real64), allocatable :: A_hst2__dp_c2d(:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev__dp_c2d(:,:)

  complex(real64), allocatable :: A_hst0__dp_c3d(:,:,:)
  complex(real64), allocatable :: A_hst1__dp_c3d(:,:,:)
  complex(real64), allocatable :: A_hst2__dp_c3d(:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev__dp_c3d(:,:,:)

  complex(real64), allocatable :: A_hst0__dp_c4d(:,:,:,:)
  complex(real64), allocatable :: A_hst1__dp_c4d(:,:,:,:)
  complex(real64), allocatable :: A_hst2__dp_c4d(:,:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev__dp_c4d(:,:,:,:)



  integer(int32), allocatable :: A_hst0__i4_i1d(:)
  integer(int32), allocatable :: A_hst1__i4_i1d(:)
  integer(int32), allocatable :: A_hst2__i4_i1d(:)
  integer(int32), allocatable DEV_ATTR :: A_dev__i4_i1d(:)

  integer(int32), allocatable :: A_hst0__i4_i2d(:,:)
  integer(int32), allocatable :: A_hst1__i4_i2d(:,:)
  integer(int32), allocatable :: A_hst2__i4_i2d(:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev__i4_i2d(:,:)

  integer(int32), allocatable :: A_hst0__i4_i3d(:,:,:)
  integer(int32), allocatable :: A_hst1__i4_i3d(:,:,:)
  integer(int32), allocatable :: A_hst2__i4_i3d(:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev__i4_i3d(:,:,:)

  integer(int32), allocatable :: A_hst0__i4_i4d(:,:,:,:)
  integer(int32), allocatable :: A_hst1__i4_i4d(:,:,:,:)
  integer(int32), allocatable :: A_hst2__i4_i4d(:,:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev__i4_i4d(:,:,:,:)


  integer(int64), allocatable :: A_hst0__i8_i1d(:)
  integer(int64), allocatable :: A_hst1__i8_i1d(:)
  integer(int64), allocatable :: A_hst2__i8_i1d(:)
  integer(int64), allocatable DEV_ATTR :: A_dev__i8_i1d(:)

  integer(int64), allocatable :: A_hst0__i8_i2d(:,:)
  integer(int64), allocatable :: A_hst1__i8_i2d(:,:)
  integer(int64), allocatable :: A_hst2__i8_i2d(:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev__i8_i2d(:,:)

  integer(int64), allocatable :: A_hst0__i8_i3d(:,:,:)
  integer(int64), allocatable :: A_hst1__i8_i3d(:,:,:)
  integer(int64), allocatable :: A_hst2__i8_i3d(:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev__i8_i3d(:,:,:)

  integer(int64), allocatable :: A_hst0__i8_i4d(:,:,:,:)
  integer(int64), allocatable :: A_hst1__i8_i4d(:,:,:,:)
  integer(int64), allocatable :: A_hst2__i8_i4d(:,:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev__i8_i4d(:,:,:,:)



  logical(int32), allocatable :: A_hst0__l4_l1d(:)
  logical(int32), allocatable :: A_hst1__l4_l1d(:)
  logical(int32), allocatable :: A_hst2__l4_l1d(:)
  logical(int32), allocatable DEV_ATTR :: A_dev__l4_l1d(:)

  logical(int32), allocatable :: A_hst0__l4_l2d(:,:)
  logical(int32), allocatable :: A_hst1__l4_l2d(:,:)
  logical(int32), allocatable :: A_hst2__l4_l2d(:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev__l4_l2d(:,:)

  logical(int32), allocatable :: A_hst0__l4_l3d(:,:,:)
  logical(int32), allocatable :: A_hst1__l4_l3d(:,:,:)
  logical(int32), allocatable :: A_hst2__l4_l3d(:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev__l4_l3d(:,:,:)

  logical(int32), allocatable :: A_hst0__l4_l4d(:,:,:,:)
  logical(int32), allocatable :: A_hst1__l4_l4d(:,:,:,:)
  logical(int32), allocatable :: A_hst2__l4_l4d(:,:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev__l4_l4d(:,:,:,:)



  
  integer :: i,ierr
  integer :: ndim1, ndim2, ndim3, ndim4 
  integer :: lbound1, lbound2, lbound3, lbound4 
  integer :: range1(2), range2(2), range3(2), range4(2) 
  integer :: bound1(2), bound2(2), bound3(2), bound4(2) 
  character(256) :: arg, str

!
!============================
! get dims
!============================
!
  ! defaults
  ndim(:)=100
  vrange(1,:)=1
  vrange(2,:)=ndim
  vlbound(:)=1

  i=0
  do
    call get_command_argument(i, arg)
    if (len_trim(arg) == 0) exit
    !
    select case (trim(arg))
    case("-h","--help")
      write(6,"(a)") "Usage: "
      write(6,"(a)") "   ./test_memset.x [--dims <vals>] [--range <vals>] [--lbound <vals>]"
      stop
    end select
    !
    i = i+1
    call get_command_argument(i, str)
    if (len_trim(str) == 0) exit
    !
    select case (trim(arg))
    case("-dims","--dims")
      read(str,*,iostat=ierr) ndim(:)
      if (ierr/=0) STOP "reading cmd-line args: dims"
    case("-range","--range")
      read(str,*,iostat=ierr) vrange(:,:)
      if (ierr/=0) STOP "reading cmd-line args: range"
    case("-lbound","--lbound")
      read(str,*,iostat=ierr) vlbound(:)
      if (ierr/=0) STOP "reading cmd-line args: lbound"
    end select
  enddo
  !
  write(6,"(/,a,/)") "Running test with params: "
  write(6,"(3x,a,10i5)") "  ndim: ", ndim(:)
  write(6,"(3x,a,10i5)") "lbound: ", vlbound(:)
  do i = 1, nranks
     write(6,"(3x,a,i2,3x,10i5)") " range: ", i, vrange(:,i)
  enddo
  write(6,"()")
  !
  npass=0
  nfail=0
  nfail_malloc=0


  ndim1=ndim(1)
  lbound1=vlbound(1)
  range1=vrange(:,1)
  bound1(1)=lbound1
  bound1(2)=lbound1+ndim1-1
  ndim2=ndim(2)
  lbound2=vlbound(2)
  range2=vrange(:,2)
  bound2(1)=lbound2
  bound2(2)=lbound2+ndim2-1
  ndim3=ndim(3)
  lbound3=vlbound(3)
  range3=vrange(:,3)
  bound3(1)=lbound3
  bound3(2)=lbound3+ndim3-1
  ndim4=ndim(4)
  lbound4=vlbound(4)
  range4=vrange(:,4)
  bound4(1)=lbound4
  bound4(2)=lbound4+ndim4-1

!
!============================
! check memcpy
!============================
!
  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_r1d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst1__sp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst2__sp_r1d(bound1(1):bound1(2)) )
  allocate( A_dev__sp_r1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_r1d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_r1d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real32 = 4.0
  
  !
  ! memset
  !
    A_hst0__sp_r1d=r_const_real32
    call devxlib_memset_h(A_hst1__sp_r1d,r_const_real32)
    call devxlib_memset_d(A_dev__sp_r1d,r_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r1d, A_dev__sp_r1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_r1d -A_hst0__sp_r1d )> thr) .or. &
       any(abs(A_hst2__sp_r1d -A_hst0__sp_r1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_r1d)
  deallocate(A_hst1__sp_r1d)
  deallocate(A_hst2__sp_r1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_r1d)
  is_alloc = devxlib_mapped( A_dev__sp_r1d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_r1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_r2d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst1__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_r2d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_r2d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real32 = 4.0
  
  !
  ! memset
  !
    A_hst0__sp_r2d=r_const_real32
    call devxlib_memset_h(A_hst1__sp_r2d,r_const_real32)
    call devxlib_memset_d(A_dev__sp_r2d,r_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r2d, A_dev__sp_r2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_r2d -A_hst0__sp_r2d )> thr) .or. &
       any(abs(A_hst2__sp_r2d -A_hst0__sp_r2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_r2d)
  deallocate(A_hst1__sp_r2d)
  deallocate(A_hst2__sp_r2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_r2d)
  is_alloc = devxlib_mapped( A_dev__sp_r2d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_r2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_r3d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst1__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_r3d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_r3d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real32 = 4.0
  
  !
  ! memset
  !
    A_hst0__sp_r3d=r_const_real32
    call devxlib_memset_h(A_hst1__sp_r3d,r_const_real32)
    call devxlib_memset_d(A_dev__sp_r3d,r_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r3d, A_dev__sp_r3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_r3d -A_hst0__sp_r3d )> thr) .or. &
       any(abs(A_hst2__sp_r3d -A_hst0__sp_r3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_r3d)
  deallocate(A_hst1__sp_r3d)
  deallocate(A_hst2__sp_r3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_r3d)
  is_alloc = devxlib_mapped( A_dev__sp_r3d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_r3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_r4d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst1__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_r4d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_r4d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real32 = 4.0
  
  !
  ! memset
  !
    A_hst0__sp_r4d=r_const_real32
    call devxlib_memset_h(A_hst1__sp_r4d,r_const_real32)
    call devxlib_memset_d(A_dev__sp_r4d,r_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r4d, A_dev__sp_r4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_r4d -A_hst0__sp_r4d )> thr) .or. &
       any(abs(A_hst2__sp_r4d -A_hst0__sp_r4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_r4d)
  deallocate(A_hst1__sp_r4d)
  deallocate(A_hst2__sp_r4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_r4d)
  is_alloc = devxlib_mapped( A_dev__sp_r4d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_r4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_r1d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst1__dp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst2__dp_r1d(bound1(1):bound1(2)) )
  allocate( A_dev__dp_r1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_r1d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_r1d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real64 = 4.0
  
  !
  ! memset
  !
    A_hst0__dp_r1d=r_const_real64
    call devxlib_memset_h(A_hst1__dp_r1d,r_const_real64)
    call devxlib_memset_d(A_dev__dp_r1d,r_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r1d, A_dev__dp_r1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_r1d -A_hst0__dp_r1d )> thr) .or. &
       any(abs(A_hst2__dp_r1d -A_hst0__dp_r1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_r1d)
  deallocate(A_hst1__dp_r1d)
  deallocate(A_hst2__dp_r1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_r1d)
  is_alloc = devxlib_mapped( A_dev__dp_r1d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_r1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_r2d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst1__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_r2d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_r2d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real64 = 4.0
  
  !
  ! memset
  !
    A_hst0__dp_r2d=r_const_real64
    call devxlib_memset_h(A_hst1__dp_r2d,r_const_real64)
    call devxlib_memset_d(A_dev__dp_r2d,r_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r2d, A_dev__dp_r2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_r2d -A_hst0__dp_r2d )> thr) .or. &
       any(abs(A_hst2__dp_r2d -A_hst0__dp_r2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_r2d)
  deallocate(A_hst1__dp_r2d)
  deallocate(A_hst2__dp_r2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_r2d)
  is_alloc = devxlib_mapped( A_dev__dp_r2d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_r2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_r3d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst1__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_r3d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_r3d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real64 = 4.0
  
  !
  ! memset
  !
    A_hst0__dp_r3d=r_const_real64
    call devxlib_memset_h(A_hst1__dp_r3d,r_const_real64)
    call devxlib_memset_d(A_dev__dp_r3d,r_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r3d, A_dev__dp_r3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_r3d -A_hst0__dp_r3d )> thr) .or. &
       any(abs(A_hst2__dp_r3d -A_hst0__dp_r3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_r3d)
  deallocate(A_hst1__dp_r3d)
  deallocate(A_hst2__dp_r3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_r3d)
  is_alloc = devxlib_mapped( A_dev__dp_r3d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_r3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_r4d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst1__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_r4d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_r4d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    r_const_real64 = 4.0
  
  !
  ! memset
  !
    A_hst0__dp_r4d=r_const_real64
    call devxlib_memset_h(A_hst1__dp_r4d,r_const_real64)
    call devxlib_memset_d(A_dev__dp_r4d,r_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r4d, A_dev__dp_r4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_r4d -A_hst0__dp_r4d )> thr) .or. &
       any(abs(A_hst2__dp_r4d -A_hst0__dp_r4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_r4d)
  deallocate(A_hst1__dp_r4d)
  deallocate(A_hst2__dp_r4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_r4d)
  is_alloc = devxlib_mapped( A_dev__dp_r4d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_r4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_c1d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst1__sp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst2__sp_c1d(bound1(1):bound1(2)) )
  allocate( A_dev__sp_c1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_c1d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_c1d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real32 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__sp_c1d=c_const_real32
    call devxlib_memset_h(A_hst1__sp_c1d,c_const_real32)
    call devxlib_memset_d(A_dev__sp_c1d,c_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c1d, A_dev__sp_c1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_c1d -A_hst0__sp_c1d )> thr) .or. &
       any(abs(A_hst2__sp_c1d -A_hst0__sp_c1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_c1d)
  deallocate(A_hst1__sp_c1d)
  deallocate(A_hst2__sp_c1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_c1d)
  is_alloc = devxlib_mapped( A_dev__sp_c1d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_c1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_c2d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst1__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_c2d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_c2d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real32 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__sp_c2d=c_const_real32
    call devxlib_memset_h(A_hst1__sp_c2d,c_const_real32)
    call devxlib_memset_d(A_dev__sp_c2d,c_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c2d, A_dev__sp_c2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_c2d -A_hst0__sp_c2d )> thr) .or. &
       any(abs(A_hst2__sp_c2d -A_hst0__sp_c2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_c2d)
  deallocate(A_hst1__sp_c2d)
  deallocate(A_hst2__sp_c2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_c2d)
  is_alloc = devxlib_mapped( A_dev__sp_c2d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_c2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_c3d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst1__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_c3d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_c3d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real32 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__sp_c3d=c_const_real32
    call devxlib_memset_h(A_hst1__sp_c3d,c_const_real32)
    call devxlib_memset_d(A_dev__sp_c3d,c_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c3d, A_dev__sp_c3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_c3d -A_hst0__sp_c3d )> thr) .or. &
       any(abs(A_hst2__sp_c3d -A_hst0__sp_c3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_c3d)
  deallocate(A_hst1__sp_c3d)
  deallocate(A_hst2__sp_c3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_c3d)
  is_alloc = devxlib_mapped( A_dev__sp_c3d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_c3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__sp_c4d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst1__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__sp_c4d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__sp_c4d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real32 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__sp_c4d=c_const_real32
    call devxlib_memset_h(A_hst1__sp_c4d,c_const_real32)
    call devxlib_memset_d(A_dev__sp_c4d,c_const_real32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c4d, A_dev__sp_c4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__sp_c4d -A_hst0__sp_c4d )> thr) .or. &
       any(abs(A_hst2__sp_c4d -A_hst0__sp_c4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__sp_c4d)
  deallocate(A_hst1__sp_c4d)
  deallocate(A_hst2__sp_c4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__sp_c4d)
  is_alloc = devxlib_mapped( A_dev__sp_c4d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__sp_c4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_c1d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst1__dp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst2__dp_c1d(bound1(1):bound1(2)) )
  allocate( A_dev__dp_c1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_c1d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_c1d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real64 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__dp_c1d=c_const_real64
    call devxlib_memset_h(A_hst1__dp_c1d,c_const_real64)
    call devxlib_memset_d(A_dev__dp_c1d,c_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c1d, A_dev__dp_c1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_c1d -A_hst0__dp_c1d )> thr) .or. &
       any(abs(A_hst2__dp_c1d -A_hst0__dp_c1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_c1d)
  deallocate(A_hst1__dp_c1d)
  deallocate(A_hst2__dp_c1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_c1d)
  is_alloc = devxlib_mapped( A_dev__dp_c1d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_c1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_c2d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst1__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_c2d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_c2d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real64 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__dp_c2d=c_const_real64
    call devxlib_memset_h(A_hst1__dp_c2d,c_const_real64)
    call devxlib_memset_d(A_dev__dp_c2d,c_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c2d, A_dev__dp_c2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_c2d -A_hst0__dp_c2d )> thr) .or. &
       any(abs(A_hst2__dp_c2d -A_hst0__dp_c2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_c2d)
  deallocate(A_hst1__dp_c2d)
  deallocate(A_hst2__dp_c2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_c2d)
  is_alloc = devxlib_mapped( A_dev__dp_c2d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_c2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_c3d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst1__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_c3d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_c3d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real64 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__dp_c3d=c_const_real64
    call devxlib_memset_h(A_hst1__dp_c3d,c_const_real64)
    call devxlib_memset_d(A_dev__dp_c3d,c_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c3d, A_dev__dp_c3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_c3d -A_hst0__dp_c3d )> thr) .or. &
       any(abs(A_hst2__dp_c3d -A_hst0__dp_c3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_c3d)
  deallocate(A_hst1__dp_c3d)
  deallocate(A_hst2__dp_c3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_c3d)
  is_alloc = devxlib_mapped( A_dev__dp_c3d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_c3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__dp_c4d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst1__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__dp_c4d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__dp_c4d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    c_const_real64 = (1.0,2.0)
  
  !
  ! memset
  !
    A_hst0__dp_c4d=c_const_real64
    call devxlib_memset_h(A_hst1__dp_c4d,c_const_real64)
    call devxlib_memset_d(A_dev__dp_c4d,c_const_real64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c4d, A_dev__dp_c4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__dp_c4d -A_hst0__dp_c4d )> thr) .or. &
       any(abs(A_hst2__dp_c4d -A_hst0__dp_c4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__dp_c4d)
  deallocate(A_hst1__dp_c4d)
  deallocate(A_hst2__dp_c4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__dp_c4d)
  is_alloc = devxlib_mapped( A_dev__dp_c4d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__dp_c4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i4_i1d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i4_i1d(bound1(1):bound1(2)) )
  allocate( A_hst1__i4_i1d(bound1(1):bound1(2)) )
  allocate( A_hst2__i4_i1d(bound1(1):bound1(2)) )
  allocate( A_dev__i4_i1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i4_i1d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i4_i1d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int32 = 3
  
  !
  ! memset
  !
    A_hst0__i4_i1d=i_const_int32
    call devxlib_memset_h(A_hst1__i4_i1d,i_const_int32)
    call devxlib_memset_d(A_dev__i4_i1d,i_const_int32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i1d, A_dev__i4_i1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i4_i1d -A_hst0__i4_i1d )> thr) .or. &
       any(abs(A_hst2__i4_i1d -A_hst0__i4_i1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i4_i1d)
  deallocate(A_hst1__i4_i1d)
  deallocate(A_hst2__i4_i1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i4_i1d)
  is_alloc = devxlib_mapped( A_dev__i4_i1d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i4_i1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i4_i2d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst1__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i4_i2d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i4_i2d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int32 = 3
  
  !
  ! memset
  !
    A_hst0__i4_i2d=i_const_int32
    call devxlib_memset_h(A_hst1__i4_i2d,i_const_int32)
    call devxlib_memset_d(A_dev__i4_i2d,i_const_int32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i2d, A_dev__i4_i2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i4_i2d -A_hst0__i4_i2d )> thr) .or. &
       any(abs(A_hst2__i4_i2d -A_hst0__i4_i2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i4_i2d)
  deallocate(A_hst1__i4_i2d)
  deallocate(A_hst2__i4_i2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i4_i2d)
  is_alloc = devxlib_mapped( A_dev__i4_i2d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i4_i2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i4_i3d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst1__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i4_i3d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i4_i3d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int32 = 3
  
  !
  ! memset
  !
    A_hst0__i4_i3d=i_const_int32
    call devxlib_memset_h(A_hst1__i4_i3d,i_const_int32)
    call devxlib_memset_d(A_dev__i4_i3d,i_const_int32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i3d, A_dev__i4_i3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i4_i3d -A_hst0__i4_i3d )> thr) .or. &
       any(abs(A_hst2__i4_i3d -A_hst0__i4_i3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i4_i3d)
  deallocate(A_hst1__i4_i3d)
  deallocate(A_hst2__i4_i3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i4_i3d)
  is_alloc = devxlib_mapped( A_dev__i4_i3d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i4_i3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i4_i4d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst1__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i4_i4d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i4_i4d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int32 = 3
  
  !
  ! memset
  !
    A_hst0__i4_i4d=i_const_int32
    call devxlib_memset_h(A_hst1__i4_i4d,i_const_int32)
    call devxlib_memset_d(A_dev__i4_i4d,i_const_int32)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i4d, A_dev__i4_i4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i4_i4d -A_hst0__i4_i4d )> thr) .or. &
       any(abs(A_hst2__i4_i4d -A_hst0__i4_i4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i4_i4d)
  deallocate(A_hst1__i4_i4d)
  deallocate(A_hst2__i4_i4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i4_i4d)
  is_alloc = devxlib_mapped( A_dev__i4_i4d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i4_i4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i8_i1d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i8_i1d(bound1(1):bound1(2)) )
  allocate( A_hst1__i8_i1d(bound1(1):bound1(2)) )
  allocate( A_hst2__i8_i1d(bound1(1):bound1(2)) )
  allocate( A_dev__i8_i1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i8_i1d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i8_i1d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int64 = 3
  
  !
  ! memset
  !
    A_hst0__i8_i1d=i_const_int64
    call devxlib_memset_h(A_hst1__i8_i1d,i_const_int64)
    call devxlib_memset_d(A_dev__i8_i1d,i_const_int64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i1d, A_dev__i8_i1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i8_i1d -A_hst0__i8_i1d )> thr) .or. &
       any(abs(A_hst2__i8_i1d -A_hst0__i8_i1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i8_i1d)
  deallocate(A_hst1__i8_i1d)
  deallocate(A_hst2__i8_i1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i8_i1d)
  is_alloc = devxlib_mapped( A_dev__i8_i1d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i8_i1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i8_i2d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst1__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i8_i2d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i8_i2d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int64 = 3
  
  !
  ! memset
  !
    A_hst0__i8_i2d=i_const_int64
    call devxlib_memset_h(A_hst1__i8_i2d,i_const_int64)
    call devxlib_memset_d(A_dev__i8_i2d,i_const_int64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i2d, A_dev__i8_i2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i8_i2d -A_hst0__i8_i2d )> thr) .or. &
       any(abs(A_hst2__i8_i2d -A_hst0__i8_i2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i8_i2d)
  deallocate(A_hst1__i8_i2d)
  deallocate(A_hst2__i8_i2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i8_i2d)
  is_alloc = devxlib_mapped( A_dev__i8_i2d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i8_i2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i8_i3d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst1__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i8_i3d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i8_i3d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int64 = 3
  
  !
  ! memset
  !
    A_hst0__i8_i3d=i_const_int64
    call devxlib_memset_h(A_hst1__i8_i3d,i_const_int64)
    call devxlib_memset_d(A_dev__i8_i3d,i_const_int64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i3d, A_dev__i8_i3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i8_i3d -A_hst0__i8_i3d )> thr) .or. &
       any(abs(A_hst2__i8_i3d -A_hst0__i8_i3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i8_i3d)
  deallocate(A_hst1__i8_i3d)
  deallocate(A_hst2__i8_i3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i8_i3d)
  is_alloc = devxlib_mapped( A_dev__i8_i3d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i8_i3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__i8_i4d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst1__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__i8_i4d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__i8_i4d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    i_const_int64 = 3
  
  !
  ! memset
  !
    A_hst0__i8_i4d=i_const_int64
    call devxlib_memset_h(A_hst1__i8_i4d,i_const_int64)
    call devxlib_memset_d(A_dev__i8_i4d,i_const_int64)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i4d, A_dev__i8_i4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any(abs(A_hst1__i8_i4d -A_hst0__i8_i4d )> thr) .or. &
       any(abs(A_hst2__i8_i4d -A_hst0__i8_i4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__i8_i4d)
  deallocate(A_hst1__i8_i4d)
  deallocate(A_hst2__i8_i4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__i8_i4d)
  is_alloc = devxlib_mapped( A_dev__i8_i4d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__i8_i4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__l4_l1d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__l4_l1d(bound1(1):bound1(2)) )
  allocate( A_hst1__l4_l1d(bound1(1):bound1(2)) )
  allocate( A_hst2__l4_l1d(bound1(1):bound1(2)) )
  allocate( A_dev__l4_l1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__l4_l1d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__l4_l1d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    l_const = .true.
  
  !
  ! memset
  !
    A_hst0__l4_l1d=l_const
    call devxlib_memset_h(A_hst1__l4_l1d,l_const)
    call devxlib_memset_d(A_dev__l4_l1d,l_const)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l1d, A_dev__l4_l1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any( A_hst1__l4_l1d .neqv. A_hst0__l4_l1d ) .or. &
       any( A_hst2__l4_l1d .neqv. A_hst0__l4_l1d )    ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__l4_l1d)
  deallocate(A_hst1__l4_l1d)
  deallocate(A_hst2__l4_l1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__l4_l1d)
  is_alloc = devxlib_mapped( A_dev__l4_l1d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__l4_l1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__l4_l2d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst1__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__l4_l2d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__l4_l2d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    l_const = .true.
  
  !
  ! memset
  !
    A_hst0__l4_l2d=l_const
    call devxlib_memset_h(A_hst1__l4_l2d,l_const)
    call devxlib_memset_d(A_dev__l4_l2d,l_const)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l2d, A_dev__l4_l2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any( A_hst1__l4_l2d .neqv. A_hst0__l4_l2d ) .or. &
       any( A_hst2__l4_l2d .neqv. A_hst0__l4_l2d )    ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__l4_l2d)
  deallocate(A_hst1__l4_l2d)
  deallocate(A_hst2__l4_l2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__l4_l2d)
  is_alloc = devxlib_mapped( A_dev__l4_l2d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__l4_l2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__l4_l3d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst1__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__l4_l3d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__l4_l3d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    l_const = .true.
  
  !
  ! memset
  !
    A_hst0__l4_l3d=l_const
    call devxlib_memset_h(A_hst1__l4_l3d,l_const)
    call devxlib_memset_d(A_dev__l4_l3d,l_const)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l3d, A_dev__l4_l3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any( A_hst1__l4_l3d .neqv. A_hst0__l4_l3d ) .or. &
       any( A_hst2__l4_l3d .neqv. A_hst0__l4_l3d )    ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__l4_l3d)
  deallocate(A_hst1__l4_l3d)
  deallocate(A_hst2__l4_l3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__l4_l3d)
  is_alloc = devxlib_mapped( A_dev__l4_l3d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__l4_l3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev__l4_l4d )
  !
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst0__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst1__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev__l4_l4d)
#endif
  !
  is_alloc = devxlib_mapped( A_dev__l4_l4d )
  !
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED" 
  endif
  !
  ! init
    l_const = .true.
  
  !
  ! memset
  !
    A_hst0__l4_l4d=l_const
    call devxlib_memset_h(A_hst1__l4_l4d,l_const)
    call devxlib_memset_d(A_dev__l4_l4d,l_const)
  
  ! 
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l4d, A_dev__l4_l4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED" 
  !
  if ( any( A_hst1__l4_l4d .neqv. A_hst0__l4_l4d ) .or. &
       any( A_hst2__l4_l4d .neqv. A_hst0__l4_l4d )    ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst0__l4_l4d)
  deallocate(A_hst1__l4_l4d)
  deallocate(A_hst2__l4_l4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev__l4_l4d)
  is_alloc = devxlib_mapped( A_dev__l4_l4d )
  !
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED" 
  endif
  !
#endif
  !
  deallocate(A_dev__l4_l4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !




  !
  ! summary
  !
  write(6,"(/,a)") "# Test SUMMARY:"
  write(6,"(3x,a,i5)") "# passed: ", npass
  write(6,"(3x,a,i5)") "# failed: ", nfail
  write(6,"(3x,a,i5)") "# failed malloc: ", nfail_malloc
  write(6,"()")

end program test_memset
