!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to perform memory deallocation on the device
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_malloc_free.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_malloc) devxlib_malloc_free

   implicit none

   contains
!
      module subroutine sp_devxlib_malloc_free_r1d(dev_ptr, device_id)
          implicit none
          !
          real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_r1d
      !
!
      module subroutine sp_devxlib_malloc_free_r2d(dev_ptr, device_id)
          implicit none
          !
          real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_r2d
      !
!
      module subroutine sp_devxlib_malloc_free_r3d(dev_ptr, device_id)
          implicit none
          !
          real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_r3d
      !
!
      module subroutine sp_devxlib_malloc_free_r4d(dev_ptr, device_id)
          implicit none
          !
          real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_r4d
      !
!
      module subroutine sp_devxlib_malloc_free_r5d(dev_ptr, device_id)
          implicit none
          !
          real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_r5d
      !
!
      module subroutine sp_devxlib_malloc_free_r6d(dev_ptr, device_id)
          implicit none
          !
          real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_r6d
      !
!
      module subroutine dp_devxlib_malloc_free_r1d(dev_ptr, device_id)
          implicit none
          !
          real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_r1d
      !
!
      module subroutine dp_devxlib_malloc_free_r2d(dev_ptr, device_id)
          implicit none
          !
          real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_r2d
      !
!
      module subroutine dp_devxlib_malloc_free_r3d(dev_ptr, device_id)
          implicit none
          !
          real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_r3d
      !
!
      module subroutine dp_devxlib_malloc_free_r4d(dev_ptr, device_id)
          implicit none
          !
          real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_r4d
      !
!
      module subroutine dp_devxlib_malloc_free_r5d(dev_ptr, device_id)
          implicit none
          !
          real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_r5d
      !
!
      module subroutine dp_devxlib_malloc_free_r6d(dev_ptr, device_id)
          implicit none
          !
          real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_r6d
      !
!
      module subroutine sp_devxlib_malloc_free_c1d(dev_ptr, device_id)
          implicit none
          !
          complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_c1d
      !
!
      module subroutine sp_devxlib_malloc_free_c2d(dev_ptr, device_id)
          implicit none
          !
          complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_c2d
      !
!
      module subroutine sp_devxlib_malloc_free_c3d(dev_ptr, device_id)
          implicit none
          !
          complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_c3d
      !
!
      module subroutine sp_devxlib_malloc_free_c4d(dev_ptr, device_id)
          implicit none
          !
          complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_c4d
      !
!
      module subroutine sp_devxlib_malloc_free_c5d(dev_ptr, device_id)
          implicit none
          !
          complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_c5d
      !
!
      module subroutine sp_devxlib_malloc_free_c6d(dev_ptr, device_id)
          implicit none
          !
          complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine sp_devxlib_malloc_free_c6d
      !
!
      module subroutine dp_devxlib_malloc_free_c1d(dev_ptr, device_id)
          implicit none
          !
          complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_c1d
      !
!
      module subroutine dp_devxlib_malloc_free_c2d(dev_ptr, device_id)
          implicit none
          !
          complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_c2d
      !
!
      module subroutine dp_devxlib_malloc_free_c3d(dev_ptr, device_id)
          implicit none
          !
          complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_c3d
      !
!
      module subroutine dp_devxlib_malloc_free_c4d(dev_ptr, device_id)
          implicit none
          !
          complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_c4d
      !
!
      module subroutine dp_devxlib_malloc_free_c5d(dev_ptr, device_id)
          implicit none
          !
          complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_c5d
      !
!
      module subroutine dp_devxlib_malloc_free_c6d(dev_ptr, device_id)
          implicit none
          !
          complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine dp_devxlib_malloc_free_c6d
      !
!
      module subroutine i4_devxlib_malloc_free_i1d(dev_ptr, device_id)
          implicit none
          !
          integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i4_devxlib_malloc_free_i1d
      !
!
      module subroutine i4_devxlib_malloc_free_i2d(dev_ptr, device_id)
          implicit none
          !
          integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i4_devxlib_malloc_free_i2d
      !
!
      module subroutine i4_devxlib_malloc_free_i3d(dev_ptr, device_id)
          implicit none
          !
          integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i4_devxlib_malloc_free_i3d
      !
!
      module subroutine i4_devxlib_malloc_free_i4d(dev_ptr, device_id)
          implicit none
          !
          integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i4_devxlib_malloc_free_i4d
      !
!
      module subroutine i4_devxlib_malloc_free_i5d(dev_ptr, device_id)
          implicit none
          !
          integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i4_devxlib_malloc_free_i5d
      !
!
      module subroutine i4_devxlib_malloc_free_i6d(dev_ptr, device_id)
          implicit none
          !
          integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i4_devxlib_malloc_free_i6d
      !
!
      module subroutine i8_devxlib_malloc_free_i1d(dev_ptr, device_id)
          implicit none
          !
          integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i8_devxlib_malloc_free_i1d
      !
!
      module subroutine i8_devxlib_malloc_free_i2d(dev_ptr, device_id)
          implicit none
          !
          integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i8_devxlib_malloc_free_i2d
      !
!
      module subroutine i8_devxlib_malloc_free_i3d(dev_ptr, device_id)
          implicit none
          !
          integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i8_devxlib_malloc_free_i3d
      !
!
      module subroutine i8_devxlib_malloc_free_i4d(dev_ptr, device_id)
          implicit none
          !
          integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i8_devxlib_malloc_free_i4d
      !
!
      module subroutine i8_devxlib_malloc_free_i5d(dev_ptr, device_id)
          implicit none
          !
          integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i8_devxlib_malloc_free_i5d
      !
!
      module subroutine i8_devxlib_malloc_free_i6d(dev_ptr, device_id)
          implicit none
          !
          integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine i8_devxlib_malloc_free_i6d
      !
!
      module subroutine l4_devxlib_malloc_free_l1d(dev_ptr, device_id)
          implicit none
          !
          logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine l4_devxlib_malloc_free_l1d
      !
!
      module subroutine l4_devxlib_malloc_free_l2d(dev_ptr, device_id)
          implicit none
          !
          logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine l4_devxlib_malloc_free_l2d
      !
!
      module subroutine l4_devxlib_malloc_free_l3d(dev_ptr, device_id)
          implicit none
          !
          logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine l4_devxlib_malloc_free_l3d
      !
!
      module subroutine l4_devxlib_malloc_free_l4d(dev_ptr, device_id)
          implicit none
          !
          logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine l4_devxlib_malloc_free_l4d
      !
!
      module subroutine l4_devxlib_malloc_free_l5d(dev_ptr, device_id)
          implicit none
          !
          logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine l4_devxlib_malloc_free_l5d
      !
!
      module subroutine l4_devxlib_malloc_free_l6d(dev_ptr, device_id)
          implicit none
          !
          logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          integer, optional, intent(in)  :: device_id
#if defined __DXL_CUDAF
          integer :: istat
          integer(kind=c_int) :: info
#endif
#if defined __DXL_OPENMP_GPU
          integer :: device_id_local
#endif
          !
#if defined __DXL_CUDAF
          if (present(device_id)) istat = cudaSetDevice(device_id)
#elif defined __DXL_OPENMP_GPU
          if (present(device_id)) then
             device_id_local= device_id
          else
             device_id_local = omp_get_default_device()
          endif
#endif
          !
#if defined __DXL_CUDAF || defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
          if (associated(dev_ptr)) then
#if defined __DXL_CUDAF
             info = cudaFree_f(c_loc(dev_ptr))
#elif defined __DXL_OPENACC
             call acc_free_f(c_loc(dev_ptr))
#elif defined __DXL_OPENMP_GPU
             call omp_target_free(c_loc(dev_ptr), device_id_local)
#endif
             nullify(dev_ptr)
          endif
#else
          if (associated(dev_ptr)) deallocate( dev_ptr)
#endif
          !
      end subroutine l4_devxlib_malloc_free_l6d
      !

endsubmodule devxlib_malloc_free