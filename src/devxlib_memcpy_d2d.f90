!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
!
! Utility functions to perform sync and async device-device memcpy
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_memcpy_d2d.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
! Note about dimensions:
! The lower bound of the assumed shape array passed to the subroutine is 1
! lbound and range instead refer to the indexing in the parent caller.
!
submodule (devxlib_memcpy) devxlib_memcpy_d2d

   implicit none

   contains
      module subroutine sp_devxlib_memcpy_d2d_r1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         !DEV_OMP  parallel do
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_r1d
!
      module subroutine sp_devxlib_memcpy_d2d_r2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         !DEV_OMP  parallel do
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_r2d
!
      module subroutine sp_devxlib_memcpy_d2d_r3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         !DEV_OMP  parallel do
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_r3d
!
      module subroutine sp_devxlib_memcpy_d2d_r4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         !DEV_OMP  parallel do
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_r4d
!
      module subroutine sp_devxlib_memcpy_d2d_r5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         !DEV_OMP  parallel do
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_r5d
!
      module subroutine sp_devxlib_memcpy_d2d_r6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         !DEV_OMP  parallel do
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_r6d
!
      module subroutine dp_devxlib_memcpy_d2d_r1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         !DEV_OMP  parallel do
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_r1d
!
      module subroutine dp_devxlib_memcpy_d2d_r2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         !DEV_OMP  parallel do
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_r2d
!
      module subroutine dp_devxlib_memcpy_d2d_r3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         !DEV_OMP  parallel do
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_r3d
!
      module subroutine dp_devxlib_memcpy_d2d_r4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         !DEV_OMP  parallel do
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_r4d
!
      module subroutine dp_devxlib_memcpy_d2d_r5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         !DEV_OMP  parallel do
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_r5d
!
      module subroutine dp_devxlib_memcpy_d2d_r6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         !DEV_OMP  parallel do
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_r6d
!
      module subroutine sp_devxlib_memcpy_d2d_c1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         !DEV_OMP  parallel do
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_c1d
!
      module subroutine sp_devxlib_memcpy_d2d_c2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         !DEV_OMP  parallel do
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_c2d
!
      module subroutine sp_devxlib_memcpy_d2d_c3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         !DEV_OMP  parallel do
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_c3d
!
      module subroutine sp_devxlib_memcpy_d2d_c4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         !DEV_OMP  parallel do
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_c4d
!
      module subroutine sp_devxlib_memcpy_d2d_c5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         !DEV_OMP  parallel do
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_c5d
!
      module subroutine sp_devxlib_memcpy_d2d_c6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         !DEV_OMP  parallel do
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_c6d
!
      module subroutine dp_devxlib_memcpy_d2d_c1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         !DEV_OMP  parallel do
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_c1d
!
      module subroutine dp_devxlib_memcpy_d2d_c2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         !DEV_OMP  parallel do
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_c2d
!
      module subroutine dp_devxlib_memcpy_d2d_c3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         !DEV_OMP  parallel do
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_c3d
!
      module subroutine dp_devxlib_memcpy_d2d_c4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         !DEV_OMP  parallel do
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_c4d
!
      module subroutine dp_devxlib_memcpy_d2d_c5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         !DEV_OMP  parallel do
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_c5d
!
      module subroutine dp_devxlib_memcpy_d2d_c6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         !DEV_OMP  parallel do
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_c6d
!
      module subroutine i4_devxlib_memcpy_d2d_i1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         !DEV_OMP  parallel do
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_i1d
!
      module subroutine i4_devxlib_memcpy_d2d_i2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         !DEV_OMP  parallel do
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_i2d
!
      module subroutine i4_devxlib_memcpy_d2d_i3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         !DEV_OMP  parallel do
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_i3d
!
      module subroutine i4_devxlib_memcpy_d2d_i4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         !DEV_OMP  parallel do
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_i4d
!
      module subroutine i4_devxlib_memcpy_d2d_i5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         !DEV_OMP  parallel do
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_i5d
!
      module subroutine i4_devxlib_memcpy_d2d_i6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         !DEV_OMP  parallel do
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_i6d
!
      module subroutine i8_devxlib_memcpy_d2d_i1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         !DEV_OMP  parallel do
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_i1d
!
      module subroutine i8_devxlib_memcpy_d2d_i2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         !DEV_OMP  parallel do
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_i2d
!
      module subroutine i8_devxlib_memcpy_d2d_i3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         !DEV_OMP  parallel do
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_i3d
!
      module subroutine i8_devxlib_memcpy_d2d_i4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         !DEV_OMP  parallel do
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_i4d
!
      module subroutine i8_devxlib_memcpy_d2d_i5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         !DEV_OMP  parallel do
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_i5d
!
      module subroutine i8_devxlib_memcpy_d2d_i6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         !DEV_OMP  parallel do
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_i6d
!
      module subroutine l4_devxlib_memcpy_d2d_l1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         !DEV_OMP  parallel do
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_l1d
!
      module subroutine l4_devxlib_memcpy_d2d_l2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         !DEV_OMP  parallel do
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_l2d
!
      module subroutine l4_devxlib_memcpy_d2d_l3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         !DEV_OMP  parallel do
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_l3d
!
      module subroutine l4_devxlib_memcpy_d2d_l4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         !DEV_OMP  parallel do
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_l4d
!
      module subroutine l4_devxlib_memcpy_d2d_l5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         !DEV_OMP  parallel do
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_l5d
!
      module subroutine l4_devxlib_memcpy_d2d_l6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6)
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         !DEV_OMP  parallel do
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_l6d
!
      module subroutine sp_devxlib_memcpy_d2d_async_r1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_r1d
!
      module subroutine sp_devxlib_memcpy_d2d_async_r2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_r2d
!
      module subroutine sp_devxlib_memcpy_d2d_async_r3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_r3d
!
      module subroutine sp_devxlib_memcpy_d2d_async_r4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_r4d
!
      module subroutine sp_devxlib_memcpy_d2d_async_r5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_r5d
!
      module subroutine sp_devxlib_memcpy_d2d_async_r6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_r6d
!
      module subroutine dp_devxlib_memcpy_d2d_async_r1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_r1d
!
      module subroutine dp_devxlib_memcpy_d2d_async_r2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_r2d
!
      module subroutine dp_devxlib_memcpy_d2d_async_r3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_r3d
!
      module subroutine dp_devxlib_memcpy_d2d_async_r4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_r4d
!
      module subroutine dp_devxlib_memcpy_d2d_async_r5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_r5d
!
      module subroutine dp_devxlib_memcpy_d2d_async_r6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_r6d
!
      module subroutine sp_devxlib_memcpy_d2d_async_c1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_c1d
!
      module subroutine sp_devxlib_memcpy_d2d_async_c2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_c2d
!
      module subroutine sp_devxlib_memcpy_d2d_async_c3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_c3d
!
      module subroutine sp_devxlib_memcpy_d2d_async_c4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_c4d
!
      module subroutine sp_devxlib_memcpy_d2d_async_c5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_c5d
!
      module subroutine sp_devxlib_memcpy_d2d_async_c6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine sp_devxlib_memcpy_d2d_async_c6d
!
      module subroutine dp_devxlib_memcpy_d2d_async_c1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_c1d
!
      module subroutine dp_devxlib_memcpy_d2d_async_c2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_c2d
!
      module subroutine dp_devxlib_memcpy_d2d_async_c3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_c3d
!
      module subroutine dp_devxlib_memcpy_d2d_async_c4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_c4d
!
      module subroutine dp_devxlib_memcpy_d2d_async_c5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_c5d
!
      module subroutine dp_devxlib_memcpy_d2d_async_c6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine dp_devxlib_memcpy_d2d_async_c6d
!
      module subroutine i4_devxlib_memcpy_d2d_async_i1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_async_i1d
!
      module subroutine i4_devxlib_memcpy_d2d_async_i2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_async_i2d
!
      module subroutine i4_devxlib_memcpy_d2d_async_i3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_async_i3d
!
      module subroutine i4_devxlib_memcpy_d2d_async_i4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_async_i4d
!
      module subroutine i4_devxlib_memcpy_d2d_async_i5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_async_i5d
!
      module subroutine i4_devxlib_memcpy_d2d_async_i6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i4_devxlib_memcpy_d2d_async_i6d
!
      module subroutine i8_devxlib_memcpy_d2d_async_i1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_async_i1d
!
      module subroutine i8_devxlib_memcpy_d2d_async_i2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_async_i2d
!
      module subroutine i8_devxlib_memcpy_d2d_async_i3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_async_i3d
!
      module subroutine i8_devxlib_memcpy_d2d_async_i4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_async_i4d
!
      module subroutine i8_devxlib_memcpy_d2d_async_i5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_async_i5d
!
      module subroutine i8_devxlib_memcpy_d2d_async_i6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine i8_devxlib_memcpy_d2d_async_i6d
!
      module subroutine l4_devxlib_memcpy_d2d_async_l1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         !DEV_CUF kernel do(1) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(1) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(1)
         do i1 = d1s, d1e
            array_out(i1 ) = array_in(i1 )
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_async_l1d
!
      module subroutine l4_devxlib_memcpy_d2d_async_l2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         !DEV_CUF kernel do(2) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(2) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = array_in(i1,i2 )
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_async_l2d
!
      module subroutine l4_devxlib_memcpy_d2d_async_l3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         !DEV_CUF kernel do(3) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(3) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_async_l3d
!
      module subroutine l4_devxlib_memcpy_d2d_async_l4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         !DEV_CUF kernel do(4) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(4) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_async_l4d
!
      module subroutine l4_devxlib_memcpy_d2d_async_l5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         !DEV_CUF kernel do(5) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(5) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_async_l5d
!
      module subroutine l4_devxlib_memcpy_d2d_async_l6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         !
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         !DEV_CUF kernel do(6) <<<*,*,0,stream=async_id>>>
         !DEV_ACC data present(array_out, array_in)
         !DEV_ACC parallel loop collapse(6) async(async_id)
         !DEV_OMPGPU target map(present,alloc:array_out, array_in)
         !DEV_OMPGPU teams loop collapse(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !DEV_ACC end data
         !DEV_OMPGPU end target
         !
      end subroutine l4_devxlib_memcpy_d2d_async_l6d
!
      module subroutine sp_devxlib_memcpy_d2d_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_r1d_p
!
      module subroutine sp_devxlib_memcpy_d2d_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_r2d_p
!
      module subroutine sp_devxlib_memcpy_d2d_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_r3d_p
!
      module subroutine sp_devxlib_memcpy_d2d_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_r4d_p
!
      module subroutine sp_devxlib_memcpy_d2d_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_r5d_p
!
      module subroutine sp_devxlib_memcpy_d2d_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_r6d_p
!
      module subroutine dp_devxlib_memcpy_d2d_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_r1d_p
!
      module subroutine dp_devxlib_memcpy_d2d_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_r2d_p
!
      module subroutine dp_devxlib_memcpy_d2d_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_r3d_p
!
      module subroutine dp_devxlib_memcpy_d2d_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_r4d_p
!
      module subroutine dp_devxlib_memcpy_d2d_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_r5d_p
!
      module subroutine dp_devxlib_memcpy_d2d_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_r6d_p
!
      module subroutine sp_devxlib_memcpy_d2d_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_c1d_p
!
      module subroutine sp_devxlib_memcpy_d2d_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_c2d_p
!
      module subroutine sp_devxlib_memcpy_d2d_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_c3d_p
!
      module subroutine sp_devxlib_memcpy_d2d_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_c4d_p
!
      module subroutine sp_devxlib_memcpy_d2d_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_c5d_p
!
      module subroutine sp_devxlib_memcpy_d2d_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine sp_devxlib_memcpy_d2d_c6d_p
!
      module subroutine dp_devxlib_memcpy_d2d_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_c1d_p
!
      module subroutine dp_devxlib_memcpy_d2d_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_c2d_p
!
      module subroutine dp_devxlib_memcpy_d2d_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_c3d_p
!
      module subroutine dp_devxlib_memcpy_d2d_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_c4d_p
!
      module subroutine dp_devxlib_memcpy_d2d_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_c5d_p
!
      module subroutine dp_devxlib_memcpy_d2d_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine dp_devxlib_memcpy_d2d_c6d_p
!
      module subroutine i4_devxlib_memcpy_d2d_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i4_devxlib_memcpy_d2d_i1d_p
!
      module subroutine i4_devxlib_memcpy_d2d_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i4_devxlib_memcpy_d2d_i2d_p
!
      module subroutine i4_devxlib_memcpy_d2d_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i4_devxlib_memcpy_d2d_i3d_p
!
      module subroutine i4_devxlib_memcpy_d2d_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i4_devxlib_memcpy_d2d_i4d_p
!
      module subroutine i4_devxlib_memcpy_d2d_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i4_devxlib_memcpy_d2d_i5d_p
!
      module subroutine i4_devxlib_memcpy_d2d_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i4_devxlib_memcpy_d2d_i6d_p
!
      module subroutine i8_devxlib_memcpy_d2d_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i8_devxlib_memcpy_d2d_i1d_p
!
      module subroutine i8_devxlib_memcpy_d2d_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i8_devxlib_memcpy_d2d_i2d_p
!
      module subroutine i8_devxlib_memcpy_d2d_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i8_devxlib_memcpy_d2d_i3d_p
!
      module subroutine i8_devxlib_memcpy_d2d_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i8_devxlib_memcpy_d2d_i4d_p
!
      module subroutine i8_devxlib_memcpy_d2d_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i8_devxlib_memcpy_d2d_i5d_p
!
      module subroutine i8_devxlib_memcpy_d2d_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine i8_devxlib_memcpy_d2d_i6d_p
!
      module subroutine l4_devxlib_memcpy_d2d_l1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine l4_devxlib_memcpy_d2d_l1d_p
!
      module subroutine l4_devxlib_memcpy_d2d_l2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine l4_devxlib_memcpy_d2d_l2d_p
!
      module subroutine l4_devxlib_memcpy_d2d_l3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine l4_devxlib_memcpy_d2d_l3d_p
!
      module subroutine l4_devxlib_memcpy_d2d_l4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine l4_devxlib_memcpy_d2d_l4d_p
!
      module subroutine l4_devxlib_memcpy_d2d_l5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine l4_devxlib_memcpy_d2d_l5d_p
!
      module subroutine l4_devxlib_memcpy_d2d_l6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         !
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
         integer                       :: device_id_out_local, device_id_in_local
#endif
         !
#if defined __DXL_CUDAF || defined __DXL_OPENMP_GPU
         integer :: ierr = 0
#endif
         !
#if defined __DXL_CUDAF
         ierr = cudaMemcpy_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                             size(array_out,kind=int64) / 8_int64,c_size_t), cudaMemcpyDeviceToDevice)
#elif defined __DXL_OPENACC
         call acc_memcpy_device_f(c_loc(array_out), c_loc(array_in), int(storage_size(array_out,kind=int64) * &
                                  size(array_out,kind=int64) / 8_int64, c_size_t))
#elif defined __DXL_OPENMP_GPU
         if (present(device_id_out)) then
            device_id_out_local = device_id_out
         else
            device_id_out_local = omp_get_default_device()
         endif
         if (present(device_id_in)) then
            device_id_in_local = device_id_in
         else
            device_id_in_local = omp_get_default_device()
         endif
         ierr = int( omp_target_memcpy(c_loc(array_out), c_loc(array_in), &
                     int(storage_size(array_out,kind=int64) *size(array_out,kind=int64) / 8_int64, c_size_t), &
                     int(0,c_size_t), int(0,c_size_t), device_id_out_local, device_id_in_local), kind=int32)
#else
         ! host2host fall-back
         array_out=array_in
#endif
         !
      end subroutine l4_devxlib_memcpy_d2d_l6d_p
!
endsubmodule devxlib_memcpy_d2d