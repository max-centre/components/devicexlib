!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Testing: devxlib_memcpy, devxlib_mapping, dev_auxfunc
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from test_mapping.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
program test_mapping
  !
  ! This program tests the routines and interfaces related to
  ! device_memcpy and some device_auxfunc.
  !
  use iso_fortran_env, only : int32, int64, real32, real64
  use devxlib_memcpy
  use devxlib_mapping
  use devxlib_auxfunc

  implicit none

  integer(int64) :: cnt, count_max
  real(real64), parameter :: thr=1.0d-6
  integer,      parameter :: nranks=4
  integer(int64) :: ndim(nranks)
  integer(int64) :: bounds(2*nranks)
  real(real64):: t0, t1, count_rate
  logical :: is_mapped, lfail_mapping
  integer :: npass, nfail
  integer(int32) :: ierr
  integer :: nfail_mapping
  real(real32), allocatable :: A_hst1__sp_r1d(:)
  real(real32), allocatable :: A_hst2__sp_r1d(:)
  real(real32), allocatable :: A_hst3__sp_r1d(:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r1d(:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r1d(:)
  

  real(real32), allocatable :: A_hst1__sp_r2d(:,:)
  real(real32), allocatable :: A_hst2__sp_r2d(:,:)
  real(real32), allocatable :: A_hst3__sp_r2d(:,:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r2d(:,:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r2d(:,:)
  

  real(real32), allocatable :: A_hst1__sp_r3d(:,:,:)
  real(real32), allocatable :: A_hst2__sp_r3d(:,:,:)
  real(real32), allocatable :: A_hst3__sp_r3d(:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r3d(:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r3d(:,:,:)
  

  real(real32), allocatable :: A_hst1__sp_r4d(:,:,:,:)
  real(real32), allocatable :: A_hst2__sp_r4d(:,:,:,:)
  real(real32), allocatable :: A_hst3__sp_r4d(:,:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r4d(:,:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r4d(:,:,:,:)
  


  real(real64), allocatable :: A_hst1__dp_r1d(:)
  real(real64), allocatable :: A_hst2__dp_r1d(:)
  real(real64), allocatable :: A_hst3__dp_r1d(:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r1d(:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r1d(:)
  

  real(real64), allocatable :: A_hst1__dp_r2d(:,:)
  real(real64), allocatable :: A_hst2__dp_r2d(:,:)
  real(real64), allocatable :: A_hst3__dp_r2d(:,:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r2d(:,:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r2d(:,:)
  

  real(real64), allocatable :: A_hst1__dp_r3d(:,:,:)
  real(real64), allocatable :: A_hst2__dp_r3d(:,:,:)
  real(real64), allocatable :: A_hst3__dp_r3d(:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r3d(:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r3d(:,:,:)
  

  real(real64), allocatable :: A_hst1__dp_r4d(:,:,:,:)
  real(real64), allocatable :: A_hst2__dp_r4d(:,:,:,:)
  real(real64), allocatable :: A_hst3__dp_r4d(:,:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r4d(:,:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r4d(:,:,:,:)
  



  complex(real32), allocatable :: A_hst1__sp_c1d(:)
  complex(real32), allocatable :: A_hst2__sp_c1d(:)
  complex(real32), allocatable :: A_hst3__sp_c1d(:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c1d(:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c1d(:)
  
  real(real32), allocatable :: A_rtmp__sp_1d(:)
  

  complex(real32), allocatable :: A_hst1__sp_c2d(:,:)
  complex(real32), allocatable :: A_hst2__sp_c2d(:,:)
  complex(real32), allocatable :: A_hst3__sp_c2d(:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c2d(:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c2d(:,:)
  
  real(real32), allocatable :: A_rtmp__sp_2d(:,:)
  

  complex(real32), allocatable :: A_hst1__sp_c3d(:,:,:)
  complex(real32), allocatable :: A_hst2__sp_c3d(:,:,:)
  complex(real32), allocatable :: A_hst3__sp_c3d(:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c3d(:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c3d(:,:,:)
  
  real(real32), allocatable :: A_rtmp__sp_3d(:,:,:)
  

  complex(real32), allocatable :: A_hst1__sp_c4d(:,:,:,:)
  complex(real32), allocatable :: A_hst2__sp_c4d(:,:,:,:)
  complex(real32), allocatable :: A_hst3__sp_c4d(:,:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c4d(:,:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c4d(:,:,:,:)
  
  real(real32), allocatable :: A_rtmp__sp_4d(:,:,:,:)
  


  complex(real64), allocatable :: A_hst1__dp_c1d(:)
  complex(real64), allocatable :: A_hst2__dp_c1d(:)
  complex(real64), allocatable :: A_hst3__dp_c1d(:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c1d(:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c1d(:)
  
  real(real64), allocatable :: A_rtmp__dp_1d(:)
  

  complex(real64), allocatable :: A_hst1__dp_c2d(:,:)
  complex(real64), allocatable :: A_hst2__dp_c2d(:,:)
  complex(real64), allocatable :: A_hst3__dp_c2d(:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c2d(:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c2d(:,:)
  
  real(real64), allocatable :: A_rtmp__dp_2d(:,:)
  

  complex(real64), allocatable :: A_hst1__dp_c3d(:,:,:)
  complex(real64), allocatable :: A_hst2__dp_c3d(:,:,:)
  complex(real64), allocatable :: A_hst3__dp_c3d(:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c3d(:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c3d(:,:,:)
  
  real(real64), allocatable :: A_rtmp__dp_3d(:,:,:)
  

  complex(real64), allocatable :: A_hst1__dp_c4d(:,:,:,:)
  complex(real64), allocatable :: A_hst2__dp_c4d(:,:,:,:)
  complex(real64), allocatable :: A_hst3__dp_c4d(:,:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c4d(:,:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c4d(:,:,:,:)
  
  real(real64), allocatable :: A_rtmp__dp_4d(:,:,:,:)
  



  integer(int32), allocatable :: A_hst1__i4_i1d(:)
  integer(int32), allocatable :: A_hst2__i4_i1d(:)
  integer(int32), allocatable :: A_hst3__i4_i1d(:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i1d(:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i1d(:)
  
  real(int32), allocatable :: A_rtmp__i4_1d(:)
  

  integer(int32), allocatable :: A_hst1__i4_i2d(:,:)
  integer(int32), allocatable :: A_hst2__i4_i2d(:,:)
  integer(int32), allocatable :: A_hst3__i4_i2d(:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i2d(:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i2d(:,:)
  
  real(int32), allocatable :: A_rtmp__i4_2d(:,:)
  

  integer(int32), allocatable :: A_hst1__i4_i3d(:,:,:)
  integer(int32), allocatable :: A_hst2__i4_i3d(:,:,:)
  integer(int32), allocatable :: A_hst3__i4_i3d(:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i3d(:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i3d(:,:,:)
  
  real(int32), allocatable :: A_rtmp__i4_3d(:,:,:)
  

  integer(int32), allocatable :: A_hst1__i4_i4d(:,:,:,:)
  integer(int32), allocatable :: A_hst2__i4_i4d(:,:,:,:)
  integer(int32), allocatable :: A_hst3__i4_i4d(:,:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i4d(:,:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i4d(:,:,:,:)
  
  real(int32), allocatable :: A_rtmp__i4_4d(:,:,:,:)
  


  integer(int64), allocatable :: A_hst1__i8_i1d(:)
  integer(int64), allocatable :: A_hst2__i8_i1d(:)
  integer(int64), allocatable :: A_hst3__i8_i1d(:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i1d(:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i1d(:)
  
  real(int64), allocatable :: A_rtmp__i8_1d(:)
  

  integer(int64), allocatable :: A_hst1__i8_i2d(:,:)
  integer(int64), allocatable :: A_hst2__i8_i2d(:,:)
  integer(int64), allocatable :: A_hst3__i8_i2d(:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i2d(:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i2d(:,:)
  
  real(int64), allocatable :: A_rtmp__i8_2d(:,:)
  

  integer(int64), allocatable :: A_hst1__i8_i3d(:,:,:)
  integer(int64), allocatable :: A_hst2__i8_i3d(:,:,:)
  integer(int64), allocatable :: A_hst3__i8_i3d(:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i3d(:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i3d(:,:,:)
  
  real(int64), allocatable :: A_rtmp__i8_3d(:,:,:)
  

  integer(int64), allocatable :: A_hst1__i8_i4d(:,:,:,:)
  integer(int64), allocatable :: A_hst2__i8_i4d(:,:,:,:)
  integer(int64), allocatable :: A_hst3__i8_i4d(:,:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i4d(:,:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i4d(:,:,:,:)
  
  real(int64), allocatable :: A_rtmp__i8_4d(:,:,:,:)
  



  logical(int32), allocatable :: A_hst1__l4_l1d(:)
  logical(int32), allocatable :: A_hst2__l4_l1d(:)
  logical(int32), allocatable :: A_hst3__l4_l1d(:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l1d(:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l1d(:)
  
  real(int32), allocatable :: A_rtmp__l4_1d(:)
  

  logical(int32), allocatable :: A_hst1__l4_l2d(:,:)
  logical(int32), allocatable :: A_hst2__l4_l2d(:,:)
  logical(int32), allocatable :: A_hst3__l4_l2d(:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l2d(:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l2d(:,:)
  
  real(int32), allocatable :: A_rtmp__l4_2d(:,:)
  

  logical(int32), allocatable :: A_hst1__l4_l3d(:,:,:)
  logical(int32), allocatable :: A_hst2__l4_l3d(:,:,:)
  logical(int32), allocatable :: A_hst3__l4_l3d(:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l3d(:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l3d(:,:,:)
  
  real(int32), allocatable :: A_rtmp__l4_3d(:,:,:)
  

  logical(int32), allocatable :: A_hst1__l4_l4d(:,:,:,:)
  logical(int32), allocatable :: A_hst2__l4_l4d(:,:,:,:)
  logical(int32), allocatable :: A_hst3__l4_l4d(:,:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l4d(:,:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l4d(:,:,:,:)
  
  real(int32), allocatable :: A_rtmp__l4_4d(:,:,:,:)
  



  
  integer :: i
  character(256) :: arg, str

!
!============================
! get dims
!============================
!
  ! defaults
  ndim(:)=100
  do i=1,2*nranks,2
     bounds(i)   = 1
     bounds(i+1) = ndim((i+1)/2)
  enddo

  i=0
  do
    call get_command_argument(i, arg)
    if (len_trim(arg) == 0) exit
    !
    select case (trim(arg))
    case("-h","--help")
      write(6,"(a)") "Usage: "
      write(6,"(a)") "   ./test_mapping.x [--dims <vals>] [--bounds <vals>]"
      stop
    end select
    !
    i = i+1
    call get_command_argument(i, str)
    if (len_trim(str) == 0) exit
    !
    select case (trim(arg))
    case("-dims","--dims")
      read(str,*,iostat=ierr) ndim(:)
      if (ierr/=0) STOP "reading cmd-line args: dims"
    case("-bounds","--bounds")
      read(str,*,iostat=ierr) bounds(:)
      if (ierr/=0) STOP "reading cmd-line args: bounds"
    end select
  enddo
  !
  write(6,"(/,a,/)") "Running test with params: "
  write(6,"(3x,a,10i5)") "  ndim: ", ndim(:)
  write(6,"(3x,a,10i5)") "bounds: ", bounds(:)
  write(6,"()")
  !
  npass=0
  nfail=0
  nfail_mapping=0

!
!============================
! check memcpy
!============================
!
  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r1d ) .or. &
              devxlib_mapped( A_dev2__sp_r1d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_r1d(bounds(1):bounds(2)) )
  allocate( A_hst2__sp_r1d(bounds(1):bounds(2)) )
  allocate( A_hst3__sp_r1d(bounds(1):bounds(2)) )
  allocate( A_dev1__sp_r1d(bounds(1):bounds(2)) )
  allocate( A_dev2__sp_r1d(bounds(1):bounds(2)) )
  call devxlib_map(A_dev1__sp_r1d)
  call devxlib_map(A_dev2__sp_r1d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r1d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__sp_r1d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r1d, A_hst1__sp_r1d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_r1d, A_dev1__sp_r1d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r1d, A_dev2__sp_r1d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r1d, A_hst2__sp_r1d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_r1d -A_hst1__sp_r1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r1d)
  deallocate(A_hst2__sp_r1d)
  deallocate(A_hst3__sp_r1d)
  !
  call devxlib_unmap(A_dev1__sp_r1d, ierr)
  call devxlib_unmap(A_dev2__sp_r1d, ierr)
  deallocate(A_dev1__sp_r1d)
  deallocate(A_dev2__sp_r1d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r1d ) .or. &
              devxlib_mapped( A_dev2__sp_r1d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r2d ) .or. &
              devxlib_mapped( A_dev2__sp_r2d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst2__sp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst3__sp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev1__sp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev2__sp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  call devxlib_map(A_dev1__sp_r2d)
  call devxlib_map(A_dev2__sp_r2d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r2d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__sp_r2d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r2d, A_hst1__sp_r2d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_r2d, A_dev1__sp_r2d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r2d, A_dev2__sp_r2d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r2d, A_hst2__sp_r2d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_r2d -A_hst1__sp_r2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r2d)
  deallocate(A_hst2__sp_r2d)
  deallocate(A_hst3__sp_r2d)
  !
  call devxlib_unmap(A_dev1__sp_r2d, ierr)
  call devxlib_unmap(A_dev2__sp_r2d, ierr)
  deallocate(A_dev1__sp_r2d)
  deallocate(A_dev2__sp_r2d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r2d ) .or. &
              devxlib_mapped( A_dev2__sp_r2d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r3d ) .or. &
              devxlib_mapped( A_dev2__sp_r3d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst2__sp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst3__sp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev1__sp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev2__sp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  call devxlib_map(A_dev1__sp_r3d)
  call devxlib_map(A_dev2__sp_r3d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r3d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__sp_r3d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r3d, A_hst1__sp_r3d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_r3d, A_dev1__sp_r3d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r3d, A_dev2__sp_r3d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r3d, A_hst2__sp_r3d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_r3d -A_hst1__sp_r3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r3d)
  deallocate(A_hst2__sp_r3d)
  deallocate(A_hst3__sp_r3d)
  !
  call devxlib_unmap(A_dev1__sp_r3d, ierr)
  call devxlib_unmap(A_dev2__sp_r3d, ierr)
  deallocate(A_dev1__sp_r3d)
  deallocate(A_dev2__sp_r3d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r3d ) .or. &
              devxlib_mapped( A_dev2__sp_r3d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_r4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r4d ) .or. &
              devxlib_mapped( A_dev2__sp_r4d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst2__sp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst3__sp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev1__sp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev2__sp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  call devxlib_map(A_dev1__sp_r4d)
  call devxlib_map(A_dev2__sp_r4d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r4d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__sp_r4d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r4d, A_hst1__sp_r4d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_r4d, A_dev1__sp_r4d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r4d, A_dev2__sp_r4d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r4d, A_hst2__sp_r4d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_r4d -A_hst1__sp_r4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r4d)
  deallocate(A_hst2__sp_r4d)
  deallocate(A_hst3__sp_r4d)
  !
  call devxlib_unmap(A_dev1__sp_r4d, ierr)
  call devxlib_unmap(A_dev2__sp_r4d, ierr)
  deallocate(A_dev1__sp_r4d)
  deallocate(A_dev2__sp_r4d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_r4d ) .or. &
              devxlib_mapped( A_dev2__sp_r4d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r1d ) .or. &
              devxlib_mapped( A_dev2__dp_r1d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_r1d(bounds(1):bounds(2)) )
  allocate( A_hst2__dp_r1d(bounds(1):bounds(2)) )
  allocate( A_hst3__dp_r1d(bounds(1):bounds(2)) )
  allocate( A_dev1__dp_r1d(bounds(1):bounds(2)) )
  allocate( A_dev2__dp_r1d(bounds(1):bounds(2)) )
  call devxlib_map(A_dev1__dp_r1d)
  call devxlib_map(A_dev2__dp_r1d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r1d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__dp_r1d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r1d, A_hst1__dp_r1d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_r1d, A_dev1__dp_r1d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r1d, A_dev2__dp_r1d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r1d, A_hst2__dp_r1d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_r1d -A_hst1__dp_r1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r1d)
  deallocate(A_hst2__dp_r1d)
  deallocate(A_hst3__dp_r1d)
  !
  call devxlib_unmap(A_dev1__dp_r1d, ierr)
  call devxlib_unmap(A_dev2__dp_r1d, ierr)
  deallocate(A_dev1__dp_r1d)
  deallocate(A_dev2__dp_r1d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r1d ) .or. &
              devxlib_mapped( A_dev2__dp_r1d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r2d ) .or. &
              devxlib_mapped( A_dev2__dp_r2d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst2__dp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst3__dp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev1__dp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev2__dp_r2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  call devxlib_map(A_dev1__dp_r2d)
  call devxlib_map(A_dev2__dp_r2d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r2d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__dp_r2d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r2d, A_hst1__dp_r2d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_r2d, A_dev1__dp_r2d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r2d, A_dev2__dp_r2d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r2d, A_hst2__dp_r2d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_r2d -A_hst1__dp_r2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r2d)
  deallocate(A_hst2__dp_r2d)
  deallocate(A_hst3__dp_r2d)
  !
  call devxlib_unmap(A_dev1__dp_r2d, ierr)
  call devxlib_unmap(A_dev2__dp_r2d, ierr)
  deallocate(A_dev1__dp_r2d)
  deallocate(A_dev2__dp_r2d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r2d ) .or. &
              devxlib_mapped( A_dev2__dp_r2d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r3d ) .or. &
              devxlib_mapped( A_dev2__dp_r3d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst2__dp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst3__dp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev1__dp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev2__dp_r3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  call devxlib_map(A_dev1__dp_r3d)
  call devxlib_map(A_dev2__dp_r3d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r3d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__dp_r3d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r3d, A_hst1__dp_r3d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_r3d, A_dev1__dp_r3d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r3d, A_dev2__dp_r3d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r3d, A_hst2__dp_r3d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_r3d -A_hst1__dp_r3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r3d)
  deallocate(A_hst2__dp_r3d)
  deallocate(A_hst3__dp_r3d)
  !
  call devxlib_unmap(A_dev1__dp_r3d, ierr)
  call devxlib_unmap(A_dev2__dp_r3d, ierr)
  deallocate(A_dev1__dp_r3d)
  deallocate(A_dev2__dp_r3d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r3d ) .or. &
              devxlib_mapped( A_dev2__dp_r3d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_r4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r4d ) .or. &
              devxlib_mapped( A_dev2__dp_r4d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst2__dp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst3__dp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev1__dp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev2__dp_r4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  call devxlib_map(A_dev1__dp_r4d)
  call devxlib_map(A_dev2__dp_r4d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r4d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_hst1__dp_r4d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r4d, A_hst1__dp_r4d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_r4d, A_dev1__dp_r4d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r4d, A_dev2__dp_r4d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r4d, A_hst2__dp_r4d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_r4d -A_hst1__dp_r4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r4d)
  deallocate(A_hst2__dp_r4d)
  deallocate(A_hst3__dp_r4d)
  !
  call devxlib_unmap(A_dev1__dp_r4d, ierr)
  call devxlib_unmap(A_dev2__dp_r4d, ierr)
  deallocate(A_dev1__dp_r4d)
  deallocate(A_dev2__dp_r4d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_r4d ) .or. &
              devxlib_mapped( A_dev2__dp_r4d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c1d ) .or. &
              devxlib_mapped( A_dev2__sp_c1d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_c1d(bounds(1):bounds(2)) )
  allocate( A_hst2__sp_c1d(bounds(1):bounds(2)) )
  allocate( A_hst3__sp_c1d(bounds(1):bounds(2)) )
  allocate( A_dev1__sp_c1d(bounds(1):bounds(2)) )
  allocate( A_dev2__sp_c1d(bounds(1):bounds(2)) )
  call devxlib_map(A_dev1__sp_c1d)
  call devxlib_map(A_dev2__sp_c1d)
  allocate( A_rtmp__sp_1d(bounds(1):bounds(2)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c1d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__sp_1d )
  A_hst1__sp_c1d=A_rtmp__sp_1d
  call random_number( A_rtmp__sp_1d )
  A_hst1__sp_c1d=A_hst1__sp_c1d+cmplx(0.0,1.0)*A_rtmp__sp_1d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c1d, A_hst1__sp_c1d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_c1d, A_dev1__sp_c1d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c1d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c1d, A_dev2__sp_c1d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__sp_c1d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c1d, A_hst2__sp_c1d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_c1d -A_hst1__sp_c1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c1d)
  deallocate(A_hst2__sp_c1d)
  deallocate(A_hst3__sp_c1d)
  !
  call devxlib_unmap(A_dev1__sp_c1d, ierr)
  call devxlib_unmap(A_dev2__sp_c1d, ierr)
  deallocate(A_dev1__sp_c1d)
  deallocate(A_dev2__sp_c1d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c1d ) .or. &
              devxlib_mapped( A_dev2__sp_c1d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c2d ) .or. &
              devxlib_mapped( A_dev2__sp_c2d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst2__sp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst3__sp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev1__sp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev2__sp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  call devxlib_map(A_dev1__sp_c2d)
  call devxlib_map(A_dev2__sp_c2d)
  allocate( A_rtmp__sp_2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c2d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__sp_2d )
  A_hst1__sp_c2d=A_rtmp__sp_2d
  call random_number( A_rtmp__sp_2d )
  A_hst1__sp_c2d=A_hst1__sp_c2d+cmplx(0.0,1.0)*A_rtmp__sp_2d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c2d, A_hst1__sp_c2d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_c2d, A_dev1__sp_c2d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c2d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c2d, A_dev2__sp_c2d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__sp_c2d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c2d, A_hst2__sp_c2d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_c2d -A_hst1__sp_c2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c2d)
  deallocate(A_hst2__sp_c2d)
  deallocate(A_hst3__sp_c2d)
  !
  call devxlib_unmap(A_dev1__sp_c2d, ierr)
  call devxlib_unmap(A_dev2__sp_c2d, ierr)
  deallocate(A_dev1__sp_c2d)
  deallocate(A_dev2__sp_c2d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c2d ) .or. &
              devxlib_mapped( A_dev2__sp_c2d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c3d ) .or. &
              devxlib_mapped( A_dev2__sp_c3d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst2__sp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst3__sp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev1__sp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev2__sp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  call devxlib_map(A_dev1__sp_c3d)
  call devxlib_map(A_dev2__sp_c3d)
  allocate( A_rtmp__sp_3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c3d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__sp_3d )
  A_hst1__sp_c3d=A_rtmp__sp_3d
  call random_number( A_rtmp__sp_3d )
  A_hst1__sp_c3d=A_hst1__sp_c3d+cmplx(0.0,1.0)*A_rtmp__sp_3d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c3d, A_hst1__sp_c3d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_c3d, A_dev1__sp_c3d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c3d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c3d, A_dev2__sp_c3d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__sp_c3d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c3d, A_hst2__sp_c3d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_c3d -A_hst1__sp_c3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c3d)
  deallocate(A_hst2__sp_c3d)
  deallocate(A_hst3__sp_c3d)
  !
  call devxlib_unmap(A_dev1__sp_c3d, ierr)
  call devxlib_unmap(A_dev2__sp_c3d, ierr)
  deallocate(A_dev1__sp_c3d)
  deallocate(A_dev2__sp_c3d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c3d ) .or. &
              devxlib_mapped( A_dev2__sp_c3d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking sp_c4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c4d ) .or. &
              devxlib_mapped( A_dev2__sp_c4d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__sp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst2__sp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst3__sp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev1__sp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev2__sp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  call devxlib_map(A_dev1__sp_c4d)
  call devxlib_map(A_dev2__sp_c4d)
  allocate( A_rtmp__sp_4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c4d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__sp_4d )
  A_hst1__sp_c4d=A_rtmp__sp_4d
  call random_number( A_rtmp__sp_4d )
  A_hst1__sp_c4d=A_hst1__sp_c4d+cmplx(0.0,1.0)*A_rtmp__sp_4d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c4d, A_hst1__sp_c4d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__sp_c4d, A_dev1__sp_c4d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c4d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c4d, A_dev2__sp_c4d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__sp_c4d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c4d, A_hst2__sp_c4d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__sp_c4d -A_hst1__sp_c4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c4d)
  deallocate(A_hst2__sp_c4d)
  deallocate(A_hst3__sp_c4d)
  !
  call devxlib_unmap(A_dev1__sp_c4d, ierr)
  call devxlib_unmap(A_dev2__sp_c4d, ierr)
  deallocate(A_dev1__sp_c4d)
  deallocate(A_dev2__sp_c4d)
  !
  is_mapped = devxlib_mapped( A_dev1__sp_c4d ) .or. &
              devxlib_mapped( A_dev2__sp_c4d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c1d ) .or. &
              devxlib_mapped( A_dev2__dp_c1d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_c1d(bounds(1):bounds(2)) )
  allocate( A_hst2__dp_c1d(bounds(1):bounds(2)) )
  allocate( A_hst3__dp_c1d(bounds(1):bounds(2)) )
  allocate( A_dev1__dp_c1d(bounds(1):bounds(2)) )
  allocate( A_dev2__dp_c1d(bounds(1):bounds(2)) )
  call devxlib_map(A_dev1__dp_c1d)
  call devxlib_map(A_dev2__dp_c1d)
  allocate( A_rtmp__dp_1d(bounds(1):bounds(2)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c1d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__dp_1d )
  A_hst1__dp_c1d=A_rtmp__dp_1d
  call random_number( A_rtmp__dp_1d )
  A_hst1__dp_c1d=A_hst1__dp_c1d+cmplx(0.0,1.0)*A_rtmp__dp_1d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c1d, A_hst1__dp_c1d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_c1d, A_dev1__dp_c1d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c1d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c1d, A_dev2__dp_c1d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__dp_c1d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c1d, A_hst2__dp_c1d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_c1d -A_hst1__dp_c1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c1d)
  deallocate(A_hst2__dp_c1d)
  deallocate(A_hst3__dp_c1d)
  !
  call devxlib_unmap(A_dev1__dp_c1d, ierr)
  call devxlib_unmap(A_dev2__dp_c1d, ierr)
  deallocate(A_dev1__dp_c1d)
  deallocate(A_dev2__dp_c1d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c1d ) .or. &
              devxlib_mapped( A_dev2__dp_c1d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c2d ) .or. &
              devxlib_mapped( A_dev2__dp_c2d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst2__dp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst3__dp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev1__dp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev2__dp_c2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  call devxlib_map(A_dev1__dp_c2d)
  call devxlib_map(A_dev2__dp_c2d)
  allocate( A_rtmp__dp_2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c2d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__dp_2d )
  A_hst1__dp_c2d=A_rtmp__dp_2d
  call random_number( A_rtmp__dp_2d )
  A_hst1__dp_c2d=A_hst1__dp_c2d+cmplx(0.0,1.0)*A_rtmp__dp_2d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c2d, A_hst1__dp_c2d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_c2d, A_dev1__dp_c2d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c2d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c2d, A_dev2__dp_c2d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__dp_c2d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c2d, A_hst2__dp_c2d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_c2d -A_hst1__dp_c2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c2d)
  deallocate(A_hst2__dp_c2d)
  deallocate(A_hst3__dp_c2d)
  !
  call devxlib_unmap(A_dev1__dp_c2d, ierr)
  call devxlib_unmap(A_dev2__dp_c2d, ierr)
  deallocate(A_dev1__dp_c2d)
  deallocate(A_dev2__dp_c2d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c2d ) .or. &
              devxlib_mapped( A_dev2__dp_c2d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c3d ) .or. &
              devxlib_mapped( A_dev2__dp_c3d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst2__dp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst3__dp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev1__dp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev2__dp_c3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  call devxlib_map(A_dev1__dp_c3d)
  call devxlib_map(A_dev2__dp_c3d)
  allocate( A_rtmp__dp_3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c3d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__dp_3d )
  A_hst1__dp_c3d=A_rtmp__dp_3d
  call random_number( A_rtmp__dp_3d )
  A_hst1__dp_c3d=A_hst1__dp_c3d+cmplx(0.0,1.0)*A_rtmp__dp_3d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c3d, A_hst1__dp_c3d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_c3d, A_dev1__dp_c3d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c3d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c3d, A_dev2__dp_c3d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__dp_c3d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c3d, A_hst2__dp_c3d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_c3d -A_hst1__dp_c3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c3d)
  deallocate(A_hst2__dp_c3d)
  deallocate(A_hst3__dp_c3d)
  !
  call devxlib_unmap(A_dev1__dp_c3d, ierr)
  call devxlib_unmap(A_dev2__dp_c3d, ierr)
  deallocate(A_dev1__dp_c3d)
  deallocate(A_dev2__dp_c3d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c3d ) .or. &
              devxlib_mapped( A_dev2__dp_c3d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking dp_c4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c4d ) .or. &
              devxlib_mapped( A_dev2__dp_c4d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__dp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst2__dp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst3__dp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev1__dp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev2__dp_c4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  call devxlib_map(A_dev1__dp_c4d)
  call devxlib_map(A_dev2__dp_c4d)
  allocate( A_rtmp__dp_4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c4d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__dp_4d )
  A_hst1__dp_c4d=A_rtmp__dp_4d
  call random_number( A_rtmp__dp_4d )
  A_hst1__dp_c4d=A_hst1__dp_c4d+cmplx(0.0,1.0)*A_rtmp__dp_4d
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c4d, A_hst1__dp_c4d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__dp_c4d, A_dev1__dp_c4d )
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c4d )
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c4d, A_dev2__dp_c4d)
  !
  ! retrieve conjg data
  call devxlib_conjg_h( A_hst2__dp_c4d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c4d, A_hst2__dp_c4d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__dp_c4d -A_hst1__dp_c4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c4d)
  deallocate(A_hst2__dp_c4d)
  deallocate(A_hst3__dp_c4d)
  !
  call devxlib_unmap(A_dev1__dp_c4d, ierr)
  call devxlib_unmap(A_dev2__dp_c4d, ierr)
  deallocate(A_dev1__dp_c4d)
  deallocate(A_dev2__dp_c4d)
  !
  is_mapped = devxlib_mapped( A_dev1__dp_c4d ) .or. &
              devxlib_mapped( A_dev2__dp_c4d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i1d ) .or. &
              devxlib_mapped( A_dev2__i4_i1d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i4_i1d(bounds(1):bounds(2)) )
  allocate( A_hst2__i4_i1d(bounds(1):bounds(2)) )
  allocate( A_hst3__i4_i1d(bounds(1):bounds(2)) )
  allocate( A_dev1__i4_i1d(bounds(1):bounds(2)) )
  allocate( A_dev2__i4_i1d(bounds(1):bounds(2)) )
  call devxlib_map(A_dev1__i4_i1d)
  call devxlib_map(A_dev2__i4_i1d)
  allocate( A_rtmp__i4_1d(bounds(1):bounds(2)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i1d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i4_1d )
  A_hst1__i4_i1d=int( 1000*A_rtmp__i4_1d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i1d, A_hst1__i4_i1d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i4_i1d, A_dev1__i4_i1d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i1d, A_dev2__i4_i1d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i1d, A_hst2__i4_i1d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i4_i1d -A_hst1__i4_i1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i1d)
  deallocate(A_hst2__i4_i1d)
  deallocate(A_hst3__i4_i1d)
  !
  call devxlib_unmap(A_dev1__i4_i1d, ierr)
  call devxlib_unmap(A_dev2__i4_i1d, ierr)
  deallocate(A_dev1__i4_i1d)
  deallocate(A_dev2__i4_i1d)
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i1d ) .or. &
              devxlib_mapped( A_dev2__i4_i1d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i2d ) .or. &
              devxlib_mapped( A_dev2__i4_i2d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i4_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst2__i4_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst3__i4_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev1__i4_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev2__i4_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  call devxlib_map(A_dev1__i4_i2d)
  call devxlib_map(A_dev2__i4_i2d)
  allocate( A_rtmp__i4_2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i2d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i4_2d )
  A_hst1__i4_i2d=int( 1000*A_rtmp__i4_2d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i2d, A_hst1__i4_i2d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i4_i2d, A_dev1__i4_i2d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i2d, A_dev2__i4_i2d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i2d, A_hst2__i4_i2d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i4_i2d -A_hst1__i4_i2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i2d)
  deallocate(A_hst2__i4_i2d)
  deallocate(A_hst3__i4_i2d)
  !
  call devxlib_unmap(A_dev1__i4_i2d, ierr)
  call devxlib_unmap(A_dev2__i4_i2d, ierr)
  deallocate(A_dev1__i4_i2d)
  deallocate(A_dev2__i4_i2d)
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i2d ) .or. &
              devxlib_mapped( A_dev2__i4_i2d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i3d ) .or. &
              devxlib_mapped( A_dev2__i4_i3d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i4_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst2__i4_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst3__i4_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev1__i4_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev2__i4_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  call devxlib_map(A_dev1__i4_i3d)
  call devxlib_map(A_dev2__i4_i3d)
  allocate( A_rtmp__i4_3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i3d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i4_3d )
  A_hst1__i4_i3d=int( 1000*A_rtmp__i4_3d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i3d, A_hst1__i4_i3d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i4_i3d, A_dev1__i4_i3d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i3d, A_dev2__i4_i3d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i3d, A_hst2__i4_i3d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i4_i3d -A_hst1__i4_i3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i3d)
  deallocate(A_hst2__i4_i3d)
  deallocate(A_hst3__i4_i3d)
  !
  call devxlib_unmap(A_dev1__i4_i3d, ierr)
  call devxlib_unmap(A_dev2__i4_i3d, ierr)
  deallocate(A_dev1__i4_i3d)
  deallocate(A_dev2__i4_i3d)
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i3d ) .or. &
              devxlib_mapped( A_dev2__i4_i3d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i4_i4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i4d ) .or. &
              devxlib_mapped( A_dev2__i4_i4d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i4_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst2__i4_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst3__i4_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev1__i4_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev2__i4_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  call devxlib_map(A_dev1__i4_i4d)
  call devxlib_map(A_dev2__i4_i4d)
  allocate( A_rtmp__i4_4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i4d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i4_4d )
  A_hst1__i4_i4d=int( 1000*A_rtmp__i4_4d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i4d, A_hst1__i4_i4d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i4_i4d, A_dev1__i4_i4d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i4d, A_dev2__i4_i4d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i4d, A_hst2__i4_i4d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i4_i4d -A_hst1__i4_i4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i4d)
  deallocate(A_hst2__i4_i4d)
  deallocate(A_hst3__i4_i4d)
  !
  call devxlib_unmap(A_dev1__i4_i4d, ierr)
  call devxlib_unmap(A_dev2__i4_i4d, ierr)
  deallocate(A_dev1__i4_i4d)
  deallocate(A_dev2__i4_i4d)
  !
  is_mapped = devxlib_mapped( A_dev1__i4_i4d ) .or. &
              devxlib_mapped( A_dev2__i4_i4d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i1d ) .or. &
              devxlib_mapped( A_dev2__i8_i1d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i8_i1d(bounds(1):bounds(2)) )
  allocate( A_hst2__i8_i1d(bounds(1):bounds(2)) )
  allocate( A_hst3__i8_i1d(bounds(1):bounds(2)) )
  allocate( A_dev1__i8_i1d(bounds(1):bounds(2)) )
  allocate( A_dev2__i8_i1d(bounds(1):bounds(2)) )
  call devxlib_map(A_dev1__i8_i1d)
  call devxlib_map(A_dev2__i8_i1d)
  allocate( A_rtmp__i8_1d(bounds(1):bounds(2)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i1d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i8_1d )
  A_hst1__i8_i1d=int( 1000*A_rtmp__i8_1d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i1d, A_hst1__i8_i1d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i8_i1d, A_dev1__i8_i1d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i1d, A_dev2__i8_i1d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i1d, A_hst2__i8_i1d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i8_i1d -A_hst1__i8_i1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i1d)
  deallocate(A_hst2__i8_i1d)
  deallocate(A_hst3__i8_i1d)
  !
  call devxlib_unmap(A_dev1__i8_i1d, ierr)
  call devxlib_unmap(A_dev2__i8_i1d, ierr)
  deallocate(A_dev1__i8_i1d)
  deallocate(A_dev2__i8_i1d)
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i1d ) .or. &
              devxlib_mapped( A_dev2__i8_i1d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i2d ) .or. &
              devxlib_mapped( A_dev2__i8_i2d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i8_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst2__i8_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst3__i8_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev1__i8_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev2__i8_i2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  call devxlib_map(A_dev1__i8_i2d)
  call devxlib_map(A_dev2__i8_i2d)
  allocate( A_rtmp__i8_2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i2d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i8_2d )
  A_hst1__i8_i2d=int( 1000*A_rtmp__i8_2d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i2d, A_hst1__i8_i2d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i8_i2d, A_dev1__i8_i2d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i2d, A_dev2__i8_i2d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i2d, A_hst2__i8_i2d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i8_i2d -A_hst1__i8_i2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i2d)
  deallocate(A_hst2__i8_i2d)
  deallocate(A_hst3__i8_i2d)
  !
  call devxlib_unmap(A_dev1__i8_i2d, ierr)
  call devxlib_unmap(A_dev2__i8_i2d, ierr)
  deallocate(A_dev1__i8_i2d)
  deallocate(A_dev2__i8_i2d)
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i2d ) .or. &
              devxlib_mapped( A_dev2__i8_i2d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i3d ) .or. &
              devxlib_mapped( A_dev2__i8_i3d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i8_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst2__i8_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst3__i8_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev1__i8_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev2__i8_i3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  call devxlib_map(A_dev1__i8_i3d)
  call devxlib_map(A_dev2__i8_i3d)
  allocate( A_rtmp__i8_3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i3d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i8_3d )
  A_hst1__i8_i3d=int( 1000*A_rtmp__i8_3d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i3d, A_hst1__i8_i3d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i8_i3d, A_dev1__i8_i3d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i3d, A_dev2__i8_i3d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i3d, A_hst2__i8_i3d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i8_i3d -A_hst1__i8_i3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i3d)
  deallocate(A_hst2__i8_i3d)
  deallocate(A_hst3__i8_i3d)
  !
  call devxlib_unmap(A_dev1__i8_i3d, ierr)
  call devxlib_unmap(A_dev2__i8_i3d, ierr)
  deallocate(A_dev1__i8_i3d)
  deallocate(A_dev2__i8_i3d)
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i3d ) .or. &
              devxlib_mapped( A_dev2__i8_i3d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking i8_i4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i4d ) .or. &
              devxlib_mapped( A_dev2__i8_i4d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__i8_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst2__i8_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst3__i8_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev1__i8_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev2__i8_i4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  call devxlib_map(A_dev1__i8_i4d)
  call devxlib_map(A_dev2__i8_i4d)
  allocate( A_rtmp__i8_4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i4d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__i8_4d )
  A_hst1__i8_i4d=int( 1000*A_rtmp__i8_4d )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i4d, A_hst1__i8_i4d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__i8_i4d, A_dev1__i8_i4d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i4d, A_dev2__i8_i4d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i4d, A_hst2__i8_i4d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any(abs(A_hst3__i8_i4d -A_hst1__i8_i4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i4d)
  deallocate(A_hst2__i8_i4d)
  deallocate(A_hst3__i8_i4d)
  !
  call devxlib_unmap(A_dev1__i8_i4d, ierr)
  call devxlib_unmap(A_dev2__i8_i4d, ierr)
  deallocate(A_dev1__i8_i4d)
  deallocate(A_dev2__i8_i4d)
  !
  is_mapped = devxlib_mapped( A_dev1__i8_i4d ) .or. &
              devxlib_mapped( A_dev2__i8_i4d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l1d ) .or. &
              devxlib_mapped( A_dev2__l4_l1d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__l4_l1d(bounds(1):bounds(2)) )
  allocate( A_hst2__l4_l1d(bounds(1):bounds(2)) )
  allocate( A_hst3__l4_l1d(bounds(1):bounds(2)) )
  allocate( A_dev1__l4_l1d(bounds(1):bounds(2)) )
  allocate( A_dev2__l4_l1d(bounds(1):bounds(2)) )
  call devxlib_map(A_dev1__l4_l1d)
  call devxlib_map(A_dev2__l4_l1d)
  allocate( A_rtmp__l4_1d(bounds(1):bounds(2)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l1d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__l4_1d )
  A_hst1__l4_l1d=( nint( A_rtmp__l4_1d ) == 1 )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l1d, A_hst1__l4_l1d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__l4_l1d, A_dev1__l4_l1d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l1d, A_dev2__l4_l1d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l1d, A_hst2__l4_l1d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any( A_hst3__l4_l1d .neqv. A_hst1__l4_l1d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l1d)
  deallocate(A_hst2__l4_l1d)
  deallocate(A_hst3__l4_l1d)
  !
  call devxlib_unmap(A_dev1__l4_l1d, ierr)
  call devxlib_unmap(A_dev2__l4_l1d, ierr)
  deallocate(A_dev1__l4_l1d)
  deallocate(A_dev2__l4_l1d)
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l1d ) .or. &
              devxlib_mapped( A_dev2__l4_l1d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l2d ) .or. &
              devxlib_mapped( A_dev2__l4_l2d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__l4_l2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst2__l4_l2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_hst3__l4_l2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev1__l4_l2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  allocate( A_dev2__l4_l2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  call devxlib_map(A_dev1__l4_l2d)
  call devxlib_map(A_dev2__l4_l2d)
  allocate( A_rtmp__l4_2d(bounds(1):bounds(2),bounds(3):bounds(4)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l2d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__l4_2d )
  A_hst1__l4_l2d=( nint( A_rtmp__l4_2d ) == 1 )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l2d, A_hst1__l4_l2d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__l4_l2d, A_dev1__l4_l2d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l2d, A_dev2__l4_l2d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l2d, A_hst2__l4_l2d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any( A_hst3__l4_l2d .neqv. A_hst1__l4_l2d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l2d)
  deallocate(A_hst2__l4_l2d)
  deallocate(A_hst3__l4_l2d)
  !
  call devxlib_unmap(A_dev1__l4_l2d, ierr)
  call devxlib_unmap(A_dev2__l4_l2d, ierr)
  deallocate(A_dev1__l4_l2d)
  deallocate(A_dev2__l4_l2d)
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l2d ) .or. &
              devxlib_mapped( A_dev2__l4_l2d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l3d ) .or. &
              devxlib_mapped( A_dev2__l4_l3d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__l4_l3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst2__l4_l3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_hst3__l4_l3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev1__l4_l3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  allocate( A_dev2__l4_l3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  call devxlib_map(A_dev1__l4_l3d)
  call devxlib_map(A_dev2__l4_l3d)
  allocate( A_rtmp__l4_3d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l3d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__l4_3d )
  A_hst1__l4_l3d=( nint( A_rtmp__l4_3d ) == 1 )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l3d, A_hst1__l4_l3d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__l4_l3d, A_dev1__l4_l3d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l3d, A_dev2__l4_l3d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l3d, A_hst2__l4_l3d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any( A_hst3__l4_l3d .neqv. A_hst1__l4_l3d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l3d)
  deallocate(A_hst2__l4_l3d)
  deallocate(A_hst3__l4_l3d)
  !
  call devxlib_unmap(A_dev1__l4_l3d, ierr)
  call devxlib_unmap(A_dev2__l4_l3d, ierr)
  deallocate(A_dev1__l4_l3d)
  deallocate(A_dev2__l4_l3d)
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l3d ) .or. &
              devxlib_mapped( A_dev2__l4_l3d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  write(6,"(/,3x,a)") "checking l4_l4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_mapping=.false.
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l4d ) .or. &
              devxlib_mapped( A_dev2__l4_l4d )
  if (is_mapped) then
     nfail_mapping=nfail_mapping+1
     lfail_mapping=.true.
     write(6,"(3x,a)") "mem mapping check-pre FAILED" 
  endif
  !
  ! allocations
  allocate( A_hst1__l4_l4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst2__l4_l4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_hst3__l4_l4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev1__l4_l4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  allocate( A_dev2__l4_l4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  call devxlib_map(A_dev1__l4_l4d)
  call devxlib_map(A_dev2__l4_l4d)
  allocate( A_rtmp__l4_4d(bounds(1):bounds(2),bounds(3):bounds(4),bounds(5):bounds(6),bounds(7):bounds(8)) )
  
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l4d )
  if (.not.is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post FAILED" 
  endif
  !
  ! init
  call random_number( A_rtmp__l4_4d )
  A_hst1__l4_l4d=( nint( A_rtmp__l4_4d ) == 1 )
  
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l4d, A_hst1__l4_l4d)
  !
  ! mem copy
  call devxlib_memcpy_d2d(A_dev2__l4_l4d, A_dev1__l4_l4d )
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l4d, A_dev2__l4_l4d)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l4d, A_hst2__l4_l4d)
  !
  ! check
  if (lfail_mapping) write(6,"(3x,a)") "Mapping FAILED" 
  !
  if ( any( A_hst3__l4_l4d .neqv. A_hst1__l4_l4d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l4d)
  deallocate(A_hst2__l4_l4d)
  deallocate(A_hst3__l4_l4d)
  !
  call devxlib_unmap(A_dev1__l4_l4d, ierr)
  call devxlib_unmap(A_dev2__l4_l4d, ierr)
  deallocate(A_dev1__l4_l4d)
  deallocate(A_dev2__l4_l4d)
  !
  is_mapped = devxlib_mapped( A_dev1__l4_l4d ) .or. &
              devxlib_mapped( A_dev2__l4_l4d )
  if (is_mapped) then
    nfail_mapping=nfail_mapping+1
    lfail_mapping=.true.
    write(6,"(3x,a)") "mem mapping check-post2 FAILED" 
  endif
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !




  !
  ! summary
  !
  write(6,"(/,a)") "# Test SUMMARY:"
  write(6,"(3x,a,i5)") "# passed: ", npass
  write(6,"(3x,a,i5)") "# failed: ", nfail
  write(6,"(3x,a,i5)") "# failed mapping ", nfail_mapping
  write(6,"()")

end program test_mapping
