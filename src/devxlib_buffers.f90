module devxlib_buffers
   use devxlib_buffer
   use devxlib_pinned

   implicit none

   type(devxlib_buffer_t) :: gpu_buffer
   type(devxlib_pinned_t) :: pin_buffer
endmodule devxlib_buffers
