!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to check allocation status on the device
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_malloc_allocated.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_malloc) devxlib_malloc_allocated

   implicit none

   contains
      module function sp_devxlib_malloc_allocated_r1d(array) result(res)
          implicit none
          !
          logical :: res
          real(real32), allocatable, intent(in) DEV_ATTR :: array(:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_r1d
      !
      module function sp_devxlib_malloc_allocated_r2d(array) result(res)
          implicit none
          !
          logical :: res
          real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_r2d
      !
      module function sp_devxlib_malloc_allocated_r3d(array) result(res)
          implicit none
          !
          logical :: res
          real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_r3d
      !
      module function sp_devxlib_malloc_allocated_r4d(array) result(res)
          implicit none
          !
          logical :: res
          real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_r4d
      !
      module function sp_devxlib_malloc_allocated_r5d(array) result(res)
          implicit none
          !
          logical :: res
          real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_r5d
      !
      module function sp_devxlib_malloc_allocated_r6d(array) result(res)
          implicit none
          !
          logical :: res
          real(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_r6d
      !
      module function dp_devxlib_malloc_allocated_r1d(array) result(res)
          implicit none
          !
          logical :: res
          real(real64), allocatable, intent(in) DEV_ATTR :: array(:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_r1d
      !
      module function dp_devxlib_malloc_allocated_r2d(array) result(res)
          implicit none
          !
          logical :: res
          real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_r2d
      !
      module function dp_devxlib_malloc_allocated_r3d(array) result(res)
          implicit none
          !
          logical :: res
          real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_r3d
      !
      module function dp_devxlib_malloc_allocated_r4d(array) result(res)
          implicit none
          !
          logical :: res
          real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_r4d
      !
      module function dp_devxlib_malloc_allocated_r5d(array) result(res)
          implicit none
          !
          logical :: res
          real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_r5d
      !
      module function dp_devxlib_malloc_allocated_r6d(array) result(res)
          implicit none
          !
          logical :: res
          real(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_r6d
      !
      module function sp_devxlib_malloc_allocated_c1d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real32), allocatable, intent(in) DEV_ATTR :: array(:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_c1d
      !
      module function sp_devxlib_malloc_allocated_c2d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_c2d
      !
      module function sp_devxlib_malloc_allocated_c3d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_c3d
      !
      module function sp_devxlib_malloc_allocated_c4d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_c4d
      !
      module function sp_devxlib_malloc_allocated_c5d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_c5d
      !
      module function sp_devxlib_malloc_allocated_c6d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
          res=allocated(array)
          return
      end function sp_devxlib_malloc_allocated_c6d
      !
      module function dp_devxlib_malloc_allocated_c1d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real64), allocatable, intent(in) DEV_ATTR :: array(:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_c1d
      !
      module function dp_devxlib_malloc_allocated_c2d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_c2d
      !
      module function dp_devxlib_malloc_allocated_c3d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_c3d
      !
      module function dp_devxlib_malloc_allocated_c4d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_c4d
      !
      module function dp_devxlib_malloc_allocated_c5d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_c5d
      !
      module function dp_devxlib_malloc_allocated_c6d(array) result(res)
          implicit none
          !
          logical :: res
          complex(real64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
          res=allocated(array)
          return
      end function dp_devxlib_malloc_allocated_c6d
      !
      module function i4_devxlib_malloc_allocated_i1d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int32), allocatable, intent(in) DEV_ATTR :: array(:)
          res=allocated(array)
          return
      end function i4_devxlib_malloc_allocated_i1d
      !
      module function i4_devxlib_malloc_allocated_i2d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:)
          res=allocated(array)
          return
      end function i4_devxlib_malloc_allocated_i2d
      !
      module function i4_devxlib_malloc_allocated_i3d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
          res=allocated(array)
          return
      end function i4_devxlib_malloc_allocated_i3d
      !
      module function i4_devxlib_malloc_allocated_i4d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
          res=allocated(array)
          return
      end function i4_devxlib_malloc_allocated_i4d
      !
      module function i4_devxlib_malloc_allocated_i5d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
          res=allocated(array)
          return
      end function i4_devxlib_malloc_allocated_i5d
      !
      module function i4_devxlib_malloc_allocated_i6d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
          res=allocated(array)
          return
      end function i4_devxlib_malloc_allocated_i6d
      !
      module function i8_devxlib_malloc_allocated_i1d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int64), allocatable, intent(in) DEV_ATTR :: array(:)
          res=allocated(array)
          return
      end function i8_devxlib_malloc_allocated_i1d
      !
      module function i8_devxlib_malloc_allocated_i2d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:)
          res=allocated(array)
          return
      end function i8_devxlib_malloc_allocated_i2d
      !
      module function i8_devxlib_malloc_allocated_i3d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
          res=allocated(array)
          return
      end function i8_devxlib_malloc_allocated_i3d
      !
      module function i8_devxlib_malloc_allocated_i4d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
          res=allocated(array)
          return
      end function i8_devxlib_malloc_allocated_i4d
      !
      module function i8_devxlib_malloc_allocated_i5d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
          res=allocated(array)
          return
      end function i8_devxlib_malloc_allocated_i5d
      !
      module function i8_devxlib_malloc_allocated_i6d(array) result(res)
          implicit none
          !
          logical :: res
          integer(int64), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
          res=allocated(array)
          return
      end function i8_devxlib_malloc_allocated_i6d
      !
      module function l4_devxlib_malloc_allocated_l1d(array) result(res)
          implicit none
          !
          logical :: res
          logical(int32), allocatable, intent(in) DEV_ATTR :: array(:)
          res=allocated(array)
          return
      end function l4_devxlib_malloc_allocated_l1d
      !
      module function l4_devxlib_malloc_allocated_l2d(array) result(res)
          implicit none
          !
          logical :: res
          logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:)
          res=allocated(array)
          return
      end function l4_devxlib_malloc_allocated_l2d
      !
      module function l4_devxlib_malloc_allocated_l3d(array) result(res)
          implicit none
          !
          logical :: res
          logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:)
          res=allocated(array)
          return
      end function l4_devxlib_malloc_allocated_l3d
      !
      module function l4_devxlib_malloc_allocated_l4d(array) result(res)
          implicit none
          !
          logical :: res
          logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:)
          res=allocated(array)
          return
      end function l4_devxlib_malloc_allocated_l4d
      !
      module function l4_devxlib_malloc_allocated_l5d(array) result(res)
          implicit none
          !
          logical :: res
          logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:)
          res=allocated(array)
          return
      end function l4_devxlib_malloc_allocated_l5d
      !
      module function l4_devxlib_malloc_allocated_l6d(array) result(res)
          implicit none
          !
          logical :: res
          logical(int32), allocatable, intent(in) DEV_ATTR :: array(:,:,:,:,:,:)
          res=allocated(array)
          return
      end function l4_devxlib_malloc_allocated_l6d
      !
      module function sp_devxlib_malloc_allocated_r1d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_r1d_p
      !
      module function sp_devxlib_malloc_allocated_r2d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_r2d_p
      !
      module function sp_devxlib_malloc_allocated_r3d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_r3d_p
      !
      module function sp_devxlib_malloc_allocated_r4d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_r4d_p
      !
      module function sp_devxlib_malloc_allocated_r5d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_r5d_p
      !
      module function sp_devxlib_malloc_allocated_r6d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_r6d_p
      !
      module function dp_devxlib_malloc_allocated_r1d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_r1d_p
      !
      module function dp_devxlib_malloc_allocated_r2d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_r2d_p
      !
      module function dp_devxlib_malloc_allocated_r3d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_r3d_p
      !
      module function dp_devxlib_malloc_allocated_r4d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_r4d_p
      !
      module function dp_devxlib_malloc_allocated_r5d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_r5d_p
      !
      module function dp_devxlib_malloc_allocated_r6d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          real(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_r6d_p
      !
      module function sp_devxlib_malloc_allocated_c1d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_c1d_p
      !
      module function sp_devxlib_malloc_allocated_c2d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_c2d_p
      !
      module function sp_devxlib_malloc_allocated_c3d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_c3d_p
      !
      module function sp_devxlib_malloc_allocated_c4d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_c4d_p
      !
      module function sp_devxlib_malloc_allocated_c5d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_c5d_p
      !
      module function sp_devxlib_malloc_allocated_c6d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function sp_devxlib_malloc_allocated_c6d_p
      !
      module function dp_devxlib_malloc_allocated_c1d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_c1d_p
      !
      module function dp_devxlib_malloc_allocated_c2d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_c2d_p
      !
      module function dp_devxlib_malloc_allocated_c3d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_c3d_p
      !
      module function dp_devxlib_malloc_allocated_c4d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_c4d_p
      !
      module function dp_devxlib_malloc_allocated_c5d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_c5d_p
      !
      module function dp_devxlib_malloc_allocated_c6d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          complex(real64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function dp_devxlib_malloc_allocated_c6d_p
      !
      module function i4_devxlib_malloc_allocated_i1d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
          res=associated(dev_ptr)
          return
      end function i4_devxlib_malloc_allocated_i1d_p
      !
      module function i4_devxlib_malloc_allocated_i2d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
          res=associated(dev_ptr)
          return
      end function i4_devxlib_malloc_allocated_i2d_p
      !
      module function i4_devxlib_malloc_allocated_i3d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
          res=associated(dev_ptr)
          return
      end function i4_devxlib_malloc_allocated_i3d_p
      !
      module function i4_devxlib_malloc_allocated_i4d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
          res=associated(dev_ptr)
          return
      end function i4_devxlib_malloc_allocated_i4d_p
      !
      module function i4_devxlib_malloc_allocated_i5d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function i4_devxlib_malloc_allocated_i5d_p
      !
      module function i4_devxlib_malloc_allocated_i6d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function i4_devxlib_malloc_allocated_i6d_p
      !
      module function i8_devxlib_malloc_allocated_i1d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
          res=associated(dev_ptr)
          return
      end function i8_devxlib_malloc_allocated_i1d_p
      !
      module function i8_devxlib_malloc_allocated_i2d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
          res=associated(dev_ptr)
          return
      end function i8_devxlib_malloc_allocated_i2d_p
      !
      module function i8_devxlib_malloc_allocated_i3d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
          res=associated(dev_ptr)
          return
      end function i8_devxlib_malloc_allocated_i3d_p
      !
      module function i8_devxlib_malloc_allocated_i4d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
          res=associated(dev_ptr)
          return
      end function i8_devxlib_malloc_allocated_i4d_p
      !
      module function i8_devxlib_malloc_allocated_i5d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function i8_devxlib_malloc_allocated_i5d_p
      !
      module function i8_devxlib_malloc_allocated_i6d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          integer(int64), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function i8_devxlib_malloc_allocated_i6d_p
      !
      module function l4_devxlib_malloc_allocated_l1d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:)
          res=associated(dev_ptr)
          return
      end function l4_devxlib_malloc_allocated_l1d_p
      !
      module function l4_devxlib_malloc_allocated_l2d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:)
          res=associated(dev_ptr)
          return
      end function l4_devxlib_malloc_allocated_l2d_p
      !
      module function l4_devxlib_malloc_allocated_l3d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:)
          res=associated(dev_ptr)
          return
      end function l4_devxlib_malloc_allocated_l3d_p
      !
      module function l4_devxlib_malloc_allocated_l4d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:)
          res=associated(dev_ptr)
          return
      end function l4_devxlib_malloc_allocated_l4d_p
      !
      module function l4_devxlib_malloc_allocated_l5d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function l4_devxlib_malloc_allocated_l5d_p
      !
      module function l4_devxlib_malloc_allocated_l6d_p(dev_ptr) result(res)
          implicit none
          !
          logical :: res
          logical(int32), pointer, intent(in) DEV_ATTR :: dev_ptr(:,:,:,:,:,:)
          res=associated(dev_ptr)
          return
      end function l4_devxlib_malloc_allocated_l6d_p
      !

endsubmodule devxlib_malloc_allocated