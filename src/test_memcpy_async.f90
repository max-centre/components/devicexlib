!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Testing: dev_memcpy_async, devxlib_map, dev_auxfunc
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from test_memcpy_async.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
program test_memcpy_async
  !
  ! This program tests the routines and interfaces related to
  ! device_memcpy_async and some device_auxfunc.
  !
  use iso_fortran_env, only : int32, int64, real32, real64
  use devxlib_async
  use devxlib_memcpy
  use devxlib_mapping
  use devxlib_auxfunc
  !
#if defined __DXL_CUDAF
  use cudafor
#elif defined __DXL_OPENACC
  use openacc
#endif
  implicit none

  integer(int64) :: cnt, count_max
  real(real64), parameter :: thr=1.0d-6
  integer,      parameter :: nranks=4
  integer      :: ndim(nranks)
  integer      :: vrange(2,nranks)
  integer      :: vlbound(nranks)
  real(real64) :: t0, t1, count_rate
  integer      :: npass,nfail
  integer      :: nfail_malloc
  logical      :: is_alloc, lfail_malloc
  integer(kind=dxl_async_kind) :: async_id, default_async_id
  real(real32), allocatable DEV_PINN :: A_hst1__sp_r1d(:)
  real(real32), allocatable DEV_PINN :: A_hst2__sp_r1d(:)
  real(real32), allocatable DEV_PINN :: A_hst3__sp_r1d(:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r1d(:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r1d(:)
  

  real(real32), allocatable DEV_PINN :: A_hst1__sp_r2d(:,:)
  real(real32), allocatable DEV_PINN :: A_hst2__sp_r2d(:,:)
  real(real32), allocatable DEV_PINN :: A_hst3__sp_r2d(:,:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r2d(:,:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r2d(:,:)
  

  real(real32), allocatable DEV_PINN :: A_hst1__sp_r3d(:,:,:)
  real(real32), allocatable DEV_PINN :: A_hst2__sp_r3d(:,:,:)
  real(real32), allocatable DEV_PINN :: A_hst3__sp_r3d(:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r3d(:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r3d(:,:,:)
  

  real(real32), allocatable DEV_PINN :: A_hst1__sp_r4d(:,:,:,:)
  real(real32), allocatable DEV_PINN :: A_hst2__sp_r4d(:,:,:,:)
  real(real32), allocatable DEV_PINN :: A_hst3__sp_r4d(:,:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev1__sp_r4d(:,:,:,:)
  real(real32), allocatable DEV_ATTR :: A_dev2__sp_r4d(:,:,:,:)
  


  real(real64), allocatable DEV_PINN :: A_hst1__dp_r1d(:)
  real(real64), allocatable DEV_PINN :: A_hst2__dp_r1d(:)
  real(real64), allocatable DEV_PINN :: A_hst3__dp_r1d(:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r1d(:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r1d(:)
  

  real(real64), allocatable DEV_PINN :: A_hst1__dp_r2d(:,:)
  real(real64), allocatable DEV_PINN :: A_hst2__dp_r2d(:,:)
  real(real64), allocatable DEV_PINN :: A_hst3__dp_r2d(:,:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r2d(:,:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r2d(:,:)
  

  real(real64), allocatable DEV_PINN :: A_hst1__dp_r3d(:,:,:)
  real(real64), allocatable DEV_PINN :: A_hst2__dp_r3d(:,:,:)
  real(real64), allocatable DEV_PINN :: A_hst3__dp_r3d(:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r3d(:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r3d(:,:,:)
  

  real(real64), allocatable DEV_PINN :: A_hst1__dp_r4d(:,:,:,:)
  real(real64), allocatable DEV_PINN :: A_hst2__dp_r4d(:,:,:,:)
  real(real64), allocatable DEV_PINN :: A_hst3__dp_r4d(:,:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev1__dp_r4d(:,:,:,:)
  real(real64), allocatable DEV_ATTR :: A_dev2__dp_r4d(:,:,:,:)
  



  complex(real32), allocatable DEV_PINN :: A_hst1__sp_c1d(:)
  complex(real32), allocatable DEV_PINN :: A_hst2__sp_c1d(:)
  complex(real32), allocatable DEV_PINN :: A_hst3__sp_c1d(:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c1d(:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c1d(:)
  
  real(real32),  allocatable DEV_PINN :: A_rtmp__sp_1d(:)
  

  complex(real32), allocatable DEV_PINN :: A_hst1__sp_c2d(:,:)
  complex(real32), allocatable DEV_PINN :: A_hst2__sp_c2d(:,:)
  complex(real32), allocatable DEV_PINN :: A_hst3__sp_c2d(:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c2d(:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c2d(:,:)
  
  real(real32),  allocatable DEV_PINN :: A_rtmp__sp_2d(:,:)
  

  complex(real32), allocatable DEV_PINN :: A_hst1__sp_c3d(:,:,:)
  complex(real32), allocatable DEV_PINN :: A_hst2__sp_c3d(:,:,:)
  complex(real32), allocatable DEV_PINN :: A_hst3__sp_c3d(:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c3d(:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c3d(:,:,:)
  
  real(real32),  allocatable DEV_PINN :: A_rtmp__sp_3d(:,:,:)
  

  complex(real32), allocatable DEV_PINN :: A_hst1__sp_c4d(:,:,:,:)
  complex(real32), allocatable DEV_PINN :: A_hst2__sp_c4d(:,:,:,:)
  complex(real32), allocatable DEV_PINN :: A_hst3__sp_c4d(:,:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev1__sp_c4d(:,:,:,:)
  complex(real32), allocatable DEV_ATTR :: A_dev2__sp_c4d(:,:,:,:)
  
  real(real32),  allocatable DEV_PINN :: A_rtmp__sp_4d(:,:,:,:)
  


  complex(real64), allocatable DEV_PINN :: A_hst1__dp_c1d(:)
  complex(real64), allocatable DEV_PINN :: A_hst2__dp_c1d(:)
  complex(real64), allocatable DEV_PINN :: A_hst3__dp_c1d(:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c1d(:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c1d(:)
  
  real(real64),  allocatable DEV_PINN :: A_rtmp__dp_1d(:)
  

  complex(real64), allocatable DEV_PINN :: A_hst1__dp_c2d(:,:)
  complex(real64), allocatable DEV_PINN :: A_hst2__dp_c2d(:,:)
  complex(real64), allocatable DEV_PINN :: A_hst3__dp_c2d(:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c2d(:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c2d(:,:)
  
  real(real64),  allocatable DEV_PINN :: A_rtmp__dp_2d(:,:)
  

  complex(real64), allocatable DEV_PINN :: A_hst1__dp_c3d(:,:,:)
  complex(real64), allocatable DEV_PINN :: A_hst2__dp_c3d(:,:,:)
  complex(real64), allocatable DEV_PINN :: A_hst3__dp_c3d(:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c3d(:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c3d(:,:,:)
  
  real(real64),  allocatable DEV_PINN :: A_rtmp__dp_3d(:,:,:)
  

  complex(real64), allocatable DEV_PINN :: A_hst1__dp_c4d(:,:,:,:)
  complex(real64), allocatable DEV_PINN :: A_hst2__dp_c4d(:,:,:,:)
  complex(real64), allocatable DEV_PINN :: A_hst3__dp_c4d(:,:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev1__dp_c4d(:,:,:,:)
  complex(real64), allocatable DEV_ATTR :: A_dev2__dp_c4d(:,:,:,:)
  
  real(real64),  allocatable DEV_PINN :: A_rtmp__dp_4d(:,:,:,:)
  



  integer(int32), allocatable DEV_PINN :: A_hst1__i4_i1d(:)
  integer(int32), allocatable DEV_PINN :: A_hst2__i4_i1d(:)
  integer(int32), allocatable DEV_PINN :: A_hst3__i4_i1d(:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i1d(:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i1d(:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__i4_1d(:)
  

  integer(int32), allocatable DEV_PINN :: A_hst1__i4_i2d(:,:)
  integer(int32), allocatable DEV_PINN :: A_hst2__i4_i2d(:,:)
  integer(int32), allocatable DEV_PINN :: A_hst3__i4_i2d(:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i2d(:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i2d(:,:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__i4_2d(:,:)
  

  integer(int32), allocatable DEV_PINN :: A_hst1__i4_i3d(:,:,:)
  integer(int32), allocatable DEV_PINN :: A_hst2__i4_i3d(:,:,:)
  integer(int32), allocatable DEV_PINN :: A_hst3__i4_i3d(:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i3d(:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i3d(:,:,:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__i4_3d(:,:,:)
  

  integer(int32), allocatable DEV_PINN :: A_hst1__i4_i4d(:,:,:,:)
  integer(int32), allocatable DEV_PINN :: A_hst2__i4_i4d(:,:,:,:)
  integer(int32), allocatable DEV_PINN :: A_hst3__i4_i4d(:,:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev1__i4_i4d(:,:,:,:)
  integer(int32), allocatable DEV_ATTR :: A_dev2__i4_i4d(:,:,:,:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__i4_4d(:,:,:,:)
  


  integer(int64), allocatable DEV_PINN :: A_hst1__i8_i1d(:)
  integer(int64), allocatable DEV_PINN :: A_hst2__i8_i1d(:)
  integer(int64), allocatable DEV_PINN :: A_hst3__i8_i1d(:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i1d(:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i1d(:)
  
  real(int64),  allocatable DEV_PINN :: A_rtmp__i8_1d(:)
  

  integer(int64), allocatable DEV_PINN :: A_hst1__i8_i2d(:,:)
  integer(int64), allocatable DEV_PINN :: A_hst2__i8_i2d(:,:)
  integer(int64), allocatable DEV_PINN :: A_hst3__i8_i2d(:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i2d(:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i2d(:,:)
  
  real(int64),  allocatable DEV_PINN :: A_rtmp__i8_2d(:,:)
  

  integer(int64), allocatable DEV_PINN :: A_hst1__i8_i3d(:,:,:)
  integer(int64), allocatable DEV_PINN :: A_hst2__i8_i3d(:,:,:)
  integer(int64), allocatable DEV_PINN :: A_hst3__i8_i3d(:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i3d(:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i3d(:,:,:)
  
  real(int64),  allocatable DEV_PINN :: A_rtmp__i8_3d(:,:,:)
  

  integer(int64), allocatable DEV_PINN :: A_hst1__i8_i4d(:,:,:,:)
  integer(int64), allocatable DEV_PINN :: A_hst2__i8_i4d(:,:,:,:)
  integer(int64), allocatable DEV_PINN :: A_hst3__i8_i4d(:,:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev1__i8_i4d(:,:,:,:)
  integer(int64), allocatable DEV_ATTR :: A_dev2__i8_i4d(:,:,:,:)
  
  real(int64),  allocatable DEV_PINN :: A_rtmp__i8_4d(:,:,:,:)
  



  logical(int32), allocatable DEV_PINN :: A_hst1__l4_l1d(:)
  logical(int32), allocatable DEV_PINN :: A_hst2__l4_l1d(:)
  logical(int32), allocatable DEV_PINN :: A_hst3__l4_l1d(:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l1d(:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l1d(:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__l4_1d(:)
  

  logical(int32), allocatable DEV_PINN :: A_hst1__l4_l2d(:,:)
  logical(int32), allocatable DEV_PINN :: A_hst2__l4_l2d(:,:)
  logical(int32), allocatable DEV_PINN :: A_hst3__l4_l2d(:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l2d(:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l2d(:,:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__l4_2d(:,:)
  

  logical(int32), allocatable DEV_PINN :: A_hst1__l4_l3d(:,:,:)
  logical(int32), allocatable DEV_PINN :: A_hst2__l4_l3d(:,:,:)
  logical(int32), allocatable DEV_PINN :: A_hst3__l4_l3d(:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l3d(:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l3d(:,:,:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__l4_3d(:,:,:)
  

  logical(int32), allocatable DEV_PINN :: A_hst1__l4_l4d(:,:,:,:)
  logical(int32), allocatable DEV_PINN :: A_hst2__l4_l4d(:,:,:,:)
  logical(int32), allocatable DEV_PINN :: A_hst3__l4_l4d(:,:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev1__l4_l4d(:,:,:,:)
  logical(int32), allocatable DEV_ATTR :: A_dev2__l4_l4d(:,:,:,:)
  
  real(int32),  allocatable DEV_PINN :: A_rtmp__l4_4d(:,:,:,:)
  



  
  integer :: i,ierr
  integer :: ndim1, ndim2, ndim3, ndim4 
  integer :: lbound1, lbound2, lbound3, lbound4 
  integer :: range1(2), range2(2), range3(2), range4(2) 
  integer :: bound1(2), bound2(2), bound3(2), bound4(2) 
  logical :: is_pinned(3)
  character(256) :: arg, str

!
!============================
! get dims
!============================
!
  ! defaults
  ndim(:)=100
  vrange(1,:)=1
  vrange(2,:)=ndim
  vlbound(:)=1

  i=0
  do
    call get_command_argument(i, arg)
    if (len_trim(arg) == 0) exit
    !
    select case (trim(arg))
    case("-h","--help")
      write(6,"(a)") "Usage: "
      write(6,"(a)") "   ./test_memcpy_async.x [--dims <vals>] [--range <vals>] [--lbound <vals>]"
      stop
    end select
    !
    i = i+1
    call get_command_argument(i, str)
    if (len_trim(str) == 0) exit
    !
    select case (trim(arg))
    case("-dims","--dims")
      read(str,*,iostat=ierr) ndim(:)
      if (ierr/=0) STOP "reading cmd-line args: dims"
    case("-range","--range")
      read(str,*,iostat=ierr) vrange(:,:)
      if (ierr/=0) STOP "reading cmd-line args: range"
    case("-lbound","--lbound")
      read(str,*,iostat=ierr) vlbound(:)
      if (ierr/=0) STOP "reading cmd-line args: lbound"
    end select
  enddo
  !
  write(6,"(/,a,/)") "Running test with params: "
  write(6,"(3x,a,10i5)") "  ndim: ", ndim(:)
  write(6,"(3x,a,10i5)") "lbound: ", vlbound(:)
  do i = 1, nranks
     write(6,"(3x,a,i2,3x,10i5)") " range: ", i, vrange(:,i)
  enddo
  write(6,"()")
  !
  npass=0
  nfail=0
  nfail_malloc=0

  !
  ! init async_id (streams, queues)
  !
  call devxlib_async_create(default_async_id,default_async=.true.)
  !
  async_id=0
#ifdef __DXL_CUDAF
  call devxlib_async_create(async_id)
#elif defined __DXL_OPENACC
  call devxlib_async_create(async_id,val=int(10,KIND=dxl_async_kind))
#endif

  !
  ! main loop
  !

  ndim1=ndim(1)
  lbound1=vlbound(1)
  range1=vrange(:,1)
  bound1(1)=lbound1
  bound1(2)=lbound1+ndim1-1
  ndim2=ndim(2)
  lbound2=vlbound(2)
  range2=vrange(:,2)
  bound2(1)=lbound2
  bound2(2)=lbound2+ndim2-1
  ndim3=ndim(3)
  lbound3=vlbound(3)
  range3=vrange(:,3)
  bound3(1)=lbound3
  bound3(2)=lbound3+ndim3-1
  ndim4=ndim(4)
  lbound4=vlbound(4)
  range4=vrange(:,4)
  bound4(1)=lbound4
  bound4(2)=lbound4+ndim4-1

!
!============================
! check memcpy
!============================
!
  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_r1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r1d ) .or. &
             devxlib_mapped( A_dev2__sp_r1d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_r1d(bound1(1):bound1(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_r1d(bound1(1):bound1(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_r1d(bound1(1):bound1(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst2__sp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst3__sp_r1d(bound1(1):bound1(2)) )
#endif
  allocate( A_dev1__sp_r1d(bound1(1):bound1(2)) )
  allocate( A_dev2__sp_r1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_r1d)
  call devxlib_map(A_dev2__sp_r1d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r1d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__sp_r1d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r1d, A_hst1__sp_r1d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_r1d, A_dev1__sp_r1d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r1d, A_dev2__sp_r1d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r1d, A_hst2__sp_r1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_r1d -A_hst1__sp_r1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r1d)
  deallocate(A_hst2__sp_r1d)
  deallocate(A_hst3__sp_r1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_r1d)
  call devxlib_unmap(A_dev2__sp_r1d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r1d ) .or. &
             devxlib_mapped( A_dev2__sp_r1d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_r1d)
  deallocate(A_dev2__sp_r1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_r2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r2d ) .or. &
             devxlib_mapped( A_dev2__sp_r2d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst3__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#endif
  allocate( A_dev1__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev2__sp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_r2d)
  call devxlib_map(A_dev2__sp_r2d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r2d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__sp_r2d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r2d, A_hst1__sp_r2d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_r2d, A_dev1__sp_r2d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r2d, A_dev2__sp_r2d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r2d, A_hst2__sp_r2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_r2d -A_hst1__sp_r2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r2d)
  deallocate(A_hst2__sp_r2d)
  deallocate(A_hst3__sp_r2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_r2d)
  call devxlib_unmap(A_dev2__sp_r2d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r2d ) .or. &
             devxlib_mapped( A_dev2__sp_r2d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_r2d)
  deallocate(A_dev2__sp_r2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_r3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r3d ) .or. &
             devxlib_mapped( A_dev2__sp_r3d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst3__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#endif
  allocate( A_dev1__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev2__sp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_r3d)
  call devxlib_map(A_dev2__sp_r3d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r3d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__sp_r3d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r3d, A_hst1__sp_r3d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_r3d, A_dev1__sp_r3d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r3d, A_dev2__sp_r3d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r3d, A_hst2__sp_r3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_r3d -A_hst1__sp_r3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r3d)
  deallocate(A_hst2__sp_r3d)
  deallocate(A_hst3__sp_r3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_r3d)
  call devxlib_unmap(A_dev2__sp_r3d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r3d ) .or. &
             devxlib_mapped( A_dev2__sp_r3d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_r3d)
  deallocate(A_dev2__sp_r3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_r4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r4d ) .or. &
             devxlib_mapped( A_dev2__sp_r4d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst3__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#endif
  allocate( A_dev1__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev2__sp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_r4d)
  call devxlib_map(A_dev2__sp_r4d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r4d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__sp_r4d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_r4d, A_hst1__sp_r4d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_r4d, A_dev1__sp_r4d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_r4d, A_dev2__sp_r4d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_r4d, A_hst2__sp_r4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_r4d -A_hst1__sp_r4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_r4d)
  deallocate(A_hst2__sp_r4d)
  deallocate(A_hst3__sp_r4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_r4d)
  call devxlib_unmap(A_dev2__sp_r4d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_r4d ) .or. &
             devxlib_mapped( A_dev2__sp_r4d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_r4d)
  deallocate(A_dev2__sp_r4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_r1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r1d ) .or. &
             devxlib_mapped( A_dev2__dp_r1d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_r1d(bound1(1):bound1(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_r1d(bound1(1):bound1(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_r1d(bound1(1):bound1(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst2__dp_r1d(bound1(1):bound1(2)) )
  allocate( A_hst3__dp_r1d(bound1(1):bound1(2)) )
#endif
  allocate( A_dev1__dp_r1d(bound1(1):bound1(2)) )
  allocate( A_dev2__dp_r1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_r1d)
  call devxlib_map(A_dev2__dp_r1d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r1d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__dp_r1d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r1d, A_hst1__dp_r1d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_r1d, A_dev1__dp_r1d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r1d, A_dev2__dp_r1d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r1d, A_hst2__dp_r1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_r1d -A_hst1__dp_r1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r1d)
  deallocate(A_hst2__dp_r1d)
  deallocate(A_hst3__dp_r1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_r1d)
  call devxlib_unmap(A_dev2__dp_r1d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r1d ) .or. &
             devxlib_mapped( A_dev2__dp_r1d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_r1d)
  deallocate(A_dev2__dp_r1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_r2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r2d ) .or. &
             devxlib_mapped( A_dev2__dp_r2d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst3__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#endif
  allocate( A_dev1__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev2__dp_r2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_r2d)
  call devxlib_map(A_dev2__dp_r2d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r2d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__dp_r2d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r2d, A_hst1__dp_r2d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_r2d, A_dev1__dp_r2d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r2d, A_dev2__dp_r2d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r2d, A_hst2__dp_r2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_r2d -A_hst1__dp_r2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r2d)
  deallocate(A_hst2__dp_r2d)
  deallocate(A_hst3__dp_r2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_r2d)
  call devxlib_unmap(A_dev2__dp_r2d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r2d ) .or. &
             devxlib_mapped( A_dev2__dp_r2d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_r2d)
  deallocate(A_dev2__dp_r2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_r3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r3d ) .or. &
             devxlib_mapped( A_dev2__dp_r3d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst3__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#endif
  allocate( A_dev1__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev2__dp_r3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_r3d)
  call devxlib_map(A_dev2__dp_r3d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r3d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__dp_r3d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r3d, A_hst1__dp_r3d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_r3d, A_dev1__dp_r3d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r3d, A_dev2__dp_r3d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r3d, A_hst2__dp_r3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_r3d -A_hst1__dp_r3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r3d)
  deallocate(A_hst2__dp_r3d)
  deallocate(A_hst3__dp_r3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_r3d)
  call devxlib_unmap(A_dev2__dp_r3d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r3d ) .or. &
             devxlib_mapped( A_dev2__dp_r3d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_r3d)
  deallocate(A_dev2__dp_r3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_r4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r4d ) .or. &
             devxlib_mapped( A_dev2__dp_r4d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst3__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#endif
  allocate( A_dev1__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev2__dp_r4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_r4d)
  call devxlib_map(A_dev2__dp_r4d)
#endif
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r4d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_hst1__dp_r4d )
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_r4d, A_hst1__dp_r4d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_r4d, A_dev1__dp_r4d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_r4d, A_dev2__dp_r4d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_r4d, A_hst2__dp_r4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_r4d -A_hst1__dp_r4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_r4d)
  deallocate(A_hst2__dp_r4d)
  deallocate(A_hst3__dp_r4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_r4d)
  call devxlib_unmap(A_dev2__dp_r4d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_r4d ) .or. &
             devxlib_mapped( A_dev2__dp_r4d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_r4d)
  deallocate(A_dev2__dp_r4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_c1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c1d ) .or. &
             devxlib_mapped( A_dev2__sp_c1d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_c1d(bound1(1):bound1(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_c1d(bound1(1):bound1(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_c1d(bound1(1):bound1(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst2__sp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst3__sp_c1d(bound1(1):bound1(2)) )
#endif
  allocate( A_dev1__sp_c1d(bound1(1):bound1(2)) )
  allocate( A_dev2__sp_c1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_c1d)
  call devxlib_map(A_dev2__sp_c1d)
#endif
  allocate( A_rtmp__sp_1d(bound1(1):bound1(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c1d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__sp_1d )
  A_hst1__sp_c1d=A_rtmp__sp_1d
  call random_number( A_rtmp__sp_1d )
  A_hst1__sp_c1d=A_hst1__sp_c1d+cmplx(0.0,1.0)*A_rtmp__sp_1d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c1d, A_hst1__sp_c1d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_c1d, A_dev1__sp_c1d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c1d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c1d, A_dev2__sp_c1d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__sp_c1d = conjg( A_hst2__sp_c1d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c1d, A_hst2__sp_c1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_c1d -A_hst1__sp_c1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c1d)
  deallocate(A_hst2__sp_c1d)
  deallocate(A_hst3__sp_c1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_c1d)
  call devxlib_unmap(A_dev2__sp_c1d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c1d ) .or. &
             devxlib_mapped( A_dev2__sp_c1d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_c1d)
  deallocate(A_dev2__sp_c1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_c2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c2d ) .or. &
             devxlib_mapped( A_dev2__sp_c2d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst3__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#endif
  allocate( A_dev1__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev2__sp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_c2d)
  call devxlib_map(A_dev2__sp_c2d)
#endif
  allocate( A_rtmp__sp_2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c2d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__sp_2d )
  A_hst1__sp_c2d=A_rtmp__sp_2d
  call random_number( A_rtmp__sp_2d )
  A_hst1__sp_c2d=A_hst1__sp_c2d+cmplx(0.0,1.0)*A_rtmp__sp_2d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c2d, A_hst1__sp_c2d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_c2d, A_dev1__sp_c2d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c2d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c2d, A_dev2__sp_c2d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__sp_c2d = conjg( A_hst2__sp_c2d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c2d, A_hst2__sp_c2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_c2d -A_hst1__sp_c2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c2d)
  deallocate(A_hst2__sp_c2d)
  deallocate(A_hst3__sp_c2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_c2d)
  call devxlib_unmap(A_dev2__sp_c2d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c2d ) .or. &
             devxlib_mapped( A_dev2__sp_c2d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_c2d)
  deallocate(A_dev2__sp_c2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_c3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c3d ) .or. &
             devxlib_mapped( A_dev2__sp_c3d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst3__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#endif
  allocate( A_dev1__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev2__sp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_c3d)
  call devxlib_map(A_dev2__sp_c3d)
#endif
  allocate( A_rtmp__sp_3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c3d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__sp_3d )
  A_hst1__sp_c3d=A_rtmp__sp_3d
  call random_number( A_rtmp__sp_3d )
  A_hst1__sp_c3d=A_hst1__sp_c3d+cmplx(0.0,1.0)*A_rtmp__sp_3d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c3d, A_hst1__sp_c3d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_c3d, A_dev1__sp_c3d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c3d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c3d, A_dev2__sp_c3d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__sp_c3d = conjg( A_hst2__sp_c3d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c3d, A_hst2__sp_c3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_c3d -A_hst1__sp_c3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c3d)
  deallocate(A_hst2__sp_c3d)
  deallocate(A_hst3__sp_c3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_c3d)
  call devxlib_unmap(A_dev2__sp_c3d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c3d ) .or. &
             devxlib_mapped( A_dev2__sp_c3d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_c3d)
  deallocate(A_dev2__sp_c3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking sp_c4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c4d ) .or. &
             devxlib_mapped( A_dev2__sp_c4d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst3__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#endif
  allocate( A_dev1__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev2__sp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__sp_c4d)
  call devxlib_map(A_dev2__sp_c4d)
#endif
  allocate( A_rtmp__sp_4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c4d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__sp_4d )
  A_hst1__sp_c4d=A_rtmp__sp_4d
  call random_number( A_rtmp__sp_4d )
  A_hst1__sp_c4d=A_hst1__sp_c4d+cmplx(0.0,1.0)*A_rtmp__sp_4d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__sp_c4d, A_hst1__sp_c4d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__sp_c4d, A_dev1__sp_c4d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__sp_c4d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__sp_c4d, A_dev2__sp_c4d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__sp_c4d = conjg( A_hst2__sp_c4d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__sp_c4d, A_hst2__sp_c4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__sp_c4d -A_hst1__sp_c4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__sp_c4d)
  deallocate(A_hst2__sp_c4d)
  deallocate(A_hst3__sp_c4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__sp_c4d)
  call devxlib_unmap(A_dev2__sp_c4d)
  !
  is_alloc = devxlib_mapped( A_dev1__sp_c4d ) .or. &
             devxlib_mapped( A_dev2__sp_c4d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__sp_c4d)
  deallocate(A_dev2__sp_c4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_c1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c1d ) .or. &
             devxlib_mapped( A_dev2__dp_c1d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_c1d(bound1(1):bound1(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_c1d(bound1(1):bound1(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_c1d(bound1(1):bound1(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst2__dp_c1d(bound1(1):bound1(2)) )
  allocate( A_hst3__dp_c1d(bound1(1):bound1(2)) )
#endif
  allocate( A_dev1__dp_c1d(bound1(1):bound1(2)) )
  allocate( A_dev2__dp_c1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_c1d)
  call devxlib_map(A_dev2__dp_c1d)
#endif
  allocate( A_rtmp__dp_1d(bound1(1):bound1(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c1d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__dp_1d )
  A_hst1__dp_c1d=A_rtmp__dp_1d
  call random_number( A_rtmp__dp_1d )
  A_hst1__dp_c1d=A_hst1__dp_c1d+cmplx(0.0,1.0)*A_rtmp__dp_1d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c1d, A_hst1__dp_c1d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_c1d, A_dev1__dp_c1d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c1d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c1d, A_dev2__dp_c1d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__dp_c1d = conjg( A_hst2__dp_c1d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c1d, A_hst2__dp_c1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_c1d -A_hst1__dp_c1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c1d)
  deallocate(A_hst2__dp_c1d)
  deallocate(A_hst3__dp_c1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_c1d)
  call devxlib_unmap(A_dev2__dp_c1d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c1d ) .or. &
             devxlib_mapped( A_dev2__dp_c1d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_c1d)
  deallocate(A_dev2__dp_c1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_c2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c2d ) .or. &
             devxlib_mapped( A_dev2__dp_c2d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst3__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#endif
  allocate( A_dev1__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev2__dp_c2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_c2d)
  call devxlib_map(A_dev2__dp_c2d)
#endif
  allocate( A_rtmp__dp_2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c2d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__dp_2d )
  A_hst1__dp_c2d=A_rtmp__dp_2d
  call random_number( A_rtmp__dp_2d )
  A_hst1__dp_c2d=A_hst1__dp_c2d+cmplx(0.0,1.0)*A_rtmp__dp_2d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c2d, A_hst1__dp_c2d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_c2d, A_dev1__dp_c2d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c2d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c2d, A_dev2__dp_c2d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__dp_c2d = conjg( A_hst2__dp_c2d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c2d, A_hst2__dp_c2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_c2d -A_hst1__dp_c2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c2d)
  deallocate(A_hst2__dp_c2d)
  deallocate(A_hst3__dp_c2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_c2d)
  call devxlib_unmap(A_dev2__dp_c2d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c2d ) .or. &
             devxlib_mapped( A_dev2__dp_c2d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_c2d)
  deallocate(A_dev2__dp_c2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_c3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c3d ) .or. &
             devxlib_mapped( A_dev2__dp_c3d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst3__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#endif
  allocate( A_dev1__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev2__dp_c3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_c3d)
  call devxlib_map(A_dev2__dp_c3d)
#endif
  allocate( A_rtmp__dp_3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c3d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__dp_3d )
  A_hst1__dp_c3d=A_rtmp__dp_3d
  call random_number( A_rtmp__dp_3d )
  A_hst1__dp_c3d=A_hst1__dp_c3d+cmplx(0.0,1.0)*A_rtmp__dp_3d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c3d, A_hst1__dp_c3d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_c3d, A_dev1__dp_c3d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c3d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c3d, A_dev2__dp_c3d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__dp_c3d = conjg( A_hst2__dp_c3d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c3d, A_hst2__dp_c3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_c3d -A_hst1__dp_c3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c3d)
  deallocate(A_hst2__dp_c3d)
  deallocate(A_hst3__dp_c3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_c3d)
  call devxlib_unmap(A_dev2__dp_c3d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c3d ) .or. &
             devxlib_mapped( A_dev2__dp_c3d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_c3d)
  deallocate(A_dev2__dp_c3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking dp_c4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c4d ) .or. &
             devxlib_mapped( A_dev2__dp_c4d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst3__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#endif
  allocate( A_dev1__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev2__dp_c4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__dp_c4d)
  call devxlib_map(A_dev2__dp_c4d)
#endif
  allocate( A_rtmp__dp_4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c4d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__dp_4d )
  A_hst1__dp_c4d=A_rtmp__dp_4d
  call random_number( A_rtmp__dp_4d )
  A_hst1__dp_c4d=A_hst1__dp_c4d+cmplx(0.0,1.0)*A_rtmp__dp_4d
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__dp_c4d, A_hst1__dp_c4d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__dp_c4d, A_dev1__dp_c4d, async_id)
  !
  ! make cmplx conjg
  call devxlib_conjg_d( A_dev2__dp_c4d, async_id)
  
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__dp_c4d, A_dev2__dp_c4d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! retrieve conjg data
  A_hst2__dp_c4d = conjg( A_hst2__dp_c4d )
  
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__dp_c4d, A_hst2__dp_c4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__dp_c4d -A_hst1__dp_c4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__dp_c4d)
  deallocate(A_hst2__dp_c4d)
  deallocate(A_hst3__dp_c4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__dp_c4d)
  call devxlib_unmap(A_dev2__dp_c4d)
  !
  is_alloc = devxlib_mapped( A_dev1__dp_c4d ) .or. &
             devxlib_mapped( A_dev2__dp_c4d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__dp_c4d)
  deallocate(A_dev2__dp_c4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i4_i1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i1d ) .or. &
             devxlib_mapped( A_dev2__i4_i1d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i4_i1d(bound1(1):bound1(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i4_i1d(bound1(1):bound1(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i4_i1d(bound1(1):bound1(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i4_i1d(bound1(1):bound1(2)) )
  allocate( A_hst2__i4_i1d(bound1(1):bound1(2)) )
  allocate( A_hst3__i4_i1d(bound1(1):bound1(2)) )
#endif
  allocate( A_dev1__i4_i1d(bound1(1):bound1(2)) )
  allocate( A_dev2__i4_i1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i4_i1d)
  call devxlib_map(A_dev2__i4_i1d)
#endif
  allocate( A_rtmp__i4_1d(bound1(1):bound1(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i1d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i4_1d )
  A_hst1__i4_i1d=int( 1000* A_rtmp__i4_1d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i1d, A_hst1__i4_i1d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i4_i1d, A_dev1__i4_i1d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i1d, A_dev2__i4_i1d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i1d, A_hst2__i4_i1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i4_i1d -A_hst1__i4_i1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i1d)
  deallocate(A_hst2__i4_i1d)
  deallocate(A_hst3__i4_i1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i4_i1d)
  call devxlib_unmap(A_dev2__i4_i1d)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i1d ) .or. &
             devxlib_mapped( A_dev2__i4_i1d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i4_i1d)
  deallocate(A_dev2__i4_i1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i4_i2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i2d ) .or. &
             devxlib_mapped( A_dev2__i4_i2d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst3__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#endif
  allocate( A_dev1__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev2__i4_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i4_i2d)
  call devxlib_map(A_dev2__i4_i2d)
#endif
  allocate( A_rtmp__i4_2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i2d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i4_2d )
  A_hst1__i4_i2d=int( 1000* A_rtmp__i4_2d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i2d, A_hst1__i4_i2d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i4_i2d, A_dev1__i4_i2d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i2d, A_dev2__i4_i2d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i2d, A_hst2__i4_i2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i4_i2d -A_hst1__i4_i2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i2d)
  deallocate(A_hst2__i4_i2d)
  deallocate(A_hst3__i4_i2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i4_i2d)
  call devxlib_unmap(A_dev2__i4_i2d)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i2d ) .or. &
             devxlib_mapped( A_dev2__i4_i2d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i4_i2d)
  deallocate(A_dev2__i4_i2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i4_i3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i3d ) .or. &
             devxlib_mapped( A_dev2__i4_i3d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst3__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#endif
  allocate( A_dev1__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev2__i4_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i4_i3d)
  call devxlib_map(A_dev2__i4_i3d)
#endif
  allocate( A_rtmp__i4_3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i3d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i4_3d )
  A_hst1__i4_i3d=int( 1000* A_rtmp__i4_3d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i3d, A_hst1__i4_i3d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i4_i3d, A_dev1__i4_i3d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i3d, A_dev2__i4_i3d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i3d, A_hst2__i4_i3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i4_i3d -A_hst1__i4_i3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i3d)
  deallocate(A_hst2__i4_i3d)
  deallocate(A_hst3__i4_i3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i4_i3d)
  call devxlib_unmap(A_dev2__i4_i3d)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i3d ) .or. &
             devxlib_mapped( A_dev2__i4_i3d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i4_i3d)
  deallocate(A_dev2__i4_i3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i4_i4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i4d ) .or. &
             devxlib_mapped( A_dev2__i4_i4d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst3__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#endif
  allocate( A_dev1__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev2__i4_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i4_i4d)
  call devxlib_map(A_dev2__i4_i4d)
#endif
  allocate( A_rtmp__i4_4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i4d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i4_4d )
  A_hst1__i4_i4d=int( 1000* A_rtmp__i4_4d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i4_i4d, A_hst1__i4_i4d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i4_i4d, A_dev1__i4_i4d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i4_i4d, A_dev2__i4_i4d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i4_i4d, A_hst2__i4_i4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i4_i4d -A_hst1__i4_i4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i4_i4d)
  deallocate(A_hst2__i4_i4d)
  deallocate(A_hst3__i4_i4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i4_i4d)
  call devxlib_unmap(A_dev2__i4_i4d)
  !
  is_alloc = devxlib_mapped( A_dev1__i4_i4d ) .or. &
             devxlib_mapped( A_dev2__i4_i4d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i4_i4d)
  deallocate(A_dev2__i4_i4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !


  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i8_i1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i1d ) .or. &
             devxlib_mapped( A_dev2__i8_i1d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i8_i1d(bound1(1):bound1(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i8_i1d(bound1(1):bound1(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i8_i1d(bound1(1):bound1(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i8_i1d(bound1(1):bound1(2)) )
  allocate( A_hst2__i8_i1d(bound1(1):bound1(2)) )
  allocate( A_hst3__i8_i1d(bound1(1):bound1(2)) )
#endif
  allocate( A_dev1__i8_i1d(bound1(1):bound1(2)) )
  allocate( A_dev2__i8_i1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i8_i1d)
  call devxlib_map(A_dev2__i8_i1d)
#endif
  allocate( A_rtmp__i8_1d(bound1(1):bound1(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i1d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i8_1d )
  A_hst1__i8_i1d=int( 1000* A_rtmp__i8_1d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i1d, A_hst1__i8_i1d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i8_i1d, A_dev1__i8_i1d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i1d, A_dev2__i8_i1d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i1d, A_hst2__i8_i1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i8_i1d -A_hst1__i8_i1d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i1d)
  deallocate(A_hst2__i8_i1d)
  deallocate(A_hst3__i8_i1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i8_i1d)
  call devxlib_unmap(A_dev2__i8_i1d)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i1d ) .or. &
             devxlib_mapped( A_dev2__i8_i1d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i8_i1d)
  deallocate(A_dev2__i8_i1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i8_i2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i2d ) .or. &
             devxlib_mapped( A_dev2__i8_i2d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst3__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#endif
  allocate( A_dev1__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev2__i8_i2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i8_i2d)
  call devxlib_map(A_dev2__i8_i2d)
#endif
  allocate( A_rtmp__i8_2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i2d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i8_2d )
  A_hst1__i8_i2d=int( 1000* A_rtmp__i8_2d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i2d, A_hst1__i8_i2d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i8_i2d, A_dev1__i8_i2d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i2d, A_dev2__i8_i2d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i2d, A_hst2__i8_i2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i8_i2d -A_hst1__i8_i2d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i2d)
  deallocate(A_hst2__i8_i2d)
  deallocate(A_hst3__i8_i2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i8_i2d)
  call devxlib_unmap(A_dev2__i8_i2d)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i2d ) .or. &
             devxlib_mapped( A_dev2__i8_i2d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i8_i2d)
  deallocate(A_dev2__i8_i2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i8_i3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i3d ) .or. &
             devxlib_mapped( A_dev2__i8_i3d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst3__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#endif
  allocate( A_dev1__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev2__i8_i3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i8_i3d)
  call devxlib_map(A_dev2__i8_i3d)
#endif
  allocate( A_rtmp__i8_3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i3d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i8_3d )
  A_hst1__i8_i3d=int( 1000* A_rtmp__i8_3d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i3d, A_hst1__i8_i3d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i8_i3d, A_dev1__i8_i3d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i3d, A_dev2__i8_i3d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i3d, A_hst2__i8_i3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i8_i3d -A_hst1__i8_i3d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i3d)
  deallocate(A_hst2__i8_i3d)
  deallocate(A_hst3__i8_i3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i8_i3d)
  call devxlib_unmap(A_dev2__i8_i3d)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i3d ) .or. &
             devxlib_mapped( A_dev2__i8_i3d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i8_i3d)
  deallocate(A_dev2__i8_i3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking i8_i4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i4d ) .or. &
             devxlib_mapped( A_dev2__i8_i4d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst3__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#endif
  allocate( A_dev1__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev2__i8_i4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__i8_i4d)
  call devxlib_map(A_dev2__i8_i4d)
#endif
  allocate( A_rtmp__i8_4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i4d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__i8_4d )
  A_hst1__i8_i4d=int( 1000* A_rtmp__i8_4d )
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__i8_i4d, A_hst1__i8_i4d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__i8_i4d, A_dev1__i8_i4d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__i8_i4d, A_dev2__i8_i4d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__i8_i4d, A_hst2__i8_i4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any(abs(A_hst3__i8_i4d -A_hst1__i8_i4d )> thr) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__i8_i4d)
  deallocate(A_hst2__i8_i4d)
  deallocate(A_hst3__i8_i4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__i8_i4d)
  call devxlib_unmap(A_dev2__i8_i4d)
  !
  is_alloc = devxlib_mapped( A_dev1__i8_i4d ) .or. &
             devxlib_mapped( A_dev2__i8_i4d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__i8_i4d)
  deallocate(A_dev2__i8_i4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !



  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking l4_l1d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l1d ) .or. &
             devxlib_mapped( A_dev2__l4_l1d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__l4_l1d(bound1(1):bound1(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__l4_l1d(bound1(1):bound1(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__l4_l1d(bound1(1):bound1(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__l4_l1d(bound1(1):bound1(2)) )
  allocate( A_hst2__l4_l1d(bound1(1):bound1(2)) )
  allocate( A_hst3__l4_l1d(bound1(1):bound1(2)) )
#endif
  allocate( A_dev1__l4_l1d(bound1(1):bound1(2)) )
  allocate( A_dev2__l4_l1d(bound1(1):bound1(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__l4_l1d)
  call devxlib_map(A_dev2__l4_l1d)
#endif
  allocate( A_rtmp__l4_1d(bound1(1):bound1(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l1d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__l4_1d )
  A_hst1__l4_l1d=( nint( A_rtmp__l4_1d) == 1 ) 
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l1d, A_hst1__l4_l1d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__l4_l1d, A_dev1__l4_l1d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l1d, A_dev2__l4_l1d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l1d, A_hst2__l4_l1d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any( A_hst3__l4_l1d .neqv. A_hst1__l4_l1d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l1d)
  deallocate(A_hst2__l4_l1d)
  deallocate(A_hst3__l4_l1d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__l4_l1d)
  call devxlib_unmap(A_dev2__l4_l1d)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l1d ) .or. &
             devxlib_mapped( A_dev2__l4_l1d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__l4_l1d)
  deallocate(A_dev2__l4_l1d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking l4_l2d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l2d ) .or. &
             devxlib_mapped( A_dev2__l4_l2d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst2__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_hst3__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#endif
  allocate( A_dev1__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  allocate( A_dev2__l4_l2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__l4_l2d)
  call devxlib_map(A_dev2__l4_l2d)
#endif
  allocate( A_rtmp__l4_2d(bound1(1):bound1(2),bound2(1):bound2(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l2d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__l4_2d )
  A_hst1__l4_l2d=( nint( A_rtmp__l4_2d) == 1 ) 
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l2d, A_hst1__l4_l2d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__l4_l2d, A_dev1__l4_l2d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l2d, A_dev2__l4_l2d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l2d, A_hst2__l4_l2d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any( A_hst3__l4_l2d .neqv. A_hst1__l4_l2d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l2d)
  deallocate(A_hst2__l4_l2d)
  deallocate(A_hst3__l4_l2d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__l4_l2d)
  call devxlib_unmap(A_dev2__l4_l2d)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l2d ) .or. &
             devxlib_mapped( A_dev2__l4_l2d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__l4_l2d)
  deallocate(A_dev2__l4_l2d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking l4_l3d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l3d ) .or. &
             devxlib_mapped( A_dev2__l4_l3d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst2__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_hst3__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#endif
  allocate( A_dev1__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  allocate( A_dev2__l4_l3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__l4_l3d)
  call devxlib_map(A_dev2__l4_l3d)
#endif
  allocate( A_rtmp__l4_3d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l3d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__l4_3d )
  A_hst1__l4_l3d=( nint( A_rtmp__l4_3d) == 1 ) 
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l3d, A_hst1__l4_l3d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__l4_l3d, A_dev1__l4_l3d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l3d, A_dev2__l4_l3d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l3d, A_hst2__l4_l3d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any( A_hst3__l4_l3d .neqv. A_hst1__l4_l3d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l3d)
  deallocate(A_hst2__l4_l3d)
  deallocate(A_hst3__l4_l3d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__l4_l3d)
  call devxlib_unmap(A_dev2__l4_l3d)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l3d ) .or. &
             devxlib_mapped( A_dev2__l4_l3d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__l4_l3d)
  deallocate(A_dev2__l4_l3d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !

  !
  !=====================
  !
  write(6,"(/,3x,a)") "checking l4_l4d ..." 
  call system_clock(cnt, count_rate, count_max)
  t0=cnt/count_rate
  lfail_malloc=.false.
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l4d ) .or. &
             devxlib_mapped( A_dev2__l4_l4d )
  if (is_alloc) then
     nfail_malloc=nfail_malloc+1
     lfail_malloc=.true.
     write(6,"(3x,a)") "mem allocation check-pre FAILED" 
  endif
  !
  ! allocations
#ifdef __DXL_CUDAF
  allocate( A_hst1__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(1) )
  allocate( A_hst2__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(2) )
  allocate( A_hst3__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)), PINNED=is_pinned(3) )
#else
  is_pinned=.false.
  allocate( A_hst1__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst2__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_hst3__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#endif
  allocate( A_dev1__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  allocate( A_dev2__l4_l4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_map(A_dev1__l4_l4d)
  call devxlib_map(A_dev2__l4_l4d)
#endif
  allocate( A_rtmp__l4_4d(bound1(1):bound1(2),bound2(1):bound2(2),bound3(1):bound3(2),bound4(1):bound4(2)) )
  
  !write(6,"(3x,a,2l)") "host memory is pinned: ", is_pinned(1:3)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l4d )
  if (.not.is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post FAILED"
  endif
  !
  ! init
  call random_number( A_rtmp__l4_4d )
  A_hst1__l4_l4d=( nint( A_rtmp__l4_4d) == 1 ) 
  
  !
  call devxlib_async_synchronize(default_async_id)
  call devxlib_async_synchronize(async_id)
  !
  ! mem copy h2d
  call devxlib_memcpy_h2d(A_dev1__l4_l4d, A_hst1__l4_l4d, async_id)
  !
  ! mem copy d2d
  call devxlib_memcpy_d2d(A_dev2__l4_l4d, A_dev1__l4_l4d, async_id)
  !
  !
  ! mem copy d2h
  call devxlib_memcpy_d2h(A_hst2__l4_l4d, A_dev2__l4_l4d,async_id)
  call devxlib_async_synchronize(async_id)
  !
  !
  ! mem copy h2h
  call devxlib_memcpy_h2h(A_hst3__l4_l4d, A_hst2__l4_l4d)
  !
  ! check
  if (lfail_malloc) write(6,"(3x,a)") "Malloc FAILED"
   !
  if ( any( A_hst3__l4_l4d .neqv. A_hst1__l4_l4d ) ) then
     !
     write(6,"(3x,a)") "FAILED" 
     nfail=nfail+1
  else
     write(6,"(3x,a)") "passed" 
     npass=npass+1
  endif
  !
  deallocate(A_hst1__l4_l4d)
  deallocate(A_hst2__l4_l4d)
  deallocate(A_hst3__l4_l4d)
  !
#if defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
  call devxlib_unmap(A_dev1__l4_l4d)
  call devxlib_unmap(A_dev2__l4_l4d)
  !
  is_alloc = devxlib_mapped( A_dev1__l4_l4d ) .or. &
             devxlib_mapped( A_dev2__l4_l4d )
  if (is_alloc) then
    nfail_malloc=nfail_malloc+1
    lfail_malloc=.true.
    write(6,"(3x,a)") "mem allocation check-post2 FAILED"
  endif
  !
#endif
  !
  deallocate(A_dev1__l4_l4d)
  deallocate(A_dev2__l4_l4d)
  !
  call system_clock(cnt, count_rate, count_max)
  t1=cnt/count_rate
  write(6,"(3x,a,f12.6,' sec')") "Timing: ", t1-t0
  !




  !
  ! summary
  !
  write(6,"(/,a)") "# Test SUMMARY:"
  write(6,"(3x,a,i5)") "# passed: ", npass
  write(6,"(3x,a,i5)") "# failed: ", nfail
  write(6,"(3x,a,i5)") "# failed malloc: ", nfail_malloc
  write(6,"()")

end program test_memcpy_async
