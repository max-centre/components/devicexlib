!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Auxiliary functions
!
! AF: OpenACC support is incomplete !!
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_auxfunc_vec_upd.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_auxfunc) devxlib_auxfunc_vec_upd

   implicit none

   contains

      module subroutine dp_devxlib_vec_upd_remap_r1d(ndim, vout, v1, map1, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         real(real64) DEV_ATTR, intent(inout) :: vout(:) 
         real(real64) DEV_ATTR, intent(in)    :: v1(:) 
         integer DEV_ATTR,      intent(in)    :: map1(:) 
         real(real64), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))*scal
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         else
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         endif
      end subroutine dp_devxlib_vec_upd_remap_r1d
      !
      module subroutine dp_devxlib_vec_upd_remap_c1d(ndim, vout, v1, map1, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         complex(real64) DEV_ATTR, intent(inout) :: vout(:) 
         complex(real64) DEV_ATTR, intent(in)    :: v1(:) 
         integer DEV_ATTR,      intent(in)    :: map1(:) 
         complex(real64), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))*scal
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         else
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         endif
      end subroutine dp_devxlib_vec_upd_remap_c1d
      !
      module subroutine sp_devxlib_vec_upd_remap_r1d(ndim, vout, v1, map1, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         real(real32) DEV_ATTR, intent(inout) :: vout(:) 
         real(real32) DEV_ATTR, intent(in)    :: v1(:) 
         integer DEV_ATTR,      intent(in)    :: map1(:) 
         real(real32), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))*scal
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         else
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         endif
      end subroutine sp_devxlib_vec_upd_remap_r1d
      !
      module subroutine sp_devxlib_vec_upd_remap_c1d(ndim, vout, v1, map1, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         complex(real32) DEV_ATTR, intent(inout) :: vout(:) 
         complex(real32) DEV_ATTR, intent(in)    :: v1(:) 
         integer DEV_ATTR,      intent(in)    :: map1(:) 
         complex(real32), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))*scal
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         else
            !DEV_CUF kernel do(1)
            !DEV_ACC data present(vout,v1,map1)
            !DEV_ACC parallel loop async
            !DEV_OMPGPU target map(present,alloc:vout,v1,map1) nowait
            !DEV_OMPGPU teams loop
            do i = 1, ndim
               vout(i) = v1(map1(i))
            enddo
            !DEV_ACC end data
            !DEV_OMPGPU end target
         endif
      end subroutine sp_devxlib_vec_upd_remap_c1d
      !


      module subroutine dp_devxlib_vec_upd_v_remap_v_r1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         real(real64), intent(inout) DEV_ATTR :: vout(:) 
         real(real64), intent(in)    DEV_ATTR :: v1(:) 
         integer,      intent(in)    DEV_ATTR :: map1(:) 
         real(real64), intent(in)    DEV_ATTR :: v2(:) 
         real(real64), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)*scal
            enddo
         else
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)
            enddo
         endif
      end subroutine dp_devxlib_vec_upd_v_remap_v_r1d
      !
      module subroutine dp_devxlib_vec_upd_v_remap_v_c1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         complex(real64), intent(inout) DEV_ATTR :: vout(:) 
         complex(real64), intent(in)    DEV_ATTR :: v1(:) 
         integer,      intent(in)    DEV_ATTR :: map1(:) 
         complex(real64), intent(in)    DEV_ATTR :: v2(:) 
         complex(real64), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)*scal
            enddo
         else
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)
            enddo
         endif
      end subroutine dp_devxlib_vec_upd_v_remap_v_c1d
      !
      module subroutine sp_devxlib_vec_upd_v_remap_v_r1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         real(real32), intent(inout) DEV_ATTR :: vout(:) 
         real(real32), intent(in)    DEV_ATTR :: v1(:) 
         integer,      intent(in)    DEV_ATTR :: map1(:) 
         real(real32), intent(in)    DEV_ATTR :: v2(:) 
         real(real32), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)*scal
            enddo
         else
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)
            enddo
         endif
      end subroutine sp_devxlib_vec_upd_v_remap_v_r1d
      !
      module subroutine sp_devxlib_vec_upd_v_remap_v_c1d(ndim, vout, v1, map1, v2, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         complex(real32), intent(inout) DEV_ATTR :: vout(:) 
         complex(real32), intent(in)    DEV_ATTR :: v1(:) 
         integer,      intent(in)    DEV_ATTR :: map1(:) 
         complex(real32), intent(in)    DEV_ATTR :: v2(:) 
         complex(real32), optional, intent(in)   :: scal
         integer :: i
         !   
         if (present(scal)) then
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)*scal
            enddo
         else
            !DEV_CUF kernel do(1)
            do i = 1, ndim
               vout(i) = v1(map1(i))*v2(i)
            enddo
         endif
      end subroutine sp_devxlib_vec_upd_v_remap_v_c1d
      !
!
!======================
!


      module subroutine dp_devxlib_vec_upd_v_remap_v_x_c1d(ndim, vout, v1,op1, map1, v2,op2, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         complex(real64), intent(inout) DEV_ATTR :: vout(:) 
         complex(real64), intent(in)    DEV_ATTR :: v1(:) 
         integer,      intent(in)    DEV_ATTR :: map1(:) 
         complex(real64), intent(in)    DEV_ATTR :: v2(:) 
         character(1), intent(in)    :: op1, op2
         complex(real64), optional, intent(in)    :: scal
         integer :: i
         !
         if (op1=="N".and.op2=="N") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*v2(i)*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*v2(i)
               enddo
            endif
         elseif (op1=="C".and.op2=="N") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*v2(i)*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*v2(i)
               enddo
            endif
         elseif (op1=="N".and.op2=="C") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*conjg(v2(i))*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*conjg(v2(i))
               enddo
            endif
         elseif (op1=="N".and.op2=="C") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*conjg(v2(i))*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*conjg(v2(i))
               enddo
            endif
         else
            call devxlib_error("dp_devxlib_vec_upd_v_remap_v_x_c1d","invalid op1/op2",10)
         endif
         !
      end subroutine dp_devxlib_vec_upd_v_remap_v_x_c1d
      !
      module subroutine sp_devxlib_vec_upd_v_remap_v_x_c1d(ndim, vout, v1,op1, map1, v2,op2, scal)
         implicit none
         !   
         integer,      intent(in)    :: ndim
         complex(real32), intent(inout) DEV_ATTR :: vout(:) 
         complex(real32), intent(in)    DEV_ATTR :: v1(:) 
         integer,      intent(in)    DEV_ATTR :: map1(:) 
         complex(real32), intent(in)    DEV_ATTR :: v2(:) 
         character(1), intent(in)    :: op1, op2
         complex(real32), optional, intent(in)    :: scal
         integer :: i
         !
         if (op1=="N".and.op2=="N") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*v2(i)*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*v2(i)
               enddo
            endif
         elseif (op1=="C".and.op2=="N") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*v2(i)*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*v2(i)
               enddo
            endif
         elseif (op1=="N".and.op2=="C") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*conjg(v2(i))*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = v1(map1(i))*conjg(v2(i))
               enddo
            endif
         elseif (op1=="N".and.op2=="C") then
            if (present(scal)) then
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*conjg(v2(i))*scal
               enddo
            else
               !DEV_CUF kernel do(1)
               do i = 1, ndim
                  vout(i) = conjg(v1(map1(i)))*conjg(v2(i))
               enddo
            endif
         else
            call devxlib_error("sp_devxlib_vec_upd_v_remap_v_x_c1d","invalid op1/op2",10)
         endif
         !
      end subroutine sp_devxlib_vec_upd_v_remap_v_x_c1d
      !

end submodule devxlib_auxfunc_vec_upd