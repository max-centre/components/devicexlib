!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
!
! Utility functions to perform all memory synchronizations (host to
! host, device to device, host to device and device to host), both
! synchronous and asynchronous, using CUDA-Fortran, OpenACC and
! OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_memcpy.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
! Note about dimensions:
! The lower bound of the assumed shape array passed to the subroutine is 1
! lbound and range instead refer to the indexing in the parent caller.
!
module devxlib_memcpy

   use iso_fortran_env,             only : int32, int64, real32, real64
   use, intrinsic :: iso_c_binding, only : c_ptr, c_loc, c_associated, c_size_t
   use devxlib_environment,         only : devxlib_error
   use devxlib_async,               only : dxl_async_kind
   !
#if defined __DXL_CUDAF
   use cudafor
#elif defined __DXL_OPENACC
   use openacc
   use devxlib_environment,    only : acc_memcpy_device_f, acc_memcpy_from_device_f, acc_memcpy_to_device_f
#elif defined __DXL_OPENMP_GPU
   use omp_lib,                only : omp_get_default_device, omp_get_initial_device, omp_target_memcpy
#endif

   implicit none

   !private :: dxl_async_kind

   interface devxlib_memcpy_d2d
      module procedure &
         sp_devxlib_memcpy_d2d_r1d, sp_devxlib_memcpy_d2d_r2d, sp_devxlib_memcpy_d2d_r3d, &
         sp_devxlib_memcpy_d2d_r4d, sp_devxlib_memcpy_d2d_r5d, sp_devxlib_memcpy_d2d_r6d, &
         dp_devxlib_memcpy_d2d_r1d, dp_devxlib_memcpy_d2d_r2d, dp_devxlib_memcpy_d2d_r3d, &
         dp_devxlib_memcpy_d2d_r4d, dp_devxlib_memcpy_d2d_r5d, dp_devxlib_memcpy_d2d_r6d, &
         sp_devxlib_memcpy_d2d_c1d, sp_devxlib_memcpy_d2d_c2d, sp_devxlib_memcpy_d2d_c3d, &
         sp_devxlib_memcpy_d2d_c4d, sp_devxlib_memcpy_d2d_c5d, sp_devxlib_memcpy_d2d_c6d, &
         dp_devxlib_memcpy_d2d_c1d, dp_devxlib_memcpy_d2d_c2d, dp_devxlib_memcpy_d2d_c3d, &
         dp_devxlib_memcpy_d2d_c4d, dp_devxlib_memcpy_d2d_c5d, dp_devxlib_memcpy_d2d_c6d, &
         i4_devxlib_memcpy_d2d_i1d, i4_devxlib_memcpy_d2d_i2d, i4_devxlib_memcpy_d2d_i3d, &
         i4_devxlib_memcpy_d2d_i4d, i4_devxlib_memcpy_d2d_i5d, i4_devxlib_memcpy_d2d_i6d, &
         i8_devxlib_memcpy_d2d_i1d, i8_devxlib_memcpy_d2d_i2d, i8_devxlib_memcpy_d2d_i3d, &
         i8_devxlib_memcpy_d2d_i4d, i8_devxlib_memcpy_d2d_i5d, i8_devxlib_memcpy_d2d_i6d, &
         l4_devxlib_memcpy_d2d_l1d, l4_devxlib_memcpy_d2d_l2d, l4_devxlib_memcpy_d2d_l3d, &
         l4_devxlib_memcpy_d2d_l4d, l4_devxlib_memcpy_d2d_l5d, l4_devxlib_memcpy_d2d_l6d, &
         sp_devxlib_memcpy_d2d_async_r1d, sp_devxlib_memcpy_d2d_async_r2d, sp_devxlib_memcpy_d2d_async_r3d, &
         sp_devxlib_memcpy_d2d_async_r4d, sp_devxlib_memcpy_d2d_async_r5d, sp_devxlib_memcpy_d2d_async_r6d, &
         dp_devxlib_memcpy_d2d_async_r1d, dp_devxlib_memcpy_d2d_async_r2d, dp_devxlib_memcpy_d2d_async_r3d, &
         dp_devxlib_memcpy_d2d_async_r4d, dp_devxlib_memcpy_d2d_async_r5d, dp_devxlib_memcpy_d2d_async_r6d, &
         sp_devxlib_memcpy_d2d_async_c1d, sp_devxlib_memcpy_d2d_async_c2d, sp_devxlib_memcpy_d2d_async_c3d, &
         sp_devxlib_memcpy_d2d_async_c4d, sp_devxlib_memcpy_d2d_async_c5d, sp_devxlib_memcpy_d2d_async_c6d, &
         dp_devxlib_memcpy_d2d_async_c1d, dp_devxlib_memcpy_d2d_async_c2d, dp_devxlib_memcpy_d2d_async_c3d, &
         dp_devxlib_memcpy_d2d_async_c4d, dp_devxlib_memcpy_d2d_async_c5d, dp_devxlib_memcpy_d2d_async_c6d, &
         i4_devxlib_memcpy_d2d_async_i1d, i4_devxlib_memcpy_d2d_async_i2d, i4_devxlib_memcpy_d2d_async_i3d, &
         i4_devxlib_memcpy_d2d_async_i4d, i4_devxlib_memcpy_d2d_async_i5d, i4_devxlib_memcpy_d2d_async_i6d, &
         i8_devxlib_memcpy_d2d_async_i1d, i8_devxlib_memcpy_d2d_async_i2d, i8_devxlib_memcpy_d2d_async_i3d, &
         i8_devxlib_memcpy_d2d_async_i4d, i8_devxlib_memcpy_d2d_async_i5d, i8_devxlib_memcpy_d2d_async_i6d, &
         l4_devxlib_memcpy_d2d_async_l1d, l4_devxlib_memcpy_d2d_async_l2d, l4_devxlib_memcpy_d2d_async_l3d, &
         l4_devxlib_memcpy_d2d_async_l4d, l4_devxlib_memcpy_d2d_async_l5d, l4_devxlib_memcpy_d2d_async_l6d
   endinterface devxlib_memcpy_d2d

   interface devxlib_memcpy_d2h
      module procedure &
         sp_devxlib_memcpy_d2h_r1d, sp_devxlib_memcpy_d2h_r2d, sp_devxlib_memcpy_d2h_r3d, &
         sp_devxlib_memcpy_d2h_r4d, sp_devxlib_memcpy_d2h_r5d, sp_devxlib_memcpy_d2h_r6d, &
         dp_devxlib_memcpy_d2h_r1d, dp_devxlib_memcpy_d2h_r2d, dp_devxlib_memcpy_d2h_r3d, &
         dp_devxlib_memcpy_d2h_r4d, dp_devxlib_memcpy_d2h_r5d, dp_devxlib_memcpy_d2h_r6d, &
         sp_devxlib_memcpy_d2h_c1d, sp_devxlib_memcpy_d2h_c2d, sp_devxlib_memcpy_d2h_c3d, &
         sp_devxlib_memcpy_d2h_c4d, sp_devxlib_memcpy_d2h_c5d, sp_devxlib_memcpy_d2h_c6d, &
         dp_devxlib_memcpy_d2h_c1d, dp_devxlib_memcpy_d2h_c2d, dp_devxlib_memcpy_d2h_c3d, &
         dp_devxlib_memcpy_d2h_c4d, dp_devxlib_memcpy_d2h_c5d, dp_devxlib_memcpy_d2h_c6d, &
         i4_devxlib_memcpy_d2h_i1d, i4_devxlib_memcpy_d2h_i2d, i4_devxlib_memcpy_d2h_i3d, &
         i4_devxlib_memcpy_d2h_i4d, i4_devxlib_memcpy_d2h_i5d, i4_devxlib_memcpy_d2h_i6d, &
         i8_devxlib_memcpy_d2h_i1d, i8_devxlib_memcpy_d2h_i2d, i8_devxlib_memcpy_d2h_i3d, &
         i8_devxlib_memcpy_d2h_i4d, i8_devxlib_memcpy_d2h_i5d, i8_devxlib_memcpy_d2h_i6d, &
         l4_devxlib_memcpy_d2h_l1d, l4_devxlib_memcpy_d2h_l2d, l4_devxlib_memcpy_d2h_l3d, &
         l4_devxlib_memcpy_d2h_l4d, l4_devxlib_memcpy_d2h_l5d, l4_devxlib_memcpy_d2h_l6d, &
         sp_devxlib_memcpy_d2h_async_r1d, sp_devxlib_memcpy_d2h_async_r2d, sp_devxlib_memcpy_d2h_async_r3d, &
         sp_devxlib_memcpy_d2h_async_r4d, sp_devxlib_memcpy_d2h_async_r5d, sp_devxlib_memcpy_d2h_async_r6d, &
         dp_devxlib_memcpy_d2h_async_r1d, dp_devxlib_memcpy_d2h_async_r2d, dp_devxlib_memcpy_d2h_async_r3d, &
         dp_devxlib_memcpy_d2h_async_r4d, dp_devxlib_memcpy_d2h_async_r5d, dp_devxlib_memcpy_d2h_async_r6d, &
         sp_devxlib_memcpy_d2h_async_c1d, sp_devxlib_memcpy_d2h_async_c2d, sp_devxlib_memcpy_d2h_async_c3d, &
         sp_devxlib_memcpy_d2h_async_c4d, sp_devxlib_memcpy_d2h_async_c5d, sp_devxlib_memcpy_d2h_async_c6d, &
         dp_devxlib_memcpy_d2h_async_c1d, dp_devxlib_memcpy_d2h_async_c2d, dp_devxlib_memcpy_d2h_async_c3d, &
         dp_devxlib_memcpy_d2h_async_c4d, dp_devxlib_memcpy_d2h_async_c5d, dp_devxlib_memcpy_d2h_async_c6d, &
         i4_devxlib_memcpy_d2h_async_i1d, i4_devxlib_memcpy_d2h_async_i2d, i4_devxlib_memcpy_d2h_async_i3d, &
         i4_devxlib_memcpy_d2h_async_i4d, i4_devxlib_memcpy_d2h_async_i5d, i4_devxlib_memcpy_d2h_async_i6d, &
         i8_devxlib_memcpy_d2h_async_i1d, i8_devxlib_memcpy_d2h_async_i2d, i8_devxlib_memcpy_d2h_async_i3d, &
         i8_devxlib_memcpy_d2h_async_i4d, i8_devxlib_memcpy_d2h_async_i5d, i8_devxlib_memcpy_d2h_async_i6d, &
         l4_devxlib_memcpy_d2h_async_l1d, l4_devxlib_memcpy_d2h_async_l2d, l4_devxlib_memcpy_d2h_async_l3d, &
         l4_devxlib_memcpy_d2h_async_l4d, l4_devxlib_memcpy_d2h_async_l5d, l4_devxlib_memcpy_d2h_async_l6d
   endinterface devxlib_memcpy_d2h

   interface devxlib_memcpy_h2d
      module procedure &
         sp_devxlib_memcpy_h2d_r1d, sp_devxlib_memcpy_h2d_r2d, sp_devxlib_memcpy_h2d_r3d, &
         sp_devxlib_memcpy_h2d_r4d, sp_devxlib_memcpy_h2d_r5d, sp_devxlib_memcpy_h2d_r6d, &
         dp_devxlib_memcpy_h2d_r1d, dp_devxlib_memcpy_h2d_r2d, dp_devxlib_memcpy_h2d_r3d, &
         dp_devxlib_memcpy_h2d_r4d, dp_devxlib_memcpy_h2d_r5d, dp_devxlib_memcpy_h2d_r6d, &
         sp_devxlib_memcpy_h2d_c1d, sp_devxlib_memcpy_h2d_c2d, sp_devxlib_memcpy_h2d_c3d, &
         sp_devxlib_memcpy_h2d_c4d, sp_devxlib_memcpy_h2d_c5d, sp_devxlib_memcpy_h2d_c6d, &
         dp_devxlib_memcpy_h2d_c1d, dp_devxlib_memcpy_h2d_c2d, dp_devxlib_memcpy_h2d_c3d, &
         dp_devxlib_memcpy_h2d_c4d, dp_devxlib_memcpy_h2d_c5d, dp_devxlib_memcpy_h2d_c6d, &
         i4_devxlib_memcpy_h2d_i1d, i4_devxlib_memcpy_h2d_i2d, i4_devxlib_memcpy_h2d_i3d, &
         i4_devxlib_memcpy_h2d_i4d, i4_devxlib_memcpy_h2d_i5d, i4_devxlib_memcpy_h2d_i6d, &
         i8_devxlib_memcpy_h2d_i1d, i8_devxlib_memcpy_h2d_i2d, i8_devxlib_memcpy_h2d_i3d, &
         i8_devxlib_memcpy_h2d_i4d, i8_devxlib_memcpy_h2d_i5d, i8_devxlib_memcpy_h2d_i6d, &
         l4_devxlib_memcpy_h2d_l1d, l4_devxlib_memcpy_h2d_l2d, l4_devxlib_memcpy_h2d_l3d, &
         l4_devxlib_memcpy_h2d_l4d, l4_devxlib_memcpy_h2d_l5d, l4_devxlib_memcpy_h2d_l6d, &
         sp_devxlib_memcpy_h2d_async_r1d, sp_devxlib_memcpy_h2d_async_r2d, sp_devxlib_memcpy_h2d_async_r3d, &
         sp_devxlib_memcpy_h2d_async_r4d, sp_devxlib_memcpy_h2d_async_r5d, sp_devxlib_memcpy_h2d_async_r6d, &
         dp_devxlib_memcpy_h2d_async_r1d, dp_devxlib_memcpy_h2d_async_r2d, dp_devxlib_memcpy_h2d_async_r3d, &
         dp_devxlib_memcpy_h2d_async_r4d, dp_devxlib_memcpy_h2d_async_r5d, dp_devxlib_memcpy_h2d_async_r6d, &
         sp_devxlib_memcpy_h2d_async_c1d, sp_devxlib_memcpy_h2d_async_c2d, sp_devxlib_memcpy_h2d_async_c3d, &
         sp_devxlib_memcpy_h2d_async_c4d, sp_devxlib_memcpy_h2d_async_c5d, sp_devxlib_memcpy_h2d_async_c6d, &
         dp_devxlib_memcpy_h2d_async_c1d, dp_devxlib_memcpy_h2d_async_c2d, dp_devxlib_memcpy_h2d_async_c3d, &
         dp_devxlib_memcpy_h2d_async_c4d, dp_devxlib_memcpy_h2d_async_c5d, dp_devxlib_memcpy_h2d_async_c6d, &
         i4_devxlib_memcpy_h2d_async_i1d, i4_devxlib_memcpy_h2d_async_i2d, i4_devxlib_memcpy_h2d_async_i3d, &
         i4_devxlib_memcpy_h2d_async_i4d, i4_devxlib_memcpy_h2d_async_i5d, i4_devxlib_memcpy_h2d_async_i6d, &
         i8_devxlib_memcpy_h2d_async_i1d, i8_devxlib_memcpy_h2d_async_i2d, i8_devxlib_memcpy_h2d_async_i3d, &
         i8_devxlib_memcpy_h2d_async_i4d, i8_devxlib_memcpy_h2d_async_i5d, i8_devxlib_memcpy_h2d_async_i6d, &
         l4_devxlib_memcpy_h2d_async_l1d, l4_devxlib_memcpy_h2d_async_l2d, l4_devxlib_memcpy_h2d_async_l3d, &
         l4_devxlib_memcpy_h2d_async_l4d, l4_devxlib_memcpy_h2d_async_l5d, l4_devxlib_memcpy_h2d_async_l6d
   endinterface devxlib_memcpy_h2d

   interface devxlib_memcpy_h2h
      module procedure &
         sp_devxlib_memcpy_h2h_r1d, sp_devxlib_memcpy_h2h_r2d, sp_devxlib_memcpy_h2h_r3d, &
         sp_devxlib_memcpy_h2h_r4d, sp_devxlib_memcpy_h2h_r5d, sp_devxlib_memcpy_h2h_r6d, &
         dp_devxlib_memcpy_h2h_r1d, dp_devxlib_memcpy_h2h_r2d, dp_devxlib_memcpy_h2h_r3d, &
         dp_devxlib_memcpy_h2h_r4d, dp_devxlib_memcpy_h2h_r5d, dp_devxlib_memcpy_h2h_r6d, &
         sp_devxlib_memcpy_h2h_c1d, sp_devxlib_memcpy_h2h_c2d, sp_devxlib_memcpy_h2h_c3d, &
         sp_devxlib_memcpy_h2h_c4d, sp_devxlib_memcpy_h2h_c5d, sp_devxlib_memcpy_h2h_c6d, &
         dp_devxlib_memcpy_h2h_c1d, dp_devxlib_memcpy_h2h_c2d, dp_devxlib_memcpy_h2h_c3d, &
         dp_devxlib_memcpy_h2h_c4d, dp_devxlib_memcpy_h2h_c5d, dp_devxlib_memcpy_h2h_c6d, &
         i4_devxlib_memcpy_h2h_i1d, i4_devxlib_memcpy_h2h_i2d, i4_devxlib_memcpy_h2h_i3d, &
         i4_devxlib_memcpy_h2h_i4d, i4_devxlib_memcpy_h2h_i5d, i4_devxlib_memcpy_h2h_i6d, &
         i8_devxlib_memcpy_h2h_i1d, i8_devxlib_memcpy_h2h_i2d, i8_devxlib_memcpy_h2h_i3d, &
         i8_devxlib_memcpy_h2h_i4d, i8_devxlib_memcpy_h2h_i5d, i8_devxlib_memcpy_h2h_i6d, &
         l4_devxlib_memcpy_h2h_l1d, l4_devxlib_memcpy_h2h_l2d, l4_devxlib_memcpy_h2h_l3d, &
         l4_devxlib_memcpy_h2h_l4d, l4_devxlib_memcpy_h2h_l5d, l4_devxlib_memcpy_h2h_l6d, &
         sp_devxlib_memcpy_h2h_async_r1d, sp_devxlib_memcpy_h2h_async_r2d, sp_devxlib_memcpy_h2h_async_r3d, &
         sp_devxlib_memcpy_h2h_async_r4d, sp_devxlib_memcpy_h2h_async_r5d, sp_devxlib_memcpy_h2h_async_r6d, &
         dp_devxlib_memcpy_h2h_async_r1d, dp_devxlib_memcpy_h2h_async_r2d, dp_devxlib_memcpy_h2h_async_r3d, &
         dp_devxlib_memcpy_h2h_async_r4d, dp_devxlib_memcpy_h2h_async_r5d, dp_devxlib_memcpy_h2h_async_r6d, &
         sp_devxlib_memcpy_h2h_async_c1d, sp_devxlib_memcpy_h2h_async_c2d, sp_devxlib_memcpy_h2h_async_c3d, &
         sp_devxlib_memcpy_h2h_async_c4d, sp_devxlib_memcpy_h2h_async_c5d, sp_devxlib_memcpy_h2h_async_c6d, &
         dp_devxlib_memcpy_h2h_async_c1d, dp_devxlib_memcpy_h2h_async_c2d, dp_devxlib_memcpy_h2h_async_c3d, &
         dp_devxlib_memcpy_h2h_async_c4d, dp_devxlib_memcpy_h2h_async_c5d, dp_devxlib_memcpy_h2h_async_c6d, &
         i4_devxlib_memcpy_h2h_async_i1d, i4_devxlib_memcpy_h2h_async_i2d, i4_devxlib_memcpy_h2h_async_i3d, &
         i4_devxlib_memcpy_h2h_async_i4d, i4_devxlib_memcpy_h2h_async_i5d, i4_devxlib_memcpy_h2h_async_i6d, &
         i8_devxlib_memcpy_h2h_async_i1d, i8_devxlib_memcpy_h2h_async_i2d, i8_devxlib_memcpy_h2h_async_i3d, &
         i8_devxlib_memcpy_h2h_async_i4d, i8_devxlib_memcpy_h2h_async_i5d, i8_devxlib_memcpy_h2h_async_i6d, &
         l4_devxlib_memcpy_h2h_async_l1d, l4_devxlib_memcpy_h2h_async_l2d, l4_devxlib_memcpy_h2h_async_l3d, &
         l4_devxlib_memcpy_h2h_async_l4d, l4_devxlib_memcpy_h2h_async_l5d, l4_devxlib_memcpy_h2h_async_l6d
   endinterface devxlib_memcpy_h2h

   interface devxlib_memcpy_d2d_p
      module procedure &
         sp_devxlib_memcpy_d2d_r1d_p, sp_devxlib_memcpy_d2d_r2d_p, sp_devxlib_memcpy_d2d_r3d_p, &
         sp_devxlib_memcpy_d2d_r4d_p, sp_devxlib_memcpy_d2d_r5d_p, sp_devxlib_memcpy_d2d_r6d_p, &
         dp_devxlib_memcpy_d2d_r1d_p, dp_devxlib_memcpy_d2d_r2d_p, dp_devxlib_memcpy_d2d_r3d_p, &
         dp_devxlib_memcpy_d2d_r4d_p, dp_devxlib_memcpy_d2d_r5d_p, dp_devxlib_memcpy_d2d_r6d_p, &
         sp_devxlib_memcpy_d2d_c1d_p, sp_devxlib_memcpy_d2d_c2d_p, sp_devxlib_memcpy_d2d_c3d_p, &
         sp_devxlib_memcpy_d2d_c4d_p, sp_devxlib_memcpy_d2d_c5d_p, sp_devxlib_memcpy_d2d_c6d_p, &
         dp_devxlib_memcpy_d2d_c1d_p, dp_devxlib_memcpy_d2d_c2d_p, dp_devxlib_memcpy_d2d_c3d_p, &
         dp_devxlib_memcpy_d2d_c4d_p, dp_devxlib_memcpy_d2d_c5d_p, dp_devxlib_memcpy_d2d_c6d_p, &
         i4_devxlib_memcpy_d2d_i1d_p, i4_devxlib_memcpy_d2d_i2d_p, i4_devxlib_memcpy_d2d_i3d_p, &
         i4_devxlib_memcpy_d2d_i4d_p, i4_devxlib_memcpy_d2d_i5d_p, i4_devxlib_memcpy_d2d_i6d_p, &
         i8_devxlib_memcpy_d2d_i1d_p, i8_devxlib_memcpy_d2d_i2d_p, i8_devxlib_memcpy_d2d_i3d_p, &
         i8_devxlib_memcpy_d2d_i4d_p, i8_devxlib_memcpy_d2d_i5d_p, i8_devxlib_memcpy_d2d_i6d_p, &
         l4_devxlib_memcpy_d2d_l1d_p, l4_devxlib_memcpy_d2d_l2d_p, l4_devxlib_memcpy_d2d_l3d_p, &
         l4_devxlib_memcpy_d2d_l4d_p, l4_devxlib_memcpy_d2d_l5d_p, l4_devxlib_memcpy_d2d_l6d_p
   endinterface devxlib_memcpy_d2d_p

   interface devxlib_memcpy_d2h_p
      module procedure &
         sp_devxlib_memcpy_d2h_r1d_p, sp_devxlib_memcpy_d2h_r2d_p, sp_devxlib_memcpy_d2h_r3d_p, &
         sp_devxlib_memcpy_d2h_r4d_p, sp_devxlib_memcpy_d2h_r5d_p, sp_devxlib_memcpy_d2h_r6d_p, &
         dp_devxlib_memcpy_d2h_r1d_p, dp_devxlib_memcpy_d2h_r2d_p, dp_devxlib_memcpy_d2h_r3d_p, &
         dp_devxlib_memcpy_d2h_r4d_p, dp_devxlib_memcpy_d2h_r5d_p, dp_devxlib_memcpy_d2h_r6d_p, &
         sp_devxlib_memcpy_d2h_c1d_p, sp_devxlib_memcpy_d2h_c2d_p, sp_devxlib_memcpy_d2h_c3d_p, &
         sp_devxlib_memcpy_d2h_c4d_p, sp_devxlib_memcpy_d2h_c5d_p, sp_devxlib_memcpy_d2h_c6d_p, &
         dp_devxlib_memcpy_d2h_c1d_p, dp_devxlib_memcpy_d2h_c2d_p, dp_devxlib_memcpy_d2h_c3d_p, &
         dp_devxlib_memcpy_d2h_c4d_p, dp_devxlib_memcpy_d2h_c5d_p, dp_devxlib_memcpy_d2h_c6d_p, &
         i4_devxlib_memcpy_d2h_i1d_p, i4_devxlib_memcpy_d2h_i2d_p, i4_devxlib_memcpy_d2h_i3d_p, &
         i4_devxlib_memcpy_d2h_i4d_p, i4_devxlib_memcpy_d2h_i5d_p, i4_devxlib_memcpy_d2h_i6d_p, &
         i8_devxlib_memcpy_d2h_i1d_p, i8_devxlib_memcpy_d2h_i2d_p, i8_devxlib_memcpy_d2h_i3d_p, &
         i8_devxlib_memcpy_d2h_i4d_p, i8_devxlib_memcpy_d2h_i5d_p, i8_devxlib_memcpy_d2h_i6d_p, &
         l4_devxlib_memcpy_d2h_l1d_p, l4_devxlib_memcpy_d2h_l2d_p, l4_devxlib_memcpy_d2h_l3d_p, &
         l4_devxlib_memcpy_d2h_l4d_p, l4_devxlib_memcpy_d2h_l5d_p, l4_devxlib_memcpy_d2h_l6d_p
   endinterface devxlib_memcpy_d2h_p

   interface devxlib_memcpy_h2d_p
      module procedure &
         sp_devxlib_memcpy_h2d_r1d_p, sp_devxlib_memcpy_h2d_r2d_p, sp_devxlib_memcpy_h2d_r3d_p, &
         sp_devxlib_memcpy_h2d_r4d_p, sp_devxlib_memcpy_h2d_r5d_p, sp_devxlib_memcpy_h2d_r6d_p, &
         dp_devxlib_memcpy_h2d_r1d_p, dp_devxlib_memcpy_h2d_r2d_p, dp_devxlib_memcpy_h2d_r3d_p, &
         dp_devxlib_memcpy_h2d_r4d_p, dp_devxlib_memcpy_h2d_r5d_p, dp_devxlib_memcpy_h2d_r6d_p, &
         sp_devxlib_memcpy_h2d_c1d_p, sp_devxlib_memcpy_h2d_c2d_p, sp_devxlib_memcpy_h2d_c3d_p, &
         sp_devxlib_memcpy_h2d_c4d_p, sp_devxlib_memcpy_h2d_c5d_p, sp_devxlib_memcpy_h2d_c6d_p, &
         dp_devxlib_memcpy_h2d_c1d_p, dp_devxlib_memcpy_h2d_c2d_p, dp_devxlib_memcpy_h2d_c3d_p, &
         dp_devxlib_memcpy_h2d_c4d_p, dp_devxlib_memcpy_h2d_c5d_p, dp_devxlib_memcpy_h2d_c6d_p, &
         i4_devxlib_memcpy_h2d_i1d_p, i4_devxlib_memcpy_h2d_i2d_p, i4_devxlib_memcpy_h2d_i3d_p, &
         i4_devxlib_memcpy_h2d_i4d_p, i4_devxlib_memcpy_h2d_i5d_p, i4_devxlib_memcpy_h2d_i6d_p, &
         i8_devxlib_memcpy_h2d_i1d_p, i8_devxlib_memcpy_h2d_i2d_p, i8_devxlib_memcpy_h2d_i3d_p, &
         i8_devxlib_memcpy_h2d_i4d_p, i8_devxlib_memcpy_h2d_i5d_p, i8_devxlib_memcpy_h2d_i6d_p, &
         l4_devxlib_memcpy_h2d_l1d_p, l4_devxlib_memcpy_h2d_l2d_p, l4_devxlib_memcpy_h2d_l3d_p, &
         l4_devxlib_memcpy_h2d_l4d_p, l4_devxlib_memcpy_h2d_l5d_p, l4_devxlib_memcpy_h2d_l6d_p
   endinterface devxlib_memcpy_h2d_p

   interface
      module subroutine sp_devxlib_memcpy_d2d_r1d(array_out, array_in, &
                                            range1, lbound1 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2d_r1d
      module subroutine sp_devxlib_memcpy_d2d_r2d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2d_r2d
      module subroutine sp_devxlib_memcpy_d2d_r3d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2d_r3d
      module subroutine sp_devxlib_memcpy_d2d_r4d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2d_r4d
      module subroutine sp_devxlib_memcpy_d2d_r5d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2d_r5d
      module subroutine sp_devxlib_memcpy_d2d_r6d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2d_r6d
      module subroutine dp_devxlib_memcpy_d2d_r1d(array_out, array_in, &
                                            range1, lbound1 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2d_r1d
      module subroutine dp_devxlib_memcpy_d2d_r2d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2d_r2d
      module subroutine dp_devxlib_memcpy_d2d_r3d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2d_r3d
      module subroutine dp_devxlib_memcpy_d2d_r4d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2d_r4d
      module subroutine dp_devxlib_memcpy_d2d_r5d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2d_r5d
      module subroutine dp_devxlib_memcpy_d2d_r6d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2d_r6d
      module subroutine sp_devxlib_memcpy_d2d_c1d(array_out, array_in, &
                                            range1, lbound1 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2d_c1d
      module subroutine sp_devxlib_memcpy_d2d_c2d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2d_c2d
      module subroutine sp_devxlib_memcpy_d2d_c3d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2d_c3d
      module subroutine sp_devxlib_memcpy_d2d_c4d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2d_c4d
      module subroutine sp_devxlib_memcpy_d2d_c5d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2d_c5d
      module subroutine sp_devxlib_memcpy_d2d_c6d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2d_c6d
      module subroutine dp_devxlib_memcpy_d2d_c1d(array_out, array_in, &
                                            range1, lbound1 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2d_c1d
      module subroutine dp_devxlib_memcpy_d2d_c2d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2d_c2d
      module subroutine dp_devxlib_memcpy_d2d_c3d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2d_c3d
      module subroutine dp_devxlib_memcpy_d2d_c4d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2d_c4d
      module subroutine dp_devxlib_memcpy_d2d_c5d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2d_c5d
      module subroutine dp_devxlib_memcpy_d2d_c6d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2d_c6d
      module subroutine i4_devxlib_memcpy_d2d_i1d(array_out, array_in, &
                                            range1, lbound1 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_d2d_i1d
      module subroutine i4_devxlib_memcpy_d2d_i2d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_d2d_i2d
      module subroutine i4_devxlib_memcpy_d2d_i3d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_d2d_i3d
      module subroutine i4_devxlib_memcpy_d2d_i4d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_d2d_i4d
      module subroutine i4_devxlib_memcpy_d2d_i5d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_d2d_i5d
      module subroutine i4_devxlib_memcpy_d2d_i6d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_d2d_i6d
      module subroutine i8_devxlib_memcpy_d2d_i1d(array_out, array_in, &
                                            range1, lbound1 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_d2d_i1d
      module subroutine i8_devxlib_memcpy_d2d_i2d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_d2d_i2d
      module subroutine i8_devxlib_memcpy_d2d_i3d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_d2d_i3d
      module subroutine i8_devxlib_memcpy_d2d_i4d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_d2d_i4d
      module subroutine i8_devxlib_memcpy_d2d_i5d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_d2d_i5d
      module subroutine i8_devxlib_memcpy_d2d_i6d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_d2d_i6d
      module subroutine l4_devxlib_memcpy_d2d_l1d(array_out, array_in, &
                                            range1, lbound1 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_d2d_l1d
      module subroutine l4_devxlib_memcpy_d2d_l2d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_d2d_l2d
      module subroutine l4_devxlib_memcpy_d2d_l3d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_d2d_l3d
      module subroutine l4_devxlib_memcpy_d2d_l4d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_d2d_l4d
      module subroutine l4_devxlib_memcpy_d2d_l5d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_d2d_l5d
      module subroutine l4_devxlib_memcpy_d2d_l6d(array_out, array_in, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_d2d_l6d
      module subroutine sp_devxlib_memcpy_d2d_async_r1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2d_async_r1d
      module subroutine sp_devxlib_memcpy_d2d_async_r2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2d_async_r2d
      module subroutine sp_devxlib_memcpy_d2d_async_r3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2d_async_r3d
      module subroutine sp_devxlib_memcpy_d2d_async_r4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2d_async_r4d
      module subroutine sp_devxlib_memcpy_d2d_async_r5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2d_async_r5d
      module subroutine sp_devxlib_memcpy_d2d_async_r6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2d_async_r6d
      module subroutine dp_devxlib_memcpy_d2d_async_r1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2d_async_r1d
      module subroutine dp_devxlib_memcpy_d2d_async_r2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2d_async_r2d
      module subroutine dp_devxlib_memcpy_d2d_async_r3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2d_async_r3d
      module subroutine dp_devxlib_memcpy_d2d_async_r4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2d_async_r4d
      module subroutine dp_devxlib_memcpy_d2d_async_r5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2d_async_r5d
      module subroutine dp_devxlib_memcpy_d2d_async_r6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2d_async_r6d
      module subroutine sp_devxlib_memcpy_d2d_async_c1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2d_async_c1d
      module subroutine sp_devxlib_memcpy_d2d_async_c2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2d_async_c2d
      module subroutine sp_devxlib_memcpy_d2d_async_c3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2d_async_c3d
      module subroutine sp_devxlib_memcpy_d2d_async_c4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2d_async_c4d
      module subroutine sp_devxlib_memcpy_d2d_async_c5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2d_async_c5d
      module subroutine sp_devxlib_memcpy_d2d_async_c6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2d_async_c6d
      module subroutine dp_devxlib_memcpy_d2d_async_c1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2d_async_c1d
      module subroutine dp_devxlib_memcpy_d2d_async_c2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2d_async_c2d
      module subroutine dp_devxlib_memcpy_d2d_async_c3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2d_async_c3d
      module subroutine dp_devxlib_memcpy_d2d_async_c4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2d_async_c4d
      module subroutine dp_devxlib_memcpy_d2d_async_c5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2d_async_c5d
      module subroutine dp_devxlib_memcpy_d2d_async_c6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2d_async_c6d
      module subroutine i4_devxlib_memcpy_d2d_async_i1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_d2d_async_i1d
      module subroutine i4_devxlib_memcpy_d2d_async_i2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_d2d_async_i2d
      module subroutine i4_devxlib_memcpy_d2d_async_i3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_d2d_async_i3d
      module subroutine i4_devxlib_memcpy_d2d_async_i4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_d2d_async_i4d
      module subroutine i4_devxlib_memcpy_d2d_async_i5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_d2d_async_i5d
      module subroutine i4_devxlib_memcpy_d2d_async_i6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_d2d_async_i6d
      module subroutine i8_devxlib_memcpy_d2d_async_i1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_d2d_async_i1d
      module subroutine i8_devxlib_memcpy_d2d_async_i2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_d2d_async_i2d
      module subroutine i8_devxlib_memcpy_d2d_async_i3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_d2d_async_i3d
      module subroutine i8_devxlib_memcpy_d2d_async_i4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_d2d_async_i4d
      module subroutine i8_devxlib_memcpy_d2d_async_i5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_d2d_async_i5d
      module subroutine i8_devxlib_memcpy_d2d_async_i6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_d2d_async_i6d
      module subroutine l4_devxlib_memcpy_d2d_async_l1d(array_out, array_in, async_id, &
                                                  range1, lbound1 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_d2d_async_l1d
      module subroutine l4_devxlib_memcpy_d2d_async_l2d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_d2d_async_l2d
      module subroutine l4_devxlib_memcpy_d2d_async_l3d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_d2d_async_l3d
      module subroutine l4_devxlib_memcpy_d2d_async_l4d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_d2d_async_l4d
      module subroutine l4_devxlib_memcpy_d2d_async_l5d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_d2d_async_l5d
      module subroutine l4_devxlib_memcpy_d2d_async_l6d(array_out, array_in, async_id, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_d2d_async_l6d
   endinterface

   interface
      module subroutine sp_devxlib_memcpy_d2h_r1d(array_out, array_in, &
                                             range1, lbound1  )
         implicit none
         real(real32), intent(inout) :: array_out(:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2h_r1d
      module subroutine sp_devxlib_memcpy_d2h_r2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         real(real32), intent(inout) :: array_out(:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2h_r2d
      module subroutine sp_devxlib_memcpy_d2h_r3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2h_r3d
      module subroutine sp_devxlib_memcpy_d2h_r4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2h_r4d
      module subroutine sp_devxlib_memcpy_d2h_r5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2h_r5d
      module subroutine sp_devxlib_memcpy_d2h_r6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2h_r6d
      module subroutine dp_devxlib_memcpy_d2h_r1d(array_out, array_in, &
                                             range1, lbound1  )
         implicit none
         real(real64), intent(inout) :: array_out(:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2h_r1d
      module subroutine dp_devxlib_memcpy_d2h_r2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         real(real64), intent(inout) :: array_out(:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2h_r2d
      module subroutine dp_devxlib_memcpy_d2h_r3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2h_r3d
      module subroutine dp_devxlib_memcpy_d2h_r4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2h_r4d
      module subroutine dp_devxlib_memcpy_d2h_r5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2h_r5d
      module subroutine dp_devxlib_memcpy_d2h_r6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2h_r6d
      module subroutine sp_devxlib_memcpy_d2h_c1d(array_out, array_in, &
                                             range1, lbound1  )
         implicit none
         complex(real32), intent(inout) :: array_out(:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2h_c1d
      module subroutine sp_devxlib_memcpy_d2h_c2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2h_c2d
      module subroutine sp_devxlib_memcpy_d2h_c3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2h_c3d
      module subroutine sp_devxlib_memcpy_d2h_c4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2h_c4d
      module subroutine sp_devxlib_memcpy_d2h_c5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2h_c5d
      module subroutine sp_devxlib_memcpy_d2h_c6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2h_c6d
      module subroutine dp_devxlib_memcpy_d2h_c1d(array_out, array_in, &
                                             range1, lbound1  )
         implicit none
         complex(real64), intent(inout) :: array_out(:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2h_c1d
      module subroutine dp_devxlib_memcpy_d2h_c2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2h_c2d
      module subroutine dp_devxlib_memcpy_d2h_c3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2h_c3d
      module subroutine dp_devxlib_memcpy_d2h_c4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2h_c4d
      module subroutine dp_devxlib_memcpy_d2h_c5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2h_c5d
      module subroutine dp_devxlib_memcpy_d2h_c6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2h_c6d
      module subroutine i4_devxlib_memcpy_d2h_i1d(array_out, array_in, &
                                             range1, lbound1  )
         implicit none
         integer(int32), intent(inout) :: array_out(:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_d2h_i1d
      module subroutine i4_devxlib_memcpy_d2h_i2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_d2h_i2d
      module subroutine i4_devxlib_memcpy_d2h_i3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_d2h_i3d
      module subroutine i4_devxlib_memcpy_d2h_i4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_d2h_i4d
      module subroutine i4_devxlib_memcpy_d2h_i5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_d2h_i5d
      module subroutine i4_devxlib_memcpy_d2h_i6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_d2h_i6d
      module subroutine i8_devxlib_memcpy_d2h_i1d(array_out, array_in, &
                                             range1, lbound1  )
         implicit none
         integer(int64), intent(inout) :: array_out(:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_d2h_i1d
      module subroutine i8_devxlib_memcpy_d2h_i2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_d2h_i2d
      module subroutine i8_devxlib_memcpy_d2h_i3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_d2h_i3d
      module subroutine i8_devxlib_memcpy_d2h_i4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_d2h_i4d
      module subroutine i8_devxlib_memcpy_d2h_i5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_d2h_i5d
      module subroutine i8_devxlib_memcpy_d2h_i6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_d2h_i6d
      module subroutine l4_devxlib_memcpy_d2h_l1d(array_out, array_in, &
                                             range1, lbound1  )
         implicit none
         logical(int32), intent(inout) :: array_out(:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_d2h_l1d
      module subroutine l4_devxlib_memcpy_d2h_l2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_d2h_l2d
      module subroutine l4_devxlib_memcpy_d2h_l3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_d2h_l3d
      module subroutine l4_devxlib_memcpy_d2h_l4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_d2h_l4d
      module subroutine l4_devxlib_memcpy_d2h_l5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_d2h_l5d
      module subroutine l4_devxlib_memcpy_d2h_l6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32) DEV_ATTR, intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_d2h_l6d
      module subroutine sp_devxlib_memcpy_d2h_async_r1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         real(real32), target, intent(inout)       :: array_out(:)
         real(real32), target, intent(inout) DEV_ATTR :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2h_async_r1d
      module subroutine sp_devxlib_memcpy_d2h_async_r2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         real(real32), target, intent(inout)       :: array_out(:,:)
         real(real32), target, intent(inout) DEV_ATTR :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2h_async_r2d
      module subroutine sp_devxlib_memcpy_d2h_async_r3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         real(real32), target, intent(inout)       :: array_out(:,:,:)
         real(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2h_async_r3d
      module subroutine sp_devxlib_memcpy_d2h_async_r4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         real(real32), target, intent(inout)       :: array_out(:,:,:,:)
         real(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2h_async_r4d
      module subroutine sp_devxlib_memcpy_d2h_async_r5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         real(real32), target, intent(inout)       :: array_out(:,:,:,:,:)
         real(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2h_async_r5d
      module subroutine sp_devxlib_memcpy_d2h_async_r6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         real(real32), target, intent(inout)       :: array_out(:,:,:,:,:,:)
         real(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2h_async_r6d
      module subroutine dp_devxlib_memcpy_d2h_async_r1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         real(real64), target, intent(inout)       :: array_out(:)
         real(real64), target, intent(inout) DEV_ATTR :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2h_async_r1d
      module subroutine dp_devxlib_memcpy_d2h_async_r2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         real(real64), target, intent(inout)       :: array_out(:,:)
         real(real64), target, intent(inout) DEV_ATTR :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2h_async_r2d
      module subroutine dp_devxlib_memcpy_d2h_async_r3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         real(real64), target, intent(inout)       :: array_out(:,:,:)
         real(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2h_async_r3d
      module subroutine dp_devxlib_memcpy_d2h_async_r4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         real(real64), target, intent(inout)       :: array_out(:,:,:,:)
         real(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2h_async_r4d
      module subroutine dp_devxlib_memcpy_d2h_async_r5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         real(real64), target, intent(inout)       :: array_out(:,:,:,:,:)
         real(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2h_async_r5d
      module subroutine dp_devxlib_memcpy_d2h_async_r6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         real(real64), target, intent(inout)       :: array_out(:,:,:,:,:,:)
         real(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2h_async_r6d
      module subroutine sp_devxlib_memcpy_d2h_async_c1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         complex(real32), target, intent(inout)       :: array_out(:)
         complex(real32), target, intent(inout) DEV_ATTR :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_d2h_async_c1d
      module subroutine sp_devxlib_memcpy_d2h_async_c2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         complex(real32), target, intent(inout)       :: array_out(:,:)
         complex(real32), target, intent(inout) DEV_ATTR :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_d2h_async_c2d
      module subroutine sp_devxlib_memcpy_d2h_async_c3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         complex(real32), target, intent(inout)       :: array_out(:,:,:)
         complex(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_d2h_async_c3d
      module subroutine sp_devxlib_memcpy_d2h_async_c4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         complex(real32), target, intent(inout)       :: array_out(:,:,:,:)
         complex(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_d2h_async_c4d
      module subroutine sp_devxlib_memcpy_d2h_async_c5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         complex(real32), target, intent(inout)       :: array_out(:,:,:,:,:)
         complex(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_d2h_async_c5d
      module subroutine sp_devxlib_memcpy_d2h_async_c6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         complex(real32), target, intent(inout)       :: array_out(:,:,:,:,:,:)
         complex(real32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_d2h_async_c6d
      module subroutine dp_devxlib_memcpy_d2h_async_c1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         complex(real64), target, intent(inout)       :: array_out(:)
         complex(real64), target, intent(inout) DEV_ATTR :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_d2h_async_c1d
      module subroutine dp_devxlib_memcpy_d2h_async_c2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         complex(real64), target, intent(inout)       :: array_out(:,:)
         complex(real64), target, intent(inout) DEV_ATTR :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_d2h_async_c2d
      module subroutine dp_devxlib_memcpy_d2h_async_c3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         complex(real64), target, intent(inout)       :: array_out(:,:,:)
         complex(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_d2h_async_c3d
      module subroutine dp_devxlib_memcpy_d2h_async_c4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         complex(real64), target, intent(inout)       :: array_out(:,:,:,:)
         complex(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_d2h_async_c4d
      module subroutine dp_devxlib_memcpy_d2h_async_c5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         complex(real64), target, intent(inout)       :: array_out(:,:,:,:,:)
         complex(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_d2h_async_c5d
      module subroutine dp_devxlib_memcpy_d2h_async_c6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         complex(real64), target, intent(inout)       :: array_out(:,:,:,:,:,:)
         complex(real64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_d2h_async_c6d
      module subroutine i4_devxlib_memcpy_d2h_async_i1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         integer(int32), target, intent(inout)       :: array_out(:)
         integer(int32), target, intent(inout) DEV_ATTR :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_d2h_async_i1d
      module subroutine i4_devxlib_memcpy_d2h_async_i2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         integer(int32), target, intent(inout)       :: array_out(:,:)
         integer(int32), target, intent(inout) DEV_ATTR :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_d2h_async_i2d
      module subroutine i4_devxlib_memcpy_d2h_async_i3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         integer(int32), target, intent(inout)       :: array_out(:,:,:)
         integer(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_d2h_async_i3d
      module subroutine i4_devxlib_memcpy_d2h_async_i4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         integer(int32), target, intent(inout)       :: array_out(:,:,:,:)
         integer(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_d2h_async_i4d
      module subroutine i4_devxlib_memcpy_d2h_async_i5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         integer(int32), target, intent(inout)       :: array_out(:,:,:,:,:)
         integer(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_d2h_async_i5d
      module subroutine i4_devxlib_memcpy_d2h_async_i6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         integer(int32), target, intent(inout)       :: array_out(:,:,:,:,:,:)
         integer(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_d2h_async_i6d
      module subroutine i8_devxlib_memcpy_d2h_async_i1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         integer(int64), target, intent(inout)       :: array_out(:)
         integer(int64), target, intent(inout) DEV_ATTR :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_d2h_async_i1d
      module subroutine i8_devxlib_memcpy_d2h_async_i2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         integer(int64), target, intent(inout)       :: array_out(:,:)
         integer(int64), target, intent(inout) DEV_ATTR :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_d2h_async_i2d
      module subroutine i8_devxlib_memcpy_d2h_async_i3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         integer(int64), target, intent(inout)       :: array_out(:,:,:)
         integer(int64), target, intent(inout) DEV_ATTR :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_d2h_async_i3d
      module subroutine i8_devxlib_memcpy_d2h_async_i4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         integer(int64), target, intent(inout)       :: array_out(:,:,:,:)
         integer(int64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_d2h_async_i4d
      module subroutine i8_devxlib_memcpy_d2h_async_i5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         integer(int64), target, intent(inout)       :: array_out(:,:,:,:,:)
         integer(int64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_d2h_async_i5d
      module subroutine i8_devxlib_memcpy_d2h_async_i6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         integer(int64), target, intent(inout)       :: array_out(:,:,:,:,:,:)
         integer(int64), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_d2h_async_i6d
      module subroutine l4_devxlib_memcpy_d2h_async_l1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         logical(int32), target, intent(inout)       :: array_out(:)
         logical(int32), target, intent(inout) DEV_ATTR :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_d2h_async_l1d
      module subroutine l4_devxlib_memcpy_d2h_async_l2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         logical(int32), target, intent(inout)       :: array_out(:,:)
         logical(int32), target, intent(inout) DEV_ATTR :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_d2h_async_l2d
      module subroutine l4_devxlib_memcpy_d2h_async_l3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         logical(int32), target, intent(inout)       :: array_out(:,:,:)
         logical(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_d2h_async_l3d
      module subroutine l4_devxlib_memcpy_d2h_async_l4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         logical(int32), target, intent(inout)       :: array_out(:,:,:,:)
         logical(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_d2h_async_l4d
      module subroutine l4_devxlib_memcpy_d2h_async_l5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         logical(int32), target, intent(inout)       :: array_out(:,:,:,:,:)
         logical(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_d2h_async_l5d
      module subroutine l4_devxlib_memcpy_d2h_async_l6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         logical(int32), target, intent(inout)       :: array_out(:,:,:,:,:,:)
         logical(int32), target, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_d2h_async_l6d
   endinterface

   interface
      module subroutine sp_devxlib_memcpy_h2d_r1d(array_out, array_in, &
                                             range1, lbound1 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:)
         real(real32), intent(in) :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2d_r1d
      module subroutine sp_devxlib_memcpy_h2d_r2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real32), intent(in) :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2d_r2d
      module subroutine sp_devxlib_memcpy_h2d_r3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real32), intent(in) :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2d_r3d
      module subroutine sp_devxlib_memcpy_h2d_r4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real32), intent(in) :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2d_r4d
      module subroutine sp_devxlib_memcpy_h2d_r5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real32), intent(in) :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2d_r5d
      module subroutine sp_devxlib_memcpy_h2d_r6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6 )
         implicit none
         real(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32), intent(in) :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2d_r6d
      module subroutine dp_devxlib_memcpy_h2d_r1d(array_out, array_in, &
                                             range1, lbound1 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:)
         real(real64), intent(in) :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2d_r1d
      module subroutine dp_devxlib_memcpy_h2d_r2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         real(real64), intent(in) :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2d_r2d
      module subroutine dp_devxlib_memcpy_h2d_r3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         real(real64), intent(in) :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2d_r3d
      module subroutine dp_devxlib_memcpy_h2d_r4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         real(real64), intent(in) :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2d_r4d
      module subroutine dp_devxlib_memcpy_h2d_r5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         real(real64), intent(in) :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2d_r5d
      module subroutine dp_devxlib_memcpy_h2d_r6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6 )
         implicit none
         real(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64), intent(in) :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2d_r6d
      module subroutine sp_devxlib_memcpy_h2d_c1d(array_out, array_in, &
                                             range1, lbound1 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real32), intent(in) :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2d_c1d
      module subroutine sp_devxlib_memcpy_h2d_c2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real32), intent(in) :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2d_c2d
      module subroutine sp_devxlib_memcpy_h2d_c3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real32), intent(in) :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2d_c3d
      module subroutine sp_devxlib_memcpy_h2d_c4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real32), intent(in) :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2d_c4d
      module subroutine sp_devxlib_memcpy_h2d_c5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real32), intent(in) :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2d_c5d
      module subroutine sp_devxlib_memcpy_h2d_c6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6 )
         implicit none
         complex(real32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32), intent(in) :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2d_c6d
      module subroutine dp_devxlib_memcpy_h2d_c1d(array_out, array_in, &
                                             range1, lbound1 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:)
         complex(real64), intent(in) :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2d_c1d
      module subroutine dp_devxlib_memcpy_h2d_c2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:)
         complex(real64), intent(in) :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2d_c2d
      module subroutine dp_devxlib_memcpy_h2d_c3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         complex(real64), intent(in) :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2d_c3d
      module subroutine dp_devxlib_memcpy_h2d_c4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         complex(real64), intent(in) :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2d_c4d
      module subroutine dp_devxlib_memcpy_h2d_c5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         complex(real64), intent(in) :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2d_c5d
      module subroutine dp_devxlib_memcpy_h2d_c6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6 )
         implicit none
         complex(real64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64), intent(in) :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2d_c6d
      module subroutine i4_devxlib_memcpy_h2d_i1d(array_out, array_in, &
                                             range1, lbound1 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int32), intent(in) :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_h2d_i1d
      module subroutine i4_devxlib_memcpy_h2d_i2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int32), intent(in) :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_h2d_i2d
      module subroutine i4_devxlib_memcpy_h2d_i3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int32), intent(in) :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_h2d_i3d
      module subroutine i4_devxlib_memcpy_h2d_i4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int32), intent(in) :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_h2d_i4d
      module subroutine i4_devxlib_memcpy_h2d_i5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int32), intent(in) :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_h2d_i5d
      module subroutine i4_devxlib_memcpy_h2d_i6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6 )
         implicit none
         integer(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32), intent(in) :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_h2d_i6d
      module subroutine i8_devxlib_memcpy_h2d_i1d(array_out, array_in, &
                                             range1, lbound1 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:)
         integer(int64), intent(in) :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_h2d_i1d
      module subroutine i8_devxlib_memcpy_h2d_i2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:)
         integer(int64), intent(in) :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_h2d_i2d
      module subroutine i8_devxlib_memcpy_h2d_i3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         integer(int64), intent(in) :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_h2d_i3d
      module subroutine i8_devxlib_memcpy_h2d_i4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         integer(int64), intent(in) :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_h2d_i4d
      module subroutine i8_devxlib_memcpy_h2d_i5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         integer(int64), intent(in) :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_h2d_i5d
      module subroutine i8_devxlib_memcpy_h2d_i6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6 )
         implicit none
         integer(int64) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64), intent(in) :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_h2d_i6d
      module subroutine l4_devxlib_memcpy_h2d_l1d(array_out, array_in, &
                                             range1, lbound1 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:)
         logical(int32), intent(in) :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_h2d_l1d
      module subroutine l4_devxlib_memcpy_h2d_l2d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:)
         logical(int32), intent(in) :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_h2d_l2d
      module subroutine l4_devxlib_memcpy_h2d_l3d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:)
         logical(int32), intent(in) :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_h2d_l3d
      module subroutine l4_devxlib_memcpy_h2d_l4d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:)
         logical(int32), intent(in) :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_h2d_l4d
      module subroutine l4_devxlib_memcpy_h2d_l5d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:)
         logical(int32), intent(in) :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_h2d_l5d
      module subroutine l4_devxlib_memcpy_h2d_l6d(array_out, array_in, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6 )
         implicit none
         logical(int32) DEV_ATTR, intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32), intent(in) :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_h2d_l6d
      module subroutine sp_devxlib_memcpy_h2d_async_r1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         real(real32), target, intent(inout) DEV_ATTR :: array_out(:)
         real(real32), target, intent(in) :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2d_async_r1d
      module subroutine sp_devxlib_memcpy_h2d_async_r2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         real(real32), target, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real32), target, intent(in) :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2d_async_r2d
      module subroutine sp_devxlib_memcpy_h2d_async_r3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         real(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2d_async_r3d
      module subroutine sp_devxlib_memcpy_h2d_async_r4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         real(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2d_async_r4d
      module subroutine sp_devxlib_memcpy_h2d_async_r5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         real(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2d_async_r5d
      module subroutine sp_devxlib_memcpy_h2d_async_r6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         real(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2d_async_r6d
      module subroutine dp_devxlib_memcpy_h2d_async_r1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         real(real64), target, intent(inout) DEV_ATTR :: array_out(:)
         real(real64), target, intent(in) :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2d_async_r1d
      module subroutine dp_devxlib_memcpy_h2d_async_r2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         real(real64), target, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real64), target, intent(in) :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2d_async_r2d
      module subroutine dp_devxlib_memcpy_h2d_async_r3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         real(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2d_async_r3d
      module subroutine dp_devxlib_memcpy_h2d_async_r4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         real(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2d_async_r4d
      module subroutine dp_devxlib_memcpy_h2d_async_r5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         real(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2d_async_r5d
      module subroutine dp_devxlib_memcpy_h2d_async_r6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         real(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2d_async_r6d
      module subroutine sp_devxlib_memcpy_h2d_async_c1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         complex(real32), target, intent(inout) DEV_ATTR :: array_out(:)
         complex(real32), target, intent(in) :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2d_async_c1d
      module subroutine sp_devxlib_memcpy_h2d_async_c2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         complex(real32), target, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real32), target, intent(in) :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2d_async_c2d
      module subroutine sp_devxlib_memcpy_h2d_async_c3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         complex(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2d_async_c3d
      module subroutine sp_devxlib_memcpy_h2d_async_c4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         complex(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2d_async_c4d
      module subroutine sp_devxlib_memcpy_h2d_async_c5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         complex(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2d_async_c5d
      module subroutine sp_devxlib_memcpy_h2d_async_c6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         complex(real32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2d_async_c6d
      module subroutine dp_devxlib_memcpy_h2d_async_c1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         complex(real64), target, intent(inout) DEV_ATTR :: array_out(:)
         complex(real64), target, intent(in) :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2d_async_c1d
      module subroutine dp_devxlib_memcpy_h2d_async_c2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         complex(real64), target, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real64), target, intent(in) :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2d_async_c2d
      module subroutine dp_devxlib_memcpy_h2d_async_c3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         complex(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2d_async_c3d
      module subroutine dp_devxlib_memcpy_h2d_async_c4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         complex(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2d_async_c4d
      module subroutine dp_devxlib_memcpy_h2d_async_c5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         complex(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2d_async_c5d
      module subroutine dp_devxlib_memcpy_h2d_async_c6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         complex(real64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2d_async_c6d
      module subroutine i4_devxlib_memcpy_h2d_async_i1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         integer(int32), target, intent(inout) DEV_ATTR :: array_out(:)
         integer(int32), target, intent(in) :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_h2d_async_i1d
      module subroutine i4_devxlib_memcpy_h2d_async_i2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         integer(int32), target, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int32), target, intent(in) :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_h2d_async_i2d
      module subroutine i4_devxlib_memcpy_h2d_async_i3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         integer(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_h2d_async_i3d
      module subroutine i4_devxlib_memcpy_h2d_async_i4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         integer(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_h2d_async_i4d
      module subroutine i4_devxlib_memcpy_h2d_async_i5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         integer(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_h2d_async_i5d
      module subroutine i4_devxlib_memcpy_h2d_async_i6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         integer(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_h2d_async_i6d
      module subroutine i8_devxlib_memcpy_h2d_async_i1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         integer(int64), target, intent(inout) DEV_ATTR :: array_out(:)
         integer(int64), target, intent(in) :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_h2d_async_i1d
      module subroutine i8_devxlib_memcpy_h2d_async_i2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         integer(int64), target, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int64), target, intent(in) :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_h2d_async_i2d
      module subroutine i8_devxlib_memcpy_h2d_async_i3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         integer(int64), target, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_h2d_async_i3d
      module subroutine i8_devxlib_memcpy_h2d_async_i4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         integer(int64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_h2d_async_i4d
      module subroutine i8_devxlib_memcpy_h2d_async_i5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         integer(int64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_h2d_async_i5d
      module subroutine i8_devxlib_memcpy_h2d_async_i6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         integer(int64), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_h2d_async_i6d
      module subroutine l4_devxlib_memcpy_h2d_async_l1d(array_out, array_in, async_id, &
                                             range1, lbound1  )
         implicit none
         logical(int32), target, intent(inout) DEV_ATTR :: array_out(:)
         logical(int32), target, intent(in) :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_h2d_async_l1d
      module subroutine l4_devxlib_memcpy_h2d_async_l2d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2  )
         implicit none
         logical(int32), target, intent(inout) DEV_ATTR :: array_out(:,:)
         logical(int32), target, intent(in) :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_h2d_async_l2d
      module subroutine l4_devxlib_memcpy_h2d_async_l3d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3  )
         implicit none
         logical(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_h2d_async_l3d
      module subroutine l4_devxlib_memcpy_h2d_async_l4d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4  )
         implicit none
         logical(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_h2d_async_l4d
      module subroutine l4_devxlib_memcpy_h2d_async_l5d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5  )
         implicit none
         logical(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_h2d_async_l5d
      module subroutine l4_devxlib_memcpy_h2d_async_l6d(array_out, array_in, async_id, &
                                             range1, lbound1 , &
                                             range2, lbound2 , &
                                             range3, lbound3 , &
                                             range4, lbound4 , &
                                             range5, lbound5 , &
                                             range6, lbound6  )
         implicit none
         logical(int32), target, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_h2d_async_l6d
   endinterface

   interface
      module subroutine sp_devxlib_memcpy_h2h_r1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         real(real32), intent(inout) :: array_out(:)
         real(real32), intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2h_r1d
      module subroutine sp_devxlib_memcpy_h2h_r2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         real(real32), intent(inout) :: array_out(:,:)
         real(real32), intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2h_r2d
      module subroutine sp_devxlib_memcpy_h2h_r3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2h_r3d
      module subroutine sp_devxlib_memcpy_h2h_r4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2h_r4d
      module subroutine sp_devxlib_memcpy_h2h_r5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2h_r5d
      module subroutine sp_devxlib_memcpy_h2h_r6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         real(real32), intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2h_r6d
      module subroutine dp_devxlib_memcpy_h2h_r1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         real(real64), intent(inout) :: array_out(:)
         real(real64), intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2h_r1d
      module subroutine dp_devxlib_memcpy_h2h_r2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         real(real64), intent(inout) :: array_out(:,:)
         real(real64), intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2h_r2d
      module subroutine dp_devxlib_memcpy_h2h_r3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2h_r3d
      module subroutine dp_devxlib_memcpy_h2h_r4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2h_r4d
      module subroutine dp_devxlib_memcpy_h2h_r5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2h_r5d
      module subroutine dp_devxlib_memcpy_h2h_r6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         real(real64), intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2h_r6d
      module subroutine sp_devxlib_memcpy_h2h_c1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         complex(real32), intent(inout) :: array_out(:)
         complex(real32), intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2h_c1d
      module subroutine sp_devxlib_memcpy_h2h_c2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:)
         complex(real32), intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2h_c2d
      module subroutine sp_devxlib_memcpy_h2h_c3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2h_c3d
      module subroutine sp_devxlib_memcpy_h2h_c4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2h_c4d
      module subroutine sp_devxlib_memcpy_h2h_c5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2h_c5d
      module subroutine sp_devxlib_memcpy_h2h_c6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         complex(real32), intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2h_c6d
      module subroutine dp_devxlib_memcpy_h2h_c1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         complex(real64), intent(inout) :: array_out(:)
         complex(real64), intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2h_c1d
      module subroutine dp_devxlib_memcpy_h2h_c2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:)
         complex(real64), intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2h_c2d
      module subroutine dp_devxlib_memcpy_h2h_c3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2h_c3d
      module subroutine dp_devxlib_memcpy_h2h_c4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2h_c4d
      module subroutine dp_devxlib_memcpy_h2h_c5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2h_c5d
      module subroutine dp_devxlib_memcpy_h2h_c6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         complex(real64), intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2h_c6d
      module subroutine i4_devxlib_memcpy_h2h_i1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         integer(int32), intent(inout) :: array_out(:)
         integer(int32), intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_h2h_i1d
      module subroutine i4_devxlib_memcpy_h2h_i2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:)
         integer(int32), intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_h2h_i2d
      module subroutine i4_devxlib_memcpy_h2h_i3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_h2h_i3d
      module subroutine i4_devxlib_memcpy_h2h_i4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_h2h_i4d
      module subroutine i4_devxlib_memcpy_h2h_i5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_h2h_i5d
      module subroutine i4_devxlib_memcpy_h2h_i6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         integer(int32), intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_h2h_i6d
      module subroutine i8_devxlib_memcpy_h2h_i1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         integer(int64), intent(inout) :: array_out(:)
         integer(int64), intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_h2h_i1d
      module subroutine i8_devxlib_memcpy_h2h_i2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:)
         integer(int64), intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_h2h_i2d
      module subroutine i8_devxlib_memcpy_h2h_i3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_h2h_i3d
      module subroutine i8_devxlib_memcpy_h2h_i4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_h2h_i4d
      module subroutine i8_devxlib_memcpy_h2h_i5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_h2h_i5d
      module subroutine i8_devxlib_memcpy_h2h_i6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         integer(int64), intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_h2h_i6d
      module subroutine l4_devxlib_memcpy_h2h_l1d(array_out, array_in, &
                                                  range1, lbound1 )
         implicit none
         logical(int32), intent(inout) :: array_out(:)
         logical(int32), intent(in)    :: array_in(:)
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_h2h_l1d
      module subroutine l4_devxlib_memcpy_h2h_l2d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:)
         logical(int32), intent(in)    :: array_in(:,:)
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_h2h_l2d
      module subroutine l4_devxlib_memcpy_h2h_l3d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_h2h_l3d
      module subroutine l4_devxlib_memcpy_h2h_l4d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_h2h_l4d
      module subroutine l4_devxlib_memcpy_h2h_l5d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_h2h_l5d
      module subroutine l4_devxlib_memcpy_h2h_l6d(array_out, array_in, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         implicit none
         logical(int32), intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_h2h_l6d
      module subroutine sp_devxlib_memcpy_h2h_async_r1d(array_out, array_in, async_id, &
                                            range1, lbound1 )
         real(real32), intent(inout) :: array_out(:)
         real(real32), intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2h_async_r1d
      module subroutine sp_devxlib_memcpy_h2h_async_r2d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         real(real32), intent(inout) :: array_out(:,:)
         real(real32), intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2h_async_r2d
      module subroutine sp_devxlib_memcpy_h2h_async_r3d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         real(real32), intent(inout) :: array_out(:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2h_async_r3d
      module subroutine sp_devxlib_memcpy_h2h_async_r4d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         real(real32), intent(inout) :: array_out(:,:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2h_async_r4d
      module subroutine sp_devxlib_memcpy_h2h_async_r5d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         real(real32), intent(inout) :: array_out(:,:,:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2h_async_r5d
      module subroutine sp_devxlib_memcpy_h2h_async_r6d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         real(real32), intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2h_async_r6d
      module subroutine dp_devxlib_memcpy_h2h_async_r1d(array_out, array_in, async_id, &
                                            range1, lbound1 )
         real(real64), intent(inout) :: array_out(:)
         real(real64), intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2h_async_r1d
      module subroutine dp_devxlib_memcpy_h2h_async_r2d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         real(real64), intent(inout) :: array_out(:,:)
         real(real64), intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2h_async_r2d
      module subroutine dp_devxlib_memcpy_h2h_async_r3d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         real(real64), intent(inout) :: array_out(:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2h_async_r3d
      module subroutine dp_devxlib_memcpy_h2h_async_r4d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         real(real64), intent(inout) :: array_out(:,:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2h_async_r4d
      module subroutine dp_devxlib_memcpy_h2h_async_r5d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         real(real64), intent(inout) :: array_out(:,:,:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2h_async_r5d
      module subroutine dp_devxlib_memcpy_h2h_async_r6d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         real(real64), intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64), intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2h_async_r6d
      module subroutine sp_devxlib_memcpy_h2h_async_c1d(array_out, array_in, async_id, &
                                            range1, lbound1 )
         complex(real32), intent(inout) :: array_out(:)
         complex(real32), intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine sp_devxlib_memcpy_h2h_async_c1d
      module subroutine sp_devxlib_memcpy_h2h_async_c2d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         complex(real32), intent(inout) :: array_out(:,:)
         complex(real32), intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine sp_devxlib_memcpy_h2h_async_c2d
      module subroutine sp_devxlib_memcpy_h2h_async_c3d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         complex(real32), intent(inout) :: array_out(:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine sp_devxlib_memcpy_h2h_async_c3d
      module subroutine sp_devxlib_memcpy_h2h_async_c4d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         complex(real32), intent(inout) :: array_out(:,:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine sp_devxlib_memcpy_h2h_async_c4d
      module subroutine sp_devxlib_memcpy_h2h_async_c5d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         complex(real32), intent(inout) :: array_out(:,:,:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine sp_devxlib_memcpy_h2h_async_c5d
      module subroutine sp_devxlib_memcpy_h2h_async_c6d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         complex(real32), intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine sp_devxlib_memcpy_h2h_async_c6d
      module subroutine dp_devxlib_memcpy_h2h_async_c1d(array_out, array_in, async_id, &
                                            range1, lbound1 )
         complex(real64), intent(inout) :: array_out(:)
         complex(real64), intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine dp_devxlib_memcpy_h2h_async_c1d
      module subroutine dp_devxlib_memcpy_h2h_async_c2d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         complex(real64), intent(inout) :: array_out(:,:)
         complex(real64), intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine dp_devxlib_memcpy_h2h_async_c2d
      module subroutine dp_devxlib_memcpy_h2h_async_c3d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         complex(real64), intent(inout) :: array_out(:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine dp_devxlib_memcpy_h2h_async_c3d
      module subroutine dp_devxlib_memcpy_h2h_async_c4d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         complex(real64), intent(inout) :: array_out(:,:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine dp_devxlib_memcpy_h2h_async_c4d
      module subroutine dp_devxlib_memcpy_h2h_async_c5d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         complex(real64), intent(inout) :: array_out(:,:,:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine dp_devxlib_memcpy_h2h_async_c5d
      module subroutine dp_devxlib_memcpy_h2h_async_c6d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         complex(real64), intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64), intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine dp_devxlib_memcpy_h2h_async_c6d
      module subroutine i4_devxlib_memcpy_h2h_async_i1d(array_out, array_in, async_id, &
                                            range1, lbound1 )
         integer(int32), intent(inout) :: array_out(:)
         integer(int32), intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i4_devxlib_memcpy_h2h_async_i1d
      module subroutine i4_devxlib_memcpy_h2h_async_i2d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         integer(int32), intent(inout) :: array_out(:,:)
         integer(int32), intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i4_devxlib_memcpy_h2h_async_i2d
      module subroutine i4_devxlib_memcpy_h2h_async_i3d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         integer(int32), intent(inout) :: array_out(:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i4_devxlib_memcpy_h2h_async_i3d
      module subroutine i4_devxlib_memcpy_h2h_async_i4d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         integer(int32), intent(inout) :: array_out(:,:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i4_devxlib_memcpy_h2h_async_i4d
      module subroutine i4_devxlib_memcpy_h2h_async_i5d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         integer(int32), intent(inout) :: array_out(:,:,:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i4_devxlib_memcpy_h2h_async_i5d
      module subroutine i4_devxlib_memcpy_h2h_async_i6d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         integer(int32), intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i4_devxlib_memcpy_h2h_async_i6d
      module subroutine i8_devxlib_memcpy_h2h_async_i1d(array_out, array_in, async_id, &
                                            range1, lbound1 )
         integer(int64), intent(inout) :: array_out(:)
         integer(int64), intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine i8_devxlib_memcpy_h2h_async_i1d
      module subroutine i8_devxlib_memcpy_h2h_async_i2d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         integer(int64), intent(inout) :: array_out(:,:)
         integer(int64), intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine i8_devxlib_memcpy_h2h_async_i2d
      module subroutine i8_devxlib_memcpy_h2h_async_i3d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         integer(int64), intent(inout) :: array_out(:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine i8_devxlib_memcpy_h2h_async_i3d
      module subroutine i8_devxlib_memcpy_h2h_async_i4d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         integer(int64), intent(inout) :: array_out(:,:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine i8_devxlib_memcpy_h2h_async_i4d
      module subroutine i8_devxlib_memcpy_h2h_async_i5d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         integer(int64), intent(inout) :: array_out(:,:,:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine i8_devxlib_memcpy_h2h_async_i5d
      module subroutine i8_devxlib_memcpy_h2h_async_i6d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         integer(int64), intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64), intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine i8_devxlib_memcpy_h2h_async_i6d
      module subroutine l4_devxlib_memcpy_h2h_async_l1d(array_out, array_in, async_id, &
                                            range1, lbound1 )
         logical(int32), intent(inout) :: array_out(:)
         logical(int32), intent(in)    :: array_in(:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
      end subroutine l4_devxlib_memcpy_h2h_async_l1d
      module subroutine l4_devxlib_memcpy_h2h_async_l2d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2 )
         logical(int32), intent(inout) :: array_out(:,:)
         logical(int32), intent(in)    :: array_in(:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
      end subroutine l4_devxlib_memcpy_h2h_async_l2d
      module subroutine l4_devxlib_memcpy_h2h_async_l3d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3 )
         logical(int32), intent(inout) :: array_out(:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
      end subroutine l4_devxlib_memcpy_h2h_async_l3d
      module subroutine l4_devxlib_memcpy_h2h_async_l4d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4 )
         logical(int32), intent(inout) :: array_out(:,:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
      end subroutine l4_devxlib_memcpy_h2h_async_l4d
      module subroutine l4_devxlib_memcpy_h2h_async_l5d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5 )
         logical(int32), intent(inout) :: array_out(:,:,:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
      end subroutine l4_devxlib_memcpy_h2h_async_l5d
      module subroutine l4_devxlib_memcpy_h2h_async_l6d(array_out, array_in, async_id, &
                                            range1, lbound1, &
                                            range2, lbound2, &
                                            range3, lbound3, &
                                            range4, lbound4, &
                                            range5, lbound5, &
                                            range6, lbound6 )
         logical(int32), intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32), intent(in)    :: array_in(:,:,:,:,:,:)
         integer(kind=dxl_async_kind), intent(in) :: async_id
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
      end subroutine l4_devxlib_memcpy_h2h_async_l6d
   endinterface

   interface
      module subroutine sp_devxlib_memcpy_d2d_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_r1d_p
      module subroutine sp_devxlib_memcpy_d2d_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_r2d_p
      module subroutine sp_devxlib_memcpy_d2d_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_r3d_p
      module subroutine sp_devxlib_memcpy_d2d_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_r4d_p
      module subroutine sp_devxlib_memcpy_d2d_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_r5d_p
      module subroutine sp_devxlib_memcpy_d2d_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_r6d_p
      module subroutine dp_devxlib_memcpy_d2d_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_r1d_p
      module subroutine dp_devxlib_memcpy_d2d_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_r2d_p
      module subroutine dp_devxlib_memcpy_d2d_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_r3d_p
      module subroutine dp_devxlib_memcpy_d2d_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_r4d_p
      module subroutine dp_devxlib_memcpy_d2d_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_r5d_p
      module subroutine dp_devxlib_memcpy_d2d_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_r6d_p
      module subroutine sp_devxlib_memcpy_d2d_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_c1d_p
      module subroutine sp_devxlib_memcpy_d2d_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_c2d_p
      module subroutine sp_devxlib_memcpy_d2d_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_c3d_p
      module subroutine sp_devxlib_memcpy_d2d_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_c4d_p
      module subroutine sp_devxlib_memcpy_d2d_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_c5d_p
      module subroutine sp_devxlib_memcpy_d2d_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine sp_devxlib_memcpy_d2d_c6d_p
      module subroutine dp_devxlib_memcpy_d2d_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_c1d_p
      module subroutine dp_devxlib_memcpy_d2d_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_c2d_p
      module subroutine dp_devxlib_memcpy_d2d_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_c3d_p
      module subroutine dp_devxlib_memcpy_d2d_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_c4d_p
      module subroutine dp_devxlib_memcpy_d2d_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_c5d_p
      module subroutine dp_devxlib_memcpy_d2d_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine dp_devxlib_memcpy_d2d_c6d_p
      module subroutine i4_devxlib_memcpy_d2d_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i4_devxlib_memcpy_d2d_i1d_p
      module subroutine i4_devxlib_memcpy_d2d_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i4_devxlib_memcpy_d2d_i2d_p
      module subroutine i4_devxlib_memcpy_d2d_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i4_devxlib_memcpy_d2d_i3d_p
      module subroutine i4_devxlib_memcpy_d2d_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i4_devxlib_memcpy_d2d_i4d_p
      module subroutine i4_devxlib_memcpy_d2d_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i4_devxlib_memcpy_d2d_i5d_p
      module subroutine i4_devxlib_memcpy_d2d_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i4_devxlib_memcpy_d2d_i6d_p
      module subroutine i8_devxlib_memcpy_d2d_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i8_devxlib_memcpy_d2d_i1d_p
      module subroutine i8_devxlib_memcpy_d2d_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i8_devxlib_memcpy_d2d_i2d_p
      module subroutine i8_devxlib_memcpy_d2d_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i8_devxlib_memcpy_d2d_i3d_p
      module subroutine i8_devxlib_memcpy_d2d_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i8_devxlib_memcpy_d2d_i4d_p
      module subroutine i8_devxlib_memcpy_d2d_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i8_devxlib_memcpy_d2d_i5d_p
      module subroutine i8_devxlib_memcpy_d2d_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int64), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine i8_devxlib_memcpy_d2d_i6d_p
      module subroutine l4_devxlib_memcpy_d2d_l1d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine l4_devxlib_memcpy_d2d_l1d_p
      module subroutine l4_devxlib_memcpy_d2d_l2d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine l4_devxlib_memcpy_d2d_l2d_p
      module subroutine l4_devxlib_memcpy_d2d_l3d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine l4_devxlib_memcpy_d2d_l3d_p
      module subroutine l4_devxlib_memcpy_d2d_l4d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine l4_devxlib_memcpy_d2d_l4d_p
      module subroutine l4_devxlib_memcpy_d2d_l5d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine l4_devxlib_memcpy_d2d_l5d_p
      module subroutine l4_devxlib_memcpy_d2d_l6d_p(&
#if defined __DXL_OPENMP_GPU
                                            array_out, array_in, device_id_out, device_id_in)
#else
                                            array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         logical(int32), pointer, contiguous, intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id_out, device_id_in
#endif
      end subroutine l4_devxlib_memcpy_d2d_l6d_p
   endinterface

   interface
      module subroutine sp_devxlib_memcpy_d2h_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), target,  intent(inout) :: array_out(:)
         real(real32), pointer, intent(inout) DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_r1d_p
      module subroutine sp_devxlib_memcpy_d2h_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), target,  intent(inout) :: array_out(:,:)
         real(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_r2d_p
      module subroutine sp_devxlib_memcpy_d2h_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), target,  intent(inout) :: array_out(:,:,:)
         real(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_r3d_p
      module subroutine sp_devxlib_memcpy_d2h_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), target,  intent(inout) :: array_out(:,:,:,:)
         real(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_r4d_p
      module subroutine sp_devxlib_memcpy_d2h_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), target,  intent(inout) :: array_out(:,:,:,:,:)
         real(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_r5d_p
      module subroutine sp_devxlib_memcpy_d2h_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), target,  intent(inout) :: array_out(:,:,:,:,:,:)
         real(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_r6d_p
      module subroutine dp_devxlib_memcpy_d2h_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), target,  intent(inout) :: array_out(:)
         real(real64), pointer, intent(inout) DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_r1d_p
      module subroutine dp_devxlib_memcpy_d2h_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), target,  intent(inout) :: array_out(:,:)
         real(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_r2d_p
      module subroutine dp_devxlib_memcpy_d2h_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), target,  intent(inout) :: array_out(:,:,:)
         real(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_r3d_p
      module subroutine dp_devxlib_memcpy_d2h_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), target,  intent(inout) :: array_out(:,:,:,:)
         real(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_r4d_p
      module subroutine dp_devxlib_memcpy_d2h_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), target,  intent(inout) :: array_out(:,:,:,:,:)
         real(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_r5d_p
      module subroutine dp_devxlib_memcpy_d2h_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), target,  intent(inout) :: array_out(:,:,:,:,:,:)
         real(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_r6d_p
      module subroutine sp_devxlib_memcpy_d2h_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), target,  intent(inout) :: array_out(:)
         complex(real32), pointer, intent(inout) DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_c1d_p
      module subroutine sp_devxlib_memcpy_d2h_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), target,  intent(inout) :: array_out(:,:)
         complex(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_c2d_p
      module subroutine sp_devxlib_memcpy_d2h_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), target,  intent(inout) :: array_out(:,:,:)
         complex(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_c3d_p
      module subroutine sp_devxlib_memcpy_d2h_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), target,  intent(inout) :: array_out(:,:,:,:)
         complex(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_c4d_p
      module subroutine sp_devxlib_memcpy_d2h_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), target,  intent(inout) :: array_out(:,:,:,:,:)
         complex(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_c5d_p
      module subroutine sp_devxlib_memcpy_d2h_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), target,  intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_d2h_c6d_p
      module subroutine dp_devxlib_memcpy_d2h_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), target,  intent(inout) :: array_out(:)
         complex(real64), pointer, intent(inout) DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_c1d_p
      module subroutine dp_devxlib_memcpy_d2h_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), target,  intent(inout) :: array_out(:,:)
         complex(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_c2d_p
      module subroutine dp_devxlib_memcpy_d2h_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), target,  intent(inout) :: array_out(:,:,:)
         complex(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_c3d_p
      module subroutine dp_devxlib_memcpy_d2h_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), target,  intent(inout) :: array_out(:,:,:,:)
         complex(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_c4d_p
      module subroutine dp_devxlib_memcpy_d2h_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), target,  intent(inout) :: array_out(:,:,:,:,:)
         complex(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_c5d_p
      module subroutine dp_devxlib_memcpy_d2h_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), target,  intent(inout) :: array_out(:,:,:,:,:,:)
         complex(real64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_d2h_c6d_p
      module subroutine i4_devxlib_memcpy_d2h_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), target,  intent(inout) :: array_out(:)
         integer(int32), pointer, intent(inout) DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_d2h_i1d_p
      module subroutine i4_devxlib_memcpy_d2h_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), target,  intent(inout) :: array_out(:,:)
         integer(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_d2h_i2d_p
      module subroutine i4_devxlib_memcpy_d2h_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), target,  intent(inout) :: array_out(:,:,:)
         integer(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_d2h_i3d_p
      module subroutine i4_devxlib_memcpy_d2h_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), target,  intent(inout) :: array_out(:,:,:,:)
         integer(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_d2h_i4d_p
      module subroutine i4_devxlib_memcpy_d2h_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), target,  intent(inout) :: array_out(:,:,:,:,:)
         integer(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_d2h_i5d_p
      module subroutine i4_devxlib_memcpy_d2h_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), target,  intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_d2h_i6d_p
      module subroutine i8_devxlib_memcpy_d2h_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), target,  intent(inout) :: array_out(:)
         integer(int64), pointer, intent(inout) DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_d2h_i1d_p
      module subroutine i8_devxlib_memcpy_d2h_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), target,  intent(inout) :: array_out(:,:)
         integer(int64), pointer, intent(inout) DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_d2h_i2d_p
      module subroutine i8_devxlib_memcpy_d2h_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), target,  intent(inout) :: array_out(:,:,:)
         integer(int64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_d2h_i3d_p
      module subroutine i8_devxlib_memcpy_d2h_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), target,  intent(inout) :: array_out(:,:,:,:)
         integer(int64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_d2h_i4d_p
      module subroutine i8_devxlib_memcpy_d2h_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), target,  intent(inout) :: array_out(:,:,:,:,:)
         integer(int64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_d2h_i5d_p
      module subroutine i8_devxlib_memcpy_d2h_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), target,  intent(inout) :: array_out(:,:,:,:,:,:)
         integer(int64), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_d2h_i6d_p
      module subroutine l4_devxlib_memcpy_d2h_l1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), target,  intent(inout) :: array_out(:)
         logical(int32), pointer, intent(inout) DEV_ATTR :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_d2h_l1d_p
      module subroutine l4_devxlib_memcpy_d2h_l2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), target,  intent(inout) :: array_out(:,:)
         logical(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_d2h_l2d_p
      module subroutine l4_devxlib_memcpy_d2h_l3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), target,  intent(inout) :: array_out(:,:,:)
         logical(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_d2h_l3d_p
      module subroutine l4_devxlib_memcpy_d2h_l4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), target,  intent(inout) :: array_out(:,:,:,:)
         logical(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_d2h_l4d_p
      module subroutine l4_devxlib_memcpy_d2h_l5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), target,  intent(inout) :: array_out(:,:,:,:,:)
         logical(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_d2h_l5d_p
      module subroutine l4_devxlib_memcpy_d2h_l6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), target,  intent(inout) :: array_out(:,:,:,:,:,:)
         logical(int32), pointer, intent(inout) DEV_ATTR :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_d2h_l6d_p
   endinterface

   interface
      module subroutine sp_devxlib_memcpy_h2d_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         real(real32), target, intent(in) :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_r1d_p
      module subroutine sp_devxlib_memcpy_h2d_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real32), target, intent(in) :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_r2d_p
      module subroutine sp_devxlib_memcpy_h2d_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_r3d_p
      module subroutine sp_devxlib_memcpy_h2d_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_r4d_p
      module subroutine sp_devxlib_memcpy_h2d_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_r5d_p
      module subroutine sp_devxlib_memcpy_h2d_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real32), target, intent(in) :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_r6d_p
      module subroutine dp_devxlib_memcpy_h2d_r1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         real(real64), target, intent(in) :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_r1d_p
      module subroutine dp_devxlib_memcpy_h2d_r2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         real(real64), target, intent(in) :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_r2d_p
      module subroutine dp_devxlib_memcpy_h2d_r3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_r3d_p
      module subroutine dp_devxlib_memcpy_h2d_r4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_r4d_p
      module subroutine dp_devxlib_memcpy_h2d_r5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_r5d_p
      module subroutine dp_devxlib_memcpy_h2d_r6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         real(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real64), target, intent(in) :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_r6d_p
      module subroutine sp_devxlib_memcpy_h2d_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         complex(real32), target, intent(in) :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_c1d_p
      module subroutine sp_devxlib_memcpy_h2d_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real32), target, intent(in) :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_c2d_p
      module subroutine sp_devxlib_memcpy_h2d_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_c3d_p
      module subroutine sp_devxlib_memcpy_h2d_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_c4d_p
      module subroutine sp_devxlib_memcpy_h2d_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_c5d_p
      module subroutine sp_devxlib_memcpy_h2d_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real32), target, intent(in) :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine sp_devxlib_memcpy_h2d_c6d_p
      module subroutine dp_devxlib_memcpy_h2d_c1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         complex(real64), target, intent(in) :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_c1d_p
      module subroutine dp_devxlib_memcpy_h2d_c2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real64), target, intent(in) :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_c2d_p
      module subroutine dp_devxlib_memcpy_h2d_c3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_c3d_p
      module subroutine dp_devxlib_memcpy_h2d_c4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_c4d_p
      module subroutine dp_devxlib_memcpy_h2d_c5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_c5d_p
      module subroutine dp_devxlib_memcpy_h2d_c6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         complex(real64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real64), target, intent(in) :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine dp_devxlib_memcpy_h2d_c6d_p
      module subroutine i4_devxlib_memcpy_h2d_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         integer(int32), target, intent(in) :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_h2d_i1d_p
      module subroutine i4_devxlib_memcpy_h2d_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int32), target, intent(in) :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_h2d_i2d_p
      module subroutine i4_devxlib_memcpy_h2d_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_h2d_i3d_p
      module subroutine i4_devxlib_memcpy_h2d_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_h2d_i4d_p
      module subroutine i4_devxlib_memcpy_h2d_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_h2d_i5d_p
      module subroutine i4_devxlib_memcpy_h2d_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int32), target, intent(in) :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i4_devxlib_memcpy_h2d_i6d_p
      module subroutine i8_devxlib_memcpy_h2d_i1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         integer(int64), target, intent(in) :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_h2d_i1d_p
      module subroutine i8_devxlib_memcpy_h2d_i2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         integer(int64), target, intent(in) :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_h2d_i2d_p
      module subroutine i8_devxlib_memcpy_h2d_i3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_h2d_i3d_p
      module subroutine i8_devxlib_memcpy_h2d_i4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_h2d_i4d_p
      module subroutine i8_devxlib_memcpy_h2d_i5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_h2d_i5d_p
      module subroutine i8_devxlib_memcpy_h2d_i6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         integer(int64), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         integer(int64), target, intent(in) :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine i8_devxlib_memcpy_h2d_i6d_p
      module subroutine l4_devxlib_memcpy_h2d_l1d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:)
         logical(int32), target, intent(in) :: array_in(:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_h2d_l1d_p
      module subroutine l4_devxlib_memcpy_h2d_l2d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:)
         logical(int32), target, intent(in) :: array_in(:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_h2d_l2d_p
      module subroutine l4_devxlib_memcpy_h2d_l3d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_h2d_l3d_p
      module subroutine l4_devxlib_memcpy_h2d_l4d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_h2d_l4d_p
      module subroutine l4_devxlib_memcpy_h2d_l5d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_h2d_l5d_p
      module subroutine l4_devxlib_memcpy_h2d_l6d_p(&
#if defined __DXL_OPENMP_GPU
                                             array_out, array_in, device_id)
#else
                                             array_out, array_in)
#endif
         implicit none
         logical(int32), pointer, contiguous, intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         logical(int32), target, intent(in) :: array_in(:,:,:,:,:,:)
#if defined __DXL_OPENMP_GPU
         integer, optional, intent(in) :: device_id
#endif
      end subroutine l4_devxlib_memcpy_h2d_l6d_p
   endinterface

endmodule devxlib_memcpy