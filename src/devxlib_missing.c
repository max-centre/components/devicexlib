/*
 Copyright (C) 2022, MaX CoE
 Distributed under the MIT License 
 (license terms are at http://opensource.org/licenses/MIT).

--

 Placeholder routine to replace missing implementations
 or workarounds

*/

#include <stddef.h>
#include <unistd.h>

/*
 Workaround for missing implementation
 For the moment assumes CUDA is available
*/


#if defined __DXL_OPENACC
#  if ! defined  __PGI && (__GNUC__ < 14)

#include <cuda_runtime_api.h>
#include <cuda.h>

void acc_memcpy_device( void *dst, void *src, size_t count ){
  cudaMemcpy(dst, src, count, cudaMemcpyDeviceToDevice);
}
#  endif
#endif


