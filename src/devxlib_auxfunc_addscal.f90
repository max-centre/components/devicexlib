!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Auxiliary functions
!
! AF: OpenACC support is incomplete !!
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_auxfunc_addscal.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_auxfunc) devxlib_auxfunc_addscal

   implicit none

   contains
      module subroutine dp_devxlib_mem_addscal_r1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
         use iso_fortran_env
         implicit none
         !
         real(real64), intent(inout) DEV_ATTR :: array_out(:)
         real(real64), intent(in)    DEV_ATTR :: array_in(:)
         real(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         real(real64) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(1)
         do i1 = d1s, d1e
            array_out(i1 ) = &
                array_out(i1 ) + &
                scal_*array_in(i1 )
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_r1d
      !
      module subroutine dp_devxlib_mem_addscal_r2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         use iso_fortran_env
         implicit none
         !
         real(real64), intent(inout) DEV_ATTR :: array_out(:,:)
         real(real64), intent(in)    DEV_ATTR :: array_in(:,:)
         real(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         real(real64) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = &
                array_out(i1,i2 ) + &
                scal_*array_in(i1,i2 )
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_r2d
      !
      module subroutine dp_devxlib_mem_addscal_r3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         use iso_fortran_env
         implicit none
         !
         real(real64), intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real64), intent(in)    DEV_ATTR :: array_in(:,:,:)
         real(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         real(real64) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = &
                array_out(i1,i2,i3 ) + &
                scal_*array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_r3d
      !
      module subroutine dp_devxlib_mem_addscal_r4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         use iso_fortran_env
         implicit none
         !
         real(real64), intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real64), intent(in)    DEV_ATTR :: array_in(:,:,:,:)
         real(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         real(real64) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = &
                array_out(i1,i2,i3,i4 ) + &
                scal_*array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_r4d
      !
      module subroutine dp_devxlib_mem_addscal_r5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         use iso_fortran_env
         implicit none
         !
         real(real64), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real64), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
         real(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         real(real64) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = &
                array_out(i1,i2,i3,i4,i5 ) + &
                scal_*array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_r5d
      !
      module subroutine dp_devxlib_mem_addscal_r6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         use iso_fortran_env
         implicit none
         !
         real(real64), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real64), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
         real(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         real(real64) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = &
                array_out(i1,i2,i3,i4,i5,i6 ) + &
                scal_*array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_r6d
      !
      module subroutine sp_devxlib_mem_addscal_r1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
         use iso_fortran_env
         implicit none
         !
         real(real32), intent(inout) DEV_ATTR :: array_out(:)
         real(real32), intent(in)    DEV_ATTR :: array_in(:)
         real(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         real(real32) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(1)
         do i1 = d1s, d1e
            array_out(i1 ) = &
                array_out(i1 ) + &
                scal_*array_in(i1 )
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_r1d
      !
      module subroutine sp_devxlib_mem_addscal_r2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         use iso_fortran_env
         implicit none
         !
         real(real32), intent(inout) DEV_ATTR :: array_out(:,:)
         real(real32), intent(in)    DEV_ATTR :: array_in(:,:)
         real(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         real(real32) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = &
                array_out(i1,i2 ) + &
                scal_*array_in(i1,i2 )
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_r2d
      !
      module subroutine sp_devxlib_mem_addscal_r3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         use iso_fortran_env
         implicit none
         !
         real(real32), intent(inout) DEV_ATTR :: array_out(:,:,:)
         real(real32), intent(in)    DEV_ATTR :: array_in(:,:,:)
         real(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         real(real32) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = &
                array_out(i1,i2,i3 ) + &
                scal_*array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_r3d
      !
      module subroutine sp_devxlib_mem_addscal_r4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         use iso_fortran_env
         implicit none
         !
         real(real32), intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         real(real32), intent(in)    DEV_ATTR :: array_in(:,:,:,:)
         real(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         real(real32) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = &
                array_out(i1,i2,i3,i4 ) + &
                scal_*array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_r4d
      !
      module subroutine sp_devxlib_mem_addscal_r5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         use iso_fortran_env
         implicit none
         !
         real(real32), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         real(real32), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
         real(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         real(real32) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = &
                array_out(i1,i2,i3,i4,i5 ) + &
                scal_*array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_r5d
      !
      module subroutine sp_devxlib_mem_addscal_r6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         use iso_fortran_env
         implicit none
         !
         real(real32), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         real(real32), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
         real(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         real(real32) :: scal_ = 1.0
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = &
                array_out(i1,i2,i3,i4,i5,i6 ) + &
                scal_*array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_r6d
      !
      module subroutine dp_devxlib_mem_addscal_c1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
         use iso_fortran_env
         implicit none
         !
         complex(real64), intent(inout) DEV_ATTR :: array_out(:)
         complex(real64), intent(in)    DEV_ATTR :: array_in(:)
         complex(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         complex(real64) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(1)
         do i1 = d1s, d1e
            array_out(i1 ) = &
                array_out(i1 ) + &
                scal_*array_in(i1 )
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_c1d
      !
      module subroutine dp_devxlib_mem_addscal_c2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         use iso_fortran_env
         implicit none
         !
         complex(real64), intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real64), intent(in)    DEV_ATTR :: array_in(:,:)
         complex(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         complex(real64) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = &
                array_out(i1,i2 ) + &
                scal_*array_in(i1,i2 )
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_c2d
      !
      module subroutine dp_devxlib_mem_addscal_c3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         use iso_fortran_env
         implicit none
         !
         complex(real64), intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real64), intent(in)    DEV_ATTR :: array_in(:,:,:)
         complex(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         complex(real64) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = &
                array_out(i1,i2,i3 ) + &
                scal_*array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_c3d
      !
      module subroutine dp_devxlib_mem_addscal_c4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         use iso_fortran_env
         implicit none
         !
         complex(real64), intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real64), intent(in)    DEV_ATTR :: array_in(:,:,:,:)
         complex(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         complex(real64) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = &
                array_out(i1,i2,i3,i4 ) + &
                scal_*array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_c4d
      !
      module subroutine dp_devxlib_mem_addscal_c5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         use iso_fortran_env
         implicit none
         !
         complex(real64), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real64), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
         complex(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         complex(real64) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = &
                array_out(i1,i2,i3,i4,i5 ) + &
                scal_*array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_c5d
      !
      module subroutine dp_devxlib_mem_addscal_c6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         use iso_fortran_env
         implicit none
         !
         complex(real64), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real64), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
         complex(real64), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         complex(real64) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = &
                array_out(i1,i2,i3,i4,i5,i6 ) + &
                scal_*array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine dp_devxlib_mem_addscal_c6d
      !
      module subroutine sp_devxlib_mem_addscal_c1d(array_out, array_in, scal, &
                                                  range1, lbound1 )
         use iso_fortran_env
         implicit none
         !
         complex(real32), intent(inout) DEV_ATTR :: array_out(:)
         complex(real32), intent(in)    DEV_ATTR :: array_in(:)
         complex(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2)
         integer, optional, intent(in) ::  lbound1
         !
         complex(real32) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(1)
         do i1 = d1s, d1e
            array_out(i1 ) = &
                array_out(i1 ) + &
                scal_*array_in(i1 )
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_c1d
      !
      module subroutine sp_devxlib_mem_addscal_c2d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2 )
         use iso_fortran_env
         implicit none
         !
         complex(real32), intent(inout) DEV_ATTR :: array_out(:,:)
         complex(real32), intent(in)    DEV_ATTR :: array_in(:,:)
         complex(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2)
         integer, optional, intent(in) ::  lbound1, lbound2
         !
         complex(real32) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(2)
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2 ) = &
                array_out(i1,i2 ) + &
                scal_*array_in(i1,i2 )
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_c2d
      !
      module subroutine sp_devxlib_mem_addscal_c3d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3 )
         use iso_fortran_env
         implicit none
         !
         complex(real32), intent(inout) DEV_ATTR :: array_out(:,:,:)
         complex(real32), intent(in)    DEV_ATTR :: array_in(:,:,:)
         complex(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3
         !
         complex(real32) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(3)
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3 ) = &
                array_out(i1,i2,i3 ) + &
                scal_*array_in(i1,i2,i3 )
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_c3d
      !
      module subroutine sp_devxlib_mem_addscal_c4d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4 )
         use iso_fortran_env
         implicit none
         !
         complex(real32), intent(inout) DEV_ATTR :: array_out(:,:,:,:)
         complex(real32), intent(in)    DEV_ATTR :: array_in(:,:,:,:)
         complex(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4
         !
         complex(real32) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(4)
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4 ) = &
                array_out(i1,i2,i3,i4 ) + &
                scal_*array_in(i1,i2,i3,i4 )
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_c4d
      !
      module subroutine sp_devxlib_mem_addscal_c5d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5 )
         use iso_fortran_env
         implicit none
         !
         complex(real32), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:)
         complex(real32), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:)
         complex(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5
         !
         complex(real32) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(5)
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5 ) = &
                array_out(i1,i2,i3,i4,i5 ) + &
                scal_*array_in(i1,i2,i3,i4,i5 )
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_c5d
      !
      module subroutine sp_devxlib_mem_addscal_c6d(array_out, array_in, scal, &
                                                  range1, lbound1, &
                                                  range2, lbound2, &
                                                  range3, lbound3, &
                                                  range4, lbound4, &
                                                  range5, lbound5, &
                                                  range6, lbound6 )
         use iso_fortran_env
         implicit none
         !
         complex(real32), intent(inout) DEV_ATTR :: array_out(:,:,:,:,:,:)
         complex(real32), intent(in)    DEV_ATTR :: array_in(:,:,:,:,:,:)
         complex(real32), optional, intent(in) :: scal
         integer, optional, intent(in) ::  range1(2), range2(2), range3(2), range4(2), range5(2), range6(2)
         integer, optional, intent(in) ::  lbound1, lbound2, lbound3, lbound4, lbound5, lbound6
         !
         complex(real32) :: scal_ = (1.0, 0.0)
         integer :: i1, d1s, d1e
         integer :: lbound1_, range1_(2)
         integer :: i2, d2s, d2e
         integer :: lbound2_, range2_(2)
         integer :: i3, d3s, d3e
         integer :: lbound3_, range3_(2)
         integer :: i4, d4s, d4e
         integer :: lbound4_, range4_(2)
         integer :: i5, d5s, d5e
         integer :: lbound5_, range5_(2)
         integer :: i6, d6s, d6e
         integer :: lbound6_, range6_(2)
         !
         lbound1_=1
         if (present(lbound1)) lbound1_=lbound1 
         range1_=(/1,size(array_out, 1)/)
         if (present(range1)) range1_=range1 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d1s = range1_(1) -lbound1_ +1
         d1e = range1_(2) -lbound1_ +1
         !
         lbound2_=1
         if (present(lbound2)) lbound2_=lbound2 
         range2_=(/1,size(array_out, 2)/)
         if (present(range2)) range2_=range2 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d2s = range2_(1) -lbound2_ +1
         d2e = range2_(2) -lbound2_ +1
         !
         lbound3_=1
         if (present(lbound3)) lbound3_=lbound3 
         range3_=(/1,size(array_out, 3)/)
         if (present(range3)) range3_=range3 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d3s = range3_(1) -lbound3_ +1
         d3e = range3_(2) -lbound3_ +1
         !
         lbound4_=1
         if (present(lbound4)) lbound4_=lbound4 
         range4_=(/1,size(array_out, 4)/)
         if (present(range4)) range4_=range4 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d4s = range4_(1) -lbound4_ +1
         d4e = range4_(2) -lbound4_ +1
         !
         lbound5_=1
         if (present(lbound5)) lbound5_=lbound5 
         range5_=(/1,size(array_out, 5)/)
         if (present(range5)) range5_=range5 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d5s = range5_(1) -lbound5_ +1
         d5e = range5_(2) -lbound5_ +1
         !
         lbound6_=1
         if (present(lbound6)) lbound6_=lbound6 
         range6_=(/1,size(array_out, 6)/)
         if (present(range6)) range6_=range6 
         !
         ! the lower bound of the assumed shape array passed to the subroutine is 1
         ! lbound and range instead refer to the indexing in the parent caller.
         d6s = range6_(1) -lbound6_ +1
         d6e = range6_(2) -lbound6_ +1
         !
         if (present(scal)) scal_=scal
         !
         !DEV_CUF kernel do(6)
         do i6 = d6s, d6e
         do i5 = d5s, d5e
         do i4 = d4s, d4e
         do i3 = d3s, d3e
         do i2 = d2s, d2e
         do i1 = d1s, d1e
            array_out(i1,i2,i3,i4,i5,i6 ) = &
                array_out(i1,i2,i3,i4,i5,i6 ) + &
                scal_*array_in(i1,i2,i3,i4,i5,i6 )
         enddo
         enddo
         enddo
         enddo
         enddo
         enddo
         !
      end subroutine sp_devxlib_mem_addscal_c6d
      !

end submodule devxlib_auxfunc_addscal