
#if defined __DXL_MKL_GPU
#  include "mkl_omp_offload.f90"
#endif

module devxlib_mklgpu
 !
#if defined __DXL_MKL_GPU
 use onemkl_blas_omp_offload_lp64
#endif
 !
 implicit none

end module devxlib_mklgpu

