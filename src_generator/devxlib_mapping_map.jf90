
!
! Copyright (C) 2022, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
! Utility functions to perform device memory mapping from host memory
! using CUDA-Fortran, OpenACC or OpenMP Offload
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from device_mapping.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
submodule (devxlib_mapping) devxlib_mapping_map

   implicit none

   contains
{%- for t in types %}
{%- for p in kinds[t] %}
{%- for d in range(1,dimensions+1) %}
      module subroutine {{p.name}}_devxlib_mapping_map_{{t[0]|lower}}{{d}}d(array, {% for dd in range(d) -%}
                                            {{ "range%s"|format(dd+1) }}{% if not loop.last %}, &
                                            {% endif %}{% endfor %} )
         implicit none
         !
         {{t}}({{p.val}}), allocatable DEV_ATTR, intent(inout) :: array({% for dd in range(d) %}:{% if not loop.last %}, {%- endif %}{% endfor %})
         integer, optional, intent(in) :: {% for dd in range(d) %} {{ "range%s(2)"|format(dd+1) }}{% if not loop.last %}, {%- endif %}{% endfor %}
         !
{%- for dd in range(d) %}
         integer :: i{{dd+1}}, d{{dd+1}}s, d{{dd+1}}e
{%- endfor %}
         !
{%- for dd in range(d) %}
         if (present(range{{dd+1}})) then
            d{{dd+1}}s = range{{dd+1}}(1)
            d{{dd+1}}e = range{{dd+1}}(2)
         else
!#if defined __DXL_CUDAF
!            call devxlib_error("{{p.name}}_dev_mapping_{{t[0]|lower}}{{d}}d","range not present",{{dd+1}})
!#endif
            d{{dd+1}}s = lbound(array,{{dd+1}})
            d{{dd+1}}e = ubound(array,{{dd+1}})
         endif
         !
{%- endfor %}
         !
#if defined __DXL_CUDAF
         if (.not.allocated(array)) allocate( array({% for dd in range(d) %}d{{dd+1}}s:d{{dd+1}}e{% if not loop.last %}, {%- endif %}{% endfor %}) )
#elif defined __DXL_OPENACC || defined __DXL_OPENMP_GPU
         !DEV_ACC enter data create( array({% for dd in range(d) %}d{{dd+1}}s:d{{dd+1}}e{% if not loop.last %}, {%- endif %}{% endfor %}) )
         !DEV_OMPGPU target enter data map(alloc: array )
#endif
      end subroutine {{p.name}}_devxlib_mapping_map_{{t[0]|lower}}{{d}}d
      !
{%- endfor %}
{%- endfor %}
{%- endfor %}

endsubmodule devxlib_mapping_map
