!
! Copyright (C) 2021, MaX CoE
! Distributed under the MIT License 
! (license terms are at http://opensource.org/licenses/MIT).
!
!--
!
!
! Utility functions to perform host data initializations
! using CUDA-Fortran
!
!==================================================================
!==================================================================
! *DO NOT EDIT*: automatically generated from devxlib_memset_host.jf90
!==================================================================
!==================================================================
!
#include<devxlib_macros.h>
#include<devxlib_defs.h>
!
!=======================================
!
! Note about dimensions:
! The lower bound of the assumed shape array passed to the subroutine is 1
! lbound and range instead refer to the indexing in the parent caller.
!
submodule (devxlib_memset) devxlib_memset_host

   implicit none

   contains

{%- for t in types %}
{%- for p in kinds[t] %}
{%- for d in range(1,dimensions+1) %}
      module subroutine {{p.name}}_devxlib_memset_h_{{t[0]|lower}}{{d}}d(array_out, val, &{% for dd in range(d) %}
                                            {{ "range%s"|format(dd+1) }}, {{ "lbound%s"|format(dd+1) }}{% if not loop.last %}, &
                                            {%- endif %}{% endfor %} )
         implicit none
         !
         {{t}}({{p.val}}), intent(inout) :: array_out({% for dd in range(d) %}:{% if not loop.last %}, {%- endif %}{% endfor %})
         {{t}}({{p.val}}), intent(in)    :: val
         integer, optional, intent(in) :: {% for dd in range(d) %} {{ "range%s(2)"|format(dd+1) }}{% if not loop.last %}, {%- endif %}{% endfor %}
         integer, optional, intent(in) :: {% for dd in range(d) %} {{ "lbound%s"|format(dd+1) }}{% if not loop.last %}, {%- endif %}{% endfor %}
         !
{%- for dd in range(d) %}
         integer :: i{{dd+1}}, d{{dd+1}}s, d{{dd+1}}e
         integer :: lbound{{dd+1}}_, range{{dd+1}}_(2)
{%- endfor %}
         !
{%- for dd in range(d) %}
         lbound{{dd+1}}_=1
         if (present(lbound{{dd+1}})) lbound{{dd+1}}_=lbound{{dd+1}} 
         range{{dd+1}}_=(/1,size(array_out, {{dd+1}})/)
         if (present(range{{dd+1}})) range{{dd+1}}_=range{{dd+1}} 
         !
         d{{dd+1}}s = range{{dd+1}}_(1) -lbound{{dd+1}}_ +1
         d{{dd+1}}e = range{{dd+1}}_(2) -lbound{{dd+1}}_ +1
         !
{%- endfor %}
         !DEV_OMP  parallel do
{%- for dd in range(d,0,-1) %}
         do i{{dd}} = d{{dd}}s, d{{dd}}e
{%- endfor %}
             array_out( {%- for dd in range(d) %}i{{dd+1}}{% if not loop.last %}, {%- endif %} {%- endfor %} ) = val
{%- for dd in range(d) %}
         enddo
{%- endfor %}
        !
      end subroutine {{p.name}}_devxlib_memset_h_{{t[0]|lower}}{{d}}d
      !
{%- endfor %}
{%- endfor %}
{%- endfor %}
endsubmodule devxlib_memset_host
