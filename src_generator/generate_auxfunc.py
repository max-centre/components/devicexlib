#!/usr/bin/env python3

import sys, os, jinja2
from shutil import copyfile

def render(tpl_path, context):
    path, filename = os.path.split(tpl_path)
    return jinja2.Environment(undefined=jinja2.StrictUndefined,
        loader=jinja2.FileSystemLoader(path or './')
    ).get_template(filename).render(context)

####

types_l=['real', 'complex']
nranks=6
kinds_l= [{'name':'dp', 'val':'real64'},
          {'name':'sp', 'val':'real32'}]

# Generate module
with open('devxlib_auxfunc.f90', 'w') as f:
    f.write(render('devxlib_auxfunc.jf90',
                    {'types' : types_l, 'dimensions': nranks, 'precision' : kinds_l }
                  ))

# Generate submodules
with open('devxlib_auxfunc_vec_upd.f90', 'w') as f:
    f.write(render('devxlib_auxfunc_vec_upd.jf90',
                    {'types' : types_l, 'dimensions': nranks, 'precision' : kinds_l }
                  ))
with open('devxlib_auxfunc_mat_upd.f90', 'w') as f:
    f.write(render('devxlib_auxfunc_mat_upd.jf90',
                    {'types' : types_l, 'dimensions': nranks, 'precision' : kinds_l }
                  ))
with open('devxlib_auxfunc_addscal.f90', 'w') as f:
    f.write(render('devxlib_auxfunc_addscal.jf90',
                    {'types' : types_l, 'dimensions': nranks, 'precision' : kinds_l }
                  ))
with open('devxlib_auxfunc_conjg.f90', 'w') as f:
    f.write(render('devxlib_auxfunc_conjg.jf90',
                    {'types' : types_l, 'dimensions': nranks, 'precision' : kinds_l }
                  ))

## Generate tests
#with open('test_auxfunc.f90', 'w') as f:
#    f.write(render('test_auxfunc.jf90',
#                    {'types' : types_l, 'dimensions': nranks, 'precision' : kinds_l }
#                  ))

## set multiple extensions
#copyfile("device_auxfunc.f90","device_auxfunc.F")
#copyfile("device_auxfunc_interf.f90","device_auxfunc_interf.F")

